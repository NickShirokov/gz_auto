<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "automobiles".
 *
 * @property int $id
 * @property string $mark
 * @property string $model
 * @property string $alias
 * @property string $VIN
 * @property string $sts
 * @property string $gosnomer
 * @property string $year
 * @property string $color
 * @property string $transmission
 */
class AutomobilesModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'automobiles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mark', 'model', 'alias', 'VIN', 'sts', 'gosnomer', 'year', 'color'], 'required'],
            [['year'], 'safe'],
            [['transmission'], 'string'],
            [['mark', 'model', 'alias', 'color'], 'string', 'max' => 255],
            [['VIN'], 'string', 'max' => 17],
            [['sts'], 'string', 'max' => 10],
            [['gosnomer'], 'string', 'max' => 9],
            [['VIN', 'sts', 'gosnomer'], 'unique', 'targetAttribute' => ['VIN', 'sts', 'gosnomer']],
			
			
			
			
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'mark' => Yii::t('app', 'Марка'),
            'model' => Yii::t('app', 'Модель'),
            'alias' => Yii::t('app', 'Alias'),
            'VIN' => Yii::t('app', 'Vin'),
            'sts' => Yii::t('app', 'Sts'),
            'gosnomer' => Yii::t('app', 'Гос.номер'),
            'year' => Yii::t('app', 'Год выпуска'),
            'color' => Yii::t('app', 'Цвет'),
            'transmission' => Yii::t('app', 'Коробка передач'),
        ];
    }
}
