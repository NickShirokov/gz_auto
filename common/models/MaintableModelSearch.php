<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Maintable;

/**
 * MaintableModelSearch represents the model behind the search form of `app\models\Maintable`.
 */
class MaintableModelSearch extends Maintable
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_contract', 'id_auto', 'id_driver', 'arenda_stoim', 'FullDays'], 'integer'],
            [['status_name', 'AutoMobile', 'date_start', 'FIO', 'OplachenDo'], 'safe'],
            [['ArendaSumma', 'MainSumma', 'ArendaDays','WeekPayArenda','MonthPayArenda', 'DolgPoArende', 'NewOstatok', 'PayDogovorSumma', 'SummaOurPenalties', 'SummaOurPenaltiesPvznosVykup', 'DolgPoDogovoru', 'SummaPddPenalties', 'PayPddSumma', 'DolgPddSumma', 'MainDolg'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Maintable::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				// this $params['pagesize'] is an id of dropdown list that we set in view file
				'pagesize' => (isset($params['per-page']) ? $params['per-page'] :  '100'),
			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_contract' => $this->id_contract,
            'id_auto' => $this->id_auto,
            'id_driver' => $this->id_driver,
            'date_start' => $this->date_start,
            'arenda_stoim' => $this->arenda_stoim,
            'ArendaSumma' => $this->ArendaSumma,
            'MainSumma' => $this->MainSumma,
            'ArendaDays' => $this->ArendaDays,
            'OplachenDo' => $this->OplachenDo,
            'FullDays' => $this->FullDays,
            'DolgPoArende' => $this->DolgPoArende,
            'PayDogovorSumma' => $this->PayDogovorSumma,
            'SummaOurPenalties' => $this->SummaOurPenalties,
            'SummaOurPenaltiesPvznosVykup' => $this->SummaOurPenaltiesPvznosVykup,
            'DolgPoDogovoru' => $this->DolgPoDogovoru,
            'SummaPddPenalties' => $this->SummaPddPenalties,
            'PayPddSumma' => $this->PayPddSumma,
            'DolgPddSumma' => $this->DolgPddSumma,
            'MainDolg' => $this->MainDolg,
            'NewOstatok' => $this->NewOstatok,
           

		   'WeekPayArenda' => $this->WeekPayArenda,
            'MonthPayArenda' => $this->MonthPayArenda,
        
	
		
		]);

        $query->andFilterWhere(['like', 'status_name', $this->status_name])
            ->andFilterWhere(['like', 'AutoMobile', $this->AutoMobile])
            ->andFilterWhere(['like', 'FIO', $this->FIO]);

        return $dataProvider;
    }
	
	
	
	
	
	public function searchZP($params)
    {
        $query = Maintable::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_contract' => $this->id_contract,
            'id_auto' => $this->id_auto,
            'id_driver' => $this->id_driver,
            'date_start' => $this->date_start,
            'arenda_stoim' => $this->arenda_stoim,
            'ArendaSumma' => $this->ArendaSumma,
            'MainSumma' => $this->MainSumma,
            'ArendaDays' => $this->ArendaDays,
            'OplachenDo' => $this->OplachenDo,
            'FullDays' => $this->FullDays,
            'DolgPoArende' => $this->DolgPoArende,
            'PayDogovorSumma' => $this->PayDogovorSumma,
            'SummaOurPenalties' => $this->SummaOurPenalties,
            'SummaOurPenaltiesPvznosVykup' => $this->SummaOurPenaltiesPvznosVykup,
            'DolgPoDogovoru' => $this->DolgPoDogovoru,
            'SummaPddPenalties' => $this->SummaPddPenalties,
            'PayPddSumma' => $this->PayPddSumma,
            'DolgPddSumma' => $this->DolgPddSumma,
            'MainDolg' => $this->MainDolg,
            'NewOstatok' => $this->NewOstatok,
           

		   'WeekPayArenda' => $this->WeekPayArenda,
            'MonthPayArenda' => $this->MonthPayArenda,
        
	
		
		]);
		
		$query->where(['status_name' => 'ЗП']);
		

        $query
            ->andFilterWhere(['like', 'AutoMobile', $this->AutoMobile])
            ->andFilterWhere(['like', 'FIO', $this->FIO]);

        return $dataProvider;
    }
	
	
	
	
	
	
	public function searchDolgARenda($params)
    {
        $query = Maintable::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_contract' => $this->id_contract,
            'id_auto' => $this->id_auto,
            'id_driver' => $this->id_driver,
            'date_start' => $this->date_start,
            'arenda_stoim' => $this->arenda_stoim,
            'ArendaSumma' => $this->ArendaSumma,
            'MainSumma' => $this->MainSumma,
            'ArendaDays' => $this->ArendaDays,
            'OplachenDo' => $this->OplachenDo,
            'FullDays' => $this->FullDays,
            'DolgPoArende' => $this->DolgPoArende,
            'PayDogovorSumma' => $this->PayDogovorSumma,
            'SummaOurPenalties' => $this->SummaOurPenalties,
            'SummaOurPenaltiesPvznosVykup' => $this->SummaOurPenaltiesPvznosVykup,
            'DolgPoDogovoru' => $this->DolgPoDogovoru,
            'SummaPddPenalties' => $this->SummaPddPenalties,
            'PayPddSumma' => $this->PayPddSumma,
            'DolgPddSumma' => $this->DolgPddSumma,
            'MainDolg' => $this->MainDolg,
            'NewOstatok' => $this->NewOstatok,
           

		   'WeekPayArenda' => $this->WeekPayArenda,
            'MonthPayArenda' => $this->MonthPayArenda,
        
	
		
		]);
		
		$query->andFilterCompare('DolgPoArende', '>=5000');
		

        $query
            ->andFilterWhere(['like', 'AutoMobile', $this->AutoMobile])
            ->andFilterWhere(['like', 'FIO', $this->FIO]);

        return $dataProvider;
    }
	
	
	
	
	
	public function searchDolgFull($params)
    {
        $query = Maintable::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_contract' => $this->id_contract,
            'id_auto' => $this->id_auto,
            'id_driver' => $this->id_driver,
            'date_start' => $this->date_start,
            'arenda_stoim' => $this->arenda_stoim,
            'ArendaSumma' => $this->ArendaSumma,
            'MainSumma' => $this->MainSumma,
            'ArendaDays' => $this->ArendaDays,
            'OplachenDo' => $this->OplachenDo,
            'FullDays' => $this->FullDays,
            'DolgPoArende' => $this->DolgPoArende,
            'PayDogovorSumma' => $this->PayDogovorSumma,
            'SummaOurPenalties' => $this->SummaOurPenalties,
            'SummaOurPenaltiesPvznosVykup' => $this->SummaOurPenaltiesPvznosVykup,
            'DolgPoDogovoru' => $this->DolgPoDogovoru,
            'SummaPddPenalties' => $this->SummaPddPenalties,
            'PayPddSumma' => $this->PayPddSumma,
            'DolgPddSumma' => $this->DolgPddSumma,
            'MainDolg' => $this->MainDolg,
            'NewOstatok' => $this->NewOstatok,
           

		   'WeekPayArenda' => $this->WeekPayArenda,
            'MonthPayArenda' => $this->MonthPayArenda,
        
	
		
		]);
		
		$query->andFilterCompare('MainDolg', '>=20000');
		

        $query
            ->andFilterWhere(['like', 'AutoMobile', $this->AutoMobile])
            ->andFilterWhere(['like', 'FIO', $this->FIO]);

        return $dataProvider;
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
