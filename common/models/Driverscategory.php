<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "driverscategory".
 *
 * @property int $id_cat
 * @property string $category_name
 *
 * @property Drivers[] $drivers
 */
class Driverscategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'driverscategory';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_name'], 'required'],
            [['category_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_cat' => Yii::t('app', 'ID Категории'),
            'category_name' => Yii::t('app', 'Название'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivers()
    {
        return $this->hasMany(Drivers::className(), ['driver_cat' => 'id_cat']);
    }
}
