<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Marks;

/**
 * MarksModelSearch represents the model behind the search form of `common\models\Marks`.
 */
class MarksModelSearch extends Marks
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_mark'], 'integer'],
            [['name_mark', 'fullname'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Marks::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_mark' => $this->id_mark,
        ]);

        $query->andFilterWhere(['like', 'name_mark', $this->name_mark])
            ->andFilterWhere(['like', 'fullname', $this->fullname])->orderBy('name_mark ASC');

        return $dataProvider;
    }
}
