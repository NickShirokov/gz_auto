<?php

namespace common\models;

use Yii;

use app\models\Owners;

/**
 * This is the model class for table "automobiles".
 *
 * @property int $id_auto
 * @property string $mark
 * @property string $model
 * @property string $alias
 * @property string $VIN
 * @property string $sts
 * @property string $gosnomer
 * @property string $year
 * @property string $color
 * @property string $transmission
 * @property string $status
 * @property string $osago
 * @property string $diagnostic_card
 * @property int $vialon
 * @property int $avtofon
 * @property string $comment
 */
class AutomobilesNewModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
	 
	 
     */
	 	public $file_f;
	 
	public $aa; 
	 
    public static function tableName()
    {
        return 'automobiles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
		
		
		if ($_SERVER['REQUEST_URI'] == '/admin/contracts/newcreate') {
		
			$arrr = [
				[['mark', 'model', 'status', 'ins_company', 'VIN', 'sts', 'gosnomer', 'year',  'osago', 'diagnostic_card', 'vialon', 'avtofon', 'owner_id'], 'safe'],
				[['year', 'osago', 'diagnostic_card', 'licen_id', 'razresh_text', 'galka_vserv', 'galka_fssp', 'color'], 'safe'],
				[['transmission', 'comment'], 'safe'],
				[['vialon', 'avtofon', 'odometr_km'], 'safe'],
				[['mark', 'model', 'color'], 'safe'],
				[['VIN', 'PTS'], 'safe'],
				[['sts'], 'safe'],
				[['gosnomer'], 'safe'],
				[['VIN', 'sts', 'gosnomer'], 'safe'],
				
				[['vialon_status', 'vialon_comment', 'avtofon_status', 'avtofon_comment', 'ogranichenia'], 'safe'],
				[['file_f'],'file', 'extensions' => 'csv'],
			];
		
		} else {
			
			$arrr = [
				[['mark', 'model', 'status', 'ins_company', 'VIN', 'sts', 'gosnomer', 'year',  'osago', 'diagnostic_card', 'vialon', 'avtofon', 'owner_id'], 'required'],
				[['year', 'osago', 'diagnostic_card', 'licen_id', 'razresh_text', 'galka_vserv', 'galka_fssp', 'color'], 'safe'],
				[['transmission', 'comment'], 'string'],
				[['vialon', 'avtofon', 'odometr_km'], 'integer'],
				[['mark', 'model', 'color'], 'string', 'max' => 255],
				[['VIN', 'PTS'], 'string', 'max' => 17],
				[['sts'], 'string', 'max' => 12],
				[['gosnomer'], 'string', 'max' => 9],
				[['VIN', 'sts', 'gosnomer'], 'unique', 'targetAttribute' => ['VIN', 'sts', 'gosnomer']],
				
				[['vialon_status', 'vialon_comment', 'avtofon_status', 'avtofon_comment', 'ogranichenia'], 'safe'],
				[['file_f'],'file', 'extensions' => 'csv'],
			];
			
		}
		
		return $arrr;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_auto' => Yii::t('app', 'ID Авто'),
            'mark' => Yii::t('app', 'Марка'),
            'model' => Yii::t('app', 'Модель'),
            //'alias' => Yii::t('app', 'Alias'),
            'VIN' => Yii::t('app', 'Vin'),	
			'PTS' => Yii::t('app', 'ПТС'),
            'sts' => Yii::t('app', 'СТС'),
            'gosnomer' => Yii::t('app', 'Гос.номер'),
            'year' => Yii::t('app', 'Год выпуска'),
            'color' => Yii::t('app', 'Цвет'),
            'transmission' => Yii::t('app', 'Коробка передач'),
			
			'odometr_km' => Yii::t('app', 'Показания одометра в км'),
			
			
            'status' => Yii::t('app', 'Статус'),
            'ogranichenia' => Yii::t('app', 'Ограничения'),
            'osago' => Yii::t('app', 'Осаго дата окончания'),
			'ins_company' => Yii::t('app', 'Страховая компания'),			
            'diagnostic_card' => Yii::t('app', 'Дигностическая карта дата'),
            'vialon' => Yii::t('app', 'Виалон'),
            'vialon_status' => Yii::t('app', 'Виалон статус'),
            'vialon_comment' => Yii::t('app', 'Виалон коммент'),
           



		   'avtofon' => Yii::t('app', 'Автофон'),
		   'avtofon_status' => Yii::t('app', 'Автофон статус'),
		   'avtofon_comment' => Yii::t('app', 'Автофон коммент'),
           


		   'comment' => Yii::t('app', 'Комментарий'),
            'licency_org' => Yii::t('app', 'Лицензия (разрешение)'),
            'owner_id' => Yii::t('app', 'Владелец авто'),
            'licen_id' => Yii::t('app', 'Лицензия'),
           
            'razresh_text' => Yii::t('app', 'Разрешение'),
            'galka_vserv' => Yii::t('app', 'В сервисе'),
            'galka_fssp' => Yii::t('app', 'ФССП'),
        ];
    }
	
	
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getMark0()
    {
        return $this->hasOne(Marks::className(), ['name_mark' => 'mark']);
    }
	
	
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getModel0()
    {
        return $this->hasOne(Models::className(), ['name' => 'model']);
    }
	
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(Autostatus::className(), ['id_status' => 'status']);
    }
	
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getInsurance0()
    {
        return $this->hasOne(Insurancecompany::className(), ['id_company' => 'ins_company']);
    }
	
	
	public function getauto_status()
    {
        // same as above
        return $this->hasMany(Autostatus::className(), ['id_status' => 'status']);
			//->joinWith('automobiles');
		//->viaTable('drivers', ['id_driver' => 'driver_id']);
    }
	
	
	public function getowners()
    {
        // same as above
        return $this->hasMany(Owners::className(), ['id_owner' => 'owner_id']);
			//->joinWith('automobiles');
		//->viaTable('drivers', ['id_driver' => 'driver_id']);
    }
	
	
	
	
	
	public function getAutostatus0()
    {
        // same as above
        return $this->hasOne(Autostatus::className(), ['id_status' => 'status']);
			//->joinWith('auto_status');
		//->viaTable('drivers', ['id_driver' => 'driver_id']);
    }
	
	
	
	
}
