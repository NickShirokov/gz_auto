<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "driversstatus".
 *
 * @property int $id_status
 * @property string $status_name
 *
 * @property Drivers[] $drivers
 */
class Driversstatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'driversstatus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status_name'], 'required'],
            [['status_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_status' => Yii::t('app', 'Id Статус'),
            'status_name' => Yii::t('app', 'Наименование'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivers()
    {
        return $this->hasMany(Drivers::className(), ['status' => 'id_status']);
    }
}
