<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "auto_status".
 *
 * @property int $id_status
 * @property string $status_name
 */
class Autostatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auto_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status_name'], 'required'],
            [['status_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_status' => Yii::t('app', 'Id Статуса'),
            'status_name' => Yii::t('app', 'Название статуса'),
        ];
    }
	
	
	public function getautomobiles()
    {
        // same as above
        return $this->hasMany(AutomobilesNewModel::className(), ['status' => 'id_status']);
			//->joinWith('auto_status');
		//->viaTable('drivers', ['id_driver' => 'driver_id']);
    }
	
	
}
