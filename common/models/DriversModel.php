<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "drivers".
 *
 * @property int $id_driver
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $date_birth
 * @property string $p_series
 * @property string $p_number
 * @property string $lic_series
 * @property string $lic_number
 */
class DriversModel extends \yii\db\ActiveRecord
{
	
	
	
	public $file_f;
	
	
	public $photo_f;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'drivers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
		if ($_SERVER['REQUEST_URI'] == '/admin/contracts/newcreate') {
			
			 $arrr = [
				[['first_name', 'last_name', 'date_birth', 'p_series', 'p_number', 'lic_series', 'lic_number', 'driver_cat', 'status'], 'safe'],
				[['date_birth' , 'p_who', 'address_reg', 'address_fact', 'lic_type', 'lic_dates', 'phone_1', 'phone_2'], 'safe'],
				[['first_name', 'middle_name', 'last_name'], 'safe'],
				[['p_series', 'p_number', 'lic_series', 'lic_number'], 'safe'],
				[['comment', 'main_summa', 'penalty_type', 'fio', 'photo'], 'safe'],
				[['file_f'],'file', 'extensions' => 'csv'],
				[['photo_f'],'file', 'extensions' => 'jpg, png'],
				
				
				
			];
			
		
		} else {
        $arrr = [
            [['first_name', 'last_name', 'date_birth', 'p_series', 'p_number', 'lic_series', 'lic_number', 'driver_cat', 'status'], 'required'],
            [['date_birth' , 'p_who', 'address_reg', 'address_fact', 'lic_type', 'lic_dates', 'phone_1', 'phone_2'], 'safe'],
            [['first_name', 'middle_name', 'last_name'], 'string', 'max' => 255],
            [['p_series', 'p_number', 'lic_series', 'lic_number'], 'string'],
			[['comment', 'main_summa', 'penalty_type', 'fio', 'photo'], 'safe'],
			[['file_f'],'file', 'extensions' => 'csv'],
			[['photo_f'],'file', 'extensions' => 'jpg, png'],
			
			
			
        ];
		}
		
		return $arrr;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_driver' => Yii::t('app', 'ID Водителя'),
            'first_name' => Yii::t('app', 'Имя'),
            'middle_name' => Yii::t('app', 'Отчество'),
            'last_name' => Yii::t('app', 'Фамилия'),
            
            'fio' => Yii::t('app', 'ФИО'),
            
            'date_birth' => Yii::t('app', 'Дата рождения'),
            'p_series' => Yii::t('app', 'Серия паспорта'),
            'p_number' => Yii::t('app', 'Номер паспорта'),
			'p_who' => Yii::t('app', 'Кем выдан и дата выдачи'),
			
					
			'address_reg' => Yii::t('app', 'Адрес регистрации'),
			
			'address_fact' => Yii::t('app', 'Адрес фактического проживания'),
			
			'phone_1' => Yii::t('app', 'Телефон 1'),
			
			'phone_2' => Yii::t('app', 'Телефон 2'),
	
			'main_summa' => Yii::t('app', 'Сумма(заглушка)'),	
            'lic_series' => Yii::t('app', 'Серия вод.удостоверения'),
            'lic_number' => Yii::t('app', 'Номер вод.удостоверения'),
			
			
			'lic_type' => Yii::t('app', 'Тип водительских прав (A B C ...)'),
			
			'lic_dates' => Yii::t('app', 'Даты действия водительских прав'),
			
			
			
			'driver_cat' => Yii::t('app', 'Категория водителя'),
            'status' => Yii::t('app', 'Статус'),
			'penalty_type' => Yii::t('app', 'Тип штрафа ГИБДД'),
			'comment' => Yii::t('app', 'Комментарий'),
			
			
        ];
    }
	
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getCat0()
    {
        return $this->hasOne(Driverscategory::className(), ['id_cat' => 'driver_cat']);
    }
	
	
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(Driversstatus::className(), ['id_status' => 'status']);
    }
	
	
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getPenalty0()
    {
        return $this->hasOne(Penaltytypes::className(), ['id_type' => 'penalty_type']);
    }
	
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getContracts()
	{
		return $this->hasMany(Contracts::className(), ['driver_id' => 'id_driver)']);
	}
	
	
	
	
	
}
