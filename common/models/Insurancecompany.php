<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "insurance_company".
 *
 * @property int $id_company
 * @property string $name
 */
class Insurancecompany extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'insurance_company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_company' => Yii::t('app', 'Id Компании'),
            'name' => Yii::t('app', 'Наименование'),
        ];
    }
}
