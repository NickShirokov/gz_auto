<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DriversModel;

/**
 * DriversModelSearch represents the model behind the search form of `common\models\DriversModel`.
 */
class DriversModelSearch extends DriversModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_driver', 'main_summa', 'driver_cat', 'status', 'penalty_type'], 'integer'],
            [['first_name', 'middle_name', 'last_name', 'date_birth', 'p_series', 'p_number', 'lic_series', 'lic_number', 'comment', 'fio'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DriversModel::find();
		
		
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			//'pageSize' => 1000,
			'pagination' => [
				// this $params['pagesize'] is an id of dropdown list that we set in view file
				'pagesize' => (isset($params['per-page']) ? $params['per-page'] :  '100'),
			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		
		
        // grid filtering conditions
        $query->andFilterWhere([
            'id_driver' => $this->id_driver,
            'date_birth' => $this->date_birth,
            'main_summa' => $this->main_summa,
            'driver_cat' => $this->driver_cat,
            'status' => $this->status,
            'penalty_type' => $this->penalty_type,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'middle_name', $this->middle_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'p_series', $this->p_series])
            ->andFilterWhere(['like', 'p_number', $this->p_number])
            ->andFilterWhere(['like', 'lic_series', $this->lic_series])
            ->andFilterWhere(['like', 'lic_number', $this->lic_number])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
