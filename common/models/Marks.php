<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "marks".
 *
 * @property int $id_mark
 * @property string $name_mark
 * @property string $fullname
 *
 * @property Models[] $models
 */
class Marks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'marks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_mark', 'fullname'], 'required'],
            [['name_mark', 'fullname'], 'string', 'max' => 100],
            [['name_mark'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_mark' => Yii::t('app', 'Id Марки авто'),
            'name_mark' => Yii::t('app', 'Название'),
            'fullname' => Yii::t('app', 'Полное название'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModels()
    {
        return $this->hasMany(Models::className(), ['mark' => 'name_mark']);
    }
}
