<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "penalty_types".
 *
 * @property int $id_type
 * @property string $name_type
 *
 * @property Drivers[] $drivers
 */
class Penaltytypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'penalty_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_type'], 'required'],
            [['name_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_type' => Yii::t('app', 'Id Type'),
            'name_type' => Yii::t('app', 'Name Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivers()
    {
        return $this->hasMany(Drivers::className(), ['penalty_type' => 'id_type']);
    }
}
