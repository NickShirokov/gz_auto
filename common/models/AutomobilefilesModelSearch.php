<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Automobilefiles;

/**
 * AutomobilefilesModelSearch represents the model behind the search form of `app\models\Automobilefiles`.
 */
class AutomobilefilesModelSearch extends Automobilefiles
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_file', 'id_auto'], 'integer'],
            [['file', 'comment'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Automobilefiles::find();

        // add conditions that should always apply here
		
		$query->joinWith(['automobiles']);
		
		

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort' => ['attributes' => ['gosnomer']],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_file' => $this->id_file,
            'id_auto' => $this->id_auto,
        ]);

        $query->andFilterWhere(['like', 'file', $this->file])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
