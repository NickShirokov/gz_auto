<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "models".
 *
 * @property int $id_model
 * @property string $mark
 * @property string $name
 *
 * @property Marks $mark0
 */
class Models extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'models';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mark', 'name'], 'required'],
            [['mark', 'name'], 'string', 'max' => 100],
            [['mark'], 'exist', 'skipOnError' => true, 'targetClass' => Marks::className(), 'targetAttribute' => ['mark' => 'name_mark']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_model' => Yii::t('app', 'Id Модели авто'),
            'mark' => Yii::t('app', 'Марка авто'),
            'name' => Yii::t('app', 'Название модели'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMark0()
    {
        return $this->hasOne(Marks::className(), ['name_mark' => 'mark']);
    }
	
	
}
