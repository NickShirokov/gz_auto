<?php

namespace app\models;

use Yii;


use common\models\AutomobilesNewModel;
use common\models\DriversModel;

/**
 * This is the model class for table "putevie_listi".
 *
 * @property int $id_putevoy
 * @property int $contract_id
 * @property int $auto_id
 * @property int $driver_id
 * @property string $date_start
 * @property string $date_end
 * @property string $put_type
 */
class Putevielisti extends \yii\db\ActiveRecord
{
	
	
	public $link;
	
	public $gosnomer;
	
	public $last_name;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'putevie_listi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'auto_id', 'driver_id', 'date_start', 'date_end', 'put_type'], 'required'],
            [[ 'auto_id', 'driver_id'], 'integer'],
            [['date_start', 'date_end', 'link'], 'safe'],
            [['put_type'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_putevoy' => Yii::t('app', 'Id'),

            'auto_id' => Yii::t('app', 'Машина (госномер)'),
            'driver_id' => Yii::t('app', 'Водитель (фамилия)'),
            'date_start' => Yii::t('app', 'Дата начала'),
            'date_end' => Yii::t('app', 'Дата конца'),
            'put_type' => Yii::t('app', 'Тип'),
            'link' => Yii::t('app', 'Ссылка на печать'),
        ];
    }
	
	
	
	
	public function getdrivers () {
		
		return $this->hasMany(DriversModel::className(), ['id_driver' => 'driver_id']);
		
	}
	
	
	public function getautomobiles () {
		return $this->hasMany(AutomobilesNewModel::className(), ['id_auto' => 'auto_id']);
		
	}
	
	
	
	
	
	
	
}
