<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Licen;

/**
 * LicenSearchModel represents the model behind the search form of `app\models\Licen`.
 */
class LicenSearchModel extends Licen
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_lic'], 'integer'],
            [['name_lic', 'mechanic', 'medic'], 'safe'],
			[['inn', 'ogrn', 'okpo', 'razreshenie', 'owner_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Licen::find();

		
		$query->joinWith(['owners']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_lic' => $this->id_lic,
			
        ]);

        $query->andFilterWhere(['like', 'name_lic', $this->name_lic])
            ->andFilterWhere(['like', 'mechanic', $this->mechanic])
            ->andFilterWhere(['like', 'owner_id', $this->owner_id])
            ->andFilterWhere(['like', 'medic', $this->medic])
            ->andFilterWhere(['like', 'ogrn', $this->ogrn])
            ->andFilterWhere(['like', 'inn', $this->inn])
            ->andFilterWhere(['like', 'razreshenie', $this->razreshenie])
            ->andFilterWhere(['like', 'okpo', $this->okpo]);

        return $dataProvider;
    }
}
