<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "maintable".
 *
 * @property int $id_contract
 * @property int $id_auto
 * @property int $id_driver
 * @property string $status_name
 * @property string $AutoMobile
 * @property string $date_start
 * @property string $FIO
 * @property int $arenda_stoim
 * @property string $ArendaSumma
 * @property string $MainSumma
 * @property string $ArendaDays
 * @property string $OplachenDo
 * @property int $FullDays
 * @property string $DolgPoArende
 * @property string $PayDogovorSumma
 * @property string $SummaOurPenalties
 * @property string $SummaOurPenaltiesPvznosVykup
 * @property string $DolgPoDogovoru
 * @property string $SummaPddPenalties
 * @property string $PayPddSumma
 * @property string $DolgPddSumma
 * @property string $MainDolg
 */
class Maintable extends \yii\db\ActiveRecord
{
	
	public static function primaryKey()
	{
		return array('id_contract');
	}
	
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'maintable';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_contract', 'id_auto', 'id_driver', 'arenda_stoim', 'FullDays'], 'integer'],
            [['date_start', 'arenda_stoim'], 'required'],
            [['date_start', 'OplachenDo'], 'safe'],
            [['FIO'], 'string'],
            [['ArendaSumma', 'MainSumma', 'ArendaDays', 'DolgPoArende', 'PayDogovorSumma', 'SummaOurPenalties', 'SummaOurPenaltiesPvznosVykup', 'DolgPoDogovoru', 'SummaPddPenalties', 'PayPddSumma', 'DolgPddSumma' , 'DolgPddSummaReal', 'MainDolg'], 'number'],
            [['status_name'], 'string', 'max' => 100],
            [['AutoMobile', 'NewOstatok', 'WeekPayArenda','MonthPayArenda'], 'string', 'max' => 211],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_contract' => Yii::t('app', 'Номер договора'),
            'id_auto' => Yii::t('app', 'Id Auto'),
            'id_driver' => Yii::t('app', 'Id Driver'),
            'status_name' => Yii::t('app', 'Статус'),
            'AutoMobile' => Yii::t('app', 'Машина'),
            'date_start' => Yii::t('app', 'Дата начала договора'),
            'FIO' => Yii::t('app', 'ФИО'),
            'arenda_stoim' => Yii::t('app', 'Arenda Stoim'),
            'ArendaSumma' => Yii::t('app', 'Arenda Summa'),
            'MainSumma' => Yii::t('app', 'Main Summa'),
            'ArendaDays' => Yii::t('app', 'Arenda Days'),
            'OplachenDo' => Yii::t('app', 'Оплачен до'),
            'FullDays' => Yii::t('app', 'Full Days'),
            'DolgPoArende' => Yii::t('app', 'Долг по аренде'),
            'PayDogovorSumma' => Yii::t('app', 'Pay Dogovor Summa'),
            'SummaOurPenalties' => Yii::t('app', 'Summa Our Penalties'),
            'SummaOurPenaltiesPvznosVykup' => Yii::t('app', 'Summa Our Penalties Pvznos Vykup'),
            'DolgPoDogovoru' => Yii::t('app', 'Прочие долги'),
            'SummaPddPenalties' => Yii::t('app', 'Summa Pdd Penalties'),
            'PayPddSumma' => Yii::t('app', 'Pay Pdd Summa'),
            'DolgPddSumma' => Yii::t('app', 'Долг ПДД'),
            'DolgPddSummaReal' => Yii::t('app', 'Долг ПДД реальный'),
            'MainDolg' => Yii::t('app', 'Общий долг'),
            'NewOstatok' => Yii::t('app', 'Свободный остаток'),
            'MonthPayArenda' => Yii::t('app', 'Оплатил по аренде за месяц'),
            'WeekPayArenda' => Yii::t('app', 'Оплатил по аренде за неделю'),
			
        ];
    }
}
