<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Driverscomments;

/**
 * DriverscommentsSearchModel represents the model behind the search form of `app\models\Driverscomments`.
 */
class DriverscommentsSearchModel extends Driverscomments
{
	
	
	//public $date_comment;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_comment', 'driver_id'], 'integer'],
            [['comments', 'date_comment'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Driverscomments::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_comment' => $this->id_comment,
            'driver_id' => $this->driver_id,
			'date_comment' => $this->date_comment,
        ]);

        $query->andFilterWhere(['like', 'comments', $this->comments]);

		$query->orderBy('date_comment DESC');
		
        return $dataProvider;
    }
}
