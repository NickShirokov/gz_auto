<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_items".
 *
 * @property int $id
 * @property string $type_user
 * @property int $user_id
 * @property string $type_page
 * @property int $item_id
 * @property string $date_create
 */
class Logitems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_user', 'user_id', 'type_page', 'item_id', 'date_create'], 'required'],
            [['type_user', 'type_page'], 'string'],
            [['user_id', 'item_id'], 'integer'],
            [['date_create'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_user' => 'Type User',
            'user_id' => 'User ID',
            'type_page' => 'Type Page',
            'item_id' => 'Item ID',
            'date_create' => 'Date Create',
        ];
    }
}
