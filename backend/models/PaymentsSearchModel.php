<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Payments;
use app\models\Contracts;

use app\models\DriversModel;
use app\models\AutomobilesNewModel;


/**
 * PaymentsSearchModel represents the model behind the search form of `app\models\Payments`.
 */
class PaymentsSearchModel extends Payments
{
	
	
	public $last_name;
	 
	public $gosnomer;
	
	public $driver_id;
	
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_payment', 'contract_id', 'summa',  'arenda', ], 'integer'],
            [['date_payment', 
			'driver_id','type', 'comment','contract_id', 'pmnt_variant', 'last_name', 'gosnomer', 'first_name', 'pay_pdd','pay_ourpen'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Payments::find();
		
		$query->joinWith(['contracts']);
		
		
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				// this $params['pagesize'] is an id of dropdown list that we set in view file
				'pagesize' => (isset($params['per-page']) ? $params['per-page'] :  '50'),
			],
			'sort' => ['attributes' => ['last_name', 'driver_id', 'gosnomer', 'pmnt_variant','id_payment', 'contract_id', 'date_payment', 'summa', 'arenda', 'pay_pdd', 'pay_ourpen']],
			//'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_payment' => $this->id_payment,
            'driver_id' => $this->driver_id,
            'contract_id' => $this->contract_id,
            'date_payment' => $this->date_payment,
            'summa' => $this->summa,
           // 'p_vznos' => $this->p_vznos,
            'arenda' => $this->arenda,
           // 'vykup' => $this->vykup,
            'pay_pdd' => $this->pay_pdd,
            'pay_ourpen' => $this->pay_ourpen,
            'pmnt_variant' => $this->pmnt_variant,
           // 'peredacha_rulya' => $this->peredacha_rulya,
           // 'p_vyezd_200' => $this->p_vyezd_200,
           // 'p_atrib' => $this->p_atrib,
           // 'p_put_list' => $this->p_put_list,
           // 'p_osago' => $this->p_osago,
           // 'p_uterya_doc' => $this->p_uterya_doc,
           // 'p_uterya_key' => $this->p_uterya_key,
           // 'p_diag_card' => $this->p_diag_card,
           // 'vyezd_sotr' => $this->vyezd_sotr,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'comment', $this->comment]);

			
		$query->andFilterWhere(['like', 'drivers.last_name', $this->last_name]);

		$query->andFilterWhere(['like', 'automobiles.gosnomer', $this->gosnomer]);
		

		//$query->orderBy('id_payment DESC');
			
        return $dataProvider;
    }
	
	
	
	//Пробуем новый поиск
	
	public function searchAll($params)
    {
        $query = Payments::find()/*->joinWith(['contracts'])*/->andFilterWhere([
           
            'contract_id' => $params['PaymentsSearchModel']['contract_id'],
            
        ])->orderBy('date_payment DESC')->all();
		
		
		/*
		$query->joinWith(['contracts']);
		
		

        // grid filtering conditions
        $query->andFilterWhere([
           
            'contract_id' => $this->contract_id,
            
        ])->orderBy('date_payment ASC')->all();

       */
		//$query->orderBy('date_payment DESC');	
			
        return  $query;
    }
	
	
}
