<?php

namespace app\models;

use Yii;


/**
 * This is the model class for table "violations".
 *
 */
class Pddpenaltiesapi extends \yii\db\ActiveRecord
{
	
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'violations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'vehicle_certificate', 'ruling_number', 'ruling_date', 'price', 'violation_date', 'violator', 'violator_paid', 'paid', 'created_at', 'updated_at'], 'safe'],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Id Штрафа'),
            
            'vehicle_certificate' => Yii::t('app', 'vehicle_certificate'),
            'ruling_number' => Yii::t('app', 'ruling_number'),
            'ruling_date' => Yii::t('app', 'ruling_date'),
            'price' => Yii::t('app', 'price'),
			
            'violation_date' => Yii::t('app', 'violation_date'),
            'violator' => Yii::t('app', 'violator'),
            'violator_paid' => Yii::t('app', 'violator_paid'),
            
            'paid' => Yii::t('app', 'paid'),
			'created_at' => Yii::t('app', 'created_at'),
			'updated_at' => Yii::t('app', 'updated_at'),
			
			
			
        ];
    }
	
	
	
	
	//db2
	public static function getDb() {
		return Yii::$app->db2;
	}
	
	
	
}
