<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Owners;
use app\models\Licen;

/**
 * OwnersSearchModel represents the model behind the search form of `app\models\Owners`.
 */
class OwnersSearchModel extends Owners
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_owner'], 'integer'],
            [['first_name', 'middle_name', 'last_name', 'p_series', 'p_number', 'p_who', 'address_reg', 'phone_1', 'phone_2', 'phone_3', 'licence_company'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Owners::find();

		
		$query->joinWith(['licen']);
		
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_owner' => $this->id_owner,
            'licence_company' => $this->licence_company,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'middle_name', $this->middle_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'p_series', $this->p_series])
            ->andFilterWhere(['like', 'p_number', $this->p_number])
            ->andFilterWhere(['like', 'p_who', $this->p_who])
            ->andFilterWhere(['like', 'address_reg', $this->address_reg])
            ->andFilterWhere(['like', 'phone_1', $this->phone_1])
            ->andFilterWhere(['like', 'phone_2', $this->phone_2])
            ->andFilterWhere(['like', 'phone_3', $this->phone_3]);

        return $dataProvider;
    }
}
