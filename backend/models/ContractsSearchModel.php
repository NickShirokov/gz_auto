<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Contracts;


use app\models\Owners;
use app\models\DriversModel;
use app\models\AutomobilesNewModel;
/**
 * ContractsSearchModel represents the model behind the search form of `app\models\Contracts`.
 */
class ContractsSearchModel extends Contracts
{
	
	
	public $last_name;
	 
	public $gosnomer;
	
	public $VIN;
	
	
	public $fio;
	
	//public $cntrstid;
	
	
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['owner_id', /*'driver_id',*/ 'auto_id'], 'integer'],
			[['last_name', 'gosnomer', 'VIN', 'status_itog', 'date_end', 'id_contract', 'fio'], 'safe'],
			
        //    [['date_create'], 'safe'],
			
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
		
		//$query = Contracts::find();
		
        $query = Contracts::find();
		
		$query->joinWith(['owners', 'drivers', 'automobiles'])->all();
		
		
		
		
		
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort' => ['attributes' => 
						['fio', 'gosnomer', 'VIN' , 'id_contract', 'owner_id', 'date_end', 'date_start', 'status_itog']],
			
			
			
			'pagination' => [
				// this $params['pagesize'] is an id of dropdown list that we set in view file
				'pagesize' => (isset($params['per-page']) ? $params['per-page'] :  '100'),
			],
			
			
			
        ]);
		

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            //'id_contract' => $this->id_contract,
            'contracts.owner_id' => $this->owner_id,
            'driver_id' => $this->driver_id,
            'auto_id' => $this->auto_id,
			//'first_name' => $this->last_name,
            'status_itog' => $this->status_itog,
            'date_end' => $this->date_end,
			
        ]);
		
		$query->andFilterWhere(['like', 'drivers.last_name', $this->last_name]);
		$query->andFilterWhere(['like', 'drivers.fio', $this->fio]);
		$query->andFilterWhere(['like', 'contracts.id_contract', $this->id_contract]);
		$query->andFilterWhere(['like', 'automobiles.gosnomer', $this->gosnomer]);
		$query->andFilterWhere(['like', 'automobiles.VIN', $this->VIN]);
        return $dataProvider;
    }
}
