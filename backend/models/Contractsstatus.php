<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contracts_status".
 *
 * @property int $c_status_id
 * @property string $c_status_name
 * @property string $c_comment
 */
class Contractsstatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contracts_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['c_status_name', 'c_comment'], 'required'],
            [['c_comment'], 'string'],
            [['c_status_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'c_status_id' => Yii::t('app', 'C Status ID'),
            'c_status_name' => Yii::t('app', 'C Status Name'),
            'c_comment' => Yii::t('app', 'C Comment'),
        ];
    }
}
