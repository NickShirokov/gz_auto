<?php

namespace app\models;

use Yii;

use common\models\DriversModel;

/**
 * This is the model class for table "drivers_files".
 *
 * @property int $id_file
 * @property int $id_driver
 * @property string $file
 * @property string $comment
 */
class Driversfiles extends \yii\db\ActiveRecord
{
	
	
	public $file_f;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'drivers_files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_driver', 'file', 'comment'], 'required'],
            [['id_driver'], 'integer'],
            [['comment'], 'string'],
            [['file'], 'string', 'max' => 255],
			[['file_f'],'file', 'extensions' => 'jpg, png, jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_file' => Yii::t('app', 'Id File'),
            'id_driver' => Yii::t('app', 'Id Driver'),
            'file' => Yii::t('app', 'File'),
            'comment' => Yii::t('app', 'Comment'),
        ];
    }
	
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getdrivers()
	{
		return $this->hasMany(DriversModel::className(), ['id_driver' => 'id_driver']);
	}

}
