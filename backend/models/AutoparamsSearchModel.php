<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Autoparams;

/**
 * AutoparamsSearchModel represents the model behind the search form of `app\models\Autoparams`.
 */
class AutoparamsSearchModel extends Autoparams
{
	
	
	
	public $VIN;
	
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_param'], 'integer'],
            [['param_sts', 'param_gosnomer', 'date_param', 'auto_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Autoparams::find();

        // add conditions that should always apply here

		$query->joinWith(['automobiles']);
		
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort' => ['attributes' => ['VIN']],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_param' => $this->id_param,
            'auto_id' => $this->auto_id,
            'date_param' => $this->date_param,
        ]);

        $query->andFilterWhere(['like', 'param_sts', $this->param_sts])
            ->andFilterWhere(['like', 'param_gosnomer', $this->param_gosnomer]);

			
		$query->andFilterWhere(['like', 'automobiles.VIN', $this->VIN]);	
			
        return $dataProvider;
    }
}
