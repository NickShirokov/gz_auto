<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Putevielisti;




/**
 * PutevielistiSearchModel represents the model behind the search form of `app\models\Putevielisti`.
 */
class PutevielistiSearchModel extends Putevielisti
{
	
	
public $gosnomer;
	
	public $last_name;
	
	
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_putevoy'], 'integer'],
            [['date_start', 'auto_id', 'driver_id', 'date_end', 'put_type', 'gosnomer', 'last_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Putevielisti::find();
		
		
		$query->joinWith(['automobiles','drivers']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			
			'sort' => ['attributes' => ['gosnomer', 'last_name']],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_putevoy' => $this->id_putevoy,
           
            'auto_id' => $this->auto_id,
            'driver_id' => $this->driver_id,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
        ]);

        $query->andFilterWhere(['like', 'put_type', $this->put_type]);
        $query->andFilterWhere(['like', 'automobiles.gosnomer', $this->gosnomer]);
        $query->andFilterWhere(['like', 'drivers.last_name', $this->last_name]);

		
		
		$query->orderBy('date_end DESC');
		
        return $dataProvider;
    }
}
