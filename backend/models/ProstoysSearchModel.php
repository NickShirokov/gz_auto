<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Prostoys;

/**
 * ProstoysSearchModel represents the model behind the search form of `app\models\Prostoys`.
 */
class ProstoysSearchModel extends Prostoys
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_prostoy', 'contract_id', 'summa_prostoy', 'days_prostoy', 'type_opl'], 'integer'],
            [['comment'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Prostoys::find();
		
		
		$query->joinWith(['prostoy_status', 'payments']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_prostoy' => $this->id_prostoy,
            'contract_id' => $this->contract_id,
            'summa_prostoy' => $this->summa_prostoy,
            'days_prostoy' => $this->days_prostoy,
            'type_opl' => $this->type_opl,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
