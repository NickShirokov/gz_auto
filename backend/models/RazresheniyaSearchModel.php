<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Razresheniya;

/**
 * RazresheniyaSearchModel represents the model behind the search form of `app\models\Razresheniya`.
 */
class RazresheniyaSearchModel extends Razresheniya
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_razresh', 'licen_id'], 'integer'],
            [['r_n', 'series', 'number'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Razresheniya::find();
		
		
		$query->joinWith(['licen']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_razresh' => $this->id_razresh,
            'licen_id' => $this->licen_id,
        ]);

        $query->andFilterWhere(['like', 'r_n', $this->r_n])
            ->andFilterWhere(['like', 'series', $this->series])
            ->andFilterWhere(['like', 'number', $this->number]);

        return $dataProvider;
    }
}
