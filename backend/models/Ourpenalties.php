<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "our_penalties".
 *
 * @property int $id_penalty
 * @property int $contract_id
 * @property string $date_pen
 * @property int $pen_summa
 * @property string $pen_comment
 */
class Ourpenalties extends \yii\db\ActiveRecord
{
	
	
	public $last_name;
	
	public $gosnomer;



	public $ostatok;
	
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'our_penalties';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['p_contract_id', 'date_pen', 'pen_summa', 'pen_comment'], 'required'],
            [['p_contract_id', 'pen_summa', 'status_o_pen'], 'integer'],
            [['date_pen', 'last_name', 'gosnomer', 'ostatok'], 'safe'],
            [['pen_comment'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_penalty' => Yii::t('app', 'Номер штрафа'),
            'p_contract_id' => Yii::t('app', 'Номер договора'),
            'date_pen' => Yii::t('app', 'Дата штрафа'),
            'pen_summa' => Yii::t('app', 'Сумма штрафа'),
            'pen_comment' => Yii::t('app', 'Комментарий к штрафу'),
			
			
			'last_name' => Yii::t('app', 'Водитель (фамилия)'),
			
			'gosnomer' => Yii::t('app', 'Машина (госномер)'),
			'status_o_pen' => Yii::t('app', 'Статус штрафа'),
			'ostatok' => Yii::t('app', 'Платежи'),
			
        ];
    }
	
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	

    public function getContracts()
    {
        // same as above
        return $this->hasMany(Contracts::className(), ['id_contract' => 'p_contract_id'])
			->joinWith('drivers')->joinWith('automobiles');
		
    }
	
	
	/*
	public function getOurpenaltiesstatuses()
    {
        // same as above
        return $this->hasMany(Ourpenaltiesstatus::className(), ['status_o_pen' => 'status_o_id'])
			->joinWith('drivers');
		
    }
	*/
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getOurpenaltiesstatus0()
    {
        return $this->hasOne(Ourpenaltiesstatus::className(), ['status_o_id' => 'status_o_pen']);
    }
	
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getour_penalties_status()
    {
        return $this->hasMany(Ourpenaltiesstatus::className(), ['status_o_id' => 'status_o_pen']);
    }
	
	
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getpayments()
    {
        return $this->hasMany(Payments::className(), ['id_ourpen' => 'id_penalty']);
    }
	
	
	
	
}
