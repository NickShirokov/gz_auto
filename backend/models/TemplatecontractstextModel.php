<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Templatecontractstext;

/**
 * TemplatecontractstextModel represents the model behind the search form of `app\models\Templatecontractstext`.
 */
class TemplatecontractstextModel extends Templatecontractstext
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_text'], 'integer'],
            [['text1', 'text2', 'text3', 'text4', 'text5'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Templatecontractstext::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_text' => $this->id_text,
        ]);

        $query->andFilterWhere(['like', 'text1', $this->text1])
            ->andFilterWhere(['like', 'text2', $this->text2])
            ->andFilterWhere(['like', 'text3', $this->text3])
            ->andFilterWhere(['like', 'text4', $this->text4])
            ->andFilterWhere(['like', 'text5', $this->text5]);

        return $dataProvider;
    }
}
