<?php

namespace app\models;

use Yii;

use common\models\AutomobilesNewModel;

/**
 * This is the model class for table "autoparams".
 *
 * @property int $id_param
 * @property string $param_sts
 * @property string $param_gosnomer
 * @property string $date_param
 */
class Autoparams extends \yii\db\ActiveRecord
{
	
	
	public $VIN;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'autoparams';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'date_param', 'auto_id'], 'required'],
            [['date_param', 'param_sts', 'param_gosnomer'], 'safe'],
            [['param_sts'], 'string', 'max' => 255],
            [['param_gosnomer'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_param' => Yii::t('app', 'Id'),
            'auto_id' => Yii::t('app', 'Машина (по VIN)'),
			
            'param_sts' => Yii::t('app', 'СТС'),
            'param_gosnomer' => Yii::t('app', 'Госномер'),
            'date_param' => Yii::t('app', 'Дата изменения'),
        ];
    }
	
	
	
		/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAutomobiles()
	{
		return $this->hasMany(AutomobilesNewModel::className(), ['id_auto' => 'auto_id']);
	}
	
	
	
}
