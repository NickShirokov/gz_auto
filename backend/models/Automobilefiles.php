<?php

namespace app\models;

use Yii;
use common\models\AutomobilesNewModel;
/**
 * This is the model class for table "automobile_files".
 *
 * @property int $id_file
 * @property int $id_auto
 * @property string $file
 * @property string $comment
 */
class Automobilefiles extends \yii\db\ActiveRecord
{
	
	
	public $file_f;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'automobile_files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_auto', 'file', 'comment'], 'required'],
            [['id_auto'], 'integer'],
            [['comment'], 'string'],
            [['file'], 'string', 'max' => 255],
			
			[['file_f'],'file', 'extensions' => 'jpg, png'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_file' => Yii::t('app', 'Id Картинки'),
            'id_auto' => Yii::t('app', 'Id Машины'),
            'file' => Yii::t('app', 'Файл'),
            'comment' => Yii::t('app', 'Комментарий'),
        ];
    }
	
	
	
	
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getAutomobile0()
    {
        return $this->hasOne(AutomobilesNewModel::className(), ['id_auto' => 'id_auto']);
    }
	
	
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAutomobiles()
	{
		return $this->hasMany(AutomobilesNewModel::className(), ['id_auto' => 'id_auto']);
	}
	
	
	
	
	
}
