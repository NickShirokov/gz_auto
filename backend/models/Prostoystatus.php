<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prostoy_status".
 *
 * @property int $id_prostoy_status
 * @property string $prostoy_status_name
 * @property string $comment
 */
class Prostoystatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prostoy_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prostoy_status_name', 'comment'], 'required'],
            [['comment'], 'string'],
            [['prostoy_status_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_prostoy_status' => Yii::t('app', 'Id Статуса'),
            'prostoy_status_name' => Yii::t('app', 'НАименование'),
            'comment' => Yii::t('app', 'Comment'),
        ];
    }
}
