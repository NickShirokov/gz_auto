<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AutomobilesModel;

/**
 * AutomobilesSearchModel represents the model behind the search form of `common\models\AutomobilesModel`.
 */
class AutomobilesSearchModel extends AutomobilesModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['mark', 'model', 'alias', 'VIN', 'sts', 'gosnomer', 'year', 'color', 'transmission'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AutomobilesModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				// this $params['pagesize'] is an id of dropdown list that we set in view file
				'pagesize' => (isset($params['per-page']) ? $params['per-page'] :  '20'),
			],
			
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'year' => $this->year,
        ]);

        $query->andFilterWhere(['like', 'mark', $this->mark])
            ->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'VIN', $this->VIN])
            ->andFilterWhere(['like', 'sts', $this->sts])
            ->andFilterWhere(['like', 'gosnomer', $this->gosnomer])
            ->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'transmission', $this->transmission]);

        return $dataProvider;
    }
}
