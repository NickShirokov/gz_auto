<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "razresheniya".
 *
 * @property int $id_razresh
 * @property int $licen_id
 * @property string $r_n
 * @property string $series
 * @property string $number
 */
class Razresheniya extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'razresheniya';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['licen_id', 'r_n', 'series', 'number'], 'required'],
            [['licen_id'], 'integer'],
            [['r_n', 'series', 'number'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_razresh' => Yii::t('app', 'Id'),
            'licen_id' => Yii::t('app', 'Лицензия'),
            'r_n' => Yii::t('app', 'Первый номер'),
            'series' => Yii::t('app', 'Серия'),
            'number' => Yii::t('app', 'Номер'),
        ];
    }
	
	
	
	public function getlicen () {
		
		return $this->hasMany(Licen::className(), ['id_lic' => 'licen_id']);
		
	}
	
	
}
