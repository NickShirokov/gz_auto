<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "licen".
 *
 * @property int $id_lic
 * @property string $name_lic
 * @property string $mechanic
 * @property string $medic
 */
class Licen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'licen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_lic', 'mechanic', 'medic'], 'required'],
            [['name_lic', 'mechanic', 'medic'], 'string', 'max' => 255],
			[['inn', 'ogrn', 'okpo', 'razreshenie', 'owner_id' ], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_lic' => Yii::t('app', 'Id Лицензии'),
            'owner_id' => Yii::t('app', 'Владелец'),
            'name_lic' => Yii::t('app', 'Наименование'),
            'ogrn' => Yii::t('app', 'ОГРН'),
            'inn' => Yii::t('app', 'ИНН'),
            'mechanic' => Yii::t('app', 'Механик'),
            'okpo' => Yii::t('app', 'ОКПО'),
            'razreshenie' => Yii::t('app', 'Разрешение'),
            'medic' => Yii::t('app', 'Медик'),
        ];
    }
	
	
	
	public function getowners () {
		
		return $this->hasMany(Owners::className(), ['id_owner' => 'owner_id']);
		
	}
	
	
	
}
