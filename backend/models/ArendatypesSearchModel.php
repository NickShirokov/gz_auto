<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Arendatypes;

/**
 * ArendatypesSearchModel represents the model behind the search form of `app\models\Arendatypes`.
 */
class ArendatypesSearchModel extends Arendatypes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['a_d_type'], 'integer'],
            [['name_a_type', 'short_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Arendatypes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'a_d_type' => $this->a_d_type,
        ]);

        $query->andFilterWhere(['like', 'name_a_type', $this->name_a_type]);
        $query->andFilterWhere(['like', 'short_name', $this->short_name]);

        return $dataProvider;
    }
}
