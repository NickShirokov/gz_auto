<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pddpenalties;


use common\models\AutomobilesNewModel;
use common\models\DriversModel;

/**
 * PddpenaltiesSearchModel represents the model behind the search form of `app\models\Pddpenalties`.
 */
class PddpenaltiesSearchModel extends Pddpenalties
{
	
	
	public $gosnomer;
	
	public $last_name;
	
	public $test_sum;
	
	public $sts;
	
	public $auto_item;
	
	//public $summa_opl_api;
	
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_shtraf', 'auto_id', 'param_id', 'driver_id', 'contract_id', 'type_opl', 'summa_opl', 'type_opl_api'], 'integer'],
            [['n_post', 'date_post', 'date_narush', 'sts', 'gosnomer', 'last_name', 'test_sum', 'auto_item', 'narushitel'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pddpenalties::find();

        // add conditions that should always apply here
	
		//$query->joinWith(['contracts', 'pdd_pen_status', 'pdd_pen_status_api', 'drivers', 'automobiles']);
		
		$query->joinWith(['drivers', 'automobiles', 'autoparams']);
		
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort' => ['attributes' => ['id_shtraf', 'contract_id',  'gosnomer', 'auto_item' ,'sts', 'last_name', 'summa_opl_api', 'n_post', 'date_post', 'date_narush']],
			'pagination' => [
				// this $params['pagesize'] is an id of dropdown list that we set in view file
				'pagesize' => (isset($params['per-page']) ? $params['per-page'] :  '100'),
			],
		]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_shtraf' => $this->id_shtraf,
            'driver_id' => $this->driver_id,
			
			
            'contract_id' => $this->contract_id,
            'date_post' => $this->date_post,
            'date_narush' => $this->date_narush,
            'type_opl' => $this->type_opl,
            'summa_opl' => $this->summa_opl,
            'type_opl_api' => $this->type_opl_api,
			
        ]);

        $query->andFilterWhere(['like', 'n_post', $this->n_post]);
		$query->andFilterWhere(['like', 'autoparams.param_sts', $this->sts]);
		$query->andFilterWhere(['like', 'autoparams.param_gosnomer', $this->gosnomer]);
		$query->andFilterWhere(['like', 'automobiles.model', $this->auto_item]);
		$query->andFilterWhere(['like', 'drivers.last_name', $this->last_name]);
		$query->andFilterWhere(['like', 'automobiles.VIN', $this->auto_item]);
		
		$query->andFilterWhere(['like', 'narushitel', $this->narushitel]);
		
		
		
		

        return $dataProvider;
    }
	
	
	
	
	//Пробуем новый поиск
	
	public function searchAll($params)
    {
	
		
        $query = Pddpenalties::find()/*->joinWith(['contracts'])*/->where([
           
            'contract_id' => $params['PddpenaltiesSearchModel']['contract_id'],
            
        ])->orderBy('date_post ASC')->all();
		
		/*
		$query->joinWith(['contracts']);
		
		

        // grid filtering conditions
        $query->andFilterWhere([
           
            'contract_id' => $this->contract_id,
            
        ])->orderBy('date_pen ASC')->all();

       */
		
		//print_r ($query);
		//exit;
			
        return  $query;
    }
	
	
}
