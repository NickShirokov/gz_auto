<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "arenda_types".
 *
 * @property int $a_d_type
 * @property string $name_a_type
 */
class Arendatypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'arenda_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_a_type'], 'required'],
            [['name_a_type'], 'string', 'max' => 100],
			[['short_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'a_d_type' => Yii::t('app', 'ID'),
            'name_a_type' => Yii::t('app', 'НАименование'),
            'short_name' => Yii::t('app', 'Короткое наименование'),
        ];
    }
}
