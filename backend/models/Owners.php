<?php

namespace app\models;


use app\models\Licen;

use Yii;

/**
 * This is the model class for table "owners".
 *
 * @property int $id_owner
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $p_series
 * @property string $p_number
 * @property string $p_who
 * @property string $address_reg
 * @property string $phone_1
 * @property string $phone_2
 * @property string $phone_3
 */
class Owners extends \yii\db\ActiveRecord
{
	
	
	
	public $file_f;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'owners';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'middle_name', 'last_name', 'p_series', 'p_number', 'phone_1', 'phone_2', 'phone_3'], 'required'],
            [['p_who', 'address_reg'], 'string'],
            [['first_name', 'middle_name', 'last_name', 'phone_1', 'phone_2', 'phone_3'], 'string', 'max' => 100],
            [['p_series', 'p_number'], 'string', 'max' => 10],
			[['licence_company'], 'safe'],
			[['file_f'],'file', 'extensions' => 'csv'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_owner' => Yii::t('app', 'Id Владельца'),
            'first_name' => Yii::t('app', 'Имя'),
            'middle_name' => Yii::t('app', 'Отчество'),
            'last_name' => Yii::t('app', 'Фамилия'),
            'p_series' => Yii::t('app', 'Паспорт серия'),
            'p_number' => Yii::t('app', 'Паспорт номер'),
            'p_who' => Yii::t('app', 'Кем выдан и дата выдачи'),
            'address_reg' => Yii::t('app', 'Адрес регистрации'),
            'phone_1' => Yii::t('app', 'Телефон 1'),
            'phone_2' => Yii::t('app', 'Телефон 2'),
            'phone_3' => Yii::t('app', 'Телефон 3'),
			'licence_company' => Yii::t('app', 'Лицензия'),
			
			
        ];
    }
	
	
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getContracts0()
    {
        return $this->hasOne(Contracts::className(), ['id_owner' => 'id_owner']);			
    }
	
	public function getlicen () {
		
		return $this->hasMany(Licen::className(), ['id_lic' => 'licence_company']);
	}
	
	
	
}
