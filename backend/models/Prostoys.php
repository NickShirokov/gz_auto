<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prostoys".
 *
 * @property int $id_prostoy
 * @property int $contract_id
 * @property int $summa_prostoy
 * @property int $days_prostoy
 * @property string $comment
 */
class Prostoys extends \yii\db\ActiveRecord
{
	
	
	public $ostatok;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prostoys';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contract_id', 'summa_prostoy', 'days_prostoy', 'comment'], 'required'],
            [['contract_id', 'summa_prostoy', 'days_prostoy','type_opl'], 'integer'],
            [['comment'], 'string'],
			[['ostatok'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_prostoy' => Yii::t('app', 'Id Простоя'),
            'contract_id' => Yii::t('app', 'Договор'),
            'summa_prostoy' => Yii::t('app', 'Сумма за простой'),
            'days_prostoy' => Yii::t('app', 'Количество дней простоя'),
            'type_opl' => Yii::t('app', 'Статус'),
            'comment' => Yii::t('app', 'Комментарий'),
			'ostatok' => Yii::t('app', 'Платежи'),
        ];
    }
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getProstoystatus0()
    {
        return $this->hasOne(Prostoystatus::className(), ['id_prostoy_status' => 'type_opl']);
    }
	
	
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getprostoy_status()
    {
        return $this->hasMany(Prostoystatus::className(), ['id_prostoy_status' => 'type_opl']);
    }
	
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getpayments()
    {
        return $this->hasMany(Payments::className(), ['p_id_prostoy' => 'id_prostoy']);
    }
	
	
	
	
}
