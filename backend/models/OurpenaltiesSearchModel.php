<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ourpenalties;

/**
 * OurpenaltiesSearchModel represents the model behind the search form of `app\models\Ourpenalties`.
 */
class OurpenaltiesSearchModel extends Ourpenalties
{
	
	public $last_name;
	
	public $gosnomer;
	
	
	
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_penalty', 'p_contract_id', 'pen_summa'], 'integer'],
            [['date_pen', 'pen_comment', 'last_name', 'gosnomer', 'status_o_pen'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ourpenalties::find();
		
		
		$query->joinWith(['contracts', 'our_penalties_status', 'payments']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort' => ['attributes' => ['id_penalty','p_contract_id','date_pen', 'pen_summa', 'last_name', 'gosnomer']],
			'pagination' => [
				// this $params['pagesize'] is an id of dropdown list that we set in view file
				'pagesize' => (isset($params['per-page']) ? $params['per-page'] :  '100'),
			],
		]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_penalty' => $this->id_penalty,
            'p_contract_id' => $this->p_contract_id,
            'date_pen' => $this->date_pen,
            'pen_summa' => $this->pen_summa,
			'status_o_pen' => $this->status_o_pen,
        ]);

        $query->andFilterWhere(['like', 'pen_comment', $this->pen_comment]);
		
		$query->andFilterWhere(['like', 'drivers.last_name', $this->last_name]);
		
		
		$query->andFilterWhere(['like', 'automobiles.gosnomer', $this->gosnomer]);
		
		//print_r ($query->all());

        return $dataProvider;
    }
	
	
	
	//Пробуем новый поиск
	
	public function searchAll($params)
    {
        $query = Ourpenalties::find()/*->joinWith(['contracts'])*/->andFilterWhere([
           
            'p_contract_id' => $params['OurpenaltiesSearchModel']['p_contract_id'],
            
        ])->orderBy('date_pen ASC')->all();
		
		/*
		$query->joinWith(['contracts']);
		
		

        // grid filtering conditions
        $query->andFilterWhere([
           
            'contract_id' => $this->contract_id,
            
        ])->orderBy('date_pen ASC')->all();

       */
	
			
        return  $query;
    }
	
	
}
