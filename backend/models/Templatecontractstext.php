<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "template_contracts_text".
 *
 * @property int $id_text
 * @property string $text1
 * @property string $text2
 * @property string $text3
 * @property string $text4
 * @property string $text5
 */
class Templatecontractstext extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'template_contracts_text';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text1', 'text2', 'text3', 'text4', 'text5'], 'safe'],
            [['text1', 'text2', 'text3', 'text4', 'text5'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_text' => Yii::t('app', 'Id Text'),
            'text1' => Yii::t('app', 'Text1'),
            'text2' => Yii::t('app', 'Text2'),
            'text3' => Yii::t('app', 'Text3'),
            'text4' => Yii::t('app', 'Text4'),
            'text5' => Yii::t('app', 'Text5'),
        ];
    }
}
