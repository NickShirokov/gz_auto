<?php

namespace app\models;

use Yii;


use common\models\DriversModel;

/**
 * This is the model class for table "drivers_comments".
 *
 * @property int $id_comment
 * @property int $driver_id
 * @property string $comments
 *
 * @property Drivers $driver
 */
class Driverscomments extends \yii\db\ActiveRecord
{
	
	public $file_f;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'drivers_comments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['driver_id', 'comments'], 'required'],
            [['driver_id'], 'integer'],
            [['comments'], 'string'],
			[['date_comment'], 'safe'],
			
			[['comment_file'], 'string', 'max' => 255],
			[['file_f'],'file', 'extensions' => 'jpg, png, jpeg'],
			
			
			
            [['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => DriversModel::className(), 'targetAttribute' => ['driver_id' => 'id_driver']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_comment' => Yii::t('app', 'Номер комментария'),
            'driver_id' => Yii::t('app', 'Водитель'),
			'date_comment' =>  Yii::t('app', 'Дата комментария'),
            'comments' => Yii::t('app', 'Комментарии'),
            'comment_file' => Yii::t('app', 'Картинка'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
	 
	
    public function getDriver()
    {
        return $this->hasOne(DriversModel::className(), ['id_driver' => 'driver_id']);
    }
	
	
	public function getDrivers()
    {
        return $this->hasMany(DriversModel::className(), ['id_driver' => 'driver_id']);
    }
	
	
}
