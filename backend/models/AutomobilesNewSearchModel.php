<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AutomobilesNewModel;

/**
 * AutomobilesNewSearchModel represents the model behind the search form of `common\models\AutomobilesNewModel`.
 */
class AutomobilesNewSearchModel extends AutomobilesNewModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_auto'], 'integer'],
            [['mark', 'model','PTS', 'VIN', 'sts', 'gosnomer', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AutomobilesNewModel::find();

        // add conditions that should always apply here
        
        $query->joinWith(['owners']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				// this $params['pagesize'] is an id of dropdown list that we set in view file
				'pagesize' => (isset($params['per-page']) ? $params['per-page'] :  '20'),
			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_auto' => $this->id_auto,
            //'year' => $this->year,
            //'osago' => $this->osago,
            //'diagnostic_card' => $this->diagnostic_card,
            //'vialon' => $this->vialon,
            //'avtofon' => $this->avtofon,
            //'razresh_id' => $this->razresh_id,
            'licen_id' => $this->licen_id,
			'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'mark', $this->mark])
            ->andFilterWhere(['like', 'model', $this->model])
           // ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'VIN', $this->VIN])
			->andFilterWhere(['like', 'PTS', $this->PTS])
            ->andFilterWhere(['like', 'sts', $this->sts])
            ->andFilterWhere(['like', 'gosnomer', $this->gosnomer]);
         //   ->andFilterWhere(['like', 'color', $this->color])
           // ->andFilterWhere(['like', 'transmission', $this->transmission]);
            //->andFilterWhere(['like', 'status', $this->status])
           // ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
