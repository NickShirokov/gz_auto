<?php

namespace app\models;

use app\models\DriversModel;
use app\models\Contracts;
use common\models\AutomobilesNewModel;




use Yii;

/**
 * This is the model class for table "payments".
 *
 * @property int $id_payment
 * @property int $contract_id
 * @property string $date_payment
 * @property string $type
 * @property int $summa
 * @property int $p_vznos
 * @property int $arenda
 * @property int $vykup
 * @property int $pdd
 * @property int $prosrochka
 * @property int $peredacha_rulya
 * @property int $p_vyezd_200
 * @property int $p_atrib
 * @property int $p_put_list
 * @property int $p_osago
 * @property int $p_uterya_doc
 * @property int $p_uterya_key
 * @property int $p_diag_card
 * @property int $vyezd_sotr
 * @property string $comment
 */
class Payments extends \yii\db\ActiveRecord
{
	
	public $driver_id;


	public $last_name;
	
	public $gosnomer;
	
	
	public $p_contract_id;
	
	public $date_pen;
	
	
	public $pen_summa;
	
	public $pen_comment;
	
	public $file_f;
	
	//public $pmnt_variant;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'type', 'summa',], 'required'],
            [['contract_id', 'summa',  'arenda','pay_ourpen', 'prostoy_summa', 'p_id_prostoy'], 'integer'],
            [['date_payment', 'driver_id', 'last_name','pay_pdd', 'gosnomer', 
				'id_ourpen', 
				'id_pdd_shtraf', 
				'pay_dogovor', 'pmnt_variant', 'comission_type', 'comission_summa', 'comment'], 'safe'],
            [['type' ], 'string'],
			[['file_f'],'file', 'extensions' => 'csv'],
			
			/*[['date_payment', 'contract_id'],'unique', 
				'targetAttribute' => ['date_payment', 'contract_id'],
				'message' => Yii::t('app', 'Дата платежа и номер договора должны быть уникальными в конкретную дату. Выберите другой договор или правильную дату')
				],
				
			*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_payment' => Yii::t('app', 'Номер платежа'),
            'contract_id' => Yii::t('app', 'Номер договора'),
            'date_payment' => Yii::t('app', 'Дата платежа'),
            'pmnt_variant' => Yii::t('app', 'Тип платежа'),
            'type' => Yii::t('app', 'Тип (нал/на карту)'),
            'summa' => Yii::t('app', 'Внесенная сумма'),
            //'p_vznos' => Yii::t('app', 'Первоначальный взнос'),
            'arenda' => Yii::t('app', 'Арендный платеж'),
            //'vykup' => Yii::t('app', 'Выкупной платеж'),
            //'pdd' => Yii::t('app', 'Штраф ПДД'),
            //'prosrochka' => Yii::t('app', 'Просрочка арендного платежа'),
            //'peredacha_rulya' => Yii::t('app', 'Передача руля другому лицу'),
            //'p_vyezd_200' => Yii::t('app', 'Выезд за 200 км от Москвы'),
            //'p_atrib' => Yii::t('app', 'Снятие атрибутики такси'),
            //'p_put_list' => Yii::t('app', 'Выезд без путевого листа'),
            //'p_osago' => Yii::t('app', 'Просроченное осаго'),
            //'p_uterya_doc' => Yii::t('app', 'Утеря документов на авто'),
            //'p_uterya_key' => Yii::t('app', 'Утеря ключа'),
            //'p_diag_card' => Yii::t('app', 'Просроченная диагностичка'),
            //'vyezd_sotr' => Yii::t('app', 'Выезд сотрудников + ГСМ'),
            'comment' => Yii::t('app', 'Комментарий к платежу'),
			
			
			'driver_id' => Yii::t('app', 'Водитель (фамилия)'),
			
			'last_name' => Yii::t('app', 'Водитель (фамилия)'),
			
			'pay_dogovor' => Yii::t('app', 'Платеж по договору'),
			
			
			
			'pay_pdd' => Yii::t('app', 'Платеж по ПДД'),
			'id_pdd_shtraf' => Yii::t('app', 'Номер штрафа ПДД'),
	
			'gosnomer' => Yii::t('app', 'Машина (госномер)'),
			'id_ourpen' => Yii::t('app', 'Номер нашего штрафа'),
			'pay_ourpen' => Yii::t('app', 'Сумма по нашему штрафу'),
			'prostoy_summa' => Yii::t('app', 'Сумма за день простоя'),
			'p_id_prostoy' => Yii::t('app', 'ID простоя'),
			
			
			'comission_type' => Yii::t('app', 'Комиссия'),
			'comission_summa' => Yii::t('app', 'Сумма комиссии'),
			//'prostoy_days' => Yii::t('app', 'Количество дней простоя'),
			
			
        ];
    }
	
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	

    public function getContracts()
    {
        // same as above
        return $this->hasMany(Contracts::className(), ['id_contract' => 'contract_id'])
			->joinWith('drivers')->joinWith('automobiles')->joinWith('our_penalties');
		//->viaTable('drivers', ['id_driver' => 'driver_id']);
    }
	

	
}
