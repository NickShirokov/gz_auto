<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "our_penalties_status".
 *
 * @property int $status_o_id
 * @property string $status_o_name
 */
class Ourpenaltiesstatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'our_penalties_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status_o_name'], 'required'],
            [['status_o_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'status_o_id' => Yii::t('app', 'ID Статус'),
            'status_o_name' => Yii::t('app', 'Наименование'),
        ];
    }
}
