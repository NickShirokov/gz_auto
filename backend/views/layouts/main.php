<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);



?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'GZ AUTO',
        'brandUrl' => Yii::$app->homeUrl,
		 'renderInnerContainer'=>false,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top padding-new-15',
        ],
    ]);
    
	
	
	
	
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Вход', 'url' => ['/site/login']];
        $menuItems[] = ['label' => 'Регистрация', 'url' => ['../site/signup']];
		$menuItems[] = ['label' => 'Восстановить пароль', 'url' => ['../site/requestpasswordreset/']];
    
	} elseif (Yii::$app->user->can('admin')) {
    
		$menuItems = [
        ['label' => 'Главная', 'url' => ['/site/index']],	
		['label' => 'Пользователи', 'url' => ['/users/index'],
			'items' => [
				['label' => 'Моя карточка', 'url' => ['user/view', 'id' => Yii::$app->user->getId()]],
				['label' => 'Список (пока тест)', 'url' => ['user/index']],
				['label' => 'Роли', 'url' => ['authitem/index']],
				['label' => 'Правила (служебная)', 'url' => ['authrule/index']],				
				['label' => 'Связь пользователей с ролями', 'url' => ['authassignment/index']],	
			],	
		],
		['label' => 'Платежи', 'url' => ['payments/index']],	
		[
			'label' => 'Штрафы', 'url' => ['/ourpenalties/index'],		
				'items' => [
							['label' => 'Наши штрафы', 'url' => ['/ourpenalties/index']],
							['label' => 'Штрафы ПДД', 'url' => ['/pddpenalties/index']],
							],	
		],
		[
			'label' => 'Справочники', 'url' => ['/ourpenalties/index'],	
				'items' => [
							['label' => 'Владельцы', 'url' => ['/owners/index']],
							['label' => 'Лицензии', 'url' => ['/licen/index']],		
							['label' => 'Статусы водителей', 'url' => ['driversstatus/index']],
							['label' => 'Категории водителей', 'url' => ['driverscategory/index']],
							['label' => 'Марки авто', 'url' => ['/marks/index']],
							['label' => 'Модели авто', 'url' => ['/models/index']],
							['label' => 'СТС и Гос номера', 'url' => ['/autoparams/index']],
							['label' => 'Статусы авто', 'url' => ['/autostatus/index']],
							['label' => 'Статусы наших штрафов', 'url' => ['ourpenaltiesstatus/index']],
							['label' => 'Страховые компании', 'url' => ['/insurancecompany/index']],	
							['label' => 'Типы аренды для договоров', 'url' => ['arendatypes/index']],
							['label' => 'Сканы документов машин(все)', 'url' => ['automobilefiles/index']],	
							['label' => 'Сканы документов водителей(все)', 'url' => ['driversfiles/index']],	
							['label' => 'Комментарии', 'url' => ['driverscomments/index']],
							['label' => 'Статусы простоев', 'url' => ['prostoystatus/index']],
							['label' => 'Кому платит штрафы ГИБДД', 'url' => ['penaltytypes/index']],							
							['label' => 'Шаблон для текста договора', 'url' => ['templatecontractstext/index']],							
						],	
		],
		[
			'label' => 'Печатные формы', 'url' => ['#'],
				'items' => [
							['label' => 'Путевые листы', 'url' => ['/putevielisti/index']],
							['label' => 'Договора', 'url' => ['/contracts/index']],
							['label' => 'Карточка Водитель-машина', 'url' => ['/contracts/cardnaletu']],
						],
		],
		
		[
			'label' => 'Отчеты', 'url' => ['#'],	
				'items' => [
							['label' => 'Должники по аренде более 5000', 'url' => ['/maintable/morearenda/']],
							['label' => 'Сотрудники', 'url' => ['/maintable/sotrudniki/']],
							['label' => 'Должники общий долг более 20000', 'url' => ['/maintable/bigdolg/']],	
						],
		],
		
		['label' => 'Машины', 'url' => ['/automobiles/index'],	
			'items' => [
				['label' => 'Список', 'url' => ['/automobiles/index']],
			],
		],	
		['label' => 'Водители', 'url' => ['/drivers/index'],
			'items' => [
				['label' => 'Список', 'url' => ['/drivers/index']],
				['label' => 'Договора', 'url' => ['/contracts/index']],		
				['label' => 'Простои', 'url' => ['prostoys/index']],
			],
		],
		
    ];
	

		$menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Выход (' . Yii::$app->user->identity->username . '-admin)',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    } elseif (Yii::$app->user->can('manager')) {
		
		$menuItems = [
        ['label' => 'Главная', 'url' => ['/site/index']],	
		['label' => 'Моя карточка', 'url' => ['user/view', 'id' => Yii::$app->user->getId()]],
		[
			'label' => 'Справочники', 'url' => ['/ourpenalties/index'],	
				'items' => [
							['label' => 'Лицензии', 'url' => ['/licen/index']],		
							['label' => 'Марки авто', 'url' => ['/marks/index']],
							['label' => 'Модели авто', 'url' => ['/models/index']],
							['label' => 'СТС и Гос номера', 'url' => ['/autoparams/index']],
							['label' => 'Статусы водителей', 'url' => ['driversstatus/index']],
							['label' => 'Статусы наших штрафов', 'url' => ['ourpenaltiesstatus/index']],
							['label' => 'Статусы авто', 'url' => ['/autostatus/index']],
							
							['label' => 'Категории водителей', 'url' => ['driverscategory/index']],		
							['label' => 'Страховые компании', 'url' => ['/insurancecompany/index']],	
							['label' => 'Сканы документов машин(все)', 'url' => ['automobilefiles/index']],	
							['label' => 'Сканы документов водителей(все)', 'url' => ['driversfiles/index']],	
							['label' => 'Комментарии', 'url' => ['driverscomments/index']],
							['label' => 'Кому платит штрафы ГИБДД', 'url' => ['penaltytypes/index']],							
							['label' => 'Шаблон для текста договора', 'url' => ['templatecontractstext/index']],							
						],	
		],

		[
			'label' => 'Печатные формы', 'url' => ['#'],
			
				'items' => [
							['label' => 'Путевые листы', 'url' => ['/putevielisti/index']],
							['label' => 'Договора', 'url' => ['/contracts/index']],					
							['label' => 'Карточка Водитель-машина', 'url' => ['/contracts/cardnaletu']],					
						],	
		],
		
		
		['label' => 'Машины', 'url' => ['/automobiles/index'],	
			'items' => [
				['label' => 'Список', 'url' => ['/automobiles/index']],
			],
		],
		
		
		['label' => 'Водители', 'url' => ['/drivers/index'],
		
			'items' => [
				['label' => 'Список', 'url' => ['/drivers/index']],
				['label' => 'Договора', 'url' => ['/contracts/index']],		
			],
		
		],
	
    ];
				
		$menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Выход (' . Yii::$app->user->identity->username . '-manager)',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
		
	} elseif (Yii::$app->user->can('payer')) {
		
		$menuItems = [
        ['label' => 'Главная', 'url' => ['/site/index']],
		['label' => 'Моя карточка', 'url' => ['user/view', 'id' => Yii::$app->user->getId()]],
		['label' => 'Платежи', 'url' => ['payments/index']],
	
	
		[
			'label' => 'Штрафы', 'url' => ['/ourpenalties/index'],
			
				'items' => [
							['label' => 'Наши штрафы', 'url' => ['/ourpenalties/index']],
							['label' => 'Штрафы ПДД', 'url' => ['/pddpenalties/index']],
							],
			
		],
		
		[
			'label' => 'Печатные формы', 'url' => ['#'],		
				'items' => [
							['label' => 'Путевые листы', 'url' => ['/putevielisti/index']],
							['label' => 'Договора', 'url' => ['/contracts/index']],
							['label' => 'Карточка Водитель-машина', 'url' => ['/drivers/card']],
							
						],
			
		],
		
		[
			'label' => 'Отчеты', 'url' => ['#'],
			
				'items' => [
							['label' => 'Должники по аренде более 5000', 'url' => ['/maintable/morearenda/']],
							['label' => 'Сотрудники', 'url' => ['/maintable/sotrudniki/']],
							['label' => 'Должники общий долг более 20000', 'url' => ['/maintable/bigdolg/']],					
						],
			
		],
	
		['label' => 'Машины', 'url' => ['/automobiles/index'],		
			'items' => [
				['label' => 'Список', 'url' => ['/automobiles/index']],
			],
		],
		
		['label' => 'Водители', 'url' => ['/drivers/index'],
			'items' => [
				['label' => 'Список', 'url' => ['/drivers/index']],			
				['label' => 'Договора', 'url' => ['/contracts/index']],		
				['label' => 'Простои', 'url' => ['prostoys/index']],					
			],
		
		],
    ];

		$menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Выход (' . Yii::$app->user->identity->username . '-payer)',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
	
	} else {
		
		$menuItems = [
        ['label' => 'Главная', 'url' => ['/site/index']],
		['label' => 'Моя карточка', 'url' => ['user/view', 'id' => Yii::$app->user->getId()]],
		['label' => 'Платежи', 'url' => ['payments/index']],
		[
			'label' => 'Штрафы', 'url' => ['/ourpenalties/index'],
				'items' => [
							['label' => 'Наши штрафы', 'url' => ['/ourpenalties/index']],
							['label' => 'Штрафы ПДД', 'url' => ['/pddpenalties/index']],
							
							],
			
		],
			
		[
			'label' => 'Справочники', 'url' => ['/ourpenalties/index'],
			
				'items' => [
							['label' => 'Владельцы', 'url' => ['/owners/index']],
							['label' => 'Лицензии', 'url' => ['/licen/index']],		
							['label' => 'Статусы водителей', 'url' => ['driversstatus/index']],
							['label' => 'Категории водителей', 'url' => ['driverscategory/index']],
							['label' => 'Марки авто', 'url' => ['/marks/index']],
							['label' => 'Модели авто', 'url' => ['/models/index']],
							['label' => 'Страховые компании', 'url' => ['/insurancecompany/index']],			
							['label' => 'Комментарии', 'url' => ['driverscomments/index']],
						],	
		],

		[
			'label' => 'Печатные формы', 'url' => ['#'],
				'items' => [
							['label' => 'Путевые листы', 'url' => ['/putevielisti/index']],
							['label' => 'Договора', 'url' => ['/contracts/index']],	
							['label' => 'Карточка Водитель-машина', 'url' => ['/contracts/cardnaletu']],							
						],
			
		],
		/*
		[
			'label' => 'Отчеты', 'url' => ['#'],
				'items' => [
							['label' => 'Должники по аренде более 5000', 'url' => ['/maintable/morearenda/']],
							['label' => 'Сотрудники', 'url' => ['/maintable/sotrudniki/']],
							['label' => 'Должники общий долг более 20000', 'url' => ['/maintable/bigdolg/']],					
						],
			
		],*/

		['label' => 'Машины', 'url' => ['/automobiles/index'],		
			'items' => [
				['label' => 'Список', 'url' => ['/automobiles/index']],
			],
		],
		
		
		['label' => 'Водители', 'url' => ['/drivers/index'],
			'items' => [
				['label' => 'Список', 'url' => ['/drivers/index']],		
				['label' => 'Договора', 'url' => ['/contracts/index']],		
				['label' => 'Простои', 'url' => ['prostoys/index']],					
			],
		],
		
    ];
		
		
		$menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Выход (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
		
		
		
	}
	
	
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    
	NavBar::end();
    
	?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
	
	
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; GZ AUTO <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>

<script>

$(document).ready(function(){
	
	
	//Скрывалка данных карточки
	
	$('#karta-vod').click(function(){
		
		if ($(this).hasClass('active')) {
			
			$(this).removeClass('active');
			
			$('.karta-vod').hide();
		} else {
			$(this).addClass('active');
			
			$('.karta-vod').show();
		}
		
		
		
		
		
	});
	
	//Скрывалка данных карточки
	$('#karta-auto').click(function(){
		
		if ($(this).hasClass('active')) {
			
			$(this).removeClass('active');
			
			$('.karta-auto').hide();
		} else {
			$(this).addClass('active');
			
			$('.karta-auto').show();
		}
		
		
		
		
		
	});
	
	
});

</script>




</body>
</html>
<?php $this->endPage() ?>
