<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Marks */

$this->title = Yii::t('app', 'Обновить марку авто: {name}' . ' '. $model->name_mark, [
    'name' => $model->id_mark,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Марки авто'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_mark . ' '. $model->name_mark, 'url' => ['view', 'id' => $model->id_mark]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="marks-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
