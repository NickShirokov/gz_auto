<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MarksModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Марки авто');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marks-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать марку авто'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

	
	<?php 







// Renders a export dropdown menu
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns
]);


?>	
	
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		
		'pager' => [
			'class' => \kop\y2sp\ScrollPager::className(),
			'container' => '.grid-view tbody',
			'item' => 'tr',
			'paginationSelector' => '.grid-view .pagination',
			'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',
		],
		
		
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_mark',
			
			[
				'attribute' => 'name_mark',
				'format' => 'html',
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/marks/view?id='.$data['id_mark'].'">'.$data['name_mark'].'</a>';
					
								return $str;
					
							},
            ],
			
			'fullname',

           [
				'class' => 'yii\grid\ActionColumn',
				'visibleButtons' => [
									'update' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
									'delete' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
								],
			],
        ],
    ]); ?>
    <?php //Pjax::end(); ?>
</div>
