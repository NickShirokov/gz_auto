<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Marks */

$this->title = Yii::t('app', 'Создать марку авто');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Марки авто'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marks-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
