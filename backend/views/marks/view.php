<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Marks */

$this->title = $model->id_mark. ' ' . $model->name_mark;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Марки авто'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="marks-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
		<?php
			if(
				Yii::$app->user->can('admin') 
				)  
			{ 
			
				echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_mark], ['class' => 'btn btn-primary']);
				echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_mark], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите удалить эту марку авто?'),
                'method' => 'post',
            ],
        ]) ;
			}
	
	?>
	
      
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_mark',
            'name_mark',
            'fullname',
        ],
    ]) ?>

</div>
