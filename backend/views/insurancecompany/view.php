<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Insurancecompany */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Страховые компании'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="insurancecompany-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
		<?php
			if(
				Yii::$app->user->can('admin') 
				)  
			{ 
			
				echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_company], ['class' => 'btn btn-primary']);
				echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_company], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите удалить страховую?'),
                'method' => 'post',
            ],
        ]);
			
			}
		?>
	
	
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_company',
            'name',
        ],
    ]) ?>

</div>
