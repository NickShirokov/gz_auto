<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Insurancecompany */

$this->title = Yii::t('app', 'Обновить страховую компанию: {name}', [
    'name' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Страховые компании'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id_company]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="insurancecompany-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
