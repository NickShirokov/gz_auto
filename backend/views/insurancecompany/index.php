<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\date\DatePicker;

use kartik\export\ExportMenu;


/* @var $this yii\web\View */
/* @var $searchModel common\models\InsurancecompanyModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Страховые компании');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insurancecompany-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать страховую компанию'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<?php 


$gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],

			[
				'attribute' => 'id_company',
				'format' => 'html',
				'value' => function($data) {
			
								$str = '';
					
								$str.= '<a href="/admin/insurancecompany/view?id='.$data['id_company'].'">'.$data['id_company'].'</a>';
					
								return $str;
					
							},
            ],
			
			
			[
				'attribute' => 'name',
				'format' => 'html',
				'value' => function($data) {
	
								$str = '';
					
								$str.= '<a href="/admin/insurancecompany/view?id='.$data['id_company'].'">'.$data['name'].'</a>';
					
								return $str;
					
							},
							
			],
			[
				'class' => 'yii\grid\ActionColumn',
				'visibleButtons' => [
									'update' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
									'delete' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
								],
			],

		];	



// Renders a export dropdown menu
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns
]);

?>	
	
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		
		'pager' => [
			'class' => \kop\y2sp\ScrollPager::className(),
			'container' => '.grid-view tbody',
			'item' => 'tr',
			'paginationSelector' => '.grid-view .pagination',
			'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',
		],
		
		
		
		
        'columns' => $gridColumns,	
			
			
            //
        
    ]); ?>
    <?php //Pjax::end(); ?>
</div>
