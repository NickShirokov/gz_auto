<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Insurancecompany */

$this->title = Yii::t('app', 'Создать страховую компанию');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Страховые компании'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insurancecompany-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
