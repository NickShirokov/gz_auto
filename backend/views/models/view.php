<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Models */

$this->title = $model->id_model . ' '. $model->mark .' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Модели авто'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="models-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
		<?php
			if(
				Yii::$app->user->can('admin') 
				)  
			{ 
			echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_model], ['class' => 'btn btn-primary']);
				echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_model], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите удалить модель авто?'),
                'method' => 'post',
            ],
        ]);
			}
	
		?>
	
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_model',
            'mark',
            'name',
        ],
    ]) ?>

</div>
