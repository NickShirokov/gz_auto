<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Models */

$this->title = Yii::t('app', 'Обновить модель авто: {name} ' . $model->mark. ' '  , [
    'name' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Модели'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id_model]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="models-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrMarks' => $arrMarks,
    ]) ?>

</div>
