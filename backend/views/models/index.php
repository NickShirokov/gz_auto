<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;


use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ModelsModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Модели авто');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="models-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать модель авто'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	
	<?php 







// Renders a export dropdown menu
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns
]);


?>		
	

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		
		'pager' => [
			'class' => \kop\y2sp\ScrollPager::className(),
			'container' => '.grid-view tbody',
			'item' => 'tr',
			'paginationSelector' => '.grid-view .pagination',
			'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',
		 ],
		
		
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_model',
            //'mark',
			
			[
				'attribute' => 'mark',
				//'value' => 'mark0.name_mark',
				'format' => 'html',
				'value' => function($data) {
					
					
								$str = '';
					
								$str.= '<a href="/admin/models/view?id='.$data['id_model'].'">'.$data['mark'].'</a>';
					
								return $str;
					
							},
				'filter' => $arrMarks
			],
			
			
            [
				'attribute' => 'name',
				'format' => 'html',
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/models/view?id='.$data['id_model'].'">'.$data['name'].'</a>';
					
								return $str;
					
							},
			],
			
            [
				'class' => 'yii\grid\ActionColumn',
				'visibleButtons' => [
									'update' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
									'delete' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
								],
			],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
