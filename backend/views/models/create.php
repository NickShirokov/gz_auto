<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Models */

$this->title = Yii::t('app', 'Создать модель авто');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Модели'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="models-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrMarks' => $arrMarks,
    ]) ?>

</div>
