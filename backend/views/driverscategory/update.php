<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Driverscategory */

$this->title = Yii::t('app', 'Обновить категорию: {name}', [
    'name' => $model->id_cat,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Категории водителей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_cat, 'url' => ['view', 'id' => $model->id_cat]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="driverscategory-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
