<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Driverscategory */

$this->title = $model->id_cat;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Категории водителей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="driverscategory-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
		<?php
			if(
				Yii::$app->user->can('admin') 
				)  
			{ 
					echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_cat], ['class' => 'btn btn-primary']);
			}
		?>
	
       
        
		
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_cat',
            'category_name',
        ],
    ]) ?>

</div>
