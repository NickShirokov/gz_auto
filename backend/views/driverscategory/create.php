<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Driverscategory */

$this->title = Yii::t('app', 'Создать категорию водителей');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Категории водителей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driverscategory-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
