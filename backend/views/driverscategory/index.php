<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\DriverscategoryModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Категории водителей');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driverscategory-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать категорию'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

			[
				'attribute' =>'id_cat',
				'format' => 'html',
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/driverscategory/update?id='.$data['id_cat'].'">'.$data['id_cat'].'</a>';
					
								return $str;
					
							},
            ],
			[
				'attribute' => 'category_name',
				'format' => 'html',
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/driverscategory/update?id='.$data['id_cat'].'">'.$data['category_name'].'</a>';
					
								return $str;
					
							},
			],
            [
				'class' => 'yii\grid\ActionColumn',
				'template' => '{view} {update}',
				'visibleButtons' => [
									'update' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
									'delete' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
								],
			],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
