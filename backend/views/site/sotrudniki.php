<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use kartik\date\DatePicker;

use kartik\export\ExportMenu;


/* @var $this yii\web\View */
/* @var $searchModel common\models\MaintableModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сводная таблица GZ-AUTO';
?>



<div class="maintable-index">

    <!--<h1><?= Html::encode($this->title) ?></h1>
    -->
	
	<?php //Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	
	
<?php 

$gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],

			
			[
				'attribute' => 'id_contract',
				'format' => 'html',
				'value' => function ($data) {
					
								//print_r ($data);
								
								$str.= '<a href="contracts/view?id='.$data->id_contract.'">Договор '.$data->id_contract.'</a>'; 
								
								return $str;
				},
			],
			[
				'attribute' => 'status_name',
				'contentOptions' => ['style' => 'width:20px; white-space: normal;'],
            ],
			
			//'id_auto',
            //'id_driver',
            
            //'AutoMobile',
			
			
			
			[
				'attribute' => 'AutoMobile',
				'format' => 'html',
				'value' => function($data) {
					
					
								$str = '';
								
								$str.= '<a href="/admin/automobiles/view?id='.$data['id_auto'].'">'.$data['AutoMobile'].'</a>';
					
					
								return $str;
					
						},
				
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
			],
			
			
           // 'date_start',
			
				
		    [   
				'attribute' => 'date_start',
                'value' => 'date_start',
                'format' => ['date', 'php:d.m.Y'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'MaintableModelSearch[date_start]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['MaintableModelSearch']['date_start'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],
	
			
			
            
			
			[
				'attribute' => 'FIO',
				'format' => 'html',
				
				'value' => function($data) {
								
								$str = '';
								
								$str.= '<a href="/admin/drivers/view?id='.$data['id_driver'].'">'.$data['FIO'].'</a>';
					
								return $str;
							},
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
			],
			
			
            //'arenda_stoim',
            //'ArendaSumma',
            //'MainSumma',
            //'ArendaDays',
           // 'OplachenDo',
			
		    [   
				'attribute' => 'OplachenDo',
                'value' => 'OplachenDo',
                'format' => ['date', 'php:d.m.Y'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'MaintableModelSearch[OplachenDo]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['MaintableModelSearch']['OplachenDo'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],
			
			
            //'FullDays',
            'DolgPoArende',
            
			//'DolgPddSumma',
			[
					'attribute' => 'DolgPddSummaReal',
					'format' => 'html',
					'value' => function($data) {
						
							$str.= '';
							
							if ($data['DolgPddSummaReal'] < 0) {
								$str.= '<span style="color: green;">'.(int)$data['DolgPddSummaReal'].'</span>';
							} else {
								$str.= '<span style="color: red;">'.(int)$data['DolgPddSummaReal'].'</span>';
							}
							
							return $str;
							
					},
			],
			
			'DolgPoDogovoru',
			
			//'PayDogovorSumma',
            //'SummaOurPenalties',
            //'SummaOurPenaltiesPvznosVykup',
            
            //'SummaPddPenalties',
            //'PayPddSumma',
            
            'MainDolg',
			//'NewOstatok',
			'WeekPayArenda',
			'MonthPayArenda',

            //['class' => 'yii\grid\ActionColumn'],
        ];



// Renders a export dropdown menu
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns
]);



?>	
	
	
	
	
	
  

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		
		'pager' => [
			'class' => \kop\y2sp\ScrollPager::className(),
			'container' => '.grid-view tbody',
			'item' => 'tr',
			'paginationSelector' => '.grid-view .pagination',
			'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',
		],
		
		
        'columns' => $gridColumns,
    ]); ?>
    <?php // Pjax::end(); ?>
</div>















<?php 

/*

$a = 'AAABBBCDDDAEET';

print_r ($a);

echo '<br><br>';

$a_arr = str_split($a);

print_r ($a_arr);







echo '<br><br>';

$new_arr = array();

foreach ($a_arr as $key => $value) {
	
	$new_arr[$value]['name'] = $value;
	
}


$shadr_arr = array();


foreach ($a_arr as $key => $value) {
	
	if (array_key_exists($value, $new_arr)) {
		
		if (!empty($new_arr[$value]['count'])) {
			$new_arr[$value]['count']+= 1;
		} else {
			$new_arr[$value]['count'] = 1;
		}
	}
	
}


print_r ($new_arr);

echo '<br><br>';


foreach ($new_arr as $key => $value) {
	
	echo $key . $value['count']; 
	
	
}

*/


?>









