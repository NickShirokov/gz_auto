<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\PddPenStatusApiSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pdd Pen Status Api Models');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pdd-pen-status-api-model-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Pdd Pen Status Api Model'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'p_status_id',
            'p_status_name',

            [
				'class' => 'yii\grid\ActionColumn'
				'template' => (
								Yii::$app->user->can('user') 
								or 
								Yii::$app->user->can('manager')
								or 
								Yii::$app->user->can('payer'))  ? '{view}' : '{view}{update}{delete}',
			],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
