<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PddPenStatusApiModel */

$this->title = Yii::t('app', 'Create Pdd Pen Status Api Model');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pdd Pen Status Api Models'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pdd-pen-status-api-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
