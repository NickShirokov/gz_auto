<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PddPenStatusApiModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pdd-pen-status-api-model-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'p_status_name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
