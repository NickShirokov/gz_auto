<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\Contracts */

$this->title = Yii::t('app', 'Продлить договор '. $model->id_contract);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Договора'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contracts-create">

    <h1><?= Html::encode($this->title) ?></h1>

   
   <div class="contracts-form">

    <?php $form = ActiveForm::begin(); ?>
	
	
	<div class="col-md-4">
	 <div class="form-group">
	 
	 
		<input id="days" class="form-control" value="0" autocomplete="on" name="days" type="number" />
	</div>
	
	</div>
	 <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Продлить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
   

</div>
