<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;


use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ContractsSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Штрафы ПДД по договору '.$model->id_contract);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Договора'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Договор '.$model->id_contract, 'url' => ['view', 'id' => $model->id_contract]];

$this->params['breadcrumbs'][] = $this->title;

?>
<div class="contracts-index">

    <h1><?= Html::encode($this->title) ?></h1>

 
	

    

  
  
  <p>	
	<ul class="nav nav-tabs">
	  <li role="presentation"><?= Html::a(Yii::t('app', 'Все платежи по договору'), ['payments', 'id' => $model->id_contract],  ['class' => 'btn',]) ?>
   </li>
	  <li role="presentation"><?= Html::a(Yii::t('app', 'Штрафы ПДД по договору'), ['pddshtrafs', 'id' => $model->id_contract],  ['class' => 'btn',]) ?>
    	</li>
	  <li role="presentation"><?= Html::a(Yii::t('app', 'Штрафы по договору'), ['shtrafs', 'id' => $model->id_contract],  ['class' => 'btn', ]) ?>
    	
		</li>
	</ul>

	</p>
	
	
	 
    <p>
	
	 <?= Html::a(Yii::t('app', 'Создать штраф ПДД'), ['pddpenalties/create', 'contract_id' => $model->id_contract], ['class' => 'btn btn-success']) ?>
  
	
	
		
		
		<?= Html::a(Yii::t('app', 'Договор PDF'), ['pdfcontract', 'id' => $model->id_contract],  ['class' => 'btn btn-primary', 'target'=>'_blank',]) ?>
		
		
	
		
	
	
		<?= Html::a(Yii::t('app', 'Посмотреть карточку водителя'), ['drivers/view', 'id' => $current_driver->id_driver],  ['class' => 'btn btn-primary', ]) ?>
    
		<?= Html::a(Yii::t('app', 'Посмотреть карточку машины'), ['automobiles/view', 'id' => $current_auto->id_auto],  ['class' => 'btn btn-primary', ]) ?>
    
		
		
		
		<?= Html::a(Yii::t('app', 'Новая карточка водитель-машина'), ['card', 'id' => $model->id_contract],  ['class' => 'btn btn-primary', ]) ?>
    
		
    </p>
	
  
  
  
      <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		
		
		'pager' => [
			'class' => \kop\y2sp\ScrollPager::className(),
			'container' => '.grid-view tbody',
			'item' => 'tr',
			'paginationSelector' => '.grid-view .pagination',
			'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',
		],
		
		
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

			
			[
				'attribute' => 'id_shtraf',
				'format' => 'html',
				'value' => function ($data) {
					
									$str = '';
									$str.= '<a href="/admin/pddpenalties/view?id='.$data->id_shtraf.'">'.$data->id_shtraf.'</a>';
					
					
									return $str;
					
					
								}
			],
			
			[
				'attribute' => 'contract_id',
				'format' => 'html',
				'value' => function($data) {
					
					
					
					
								$str = '';
								$str.= '<a href="/admin/contracts/view?id='.$data->contract_id.'">Договор '.$data->contract_id.'</a>';
					
					
								return $str;
							}
			],
			
			
			
			[
				'attribute' => 'sts',
				'value' => function($data) {
							

					

							
							$str ='';
							
				
							$str.=$data['autoparams'][0]['param_sts'];
							
							return $str;
                },
			],
			
			
				
			[
				'attribute' => 'gosnomer',
				'value' => function($data) {
														
							$str ='';
							
				
							$str.=$data['autoparams'][0]['param_gosnomer'];
							
							return $str;
                },
			],
			
			
			
	
			
			
			[
				'attribute' => 'last_name',
				'format' => 'html',
				'value' => function($data) {
													
							$str ='';
							
							$str.='<a href="/admin/drivers/view?id='.$data['drivers'][0]['id_driver'].'">'.$data['drivers'][0]['first_name'].' '.$data['drivers'][0]['last_name']. '</a>';
							
							return $str;
                },
				'contentOptions' => ['style' => 'width:150px; white-space: normal;'],
			],
			
			
			
			
			
			[
				'attribute' => 'auto_item',
				'format' => 'html',
				'value' => function($data) {
					
			
							$str ='';
										
							$str.='<a href="/admin/automobiles/view?id='.$data['automobiles'][0]['id_auto'].'">'.$data['automobiles'][0]['mark'].' '.$data['automobiles'][0]['model'].' '.$data['automobiles'][0]['VIN'].'</a>';
							
							return $str;
							
							
                },
				'contentOptions' => ['style' => 'width:150px; white-space: normal;'],
						
			],
			
	
			
            'n_post',

			
			[   
				'attribute' => 'date_post',
                'value' => 'date_post',
                'format' => ['date', 'php:d.m.Y'],
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'PddpenaltiesSearchModel[date_post]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['PddpenaltiesSearchModel']['date_post'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],
			
            
			[   
				'attribute' => 'date_narush',
                'value' => 'date_narush',
                'format' => ['date', 'php:d.m.Y'],
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'PddpenaltiesSearchModel[date_narush]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['PddpenaltiesSearchModel']['date_narush'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],
			

			
			[
				'attribute' => 'type_opl',
				
				'format' => 'html',
				//'value' => 'pddpenaltiesstatus0.p_status_name',
				'value' => function($data){
					
								$str = '';
								
								$arr_penst = $data['pdd_pen_status'][0];
								
								if ($arr_penst['p_status_id'] == 1) {
									
									$str.= '<span style="color: red">'.$arr_penst['p_status_name'].'</span>';
									
								} elseif ($arr_penst['p_status_id'] == 2) {
									
									$str.= '<span style="color: green">'.$arr_penst['p_status_name'].'</span>';
									
								} else {
									$str.= '<span style="">'.$arr_penst['p_status_name'].'</span>';
									
								}
						
					
								return $str;
							},
				'filter' => $arrPddpenstatus,
			],
			
			
			
			[
				'attribute' => 'type_opl_api',
				'format' => 'html',
				'value' => function($data){
					
					
					
								$str = '';
								
								$arr_penst = $data['pdd_pen_status_api'][0];
								
								if ($arr_penst['p_status_id'] == 1) {
									
									$str.= '<span style="color: red">'.$arr_penst['p_status_name_api'].'</span>';
									
								} elseif ($arr_penst['p_status_id'] == 3) {
									
									$str.= '<span style="color: green">'.$arr_penst['p_status_name_api'].'</span>';
									
								} else {
									$str.= '<span style="">'.$arr_penst['p_status_name_api'].'</span>';
									
								}
						
					
								return $str;
					
					
					
					
				},
				'filter' => $arrPddpenstatusapi,
			],
			
			
			
			
			
			
			
           // 'summa_opl',
			
			
			[
				'attribute' => 'test_sum',
				'format' => 'html',
				'value' => function($data) {
					
						//Здесь будем программировать сумму к оплате
						//Пока прокостылим, потом приведем в норму
						
							$summa = $data['summa_opl'];	
							
							
							$contracts = $data['contracts'];
							
							$pen_com = $contracts[0]['penalty_comission']; 
							$dsc_type = $contracts[0]['discount_type']; 
							
							
							$date_raznost = date_diff(new DateTime($data['date_post']), new DateTime())->days;
							
							
							if ( $date_raznost <= 19 ) {
							
								//skidka_status
							
								//Если есть галочка скидка, то ебашим скидку 50%
								if ($data['skidka_status'] == 0) {
									if ($dsc_type == 1) {					
										$summa = $summa / 2;
									}
								}
								
							} elseif ($date_raznost >= 59) {
								
								$summa = $summa * 2;
								
							} else {
								//
							}
							
							//Если есть галочка комиссия, то добавляем 50 рублей
							if ($pen_com == 1) {
								
								$summa = $summa + 50;
							}
							
							
							return '<strike>'. $data['summa_opl'] . '</strike> '.$summa;
					
				},
				
			],
			
			
			
			[
				'attribute' => 'comment',
				'contentOptions' => ['style' => 'width:150px; white-space: normal;'],
			],
			
			
			

            [
				'class' => 'yii\grid\ActionColumn',
				'template' => '{view} {update}',
				'buttons' => [
				   'update' => function ($url, $model, $key) {
									
					  return Html::a('', '/admin/pddpenalties/update?id='.$key, ['class' => 'glyphicon glyphicon-pencil']);

				   },
	   
				   'view' => function ($url, $model, $key) {			
					  return Html::a('', '/admin/pddpenalties/view?id='.$key, ['class' => 'glyphicon glyphicon-eye-open']);

				   },
				   
				   
				   'delete' => function ($url, $model, $key) {			
					  return Html::a('', '/admin/pddpenalties/delete?id='.$key, ['class' => 'glyphicon glyphicon-trash']);

				   },
				   
				   
				   
				],
			
			
			],
        ],
    ]); ?>
  
  
  
    <?php //Pjax::end(); ?>

	
	
</div>


<div class="">

<h3>Общая Сумма долга по неоплаченным штрафам без условий <?= $sumAl; ?></h3>
<h3>Общая Сумма долга по неоплаченным штрафам с нашей логикой <?= $sumRealAl; ?></h3>



<!--
<h3>
По нажатии на эту кнопку отправятся данные в АПИ, произойдет обмен и в штрафах ПДД для договора N 12
внесутся верные изменения. 

Для того чтобы потестить статусы - идем в сами штрафы (глазик в табличке или в разделе меню)
меняем те данные, которые нужно. Потом возвращаемся сюда и жмем заветную кнопку.
</h3>

<?= Html::a(Yii::t('app', 'Обмен с АПИ'), ['contracts/pddshtraftest', 'id' => $model->id_contract], ['class' => 'btn btn-primary', 'target' => '_blank']) ?>
-->

</div>


