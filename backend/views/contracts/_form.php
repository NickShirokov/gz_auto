<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;



use vova07\imperavi\Widget;


use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\models\Contracts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contracts-form">

    <?php $form = ActiveForm::begin(); ?>

		
	<?php 	
		echo $form->field($model, 'owner_id')->widget(Select2::classname(), [
									'data' => $arrOwners,
									'language' => 'ru',
									'options' => ['placeholder' => 'Select a state ...'],
									'pluginOptions' => [
										'allowClear' => true
									],
									
									'pluginEvents' => [
									   "select2:select" => 'function() { 
									   
									   var id = $(this).val();
								
										$.post(
											"/admin/contracts/owner?id="+id, 
											function(data){
															
												$("#owner-wrap").html(data);
											}
										
										);
										
										
										
										$.post(
											"/admin/contracts/autos?id="+id, 
											function(data){
													
												
													
												$("select#contracts-auto_id").html(data);
											}
										
										);
											   
									   
									   }',
									]
									
									
								]);
	
	
	?>
	
	
	
	
	
		<div id="owner-wrap" style="padding: 10px; border: 1px solid #eee">
			<?= $res_owner;?>
		</div>
	
	
	
	
	
	
	<?php 	
		echo $form->field($model, 'auto_id')->widget(Select2::classname(), [
									'data' => $arrAutomobiles,
									'language' => 'ru',
									'options' => ['placeholder' => 'Select a state ...'],
									'pluginOptions' => [
										'allowClear' => true
									],
									
									'pluginEvents' => [
									   "select2:select" => 'function() { 
									   
											var id = $(this).val();
								
											$.post(
												"/admin/contracts/auto?id="+id, 
												function(data){
													
													$("#auto-wrap").html(data);
												}
											
											);
									   
									   
											   
									   
									   }',
									]
									
									
								]);
	
	
	?>
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	<div id="auto-wrap" style="padding: 10px; border: 1px solid #eee">
	
		<?= $res_auto;?>
	
	</div>
	
	<br>
	
		
	<?php 	
		echo $form->field($model, 'driver_id')->widget(Select2::classname(), [
									'data' => $arrDrivers,
									'language' => 'ru',
									'options' => ['placeholder' => 'Select a state ...'],
									'pluginOptions' => [
										'allowClear' => true
									],
									
									'pluginEvents' => [
									   "select2:select" => 'function() { 
									   
											var id = $(this).val();
								
											$.post(
												"/admin/contracts/driver?id="+id, 
												function(data){
													
													
													
													$("#driver-wrap").html(data);
												}
											
											);
									   
									   
											   
									   
									   }',
									]
									
									
								]);
	
	
	?>
	
	
		
		
		
		
		
		
	
	<div id="driver-wrap" style="padding: 10px; border: 1px solid #eee">
	
		<?= $res_driver;?>
	
	</div>
	
	<br>
 
	
	



	
	
	
	<div class="row">
	
		<div class="col-md-4">
	
	<?php 
	if ($model->date_start): 
	?>
	
			<?= $form->field($model, 'date_start')->input('date', ['required' => false]) ?>

	
	<?php else: ?>
	
	
		<?= $form->field($model, 'date_start')->input('date', ['required' => false, 'value' => date('Y-m-d')]) ?>

	
	<?php endif; ?>	
	
	</div>
	
	
	
    
	<div class="col-md-4">
	<?= $form->field($model, 'p_vznos')->textInput() ?>
	
	
	</div>
	
	<div class="col-md-4">
	<?= $form->field($model, 'arenda_type')->dropDownList( $arrArendatypes, ['prompt' => '']) ?>
	
	</div>
	
	<div class="col-md-4">
	
	<?= $form->field($model, 'arenda_stoim')->textInput() ?>
	
	</div>
	
	<div class="col-md-4">
	<?= $form->field($model, 'arenda_items')->textInput() ?>
	</div>
	
	<div class="col-md-4">
	
	<?= $form->field($model, 'vykup')->textInput() ?>
	
	</div>
	
	
	</div>
	
	
	<?= $form->field($model, 'penalty_comission')->checkbox([
		'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
	])?>
		
	
	<?= $form->field($model, 'discount_type')->checkbox([
		'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
	])?>
	
	
	<h3>Штрафы</h3>
	
	<?= $form->field($model, 'prosrochka')->textInput() ?>
	
	<?= $form->field($model, 'peredacha_drugomu')->textInput() ?>
	
	<?= $form->field($model, 'vyezd_bez_sogl_200')->textInput() ?>
	
	<?= $form->field($model, 'shtraf_pdd')->textInput() ?>
	
	<?= $form->field($model, 'snyat_atrib')->textInput() ?>
	
	
	<?= $form->field($model, 'no_put_list')->textInput() ?>
	
	
	<?= $form->field($model, 'no_osago')->textInput() ?>
	
	
	<?= $form->field($model, 'poterya_docs')->textInput() ?>
	
	
	<?= $form->field($model, 'poterya_keys')->textInput() ?>
	
	<?= $form->field($model, 'no_prodl_diag')->textInput() ?>


	
	<?= $form->field($model, 'vyezd_sotrudnikov')->textInput() ?>
	
	
	<p>Дата окончания аренды указывается первоначальная, в самом договоре реальная дата прибавляет
	количество дней простоя
	</p>
	
	<?= $form->field($model, 'date_pay_arenda')->input('date', ['required' => false]) ?>
	

	
	 <?= $form->field($model, 'comment')->textarea(['rows' => 6, ['required' => false]]) ?>
	 
	 
	 
	  
	  
	  
	  <?= 
	  
	  
	  
	   $form->field($model, 'who_affilate')->widget(Widget::className(), [
			'settings' => [
				'lang' => 'ru',
				'minHeight' => 200,
				'plugins' => [
					'clips',
					'fullscreen',
				],
				'clips' => [
					['Lorem ipsum...', 'Lorem...'],
					['red', '<span class="label-red">red</span>'],
					['green', '<span class="label-green">green</span>'],
					['blue', '<span class="label-blue">blue</span>'],
				],
			],
		]);
	  
	  
	  
	  ?>
	  
	  
	  
	
	<h2>Блоки для редактирования текста PDF договора</h2>

	
	  
	    
	  <?= 
	  
	  
	  
	   $form->field($model, 'text1')->widget(Widget::className(), [
			'settings' => [
				'lang' => 'ru',
				'minHeight' => 200,
				'plugins' => [
					'clips',
					'fullscreen',
				],
				'clips' => [
					['Lorem ipsum...', 'Lorem...'],
					['red', '<span class="label-red">red</span>'],
					['green', '<span class="label-green">green</span>'],
					['blue', '<span class="label-blue">blue</span>'],
				],
			],
		]);
	  
	  
	  
	  ?>
	  
	  
	    
	  <?= 
	  
	  
	  
	   $form->field($model, 'text2')->widget(Widget::className(), [
			'settings' => [
				'lang' => 'ru',
				'minHeight' => 200,
				'plugins' => [
					'clips',
					'fullscreen',
				],
				'clips' => [
					['Lorem ipsum...', 'Lorem...'],
					['red', '<span class="label-red">red</span>'],
					['green', '<span class="label-green">green</span>'],
					['blue', '<span class="label-blue">blue</span>'],
				],
			],
		]);
	  
	  
	  
	  ?>
	  
	  
	    
	  <?= 
	  
	  
	  
	   $form->field($model, 'text3')->widget(Widget::className(), [
			'settings' => [
				'lang' => 'ru',
				'minHeight' => 200,
				'plugins' => [
					'clips',
					'fullscreen',
				],
				'clips' => [
					['Lorem ipsum...', 'Lorem...'],
					['red', '<span class="label-red">red</span>'],
					['green', '<span class="label-green">green</span>'],
					['blue', '<span class="label-blue">blue</span>'],
				],
			],
		]);
	  
	  
	  
	  ?>
	  
	  
	    
	  <?= 
	  
	  
	  
	   $form->field($model, 'text4')->widget(Widget::className(), [
			'settings' => [
				'lang' => 'ru',
				'minHeight' => 200,
				'plugins' => [
					'clips',
					'fullscreen',
				],
				'clips' => [
					['Lorem ipsum...', 'Lorem...'],
					['red', '<span class="label-red">red</span>'],
					['green', '<span class="label-green">green</span>'],
					['blue', '<span class="label-blue">blue</span>'],
				],
			],
		]);
	  
	  
	  
	  ?>
	  
	  
	  
	    
	  <?= 
	  
	  
	  
	   $form->field($model, 'text5')->widget(Widget::className(), [
			'settings' => [
				'lang' => 'ru',
				'minHeight' => 200,
				'plugins' => [
					'clips',
					'fullscreen',
				],
				'clips' => [
					['Lorem ipsum...', 'Lorem...'],
					['red', '<span class="label-red">red</span>'],
					['green', '<span class="label-green">green</span>'],
					['blue', '<span class="label-blue">blue</span>'],
				],
			],
		]);
	  
	  
	  
	  ?>
	  
	  
	  
	  
	  
	  
	  
	   <?= $form->field($model, 'bonus_summ')->textarea(['rows' => 6, ['required' => false]]) ?>
	   
        <?= $form->field($model, 'date_end')->input( 'date' ) ?>



	   <?= $form->field($model, 'status_itog')->dropdownList( $arrCStatuses ) ?>
	   
	
	
		<?= $form->field($model, 'putevoy_type')->checkbox([
			'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
		])?>
	
	
	
	
	
	
	
	
	
	
	
	
	
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>






