<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contracts */

$this->title = Yii::t('app', 'Создать договор');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Договора'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contracts-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrOwners' => $arrOwners,
		'arrDrivers' => $arrDrivers,
		'arrAutomobiles' => $arrAutomobiles,
		'arrArendatypes' => $arrArendatypes,
		'arrCStatuses' => $arrCStatuses,
    ]) ?>

</div>
