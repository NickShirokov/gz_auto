<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ContractsSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Штрафы по договору '.$model->id_contract);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Договора'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Договор '.$model->id_contract, 'url' => ['view', 'id' => $model->id_contract]];

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ourpenalties-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать наш штраф'), ['ourpenalties/create', 'contract_id' => $model->id_contract], ['class' => 'btn btn-success']) ?>
    </p>
	
	

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_penalty',
            
			[
				'attribute' => 'p_contract_id',
				'label' => 'Номер договора(число)'
			],
           
			
			
			
			//'date_pen',
			
			
			[   
				'attribute' => 'date_pen',
                'value' => 'date_pen',
                'format' => ['date', 'php:d.m.Y'],
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'OurpenaltiesSearchModel[date_pen]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['OurpenaltiesSearchModel']['date_pen'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],
			
			
			
			[
				'attribute' => 'last_name',
				'value' => function($data) {
												
							$str ='';
							foreach($data['contracts'] as $item)
							{
				
								$str.=$item['drivers'][0]['first_name'].' '.$item['drivers'][0]['last_name']. '';
							}
							return $str;
                },
			],
			
			[
				'attribute' => 'gosnomer',
				'value' => function($data) {
							
							$str ='';
							foreach($data['contracts'] as $item)
							{			
								$str.=$item['automobiles'][0]['mark'].' '.$item['automobiles'][0]['model']. ' ' .$item['automobiles'][0]['gosnomer'];
							}
							return $str;
							
							
                },
						
			],
			
			
            'pen_summa',
			
			
			[
				'attribute' => 'status_o_pen',
				'value' => 'ourpenaltiesstatus0.status_o_name',
				'filter' => $arrOurpenstatus,
			],
			
           // 'pen_comment:ntext',
			
			
			[
				'attribute' => 'pen_comment',
				
				'contentOptions' => ['style' => 'width:200px; white-space: normal;'],
		
			],
			
			
		
			
			
			

            [
				'class' => 'yii\grid\ActionColumn',
				'template' => '{view} {update}',
				'buttons' => [
				   'update' => function ($url, $model, $key) {
						
						
					//echo $url;	
						
					  return Html::a('', '/admin/ourpenalties/update?id='.$key, ['class' => 'glyphicon glyphicon-pencil']);

				   },
				   
				   
				   
				   
				   
				   'view' => function ($url, $model, $key) {			
					  return Html::a('', '/admin/ourpenalties/view?id='.$key, ['class' => 'glyphicon glyphicon-eye-open']);

				   },
				   
				   
				  
				   
				   
				   
				],
			
			
			],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
	
	
	
	
	
	
<?php 

//Костылик для неоплаченных, потом перекодим более аккуратно
$status_o_pen = 1;



if (!empty($arrOpen)) {
	//Сумма неоплаченных
	$summa = 0;

	//Сумма оплаченных
	$summa_op = 0;
	
	foreach ($arrOpen as $item) {
		if ($item['status_o_pen'] == 1) {
			$summa = $summa + $item['pen_summa'];
		} else {
			
			$summa_op = $summa_op + $item['pen_summa'];
		}
	}
	
	
}



?>	


<h3>Сумма неоплаченных штрафов <?php echo $summa; ?></h3>	

	
</div>
