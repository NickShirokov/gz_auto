<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


use yii\grid\GridView;
use yii\widgets\Pjax;

use kartik\date\DatePicker;

use app\models\Contractsstatus;

/* @var $this yii\web\View */
/* @var $model app\models\Contracts */

$this->title = 'Договор '.$model->id_contract;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Договора'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


\yii\web\YiiAsset::register($this);



?>


<div class="contracts-view">

<div class="buts">

    <h1><?= Html::encode($this->title) ?></h1>

   
	<p>	
	<ul class="nav nav-tabs">
	  <li role="presentation"><?= Html::a(Yii::t('app', 'Все платежи по договору'), ['payments', 'id' => $model->id_contract],  ['class' => 'btn',]) ?>
   </li>
	  <li role="presentation"><?= Html::a(Yii::t('app', 'Штрафы ПДД по договору'), ['pddshtrafs', 'id' => $model->id_contract],  ['class' => 'btn',]) ?>
    	</li>
	  <li role="presentation"><?= Html::a(Yii::t('app', 'Штрафы по договору'), ['shtrafs', 'id' => $model->id_contract],  ['class' => 'btn', ]) ?>
    	
		</li>
	</ul>

	</p>
	
	
	 
    <p>
        <?= Html::a(Yii::t('app', 'Договор'), ['view', 'id' => $model->id_contract], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id_contract], ['class' => 'btn btn-primary']) ?>
        
		
		<?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_contract], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите удалить договор?'),
                'method' => 'post',
            ],
        ]) ?>
		
		
		<?= Html::a(Yii::t('app', 'Завершить'), ['finish', 'id' => $model->id_contract], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите завершить договор?'),
                'method' => 'post',
            ],
        ]) ?>
		
		<?= Html::a(Yii::t('app', 'Удалить'), ['terminate', 'id' => $model->id_contract], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите расторгнуть договор?'),
                'method' => 'post',
            ],
        ]) ?>
		
		
		
		<?= Html::a(Yii::t('app', 'Договор PDF'), ['pdfcontract', 'id' => $model->id_contract],  ['class' => 'btn btn-primary', 'target'=>'_blank',]) ?>
			
	
		<?= Html::a(Yii::t('app', 'Посмотреть карточку водителя'), ['drivers/view', 'id' => $current_driver->id_driver],  ['class' => 'btn btn-primary', ]) ?>
    
		<?= Html::a(Yii::t('app', 'Посмотреть карточку машины'), ['automobiles/view', 'id' => $current_auto->id_auto],  ['class' => 'btn btn-primary', ]) ?>
		
		<?= Html::a(Yii::t('app', 'Новая карточка водитель-машина'), ['card', 'id' => $model->id_contract],  ['class' => 'btn btn-primary', ]) ?>
    
		
    </p>
	

	
	

</div>	
	
	
	<div class="row">
	
	
	
	
	<table class="table table-bordered detail-view">
	
	<tbody>
	
	<tr>
	
		<td>Телефон</td>
		<td><?= $driver->phone_1 ?></td>
		
		<td rowspan="4">Организация</td>
		
		<td rowspan="4">
			
			<?= $licen->name_lic ?> <br> ОГРН/ОГРНИП <?= $licen->ogrn; ?> <br >ИНН <?= $licen->inn; ?> <br> ОКПО  <?= $licen->okpo; ?>	
		
		</td>
		
	</tr>
	
	
	<tr>
	
		<td>Марка</td>
		<td>
		
		<?= $auto->mark.' '.$auto->model ?>
		
		</td>
		
	</tr>
	
	
	<tr>
	
		<td>ГРЗ</td>
		<td><?= $auto->gosnomer; ?></td>
		
	</tr>
	
	<tr>
	
		<td>Водитель</td>
		<td><?= 
		
		$driver->last_name.' '.$driver->first_name.' '.$driver->middle_name;
		
		?></td>
		
	</tr>
	
	<tr>
	
		<td colspan=1>Удостоверение</td>
		<td colspan=3><?= 
		
		$driver->lic_series.' '.$driver->lic_number. ' '.$driver->lic_type
			
		?></td>
		
	</tr>


	<tr>
	
		<td colspan=1>Разрешение</td>
		<td colspan=3>
		
		<?= 
		$auto->razresh_text;
		
		?></td>
		
	</tr>
	
	
	<tr>
	
		<td colspan="3">
	
<p>	
Контроль за осуществлением перевозок пассажиров и багажа легковым такси осуществляет московская администрация дорожного инспекция МАДИ
</p>

<p>
Адрес :129090, г Москва, ул. Каланчевская , д 49  Единый многоканальный телефон 
</p>
<p>
+7495540 76 56,  7 929 960 54 35 
</p>
<p>

Телефон (3522) 42-80-01 (доб. 609)
</p>
<p>
День 07:00 – 19:00 150 руб – 5 мин.
</p>
<p>
За МКАД – 15  руб/км
</p>
<p>
Ночь/выходной /праздники
 190 руб. 5 мин
Далее 12 руб /мин
За МКАД 15/мин
	</p>					
	
		</td>
		
		<td>
		
		
		<img width="185" src="/admin/uploads/<?= $driver->photo; ?>">
		
		</td>
		
		
		
	</tr>
	
	
	</tbody>
	
	
	
	</table>
	
	
	
	
	
	
	
	</div>
	
	
	
	
	
	
	
	
</div>	
	
	