<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


use yii\grid\GridView;
use yii\widgets\Pjax;

use kartik\date\DatePicker;



use kartik\export\ExportMenu;

use app\models\Contractsstatus;

/* @var $this yii\web\View */
/* @var $model app\models\Contracts */

$this->title = 'Договор '.$model->id_contract;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Договора'), 'url' => ['index']];
$this->params['breadcrumbs'][] =   ['label' => Yii::t('app', $this->title), 'url' => ['view?id='.$model->id_contract]];
$this->params['breadcrumbs'][] = 'Платежи по этому договору';
\yii\web\YiiAsset::register($this);
?>



<?php 



$gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],

            [
			    'attribute' => 'id_payment',
				'format' => 'html',
				'value' => function($data) {
					
					
					
								$str = '';
								$str.= '<a href="/admin/payments/view?id='.$data->id_payment.'">'.$data->id_payment.'</a>';
								return $str;
							}
			],
            
			[   
				'attribute' => 'date_payment',
                'value' => 'date_payment',
                'format' => ['date', 'php:d.m.Y'],
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'PaymentsSearchModel[date_payment]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['PaymentsSearchModel']['date_payment'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],
			
	
		
			
			
			[
				'attribute' => 'last_name',
				'format' => 'html',
				'value' => function($data) {
					
					
							
							$str ='';
							foreach($data['contracts'] as $item)
							{
											
								$str.= '<a href="/admin/drivers/view?id='.$item->driver_id.'">'. $item['drivers'][0]['first_name'].' '.$item['drivers'][0]['last_name']. '</a>';
							}
							return $str;
							
							
                },
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
				
				
			],
			
			[
				'attribute' => 'gosnomer',
				'format' => 'html',
				'value' => function($data) {
					
					
							//print_r ($data);
							
							
							$str ='';
							foreach($data['contracts'] as $item)
							{
								
								
								$str.='<a href="/admin/automobiles/view?id='.$item->auto_id.'">'.$item['automobiles'][0]['mark'].' '.$item['automobiles'][0]['model']. ' ' .$item['automobiles'][0]['gosnomer'].'</a>';
							}
							return $str;
							
							
                },
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
				
				
				
			],
			
			[   
				'attribute' => 'type',
                'value' => 'type',
				'filter' => ['Наличные' => 'Наличные', 'На карту' => 'На карту'],
			],
			

			[
				'attribute' => 'summa',
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
			],
	
			[
				'attribute' => 'arenda',
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
			],
			
		
			'pay_dogovor',
			'pay_pdd',
			'pay_ourpen',
            
			/*
			[
				'attribute' => 'pdd',
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
			],
			*/
			
            //'prosrochka',
            //'peredacha_rulya',
            //'p_vyezd_200',
            //'p_atrib',
            //'p_put_list',
            //'p_osago',
            //'p_uterya_doc',
            //'p_uterya_key',
            //'p_diag_card',
            //'vyezd_sotr',
			
			[	
			
				'attribute' => 'comment',
				  'contentOptions' => ['style' => 'width:200px; white-space: normal;'],
			],
			
            //'comment:ntext',
	
            [	
				'class' => 'yii\grid\ActionColumn',
				'template' => '{view} {update}',
				'buttons' => [
				   'update' => function ($url, $model, $key) {
									
					  return Html::a('', '/admin/payments/update?id='.$key, ['class' => 'glyphicon glyphicon-pencil']);

				   },
	   
				   'view' => function ($url, $model, $key) {			
					  return Html::a('', '/admin/payments/view?id='.$key, ['class' => 'glyphicon glyphicon-eye-open']);

				   },
				   
				   
				   'delete' => function ($url, $model, $key) {			
					  return Html::a('', '/admin/payments/delete?id='.$key, ['class' => 'glyphicon glyphicon-trash']);

				   },
				   
				   
				   
				],
			
			
			],
			
			
        ];




?>




<div class="contracts-view">

    <h1><?= Html::encode($this->title) ?></h1>

   
	<p>	
	<ul class="nav nav-tabs">
	  <li role="presentation"><?= Html::a(Yii::t('app', 'Все платежи по договору'), ['payments', 'id' => $model->id_contract],  ['class' => 'btn',]) ?>
   </li>
	  <li role="presentation"><?= Html::a(Yii::t('app', 'Штрафы ПДД по договору'), ['pddshtrafs', 'id' => $model->id_contract],  ['class' => 'btn',]) ?>
    	</li>
	  <li role="presentation"><?= Html::a(Yii::t('app', 'Штрафы по договору'), ['shtrafs', 'id' => $model->id_contract],  ['class' => 'btn', ]) ?>
    	
		</li>
	</ul>

	</p>
	
	
	 
    <p>
        <?= Html::a(Yii::t('app', 'Договор'), ['view', 'id' => $model->id_contract], ['class' => 'btn btn-success']) ?>
      
		
		
		<?= Html::a(Yii::t('app', 'Договор PDF'), ['pdfcontract', 'id' => $model->id_contract],  ['class' => 'btn btn-primary', 'target'=>'_blank',]) ?>
			
	
		<?= Html::a(Yii::t('app', 'Посмотреть карточку водителя'), ['drivers/view', 'id' => $current_driver->id_driver],  ['class' => 'btn btn-primary', ]) ?>
    
		<?= Html::a(Yii::t('app', 'Посмотреть карточку машины'), ['automobiles/view', 'id' => $current_auto->id_auto],  ['class' => 'btn btn-primary', ]) ?>
		
		<?= Html::a(Yii::t('app', 'Новая карточка водитель-машина'), ['card', 'id' => $model->id_contract],  ['class' => 'btn btn-primary', ]) ?>
    
		
    </p>

	
	
	<!--  Пробуем подтащить сюда табличку с датами платежей по конкретному договору--->
	
	
	
	 <?php //Pjax::begin(); ?>
  
  
  
  
	<h1><?= Html::encode('Все платежи по этому договору') ?></h1>
	
	
	
	
    <p>
        <?= Html::a(Yii::t('app', 'Создать платеж'), ['payments/create', 'contract_id' => $model->id_contract], ['class' => 'btn btn-success']) ?>
    </p>

	<?php 
	
	
// Renders a export dropdown menu
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns
]);
	
	?>
	
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		
		
        'columns' => $gridColumns,
    ]); ?>
    <?php //Pjax::end(); ?>
	


	
	


