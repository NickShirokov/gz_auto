<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use yii\grid\CheckboxColumn;


use kartik\date\DatePicker;

use kartik\export\ExportMenu;


use app\models\Logitems;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ContractsSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Договора');
$this->params['breadcrumbs'][] = $this->title;
?>




<div class="contracts-index">

<div class="buts">


    <h1><?= Html::encode($this->title) ?></h1>
    <?php //Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	
	
	
	
	
	
<p>	
	<ul class="nav nav-tabs">
	  <li role="presentation"><?= Html::a(Yii::t('app', 'Все договора'), ['index'], ['class' => 'btn']) ?>
		</li>
	  <li role="presentation"><?= Html::a(Yii::t('app', 'Договора на утверждении'), ['contractsprepared', 'id' => 2], ['class' => 'btn']) ?>
		</li>
	  <li role="presentation"><?= Html::a(Yii::t('app', 'Архив договоров'), ['contractsarchive', 'id' => 3], ['class' => 'btn']) ?>
		</li>
	</ul>

	</p>
	
    <p>
        <?// Html::a(Yii::t('app', 'Создать договор'), ['create'], ['class' => 'btn btn-success']) ?>
		
		 <?= Html::a(Yii::t('app', 'Создать договор'), ['newcreate'], ['class' => 'btn btn-danger']) ?>
		
		
       
		
		<?php
			if(
				Yii::$app->user->can('admin') 
				)  
			{
			 echo	Html::a(Yii::t('app', 'Загрузить договора'), ['import'], ['class' => 'btn btn-success']). ' '.
				Html::a(Yii::t('app', 'Обновить договора'), ['contractsupdate'], ['class' => 'btn btn-primary']).
				' <input type="button" class="btn btn-info" value="Удалить выбранные" id="MyButton" >
		
				<input type="button" class="btn btn-info" value="Очистить всех" id="remove-all" >';
			}
			
				
		?>
		
		
		
    </p>

	

	
<?php echo \nterms\pagesize\PageSize::widget(); ?>
	
	
	
	
<?php 

$gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],
			['class' => 'yii\grid\CheckboxColumn'],
			
			
			[
				'attribute' => 'id_contract',
				'format' => 'html',
				'value' => function ($data) {
					
					
								//print_r ($data);
					
								$str = '';
								$str.= '<a  href="view?id='.$data['id_contract'].'">Договор '.$data['id_contract'].'</a>';
					
								
								return $str;
							},
            ],
			
			//'id_owner',
			
			
			[
				'attribute' => 'owner_id',
				'format' => 'html',
				'value' => function($data) {
					
							$str ='';
							foreach($data['owners'] as $item)
							{
								$str.=$item['first_name'].' '.$item['last_name']. '';
							}
							return $str;
							
							
                },
				
				
				'filter' => $arrOwners,
			],
			
			
			
            //'driver_id',
			
			
		
			
			[
				'attribute' => 'fio',
				'format' => 'html',
				'value' => function ($data) { 
				
							$str ='';
							foreach($data['drivers'] as $item)
							{
								$str.= '<a href="/admin/drivers/update?id='.$item['id_driver'].'">'.$item['fio'].'</a>';
							}
							return $str;
				
				
				}
			
			
			],
			
			
            //'auto_id',
			
			
			[
				'attribute' => 'gosnomer',
				'format' => 'html',
				'value' => function($data) {
					
							//
					
					
							$str ='';
							foreach($data['automobiles'] as $item)
							{
								$str.='<a href="/admin/automobiles/update?id='.$item['id_auto'].'">'.$item['mark'].' '.$item['model']. ' '.$item['gosnomer'].'</a>';
							}
							return $str;
							
							
                },
				
			],
			
			
			/*
			[
				'attribute' => 'VIN',

				'value' => function($data) {
					
							$str ='';
							foreach($data['automobiles'] as $item)
							{
								$str.=$item['VIN'];
							}
							return $str;
							
							
                },
				
			],*/
			
			[
				'attribute' => 'status_itog',
				'value' => 'contractsstatus0.c_status_name',
				'filter' => $arrCStatuses
			],
			
			[
				'attribute' => 'pdf_link',
				'format' => 'html',
				'value' => function ($data){
					
								
								$str  = '';
								
								$str.= '<a style="background: none;" target="_blank" " href="/admin/contracts/pdfcontract?id='.$data['id_contract'].'"><img style="width: 30px;" src="/admin/uploads/pdf.png"></a>';
								
								return $str;
				}
			],
			
			
			[
				'attribute' => 'pdf_card',
				'format' => 'html',
				'value' => function ($data){
					
								
								$str  = '';
								
								$str.= '<a style="background: none;" target="_blank" " href="/admin/contracts/pdfcard?id='.$data['id_contract'].'"><img style="width: 30px;" src="/admin/uploads/pdf.png"></a>';
								
								return $str;
				}
			],
			
			
			
           

            [   
				'attribute' => 'date_start',
                'value' => 'date_start',
                'format' => ['date', 'php:d.m.Y'],
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'ContractsSearchModel[date_start]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['ContractsSearchModel']['date_start'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],
            
            
            [   
				'attribute' => 'date_end',
                'value' => 'date_end',
                'format' => ['date', 'php:d.m.Y'],
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'ContractsSearchModel[date_end]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['ContractsSearchModel']['date_end'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],




            [
				'class' => 'yii\grid\ActionColumn',
				'visibleButtons' => [
									'update' => function ($model) {
										
										$id = $model->id_contract;
										
										if (\Yii::$app->user->can('admin', ['post' => $model])) {
											
											return true;
										} else {
											
											$model_logitems = Logitems::find()
																->where(
																	[
																		
																		'item_id' => $id,
																		'type_page' => 'contract',
																		'user_id' => Yii::$app->user->getId(),
																	])
																	->andWhere(['>=','date_create', date('Y-m-d', strtotime('-5 days'))])
																	->all();
											
											
											if (!empty($model_logitems)) {
												
												return true;
											} else {
												return false;
											}
											
										}
									
									
									},
									
									
									
									'delete' => function ($model) {
										$id = $model->id_contract;
										
										if (\Yii::$app->user->can('admin', ['post' => $model])) {
											
											return true;
										} else {
											
											$model_logitems = Logitems::find()
																->where(
																	[
																		
																		'item_id' => $id,
																		'type_page' => 'contract',
																		'user_id' => Yii::$app->user->getId(),
																	])
																	->andWhere(['>=','date_create', date('Y-m-d', strtotime('-5 days'))])
																	->all();
											
											
											if (!empty($model_logitems)) {
												
												return true;
											} else {
												return false;
											}
											
										}
									
									},
								],
			],
        ];

		// Renders a export dropdown menu
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns
]);


?>	
	
	
	
	
	</div>
	
	
	
	
	
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		
		
		
        'columns' => $gridColumns,
		
		
		'pager' => [
			'class' => \kop\y2sp\ScrollPager::className(),
			'container' => '.grid-view tbody',
			'item' => 'tr',
			'paginationSelector' => '.grid-view .pagination',
			'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',
		 ],
		 
		 
		 
		 'filterSelector' => 'select[name="per-page"]',
		
		
    ]); ?>
    <?php //Pjax::end(); ?>
</div>





<?php

$this->registerJs('

$(document).ready(function(){
	$(\'#MyButton\').click(function(){

	var HotId = $(\'.grid-view\').yiiGridView(\'getSelectedRows\');
	
	
	$.ajax({
		type: \'POST\',
		url : \'/admin/contracts/multipledelete\',
		data : {row_id: HotId},
		success : function() {
			
			$(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
		
		}
	});

	});
	
	
	
	
	$(\'#remove-all\').click(function(){
	
		$.ajax({
			type: \'POST\',
			url : \'/admin/contracts/removeall\',
			//data : {row_id: HotId},
			success : function() {
				
				//$(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
			
			}
		});
	
	
	});
	
	
});', \yii\web\View::POS_READY);

?>
