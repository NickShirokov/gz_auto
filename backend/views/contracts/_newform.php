<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;



use vova07\imperavi\Widget;

use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\Contracts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contracts-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<h2>Новая Машина</h2>
	
	
	<?= $form->field($model, 'newcreateauto')->checkbox([
		'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
	])?>
	
	
	<div class="auto-model-box">
	
		<div class="row">
	
	
	
    <div class="col-md-12">

    
	
	
	
<?php 	
		echo $form->field($modelAuto, 'owner_id')->widget(Select2::classname(), [
									'data' => $arrOwners,
									'language' => 'ru',
									'options' => ['placeholder' => 'Select a state ...'],
									'pluginOptions' => [
										'allowClear' => true,
										'initialize' => true,
									],
									
									'pluginEvents' => [
									   "select2:select" => 'function() { 
									   
									   var id = $(this).val();
								
										$.post(
											"/admin/contracts/owner?id="+id, 
											function(data){
															
												$("#owner-wrap").html(data);
											}
										
										);
										
										
										
										$.post(
											"/admin/contracts/autos?id="+id, 
											function(data){
													
												
													
												$("select#contracts-auto_id").html(data);
											}
										
										);
											   
									   
									   }',
									]
									
									
								]);
	
	
	?>
	
	
	
	
	
		<div id="owner-wrap" style="padding: 10px; border: 1px solid #eee">
			<?= $res_owner;?>
		</div>

	
		
	
	<?= $form->field($modelAuto, 'licen_id')->dropDownList($arrLicen,
	
				[
					'prompt' => '',
					'onchange' => '
					
							var lic_id = $(this).val();
					
							$.post(
								"/admin/automobiles/razresh?id="+lic_id, 
								function(data){
										
									$("select#automobilesnewmodel-razresh_id").html(data);		
								}
							
							);
							
					
					
					',
				]
														
	); ?>
	<?= $form->field($modelAuto, 'razresh_text')->textarea(['rows' => 6, ['required' => false]])?>

	</div>
	
	
 
	<div class="col-md-6">
 
 
 
 
 
 
 
 
 
 
	<?= $form->field($modelAuto, 'mark')->dropdownList($arrMarks,
						[
							'prompt' => 'выбрать',
							'onchange' => '
								
								var mark = $(this).val();
								
								$.post(
									"/admin/automobiles/lists?mark="+mark, 
									function(data){
										
										
			
										
										$("select#automobilesnewmodel-model").html(data);
									}
								
								);
							',
				
						]
						
				) ?>
	
	</div>
	
	
	<div class="col-md-6">
	
   <?= $form->field($modelAuto, 'model')->dropdownList($arrModels, 
				[
					'prompt' => 'выбрать'
					
					
				]
	
			) ?>

	</div>
	
	
	
	
	
   
   
   <div class="col-md-4">

    <?= $form->field($modelAuto, 'VIN')->textInput(['maxlength' => true]) ?>
	
	</div>
	
	  <div class="col-md-4">
	
	 <?= $form->field($modelAuto, 'PTS')->textInput(['maxlength' => true]) ?>
	</div>
	
<div class="col-md-4">
    <?= $form->field($modelAuto, 'sts')->textInput(['maxlength' => true]) ?>

	</div>
	
	<div class="col-md-4">
    <?= $form->field($modelAuto, 'gosnomer')->textInput(['maxlength' => true]) ?>
</div>
   

	<div class="col-md-4">
	
	<?= $form->field($modelAuto, 'year')->input('number', ['required' => false, 'min' => 1950, 'max' => 2099])  ?>
	
</div>

<div class="col-md-4">
    <?= $form->field($modelAuto, 'color')->textInput(['maxlength' => true]) ?>

</div>	



<div class="col-md-4">
    <?= $form->field($modelAuto, 'transmission')->dropDownList([ 'Ручная' => 'Ручная', 'Автомат' => 'Автомат', '' => '', ], ['prompt' => '']) ?>
</div>


<div class="col-md-4">
<?= $form->field($modelAuto, 'odometr_km')->textInput(['maxlength' => true]) ?>

</div>


<div class="col-md-4">
   <?= $form->field($modelAuto, 'status')->dropdownList($arrStatus, 
				[
					'prompt' => 'выбрать'				
				]
	
			) ?>
   
   
</div>

	
	
	
	
<div class="col-md-4">	
	<?= $form->field($modelAuto, 'osago')->input('date', ['required' => false])  ?>
</div>

<div class="col-md-4">	
	<?= $form->field($modelAuto, 'ins_company')->dropdownList($arrIns, 
				[
					'prompt' => 'выбрать'				
				]
	
			) ?>
</div>
	

<div class="col-md-4">
	<?= $form->field($modelAuto, 'diagnostic_card')->input('date', ['required' => false])  ?>
</div>

</div>



	
<div class="col-md-12">
<div class="row">
	<h3>Ограничения</h3>

	<?= $form->field($modelAuto, 'ogranichenia')->checkbox([
		'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
	])?>
		
	</div>	
	<br><br>
</div>	
	
	<div class="">
		<h3>Мониторинг</h3>
	</div>
	
	<div class="row">
	
	<div class="col-md-4">
	<?= $form->field($modelAuto, 'vialon')->checkbox([
		'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
	])?>
		
	</div>	
		
	<div class="col-md-4">

		
	<?= $form->field($modelAuto, 'vialon_status')->dropdownList(['РАБОТАЕТ' => 'РАБОТАЕТ', 'ГЛЮЧИТ' => 'ГЛЮЧИТ', 'НЕ РАБОТАЕТ' => 'НЕ РАБОТАЕТ'], 
				[
					'prompt' => ''				
				]
	
			) ?>	
		
		

	</div>


	<div class="col-md-4">

		 <?= $form->field($modelAuto, 'vialon_comment')->textarea(['rows' => 6, ['required' => false]]) ?>
	

	</div>
	
	
	
	
	<div class="col-md-4">
	
	<?= $form->field($modelAuto, 'avtofon')->checkbox([
		'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
	])?>
	
	</div>
	
	<div class="col-md-4">

		
	<?= $form->field($modelAuto, 'avtofon_status')->dropdownList(['РАБОТАЕТ' => 'РАБОТАЕТ', 'ГЛЮЧИТ' => 'ГЛЮЧИТ', 'НЕ РАБОТАЕТ' => 'НЕ РАБОТАЕТ'], 
				[
					'prompt' => ''				
				]
	
			) ?>	
		
		

	</div>


	<div class="col-md-4">

		 <?= $form->field($modelAuto, 'avtofon_comment')->textarea(['rows' => 6, ['required' => false]]) ?>
	

	</div>
	
	
	
	</div>
	
	
    <?= $form->field($modelAuto, 'comment')->textarea(['rows' => 6, ['required' => false]]) ?>
    
	
	<?= $form->field($modelAuto, 'galka_vserv')->checkbox([
		'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
	])?>
	
	<?= $form->field($modelAuto, 'galka_fssp')->checkbox([
		'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
	])?>
	
	
	
	</div>
	
	
	
	
	
	
	
	<h2>Новый водитель</h2>
	
	
	<?= $form->field($model, 'newcreatedriver')->checkbox([
		'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
	])?>
	
	
	
	<div class="driver-model-box">

	
	
	
	<div class="row">
	
	<div class="col-md-4">
	 <?= $form->field($modelDriver, 'last_name')->textInput(['maxlength' => true]) ?>
	</div>

	
	<div class="col-md-4">
	
    <?= $form->field($modelDriver, 'first_name')->textInput(['maxlength' => true]) ?>
</div>
	
	<div class="col-md-4">
	
    <?= $form->field($modelDriver, 'middle_name')->textInput(['maxlength' => true]) ?>
</div>
	
	
	
		
		<div class="col-md-12"><h2>Паспортные данные</h2></div>
	
		
	
		<div class="col-md-4">
	
		 <?= $form->field($modelDriver, 'date_birth')->input('date') ?>
		</div>
	
		<div class="col-md-4">
			<?= $form->field($modelDriver, 'p_series')->textInput(['maxlength' => true]) ?>
		</div>
		
		<div class="col-md-4">
		
			<?= $form->field($modelDriver, 'p_number')->textInput(['maxlength' => true]) ?>
		</div>
			
		<div class="col-md-12">
		
			<?= $form->field($modelDriver, 'p_who')->textArea(['rows' => 6])  ?>
		</div>
		
		
		
		<div class="col-md-12">
		
			<?= $form->field($modelDriver, 'address_reg')->textArea(['rows' => 6])  ?>
		</div>
		
		
		<div class="col-md-12">
		
			<?= $form->field($modelDriver, 'address_fact')->textArea(['rows' => 6])  ?>
		</div>
		
		<div class="col-md-6">
		<?= $form->field($modelDriver, 'phone_1')->textInput(['maxlength' => true]) ?>
		</div>
		
		<div class="col-md-6">
		<?= $form->field($modelDriver, 'phone_2')->textInput(['maxlength' => true]) ?>
		</div>
	
		
		
		
		
		
		<div class="col-md-6">
		<?= $form->field($modelDriver, 'lic_series')->textInput(['maxlength' => true]) ?>
		</div>
		
		<div class="col-md-6">
		<?= $form->field($modelDriver, 'lic_number')->textInput(['maxlength' => true]) ?>
		</div>
	
	
		<div class="col-md-6">
		<?= $form->field($modelDriver, 'lic_type')->textInput(['maxlength' => true]) ?>
		</div>
		
		<div class="col-md-6">
		<?= $form->field($modelDriver, 'lic_dates')->textInput(['maxlength' => true]) ?>
		</div>
	
		
	
		
		<div class="col-md-4">
			<?= $form->field($modelDriver, 'driver_cat')->dropdownList($arrCat, ['prompt' => '']) ?>
	
	
		</div>
   
		<div class="col-md-4">
			<?= $form->field($modelDriver, 'status')->dropdownList($arrStatusDr, ['prompt' => '']) ?>
	
		</div>
		<div class="col-md-4">
			<?= $form->field($modelDriver, 'penalty_type')->dropdownList($arrPenaltytype, []) ?>
	
		</div>
	
	
	
	</div>
	


	
	<?= $form->field($modelDriver, 'comment')->textArea(['rows' => 6]) ?>
	
	
	<?= $form->field($modelDriver, 'photo_f')->fileInput() ?>
	
	
	
	
	
	
	
	</div>
	
	<h2>Данные договора</h2>
	

	
		<?php 	
		echo $form->field($model, 'auto_id')->widget(Select2::classname(), [
									'data' => $arrAutomobiles,
									'language' => 'ru',
									'options' => ['placeholder' => 'Select a state ...', 'required' => 'required'],
									'pluginOptions' => [
										'allowClear' => true,
										 'initialize' => true,
									],
									
									'pluginEvents' => [
									   "select2:select" => 'function() { 
									   
											var id = $(this).val();
								
											$.post(
												"/admin/contracts/auto?id="+id, 
												function(data){
													
													$("#auto-wrap").html(data);
												}
											
											);
											
											$.post(
												"/admin/contracts/ownernew?id="+id, 
												function(data){
																
													//$("#owner-wrap").html(data);
													
													$("select#contracts-owner_id").html(data);
												}
											
											);
									   
									   
											   
									   
									   }',
									]
									
									
								]);
	
	
	?>
	
	<div id="auto-wrap" style="padding: 10px; border: 1px solid #eee">
	
		<?= $res_auto;?>
	
	</div>
	


	<?php 	
		echo $form->field($model, 'owner_id')->widget(Select2::classname(), [
									'data' => $arrOwners,
									'language' => 'ru',
									'options' => ['placeholder' => 'Select a state ...'],
									'pluginOptions' => [
										'allowClear' => true,
										 'initialize' => true,
									],
									
									'pluginEvents' => [
									   "select2:select" => 'function() { 
									   
									   
									   
									   var id = $(this).val();
								
										$.post(
											"/admin/contracts/owner?id="+id, 
											function(data){
															
												$("#owner-wrap").html(data);
											}
										
										);
										
										
										
										
										
										
										
										$.post(
											"/admin/contracts/autos?id="+id, 
											function(data){
													
												
													
												$("select#contracts-auto_id").html(data);
											}
										
										);
											   
									   
									   }',
									]
									
									
								]);
	
	
	?>
	
	
	
	
	
		<div id="owner-wrap" style="padding: 10px; border: 1px solid #eee">
			<?= $res_owner;?>
		</div>
	
	
	
	
	
	
	
	
	
	
	
	
	<br>
	
	
	<?php 	
		echo $form->field($model, 'driver_id')->widget(Select2::classname(), [
									'data' => $arrDrivers,
									'language' => 'ru',
									'options' => ['placeholder' => 'Select a state ...'],
									'pluginOptions' => [
										'allowClear' => true,
										 'initialize' => true,
									],
									
									'pluginEvents' => [
									   "select2:select" => 'function() { 
									   
											var id = $(this).val();
								
											$.post(
												"/admin/contracts/driver?id="+id, 
												function(data){
													
													
													
													$("#driver-wrap").html(data);
												}
											
											);
									   
									   
											   
									   
									   }',
									]
									
									
								]);
	
	
	?>
	
	
		
		
		
		
		
		
	
	<div id="driver-wrap" style="padding: 10px; border: 1px solid #eee">
	
		<?= $res_driver;?>
	
	</div>
	
	<br>
  


	
	
	<?php 
	if ($model->date_start): 
	?>
	
			<?= $form->field($model, 'date_start')->input('date', ['required' => false]) ?>

	
	<?php else: ?>
	
	
		<?= $form->field($model, 'date_start')->input('date', ['required' => false, 'value' => date('Y-m-d')]) ?>

	
	<?php endif; ?>	
	
	

	
    
	
	<?= $form->field($model, 'p_vznos')->textInput() ?>
	
	<?= $form->field($model, 'arenda_type')->dropDownList( $arrArendatypes, ['prompt' => '']) ?>
	
	<?= $form->field($model, 'arenda_stoim')->textInput() ?>
	
	<?= $form->field($model, 'arenda_items')->textInput() ?>
	
	<?= $form->field($model, 'vykup')->textInput() ?>
	
	
	<?= $form->field($model, 'penalty_comission')->checkbox([
		'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
	])?>
		
	
	<?= $form->field($model, 'discount_type')->checkbox([
		'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
	])?>
	
	
	<h3>Штрафы</h3>
	
	<?= $form->field($model, 'prosrochka')->textInput() ?>
	
	<?= $form->field($model, 'peredacha_drugomu')->textInput() ?>
	
	<?= $form->field($model, 'vyezd_bez_sogl_200')->textInput() ?>
	
	<?= $form->field($model, 'shtraf_pdd')->textInput() ?>
	
	<?= $form->field($model, 'snyat_atrib')->textInput() ?>
	
	
	<?= $form->field($model, 'no_put_list')->textInput() ?>
	
	
	<?= $form->field($model, 'no_osago')->textInput() ?>
	
	
	<?= $form->field($model, 'poterya_docs')->textInput() ?>
	
	
	<?= $form->field($model, 'poterya_keys')->textInput() ?>
	
	<?= $form->field($model, 'no_prodl_diag')->textInput() ?>


	
	<?= $form->field($model, 'vyezd_sotrudnikov')->textInput() ?>
	
	
	<p>Дата окончания аренды указывается первоначальная, в самом договоре реальная дата прибавляет
	количество дней простоя
	</p>
	
	<?= $form->field($model, 'date_pay_arenda')->input('date', ['required' => false]) ?>
	

	
	 <?= $form->field($model, 'comment')->textarea(['rows' => 6, ['required' => false]]) ?>
	 
	 
	 
	  
	  
	  
	  <?= 
	  
	  
	  
	   $form->field($model, 'who_affilate')->widget(Widget::className(), [
			'settings' => [
				'lang' => 'ru',
				'minHeight' => 200,
				'plugins' => [
					'clips',
					'fullscreen',
				],
				'clips' => [
					['Lorem ipsum...', 'Lorem...'],
					['red', '<span class="label-red">red</span>'],
					['green', '<span class="label-green">green</span>'],
					['blue', '<span class="label-blue">blue</span>'],
				],
			],
		]);
	  
	  
	  
	  ?>
	  
	  
	  
	
	<h2>Блоки для редактирования текста PDF договора</h2>

	
	  
	    
	  <?= 
	  
	  
	  
	   $form->field($model, 'text1')->widget(Widget::className(), [
			'settings' => [
				'lang' => 'ru',
				'minHeight' => 200,
				'plugins' => [
					'clips',
					'fullscreen',
				],
				'clips' => [
					['Lorem ipsum...', 'Lorem...'],
					['red', '<span class="label-red">red</span>'],
					['green', '<span class="label-green">green</span>'],
					['blue', '<span class="label-blue">blue</span>'],
				],
			],
		]);
	  
	  
	  
	  ?>
	  
	  
	    
	  <?= 
	  
	  
	  
	   $form->field($model, 'text2')->widget(Widget::className(), [
			'settings' => [
				'lang' => 'ru',
				'minHeight' => 200,
				'plugins' => [
					'clips',
					'fullscreen',
				],
				'clips' => [
					['Lorem ipsum...', 'Lorem...'],
					['red', '<span class="label-red">red</span>'],
					['green', '<span class="label-green">green</span>'],
					['blue', '<span class="label-blue">blue</span>'],
				],
			],
		]);
	  
	  
	  
	  ?>
	  
	  
	    
	  <?= 
	  
	  
	  
	   $form->field($model, 'text3')->widget(Widget::className(), [
			'settings' => [
				'lang' => 'ru',
				'minHeight' => 200,
				'plugins' => [
					'clips',
					'fullscreen',
				],
				'clips' => [
					['Lorem ipsum...', 'Lorem...'],
					['red', '<span class="label-red">red</span>'],
					['green', '<span class="label-green">green</span>'],
					['blue', '<span class="label-blue">blue</span>'],
				],
			],
		]);
	  
	  
	  
	  ?>
	  
	  
	    
	  <?= 
	  
	  
	  
	   $form->field($model, 'text4')->widget(Widget::className(), [
			'settings' => [
				'lang' => 'ru',
				'minHeight' => 200,
				'plugins' => [
					'clips',
					'fullscreen',
				],
				'clips' => [
					['Lorem ipsum...', 'Lorem...'],
					['red', '<span class="label-red">red</span>'],
					['green', '<span class="label-green">green</span>'],
					['blue', '<span class="label-blue">blue</span>'],
				],
			],
		]);
	  
	  
	  
	  ?>
	  
	  
	  
	    
	  <?= 
	  
	  
	  
	   $form->field($model, 'text5')->widget(Widget::className(), [
			'settings' => [
				'lang' => 'ru',
				'minHeight' => 200,
				'plugins' => [
					'clips',
					'fullscreen',
				],
				'clips' => [
					['Lorem ipsum...', 'Lorem...'],
					['red', '<span class="label-red">red</span>'],
					['green', '<span class="label-green">green</span>'],
					['blue', '<span class="label-blue">blue</span>'],
				],
			],
		]);
	  
	  
	  
	  ?>
	  
	  
	  
	  
	  
	  
	  
	   <?= $form->field($model, 'bonus_summ')->textarea(['rows' => 6, ['required' => false]]) ?>
	   
        <?= $form->field($model, 'date_end')->input( 'date' ) ?>



	   <?= $form->field($model, 'status_itog')->dropdownList( $arrCStatuses ) ?>
	   
	
	
		<?= $form->field($model, 'putevoy_type')->checkbox([
			'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
		])?>
	
	
	
	
	
	
	
	
	
	
	
	
	
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>



<?

$this->registerJs(
'
$(document).ready(function(){
	


	$("#contracts-newcreateauto").click(function(){
		if($(this). prop("checked") == true){
			$(".auto-model-box").show();
			$(".field-contracts-owner_id").hide();
			$(".field-contracts-auto_id").hide();
			
			
			
			
			
		}
		else if($(this). prop("checked") == false){
			$(".field-contracts-owner_id").show();
			$(".auto-model-box").hide();
			$(".field-contracts-auto_id").show();
			$("form").reset();
		}
	
	
	});
	
	
	
	$("#contracts-newcreatedriver").click(function(){
		if($(this). prop("checked") == true){
			$(".driver-model-box").show();
			$(".field-contracts-driver_id").hide();
		}
		else if($(this). prop("checked") == false){
			$(".driver-model-box").hide();
			$(".field-contracts-driver_id").show();
			
			$("form").reset();
		}
	
	
	});
	
	
	
	

});
');
?>


