<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


use yii\grid\GridView;
use yii\widgets\Pjax;

use kartik\date\DatePicker;

use app\models\Contractsstatus;

/* @var $this yii\web\View */
/* @var $model app\models\Contracts */

$this->title = 'Договор '.$model->id_contract;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Договора'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="contracts-view">

    <h1><?= Html::encode($this->title) ?></h1>

	
	
	
	<p>	
	<ul class="nav nav-tabs">
	  <li role="presentation"><?= Html::a(Yii::t('app', 'Все платежи по договору'), ['payments', 'id' => $model->id_contract],  ['class' => 'btn',]) ?>
   </li>
	  <li role="presentation"><?= Html::a(Yii::t('app', 'Штрафы ПДД по договору'), ['pddshtrafs', 'id' => $model->id_contract],  ['class' => 'btn',]) ?>
    	</li>
	  <li role="presentation"><?= Html::a(Yii::t('app', 'Штрафы по договору'), ['shtrafs', 'id' => $model->id_contract],  ['class' => 'btn', ]) ?>
    	
		</li>
	</ul>

	</p>
	
	
	 
    <p>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id_contract], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_contract], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите удалить договор?'),
                'method' => 'post',
            ],
        ]) ?>
		
		
		
		<?= Html::a(Yii::t('app', 'Завершить'), ['finish', 'id' => $model->id_contract], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите завершить договор?'),
                'method' => 'post',
            ],
        ]) ?>
		
		<?= Html::a(Yii::t('app', 'Расторгнуть'), ['terminate', 'id' => $model->id_contract], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите расторгнуть договор?'),
                'method' => 'post',
            ],
        ]) ?>
		
		
		
		
		<?= Html::a(Yii::t('app', 'Пересадить'), ['peresadka', 'id' => $model->id_contract], [
            'class' => 'btn btn-success',
            
        ]) ?>
		
		<?= Html::a(Yii::t('app', 'Продлить'), ['prodlenie', 'id' => $model->id_contract], [
            'class' => 'btn btn-success',
            
        ]) ?>
		
		
		
		<br><br>
			
		<?= Html::a(Yii::t('app', 'Договор PDF'), ['pdfcontract', 'id' => $model->id_contract],  ['class' => 'btn btn-primary', 'target'=>'_blank',]) ?>
		<?= Html::a(Yii::t('app', 'Карточка PDF'), ['pdfcard', 'id' => $model->id_contract],  ['class' => 'btn btn-success', 'target'=>'_blank',]) ?>
		<?= Html::a(Yii::t('app', 'Путевые листы'), ['putevielisti/week', 'id' => $model->id_contract],  ['class' => 'btn btn-success', 'target'=>'_blank',]) ?>
		
		<?= Html::a(Yii::t('app', 'Посмотреть карточку водителя'), ['drivers/view', 'id' => $current_driver->id_driver],  ['class' => 'btn btn-primary', ]) ?>
    
		<?= Html::a(Yii::t('app', 'Посмотреть карточку машины'), ['automobiles/view', 'id' => $current_auto->id_auto],  ['class' => 'btn btn-primary', ]) ?>
    	
		<? Html::a(Yii::t('app', 'Новая карточка водитель-машина'), ['card', 'id' => $model->id_contract],  ['class' => 'btn btn-primary', ]) ?>
    
		
    </p>
	

	
	
	
	
	

	

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_contract',
            //'owner_id',
			
			[
				'attribute' => 'owner_id',
				'value' => $current_owner->first_name.' '
							.$current_owner->middle_name.' '
							.$current_owner->last_name.' <br>'
							.'<b>Паспорт:</b> '.$current_owner->p_series.' '.$current_owner->p_number.'<br>'
							.'<b>Выдан: </b>'.$current_owner->p_who.'<br>'
							.'<b>Адрес регистрации: </b>'.$current_owner->address_reg.'<br>'	
							.'<b>Телефон 1: </b>'.$current_owner->phone_1.'<br>'	
							.'<b>Телефон 2: </b>'.$current_owner->phone_2.'<br>'	
							.'<b>Телефон 3: </b>'.$current_owner->phone_3.'<br>'								
							
						   					   ,
				'format' => 'html',							   
			],
			
            //'driver_id',
            
			[
				'attribute' => 'driver_id',
				'value' => $current_driver->first_name.' '
							.$current_driver->middle_name.' '
							.$current_driver->last_name.' <br>'
							.'<b>Дата рождения: </b>'.$current_driver->date_birth.'<br>'
							.'<b>Паспорт:</b> '.$current_driver->p_series.' '.$current_driver->p_number.'<br>'
							.'<b>Выдан: </b>'.$current_driver->p_who.'<br>'
							.'<b>Адрес регистрации: </b>'.$current_driver->address_reg.'<br>'	
							.'<b>Адрес фактического проживания: </b>'.$current_driver->address_fact.'<br>'	
							
							.'<b>Водительское удостоверение:</b> '.$current_driver->lic_series.' '.$current_driver->lic_number.'<br>'
							.'<b>Категория: </b>'.$current_driver->lic_type.' '.$current_driver->lic_dates.'<br>'
							
							
							.'<b>Телефон 1: </b>'.$current_driver->phone_1.'<br>'	
							.'<b>Телефон 2: </b>'.$current_driver->phone_2.'<br>'	
															
							
						   					   ,
				'format' => 'html',							   
			],
			
			
			
		
			
			[
				'attribute' => 'auto_id',
				'value' => '<b>Марка Модель: </b>'.$current_auto->mark.' '
							.$current_auto->model.' <br>'
							
							.'<b>Идентификационный номер VIN: </b>'.$current_auto->VIN.'<br>'
							.'<b>Гос.номер:</b> '.$current_auto->gosnomer.' <br>'
							.'<b>Год выпуска: </b>'.$current_auto->year.'<br>'
											   ,
				'format' => 'html',							   
			],
			
			[
				'attribute'=>'date_start',
				'format' => ['date', 'php:d.m.Y'],
			],
			'p_vznos', 
			
			
			[
				'attribute' => 'arenda_type',
				'value' => $arType,
			], 
			
			[
				'attribute' => 'arenda_stoim',
			],
			
			
			'arenda_items',
			
			'vykup',
			
			'penalty_comission', 
			
			'discount_type',
			
			
			[	
				'attribute' => 'date_pay_arenda',
				'format' => ['date', 'php:d.m.Y'],
				
				
				
				
			],
			
			
			'date_pay_arenda_new',
			
			
			
			
			
			'vyezd_sotrudnikov',
				
			
			
			'prosrochka',	
			'peredacha_drugomu', 
			'vyezd_bez_sogl_200', 
			
			[
				'attribute' =>'shtraf_pdd',
				'value' => function ($data) {
							return $data->shtraf_pdd. '  x размер штрафа';
								
								
					}	
			],
			
			'snyat_atrib',		
			'no_put_list',	
			'no_osago',	
			'poterya_docs',	
			'poterya_keys',	
			'no_prodl_diag',
			
			'comment',
			
			[
				'attribute' => 'who_affilate',
				'format' => 'html',
			
			],
			'bonus_summ',
			
			
			[
				'attribute'=>'date_end',
				'format' => ['date', 'php:d.m.Y'],
			],
			
			
			
			
			[
				'attribute' => 'status_itog',
				'value' => Contractsstatus::find()->where(['c_status_id' => $model->status_itog])->all()[0]->c_status_name,
			]
			
			
			
			
			
			
            //'date_create',
        ],
    ]) ?>
	
	
	
	
<?php	
	
$model->date_pay_arenda = date('d.m.Y',strtotime($model->date_pay_arenda. ' - 1 days'));	
	
	
$full_summa = 0;

//Стоимость аренды в день
$arenda_item_summ = $model->arenda_stoim;
//Количество дней, оплаченных по аренде

$arenda_summa = 0;

$dog_summa = 0;

if (!empty($arrAllPays)) {
	
	//global $prostoy_days;
	
	$summa_week = 0;
	$summa_month = 0;
	
	$prostoy_main = 0;
	
	//$prostoy_days = 0;
	
	$prostoy_sss = 0;
	
	//print_r ($arrAllPays);
	
	//exit;
	$our_pen_summa =  0;
	
	$prostoys_opl_summa = 0;
	
	
	
	$pdd_opl_summa = 0; 
	
	
	foreach ($arrAllPays as $item) {
				
		$full_summa = $full_summa + $item['summa'];
		$arenda_summa = $arenda_summa + $item['arenda'];
		$dog_summa = $dog_summa + $item['pay_dogovor'];
		
		
		$our_pen_summa = $our_pen_summa + $item['pay_ourpen'];
		
		$prostoys_opl_summa = $prostoys_opl_summa + $item['prostoy_summa'];
		
		
		$pdd_opl_summa = $pdd_opl_summa + $item['pay_pdd'];
		
		
		//Пробуем просуммировать за неделю и за месяц
		$date_raznost = date_diff(new DateTime($item['date_payment']), new DateTime())->days;
		
		if ($date_raznost < 7) {
			
			$summa_week = $summa_week + $item['arenda'];
		}
		
		if ($date_raznost < 30) {
			
			$summa_month = $summa_month + $item['arenda'];
		}
		
		//$item['date_payment'];
		
		
		
		
		
		
		
	}
	
}




//Полная сумма штрафов 
$shtrafs_summa = 0;

//Сумма оплаченных
$shtrafs_summa_neop = 0;

//


//print_r ($arrAllShtrafs);



if (!empty($arrAllShtrafs)) {
	
	foreach ($arrAllShtrafs as $item) {
				
				
		if ($item['status_o_pen'] == 1 or $item['status_o_pen'] == 4) {		
			$shtrafs_summa = $shtrafs_summa + $item['pen_summa'];
		}	
	}
	
}





//Количество оплаченных дней


if (!$arenda_item_summ) {
	$days_oplach = 0;
	
} else {
	$days_oplach = (int) ($arenda_summa / $arenda_item_summ);
}



$date_oplach_do =  date('d.m.Y', strtotime($model->date_pay_arenda. ' + '.$days_oplach.' days'));



$days_oplach_and_prostoys = $days_oplach + $prostoy_days;

$date_oplach_do_and_prostoys =  date('d.m.Y', strtotime($model->date_pay_arenda. ' + '.$days_oplach_and_prostoys.' days'));

$cur_date = new DateTime();




//$itog_date = new DateTime($model->date_pay_arenda);
$itog_date = new DateTime(date('d.m.Y',strtotime($model->date_pay_arenda. ' + '.$model->arenda_items.' days')));



//Для окончания договора
if ($cur_date > $itog_date) {
	$full_date = date_diff(new DateTime($model->date_pay_arenda), $itog_date)->days;
	$date_raznost = date_diff(new DateTime($date_oplach_do), $itog_date)->days;
} else {
	$full_date = date_diff(new DateTime($model->date_pay_arenda), $cur_date)->days;
	$date_raznost = date_diff(new DateTime($date_oplach_do), $cur_date)->days;
}




print_r ($full_date);


echo '<br>';

/*
print_r  ($itog_date);
echo '<br>';
*/

//Dolgen oplatit na tec daty

$dolgen_opl_natec = $full_date * $arenda_item_summ;



//Долг по аренде
//$dolg_arenda = $date_raznost * $arenda_item_summ;



//Полная сумма по аренде
$full_arend = $full_date * $arenda_item_summ;

//echo $dolg_arenda;
//exit;






//print_r ($model->date_pay_arenda_new);

//echo '<br>';

//echo $prostoy_days;

?>	
	
	
	
	
	
	
	


<h2>Итоги</h2>


	<table class="table table-striped table-bordered detail-view">
		<tbody>
			<tr>
			
				<td>Статус автомобиля</td>
				<td><?= $auto_status; ?></td>
			</tr>
			
			
			<tr>
			
				<td>Марка модель гос.номер</td>
				<td><?= $current_auto->mark.' '
							.$current_auto->model.' '.$current_auto->gosnomer ?></td>
			</tr>
			
			
			<tr>
			
				<td>Дата первого дня оплаты аренды по договору</td>
				<td><?= date('d.m.Y',strtotime($model->date_pay_arenda)); ?></td>
			</tr>
			
			
			<tr>
			
				<td>ФИО</td>
				<td><?= $current_driver->first_name.' '
							.$current_driver->middle_name.' '
							.$current_driver->last_name ?></td>
			</tr>
			
			
			<tr>
				<td>Всего внес денег</td>
				<td><?= $full_summa; ?> рублей</td>
			</tr>
			
				
			
			<tr>
				<td>Оплачено до</td>
				<td><?php

				//echo $date_oplach_do;
				if (new DateTime ($date_oplach_do) > $itog_date) {			
					echo date('d.m.Y',strtotime($model->date_pay_arenda)); 
				} else {
					echo $date_oplach_do;
				}
				
				?>
				
				
				</td>
			</tr>



			<tr>
				<td>Оплачено до с оплаченными простоями</td>
				<td><?php

				//echo $date_oplach_do;
				if (new DateTime ($date_oplach_do_and_prostoys) > $itog_date) {			
					echo date('d.m.Y',strtotime($model->date_pay_arenda)); 
				} else {
					echo $date_oplach_do_and_prostoys;
				}
				
				?>
				
				
				</td>
			</tr>
			
			
			
			
			
			
			
			
			
			
			
			
			<tr>
				<td>Должен был оплатить по аренде на текущую дату</td>
				<td><?= $dolgen_opl_natec; ?></td>
			</tr>
			
			
			
			<tr>
			
				<td>Внес по аренде всего</td>
				<td><?= $arenda_summa; ?></td>
			</tr>
			
			
			
			<tr>
				<td>Долг по аренде на текущую дату</td>
				<td><?= $dolg_arenda =  $dolgen_opl_natec - $arenda_summa;  ?></td>
			</tr>
			
			
			
			
			
			<tr>
				<td>Сумма п взнос + выкуп</td>
				<td><?=  ($model->p_vznos + $model->vykup); ?></td>
			</tr>
			
			
			
			<tr>
				<td>Оплатил п взнос + выкуп</td>
				<td><?= $dog_summa; ?></td>
			</tr>
			
			
			
			<tr>
				<td>Долг п взнос + выкуп</td>
				<td><?php 
				
				 $ddolg_p_v = ($model->p_vznos + $model->vykup) - $dog_summa;

					echo $ddolg_p_v;

				 ?>
				
				
				
				
				
				</td>
			</tr>
			
			
			<tr>
				<td>Сумма по нашим штрафам за нарушение договора</td>
				<td><?= $shtrafs_summa ?></td>
			</tr>
			
			
			
			<tr>
				<td>Оплатил НАши штрафы</td>
				<td><?= $our_pen_summa; ?></td>
			</tr>
			
			
			<tr>
				<td>Долг по нашим штрафам </td>
				<td><?= $dolg_ourpen =  $shtrafs_summa - $our_pen_summa; ?></td>
			</tr>
			
			
			
			<tr>
				<td>Сумма по простоям </td>
				<td><?= $prostoy_ful_summa; ?></td>
			</tr>
			
			
			<tr>
				<td>Оплатил по простоям </td>
				<td><?= $prostoys_opl_summa; ?></td>
			</tr>
			
			
			<tr>
				<td>Долг по простоям </td>
				<td><?= $dolg_prostoy =  $prostoy_ful_summa - $prostoys_opl_summa; ?></td>
			</tr>
			
			
			<tr>
				<td>Сумма по штрафам ПДД </td>
				<td><?= $summa_allll; ?></td>
			</tr>
			
			
			<tr>
				<td>Оплатил  по штрафам ПДД </td>
				<td><?= $pdd_opl_summa; ?></td>
			</tr>
			
			
			
			
			<tr>
				<td>Долг  по штрафам ПДД </td>
				<td><?= $dolg_pdd = $sumRealAl ?></td>
			</tr>
			
			
			
			
			<tr>
				<td>Общий долг </td>
				<td><?=  $dolg_pdd + $dolg_prostoy +  $dolg_ourpen + $ddolg_p_v + $dolg_arenda; ?></td>
			</tr>
			
			
			
			
			
			
			
	</tbody>


	</table>



</div>	