<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contracts */

$this->title = Yii::t('app', 'Обновить договор: {name}', [
    'name' => $model->id_contract,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Договора'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Договор '.$model->id_contract, 'url' => ['view', 'id' => $model->id_contract]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="contracts-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrOwners' => $arrOwners,
		'arrDrivers' => $arrDrivers,
		'arrAutomobiles' => $arrAutomobiles,
		'res_owner' => $res_owner,
		'res_driver' => $res_driver,
		'res_auto' => $res_auto,
		'arrArendatypes' => $arrArendatypes,
		'arrCStatuses' => $arrCStatuses,
    ]) ?>

</div>
