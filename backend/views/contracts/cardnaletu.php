<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;

use kartik\select2\Select2;

use kartik\date\DatePicker;

use app\models\Contractsstatus;

/* @var $this yii\web\View */
/* @var $model app\models\Contracts */

$this->title = 'Карточка на лету';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Договора'), 'url' => ['index']];
$this->params['breadcrumbs'][] = "Карточка на лету";


\yii\web\YiiAsset::register($this);



?>


<div class="contracts-view">

<div class="buts">

    <h1><?= Html::encode($this->title) ?></h1>

</div>	
	
	
	
	
	
	<?php $form = ActiveForm::begin(); ?>
	
	<?php 	
		echo $form->field($model, 'driver_id')->widget(Select2::classname(), [
									'data' => $arrDrivers,
									'language' => 'ru',
									'options' => ['placeholder' => 'Select a state ...'],
									'pluginOptions' => [
										'allowClear' => true
									],
									
									
									
								]);
	
	
	?>
	
	
	
		
	<?php 	
		echo $form->field($model, 'auto_id')->widget(Select2::classname(), [
									'data' => $arrAutos,
									'language' => 'ru',
									'options' => ['placeholder' => 'Select a state ...'],
									'pluginOptions' => [
										'allowClear' => true
									],
									
									
									
									
								]);
	
	
	?>
	
	
	
	
	
	
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Отправить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
	
	
	
</div>	
	
	