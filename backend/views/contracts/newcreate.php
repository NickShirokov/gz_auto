<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contracts */

$this->title = Yii::t('app', 'Создать новый договор "НА ЛЕТУ"');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Договора'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contracts-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_newform', [
        'model' => $model,
        'modelAuto' => $modelAuto,
        'modelDriver' => $modelDriver,
		'arrOwners' => $arrOwners,
		'arrDrivers' => $arrDrivers,
		'arrAutomobiles' => $arrAutomobiles,
		'arrArendatypes' => $arrArendatypes,
		'arrCStatuses' => $arrCStatuses,
		
		'arrMarks' => $arrMarks,
		'arrIns' => $arrIns,
		'arrModels' => [],
		'arrStatus' => $arrStatus,	
		'arrOwners' => $arrOwners,
		'arrLicen' => $arrLicen,
		'arrRazresh' => $arrRazresh,
		'arrCat' => $arrCat,
		'arrStatus' => $arrStatus,
		'arrStatusDr' => $arrStatusDr,
		'arrPenaltytype' => $arrPenaltytype,
		
		
    ]) ?>

</div>
