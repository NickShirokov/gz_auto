<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


use yii\grid\GridView;
use yii\widgets\Pjax;

use kartik\date\DatePicker;



use kartik\export\ExportMenu;

use app\models\Contractsstatus;

/* @var $this yii\web\View */
/* @var $model app\models\Contracts */

$this->title = 'Договор '.$model->id_contract;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Договора'), 'url' => ['index']];
$this->params['breadcrumbs'][] =   ['label' => Yii::t('app', $this->title), 'url' => ['view?id='.$model->id_contract]];
$this->params['breadcrumbs'][] = 'Путевые по этому договору';
\yii\web\YiiAsset::register($this);
?>



<?php 



$gridColumns =   [
            ['class' => 'yii\grid\SerialColumn'],

            'id_putevoy',
			[
				'attribute' => 'gosnomer',
				'format' => 'html',
				'value' => function($data) {
					
								//print_r ($data);
								
								$str = '';
								$str.= $data['automobiles'][0]['mark']. 
											' '. $data['automobiles'][0]['model']. ' ' . $data['automobiles'][0]['gosnomer'];
								
								return $str;
					
							},
            ],
			
			[
				'attribute' =>'last_name',
				'format' => 'html',
				'value' => function($data) {
					
								//print_r ($data);
								
								$str = '';
								$str.= $data['drivers'][0]['first_name']. 
											' '. $data['drivers'][0]['last_name'];
								
								return $str;
					
							},
            ],
			
			
			
			
			[   
				'attribute' => 'date_start',
                'value' => 'date_start',
                'format' => ['date', 'php:d.m.Y'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'PutevielistiModelSearch[date_start]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['PutevielistiModelSearch']['date_start'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],
			
			
			[   
				'attribute' => 'date_end',
                'value' => 'date_end',
                'format' => ['date', 'php:d.m.Y'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'PutevielistiModelSearch[date_end]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['PutevielistiModelSearch']['date_end'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],
	
			
	
			
			[
				'attribute' =>'put_type',
				'format' => 'html',
				'filter' => $arrTypes,
				'value' => function ($data) {
					
								$str = '';
						
								if ($data['put_type'] == 1) {
									$str.= 'С проставлением дат';
								} else {
									
									$str.= 'Без дат';
								}
					
					
								return $str;
							}
			],
			
			[
			
				'attribute' => 'link',
				'format' => 'html',
				
				'value' => function($data) {
					
								
								$str = '';
						
								
								$str.= '<a href="/admin/putevielisti/putevoy?id='.$data['id_putevoy'].'">Печать</a>';
					
					
								return $str;

							},
			],
			
			
			
            ['class' => 'yii\grid\ActionColumn'],
        ];




?>




<div class="contracts-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Просмотр'), ['view', 'id' => $model->id_contract], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_contract], ['class' => 'btn btn-primary']) ?>
       

	   <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_contract], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите удалить договор?'),
                'method' => 'post',
            ],
        ]) ?>
		
		
		<?= Html::a(Yii::t('app', 'Договор PDF'), ['pdfcontract', 'id' => $model->id_contract],  ['class' => 'btn btn-primary', 'target'=>'_blank',]) ?>
    
	
		<?= Html::a(Yii::t('app', 'Все платежи по договору'), ['payments', 'id' => $model->id_contract],  ['class' => 'btn btn-primary',]) ?>
    
	
	
		<?= Html::a(Yii::t('app', 'Посмотреть карточку водителя'), ['drivers/view', 'id' => $current_driver->id_driver],  ['class' => 'btn btn-primary', ]) ?>
    
		<?= Html::a(Yii::t('app', 'Посмотреть карточку машины'), ['automobiles/view', 'id' => $current_auto->id_auto],  ['class' => 'btn btn-primary', ]) ?>
    
		<?= Html::a(Yii::t('app', 'Штрафы ПДД по договору'), ['pddshtrafs', 'id' => $model->id_contract],  ['class' => 'btn btn-primary',]) ?>
    
		<?= Html::a(Yii::t('app', 'Штрафы по договору'), ['shtrafs', 'id' => $model->id_contract],  ['class' => 'btn btn-primary', ]) ?>
    	
    </p>
	
	

	
	
	<!--  Пробуем подтащить сюда табличку с датами платежей по конкретному договору--->
	
	
	
	 <?php //Pjax::begin(); ?>
  
  
  
  
	<h1><?= Html::encode('Все путевые по этому договору') ?></h1>
	
	
	
	
    <p>
        <?= Html::a(Yii::t('app', 'Создать путевой'), ['putevielisti/create', 'contract_id' => $model->id_contract], ['class' => 'btn btn-success']) ?>
    </p>

	<?php 
	
	
// Renders a export dropdown menu
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns
]);
	
	?>
	
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		
		
        'columns' => $gridColumns,
    ]); ?>
    <?php //Pjax::end(); ?>
	


	
	


