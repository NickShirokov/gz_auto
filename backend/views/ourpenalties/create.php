<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ourpenalties */

$this->title = Yii::t('app', 'Создать наш штраф');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Наши штрафы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ourpenalties-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrOurpenstatus' => $arrOurpenstatus,
		'arrContracts' => $arrContracts,
    ]) ?>

</div>
