<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;



use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Ourpenalties */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ourpenalties-form">

    <?php $form = ActiveForm::begin(); ?>

	<?php 
	if ($_GET['contract_id']) {
		$var_cntr = $_GET['contract_id'];
	} 
	
	
	if ($model->date_pen) {
		$var_dt = $model->date_pen;
	} else {
		$var_dt = date('Y-m-d');
	}
	
	
	?>
	
	
	
<?php 	
		echo $form->field($model, 'p_contract_id')->widget(Select2::classname(), [
									'data' => $arrContracts,
									'language' => 'ru',
									'value' => $val_cntr,
									'options' => ['placeholder' => 'Select a state ...'],
									'pluginOptions' => [
										'allowClear' => true,
										'initialize' => true,
									],
									
									'pluginEvents' => [
									   "select2:select" => 'function() { 
									   
									
											   
									   
									   }',
									]
									
									
								]);
	
	
	?>
    <?= $form->field($model, 'date_pen')->input('date', ['required', 'value' => $var_dt]) ?>

    <?= $form->field($model, 'pen_summa')->textInput() ?>

    <?= $form->field($model, 'pen_comment')->textarea(['rows' => 6]) ?>
	
	
	 <?= $form->field($model, 'status_o_pen')->dropdownList($arrOurpenstatus,['prompt' => '']) ?>

	 
	 
	 
	 
	 
<h2>Выберите способ внесения денег и на что они должны быть расходаваны</h2>	
	
 <?= $form->field($modelPayments, 'pmnt_variant')->dropDownList(
									[
										
										'3' => 'Наши штрафы по договору',
										
									],
									
									[
										'onchange' => '
										
												
												
												
												$(".pmts-wr").find("input").val("");
												$(".pmts-wr").find("select").val("");
										
												var value_sel = $(this).val();
										
												$(".pmts-wr").hide();
										
												if (value_sel == 6) {
													$(".pmts-wr").show();
												} else {
													$(".payment-box-"+value_sel).show();
												}
												
												
										'
									]
									
									
						); 
					?>	

							

<div class="row">	
	
<?php 
	if ($modelPayments->date_payment): 
	?>
	<div class="col-md-4">
		<?= $form->field($modelPayments, 'date_payment')->input('date', ['required' => true]) ?>
	</div>
	
	<?php else: ?>
	<div class="col-md-4">
		<?= $form->field($modelPayments, 'date_payment')->input('date', ['required' => true, 'value' => date('Y-m-d')]) ?>
	</div>
	
	<?php endif; ?>


<div class="col-md-4">

    <?= $form->field($modelPayments, 'type')->dropDownList(
				[ 
					'Наличные' => 'Наличные', 
					'На карту' => 'На карту', 
					'Списано с диспетчерской' => 'Списано с диспетчерской', 
					'Бонус/Простили' => 'Бонус/Простили',
				], 
				[
					'prompt' => 'Выберите ',
					'onchange' => '
									
									if($(this).val() == "Бонус/Простили") {
										
										$("#payments-summa").val(0);
										
									} else {
										
										$("#payments-summa").val("");
									}
									
					
								',
				]
		)
	?>
</div>

<div class="col-md-4">
    <?= $form->field($modelPayments, 'summa')->textInput() ?>
</div>


<div class="col-md-12">


<?= $form->field($modelPayments, 'comission_type')->checkbox([
	'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
])?>

<?= $form->field($modelPayments, 'comission_summa')->textInput() ?>




</div>





<div style="clear:both"></div>
	 
	 
	 
	 
	 
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
