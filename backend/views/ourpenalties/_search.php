<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\OurpenaltiesSearchModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ourpenalties-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id_penalty') ?>

    <?= $form->field($model, 'contract_id') ?>

    <?= $form->field($model, 'date_pen') ?>

    <?= $form->field($model, 'pen_summa') ?>

    <?= $form->field($model, 'pen_comment') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
