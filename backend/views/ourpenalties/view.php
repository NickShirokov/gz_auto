<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Ourpenalties */

$this->title = $model->id_penalty;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Наши штрафы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ourpenalties-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
		<?php
			if(
				Yii::$app->user->can('admin') 
				)  
			{ 
			
				echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_penalty], ['class' => 'btn btn-primary']);
				echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_penalty], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите удалить этот штраф?'),
                'method' => 'post',
            ],
        ]);
			}
			
		?>
	
	
	
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_penalty',
            'p_contract_id',
           
			[
				'attribute' => 'date_pen',
				'format' => ['date', 'php:d.m.Y'],
			],
			
            'pen_summa',
            'pen_comment:ntext',
        ],
    ]) ?>

</div>
