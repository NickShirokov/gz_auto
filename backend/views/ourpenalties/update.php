<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ourpenalties */

$this->title = Yii::t('app', 'Обновить наш штраф: {name}', [
    'name' => $model->id_penalty,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Наши штрафы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_penalty, 'url' => ['view', 'id' => $model->id_penalty]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="ourpenalties-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrOurpenstatus' => $arrOurpenstatus,
		'arrContracts' => $arrContracts,
    ]) ?>

</div>
