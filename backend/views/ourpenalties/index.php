<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use kartik\date\DatePicker;


use yii\grid\CheckboxColumn;

use kartik\export\ExportMenu;
use app\models\Logitems;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OurpenaltiesSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Наши штрафы и взносы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ourpenalties-index">



<div class="buts">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать наш штраф'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Создать наш штраф на лету'), ['newcreate'], ['class' => 'btn btn-success']) ?>
		<?// Html::a(Yii::t('app', 'Обновить наши штрафы'), ['ourpenaltiesupdate'], ['class' => 'btn btn-primary']) ?>
		
		
		
		<?php
		if(
			Yii::$app->user->can('admin')) 
			{
				 echo Html::a(Yii::t('app', 'Обновить наши штрафы'), ['ourpenaltiesupdate'], ['class' => 'btn btn-primary']) . ' '.
			
				' <input type="button" class="btn btn-info" value="Удалить выбранные" id="MyButton" >
		
				<input type="button" class="btn btn-info" value="Очистить всех" id="remove-all" >';
			}
		?>
		
		
		
		
		
	
    </p>
	
	
	
	<?php echo \nterms\pagesize\PageSize::widget(); ?>	
<?php 

$gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],
			['class' => 'yii\grid\CheckboxColumn'],
            
			[
				'attribute' => 'id_penalty',
				'label' => 'Номер штрафа',
				'format' => 'html',
				'value' => function($data) {
					
								
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/ourpenalties/view?id='.$data['id_penalty'].'&contract_id='.$data['p_contract_id'].'">№ '.$data['id_penalty'].'</a>';
					
								return $str;
					
					
							},
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
            ],
			
			
			[
				'attribute' => 'p_contract_id',
				'label' => 'Номер договора (число)',
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
				
				'format' => 'html',
				'value' => function($data) {
					
								
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/contracts/view?id='.$data['p_contract_id'].'">Договор '.$data['p_contract_id'].'</a>';
					
								return $str;
					
					
							},
				
				
			],
           
			
			
			
			//'date_pen',
			
			
			[   
				'attribute' => 'date_pen',
                'value' => 'date_pen',
                'format' => ['date', 'php:d.m.Y'],
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'OurpenaltiesSearchModel[date_pen]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['OurpenaltiesSearchModel']['date_pen'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],
			
			
			
			[
				'attribute' => 'last_name',
				'format' => 'html',
				'value' => function($data) {
												
							$str ='';
							foreach($data['contracts'] as $item)
							{
				
								$str.= '<a href="/admin/drivers/view?id='.$item['drivers'][0]['id_driver'].'">' .$item['drivers'][0]['first_name'].' '.$item['drivers'][0]['last_name']. '</a>';
							}
							return $str;
                },
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
			],
			
			[
				'attribute' => 'gosnomer',
				'format' => 'html',
				'value' => function($data) {
							
							$str ='';
							foreach($data['contracts'] as $item)
							{		

								$str.= '<a href="/admin/automobiles/view?id='.$item['automobiles'][0]['id_auto'].'">' .$item['automobiles'][0]['mark'].' '.$item['automobiles'][0]['model']. ' ' .$item['automobiles'][0]['gosnomer']. '</a>';
						
							
								//$str.=;
							}
							return $str;
							
							
                },
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
						
			],
			
			[
				'attribute' => 'pen_summa',
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
			],
			
			
			[
				'attribute' => 'status_o_pen',
				//'value' => 'ourpenaltiesstatus0.status_o_name',
				'format' => 'html',
				'value' => function($data){
					
					
								$str = '';
								
								
								$st_arr = $data['our_penalties_status'][0];
					
								if ($st_arr['status_o_id'] == 1) {
									
									$str.= '<span style="color: red">'.$st_arr['status_o_name'].'</span>';
									
								} elseif($st_arr['status_o_id'] == 2) {
									
									$str.= '<span style="color: green">'.$st_arr['status_o_name'].'</span>';
									
								} else {
									$str.= '<span style="">'.$st_arr['status_o_name'].'</span>';
								}
					
								
					
								
					
								return $str;
						},
				
				
				'filter' => $arrOurpenstatus,
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
			],
			
            
			
		
			[
				'attribute' => 'pen_comment',
				'contentOptions' => ['style' => 'width:200px; white-space: normal;'],
			],
			
			
			[
				'attribute' => 'ostatok',
				'format' => 'html',
				'value' => function ($data) {
					
					
									$pmnts = $data['payments'];
					
									$str = '';
					
									foreach ($pmnts as $item) {
										
										$str.= '<a href="/admin/payments/view?id='.$item['id_payment'].'">Платеж №'.$item['id_payment'].' '.$item['pay_ourpen'].'рублей</a><br>'; 
										
									}
					
									return $str;	
								},
			],
			
		
			
			
			

            [
				'class' => 'yii\grid\ActionColumn',
				'visibleButtons' => [
									'update' => function ($model) {
										$id = $model->id_penalty;
										
										if (\Yii::$app->user->can('admin', ['post' => $model])) {
											
											return true;
										} else {
											
											$model_logitems = Logitems::find()
																->where(
																	[
																		
																		'item_id' => $id,
																		'type_page' => 'ourpenalty',
																		'user_id' => Yii::$app->user->getId(),
																	])
																	->andWhere(['>=','date_create', date('Y-m-d', strtotime('-5 days'))])
																	->all();
											
											
											if (!empty($model_logitems)) {
												
												return true;
											} else {
												return false;
											}
											
										}
									},
									'delete' => function ($model) {
										$id = $model->id_penalty;
										
										if (\Yii::$app->user->can('admin', ['post' => $model])) {
											
											return true;
										} else {
											
											$model_logitems = Logitems::find()
																->where(
																	[
																		
																		'item_id' => $id,
																		'type_page' => 'ourpenalty',
																		'user_id' => Yii::$app->user->getId(),
																	])
																	->andWhere(['>=','date_create', date('Y-m-d', strtotime('-5 days'))])
																	->all();
											
											
											if (!empty($model_logitems)) {
												
												return true;
											} else {
												return false;
											}
											
										}
									},
								],
			],
        ];


// Renders a export dropdown menu
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns
]);

?>	
	
</div>	
	

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'pager' => [
			'class' => \kop\y2sp\ScrollPager::className(),
			'container' => '.grid-view tbody',
			'item' => 'tr',
			'paginationSelector' => '.grid-view .pagination',
			'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',
		],
		'filterSelector' => 'select[name="per-page"]',
		
        'columns' => $gridColumns,
    ]); ?>
    <?php //Pjax::end(); ?>
</div>




<?php

$this->registerJs('

$(document).ready(function(){
	$(\'#MyButton\').click(function(){

	var HotId = $(\'.grid-view\').yiiGridView(\'getSelectedRows\');
	
	
	$.ajax({
		type: \'POST\',
		url : \'/admin/ourpenalties/multipledelete\',
		data : {row_id: HotId},
		success : function() {
			
			$(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
		
		}
	});

	});
	
	
	
	$(\'#remove-all\').click(function(){
	
		$.ajax({
			type: \'POST\',
			url : \'/admin/ourpenalties/removeall\',
			//data : {row_id: HotId},
			success : function() {
				
				//$(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
			
			}
		});
	
	
	});
	
	
	
	
});', \yii\web\View::POS_READY);

?>