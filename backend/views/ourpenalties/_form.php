<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\Ourpenalties */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ourpenalties-form">

    <?php $form = ActiveForm::begin(); ?>

	<?php 
	if ($_GET['contract_id']) {
		$var_cntr = $_GET['contract_id'];
	} 
	
	
	if ($model->date_pen) {
		$var_dt = $model->date_pen;
	} else {
		$var_dt = date('Y-m-d');
	}
	
	
	?>
	
			
	
<?php 	
		echo $form->field($model, 'p_contract_id')->widget(Select2::classname(), [
									'data' => $arrContracts,
									'language' => 'ru',
									'value' => $val_cntr,
									'options' => ['placeholder' => 'Select a state ...'],
									'pluginOptions' => [
										'allowClear' => true,
										'initialize' => true,
									],
									
									'pluginEvents' => [
									   "select2:select" => 'function() { 
									   
									
											   
									   
									   }',
									]
									
									
								]);
	
	
	?>
	
	
	
	
	
	
    <?= $form->field($model, 'date_pen')->input('date', ['required', 'value' => $var_dt]) ?>

    <?= $form->field($model, 'pen_summa')->textInput() ?>

    <?= $form->field($model, 'pen_comment')->textarea(['rows' => 6]) ?>
	
	
	 <?= $form->field($model, 'status_o_pen')->dropdownList($arrOurpenstatus,['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
