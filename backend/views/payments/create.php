<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Payments */

$this->title = Yii::t('app', 'Создать платеж');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Платежи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrContracts' => $arrContracts,
		'arrShtrafs' => $arrShtrafs,
		'arrPddShtrafs' => $arrPddShtrafs,
		'arrProstoys' => $arrProstoys,
    ]) ?>

</div>
