<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Payments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payments-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'contract_id')->dropDownList(
			$arrContracts, [
								'prompt' => 'Выберите договор',
							
								'onchange' => '
									
									var contract_id = $(this).val();
									
									$.post(
										//"/admin/automobiles/lists?mark="+mark, 
										"/admin/ourpenalties/shtrafsbycontract?id="+contract_id, 
										function(data){
											
											$("select#payments-id_ourpen").html(data);
										}
									
									);
									
									
									$.post(
										
										"/admin/pddpenalties/shtrafsbycontract?id="+contract_id, 
										function(data){
								
											//alert(data);
											$("select#payments-id_pdd_shtraf").html(data);
										}
									
									);
									
						
								',
								
							]) ?>
	

<div class="row">	
	
<div class="col-md-4">
    <?= $form->field($model, 'date_payment')->input('date', ['required' => true]) ?>
</div>

<div class="col-md-4">

    <?= $form->field($model, 'type')->dropDownList(
		[ 
			'Наличные' => 'Наличные', 
			'На карту' => 'На карту', 
			'Списано с диспетчерской' => 'Списано с диспетчерской', 
			'Бонус/Простили' => 'Бонус/Простили',
		], 
		[
			'prompt' => 'Выберите '
		]
		)
	?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'summa')->textInput() ?>
</div>



<div class="col-md-4">
    <?= $form->field($model, 'arenda')->textInput() ?>
</div>


<div style="clear:both"></div>

<div class="col-md-4">
	<p>Платеж за пункт договора - взносы</p>

    <?= $form->field($model, 'pay_dogovor')->textInput() ?>
</div>


<div style="clear:both"></div>


<div class="col-md-12">
	<h3>Выберите штраф</h3>

    <?= $form->field($model, 'id_ourpen')->dropdownList($arrShtrafs) ?>
</div>



<!--
<div class="col-md-4">
	<p>Выберите штраф</p>

    <?= $form->field($model, 'id_ourpen')->textInput() ?>
</div>
-->


<div class="col-md-4">
	<p>Платеж за наш штраф</p>

    <?= $form->field($model, 'pay_ourpen')->textInput() ?>
</div>



<div style="clear:both"></div>


<div class="col-md-12">
	<h3>Платеж за ПДД</h3>

    <?= $form->field($model, 'id_pdd_shtraf')->dropdownList($arrPddShtrafs,
	
				[
					'prompt' => '',
					
					'onchange' => '
									/*
									var contract_id = $(this).val();
									
									$.post(
										
										"/admin/pddpenalties/shtrafsbycontract?id="+contract_id, 
										function(data){
								
											alert(data);
											$("select#payments-id_pdd_shtraf).html(data);
										}
									
									);
						*/
						',
				
					
				
				]) ?>
</div>


<div class="col-md-4">
	

    <?= $form->field($model, 'pay_pdd')->textInput() ?>
</div>


<div style="clear:both"></div>


<div class="container"><h3>ПРостой</h3></div>

<div class="col-md-4">
	

    <?= $form->field($model, 'prostoy_summa')->textInput() ?>
</div>


<div class="col-md-4">
	

    <?= $form->field($model, 'prostoy_days')->textInput() ?>
</div>




<div style="clear:both"></div>

<div class="col-md-12">
    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
</div>


</div>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
