<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use yii\grid\GridView;
use yii\widgets\Pjax;


use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Payments */

$this->title = $model->id_payment.' Платеж';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Платежи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="payments-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Договор'), ['contracts/view', 'id' => $model->contract_id], ['class' => 'btn btn-primary']) ?>
        
		
		<?php
			if(
				Yii::$app->user->can('admin') 
				)  
			{
				echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_payment, 'contract_id' => $model->contract_id], ['class' => 'btn btn-primary']);
				echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_payment], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите удалить платеж?'),
                'method' => 'post',
            ],
        ]) ;
			} else {
				
				
				
				$id = $model->id_payment;
										
				
					
				$model_logitems = Logitems::find()
									->where(
										[									
											'item_id' => $id,
											'type_page' => 'payment',
											'user_id' => Yii::$app->user->getId(),
										])
										->andWhere(['>=','date_create', date('Y-m-d', strtotime('-5 days'))])
										->all();
					
					
				
				if (!empty($model_logitems)) {
					echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_payment, 'contract_id' => $model->contract_id], ['class' => 'btn btn-primary']);
					echo ' ';
					echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_payment], [
					'class' => 'btn btn-danger',
					'data' => [
						'confirm' => Yii::t('app', 'Вы действительно хотите удалить платеж?'),
						'method' => 'post',
					],
				]) ;
				} 
			}
		?>
		
		
		
		
    </p>
	

	

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_payment',
            'contract_id',
            
			
			[
				'attribute' => 'date_payment',
				'format' => ['date', 'php:d.m.Y'],
			],
			
			
            'type',
            'summa',
           
            'arenda',
			'pay_dogovor',
			'pay_pdd',
          
            'comment:ntext',
        ],
    ]) ?>
	
	
	
	
	

</div>


