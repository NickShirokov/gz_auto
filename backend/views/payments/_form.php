<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\Payments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payments-form">

    <?php $form = ActiveForm::begin(); ?>

	<?php 
	
	
	
	if ($_GET['contract_id']) {
	
		$val_cntr = $_GET['contract_id'];
	
	} 
	
	
	?>
	
	
	
	
		
	
<?php 	
		echo $form->field($model, 'contract_id')->widget(Select2::classname(), [
									'data' => $arrContracts,
									'language' => 'ru',
									'value' => $val_cntr,
									'options' => ['placeholder' => 'Select a state ...'],
									'pluginOptions' => [
										'allowClear' => true,
										'initialize' => true,
									],
									
									'pluginEvents' => [
									   "select2:select" => 'function() { 
									   
									   var contract_id = $(this).val();
									
									
									
									//$("#payments-contract_id option[value=\'+contract_id+\']").attr("selected", "selected");
									
									$.post(
										//"/admin/automobiles/lists?mark="+mark, 
										"/admin/ourpenalties/shtrafsbycontract?id="+contract_id, 
										function(data){
											
										
											
											$("select#payments-id_ourpen").html(data);
											
											
										}
									
									);
									
									
									$.post(
										
										"/admin/pddpenalties/shtrafsbycontract?id="+contract_id, 
										function(data){
								
											
											$("select#payments-id_pdd_shtraf").html(data);
											
										}
									
									);
									
									
									
									$.post(
										
										"/admin/prostoys/prostoysbycontract?id="+contract_id, 
										function(data){
								
											
											$("select#payments-p_id_prostoy").html(data);
											
										}
									
									);
											   
									   
									   }',
									]
									
									
								]);
	
	
	?>
	
	
	
	
	
	
	
	
	
	
	
	
	


<h2>Выберите способ внесения денег и на что они должны быть расходаваны</h2>	
	
 <?= $form->field($model, 'pmnt_variant')->dropDownList(
									[
										'1' => 'Арендный платеж',
										'2' => 'Платеж по договору (взнос и и выкуп)',
										'3' => 'Наши штрафы по договору',
										'4' => 'Штраф ПДД (строго)',
										'5' => 'Простой',
										//'6' => 'Комплексный платеж'
									],
									
									[
										'onchange' => '
										
												
												
												
												$(".pmts-wr").find("input").val("");
												$(".pmts-wr").find("select").val("");
										
												var value_sel = $(this).val();
										
												$(".pmts-wr").hide();
										
												if (value_sel == 6) {
													$(".pmts-wr").show();
												} else {
													$(".payment-box-"+value_sel).show();
												}
												
												
										'
									]
									
									
						); 
					?>	

							

<div class="row">	
	
<?php 
	if ($model->date_payment): 
	?>
	<div class="col-md-4">
		<?= $form->field($model, 'date_payment')->input('date', ['required' => true]) ?>
		
		
		
	
		
	</div>
	
	<?php else: ?>
	<div class="col-md-4">
		<?= $form->field($model, 'date_payment')->input('date', ['required' => true, 'value' => date('Y-m-d')]) ?>
	</div>
	
	
	
	
	
	
	<?php endif; ?>


	
	
	
		
	
<div class="col-md-4">

    <?= $form->field($model, 'type')->dropDownList(
				[ 
					'Наличные' => 'Наличные', 
					'На карту' => 'На карту', 
					'Списано с диспетчерской' => 'Списано с диспетчерской', 
					'Бонус/Простили' => 'Бонус/Простили',
				], 
				[
					'prompt' => 'Выберите ',
					'onchange' => '
									
									if($(this).val() == "Бонус/Простили") {
										
										$("#payments-summa").val(0);
										
									} else {
										
										$("#payments-summa").val("");
									}
									
					
								',
				]
		)
	?>
</div>

<div class="col-md-4">
    <?= $form->field($model, 'summa')->textInput() ?>
</div>


<div class="col-md-12">


<?= $form->field($model, 'comission_type')->checkbox([
	'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
])?>

<?= $form->field($model, 'comission_summa')->textInput() ?>




</div>


<!--
<div class="col-md-12 pmts-wr payment-box-1">
<h3>Арендный платеж</h3>
    <?= $form->field($model, 'arenda')->textInput() ?>
</div>


<div style="clear:both"></div>
-->

<!--
<div class="col-md-12 pmts-wr payment-box-2">
	<h3>Платеж за пункт договора - взносы</h3>

    <?= $form->field($model, 'pay_dogovor')->textInput() ?>
</div>
-->

<div style="clear:both"></div>


<div class="col-md-12 pmts-wr payment-box-3">
	<h3>Выберите наш штраф по договору</h3>

    <?= $form->field($model, 'id_ourpen')->dropdownList($arrShtrafs) ?>
	
<!--
	
	<h3>Платеж за наш штраф</h3>

	<p><span style="color: blue; font-weight: bold;">При внесении оплаты за Наш штраф возможно погашение в течение времени по частям</span></p>
	
	<p>В поле Сумма по нашему штрафу вносим всю сумму или часть относительно поля Внесенная сумма
		Потом автоматом перевычисляем и закрываем штраф, когда выполнится условие полного погашения.
		
	</p>
	
	
    <?= $form->field($model, 'pay_ourpen')->textInput() ?>
-->	
</div>




<div style="clear:both"></div>


<div class="col-md-12 pmts-wr payment-box-4">
	<h3>Платеж за ПДД</h3>

    <?= $form->field($model, 'id_pdd_shtraf')->dropdownList($arrPddShtrafs,
	
				[
					'prompt' => '',
					
					'onchange' => '
									
									var select_val = $(this).val();
									
									var element = $(this).find("option:selected"); 
									var myTag = element.attr("data-sum"); 
									
									$("#payments-pay_pdd").val(myTag);
						',
				
					
				
				]) ?>
				
	<!--			
	<p> Здесь баг надо пофиксить</p>
    <?= $form->field($model, 'pay_pdd')->hiddenInput(); ?>
		-->		
</div>





<div style="clear:both"></div>


<div class="col-md-12 pmts-wr payment-box-5">
<h3>Простой</h3>



    <?= $form->field($model, 'p_id_prostoy')->dropdownList($arrProstoys,
	
	
					[
						'prompt' => '',			
						'onchange' => '
										
										var select_val = $(this).val();
										
										var element = $(this).find("option:selected"); 
										var myTag = element.attr("data-sum"); 
										
										$("#payments-pay_pdd").val(myTag);
							',
					
					
					]
					
					
					
					) 
					
					
					
					?>



</div>


<div style="clear:both"></div>



<div class="col-md-12">
    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
</div>









</div>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?

$this->registerJs(
'
$(document).ready(function(){
	

	
	if ($("#payments-comission_type").is(":checked")) {
		
		$(".field-payments-comission_summa").show();
	} else {
		$(".field-payments-comission_summa").hide();
	}
	
	
	
	$("#payments-comission_type").click(function(){
		
		if ($("#payments-comission_type").is(":checked")) {
			$(".field-payments-comission_summa").show();
			
		} else {
			$(".field-payments-comission_summa").hide();
		}
		
		
	});
	
	
	
	
	
	var pmnt_var = $("#payments-pmnt_variant").val();
	
	
	//alert(pmnt_var);
	
	
	$(".pmts-wr").hide();

	if (pmnt_var == 6) {
		$(".pmts-wr").show();
	} else {
		$(".payment-box-"+pmnt_var).show();
	}
	
	
	
	
	var contract_id = $("#payments-contract_id").val();	
	
	$.post(
		//"/admin/automobiles/lists?mark="+mark, 
		"/admin/ourpenalties/shtrafsbycontract?id="+contract_id, 
		function(data){
			
			
			
			$("select#payments-id_ourpen").html(data);
			
			
		
			
		}
	
	);
	
	
	$.post(
		
		"/admin/pddpenalties/shtrafsbycontract?id="+contract_id, 
		function(data){

			//alert(data);
			$("select#payments-id_pdd_shtraf").html(data);
			
			//$(this).val(contract_id);
	
			//alert(contract_id);
			
		}
	
	);
	
	
	
	$.post(
		
		"prostoys/prostoysbycontract?id="+contract_id, 
		function(data){

			//alert(data);
			$("select#payments-p_id_prostoy").html(data);
			
			//$(this).val(contract_id);
	
			//alert(contract_id);
			
		}
	
	);
	
	
	
});



'



);

?>