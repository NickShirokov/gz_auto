<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PaymentsSearchModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payments-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id_payment') ?>

    <?= $form->field($model, 'contract_id') ?>

    <?= $form->field($model, 'date_payment') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'summa') ?>

    <?php // echo $form->field($model, 'p_vznos') ?>

    <?php // echo $form->field($model, 'arenda') ?>

    <?php // echo $form->field($model, 'vykup') ?>

    <?php // echo $form->field($model, 'pdd') ?>

    <?php // echo $form->field($model, 'prosrochka') ?>

    <?php // echo $form->field($model, 'peredacha_rulya') ?>

    <?php // echo $form->field($model, 'p_vyezd_200') ?>

    <?php // echo $form->field($model, 'p_atrib') ?>

    <?php // echo $form->field($model, 'p_put_list') ?>

    <?php // echo $form->field($model, 'p_osago') ?>

    <?php // echo $form->field($model, 'p_uterya_doc') ?>

    <?php // echo $form->field($model, 'p_uterya_key') ?>

    <?php // echo $form->field($model, 'p_diag_card') ?>

    <?php // echo $form->field($model, 'vyezd_sotr') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
