<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Payments */

$this->title = Yii::t('app', 'Обновить платеж: {name}', [
    'name' => $model->id_payment,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Платежи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_payment, 'url' => ['view', 'id' => $model->id_payment]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="payments-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrContracts' => $arrContracts,
		'arrShtrafs' => $arrShtrafs,
		'arrPddShtrafs' => $arrPddShtrafs,
		'arrProstoys' => $arrProstoys,
    ]) ?>

</div>
