<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;



use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Prostoys */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prostoys-form">

    <?php $form = ActiveForm::begin(); ?>

	<div class="row">
	
		<div class="col-md-12">
	
		<?php 
	if ($_GET['contract_id']) {
		$var_cntr = $_GET['contract_id'];
	} 
	
	
	
	
	?>
	
	
	
<?php 	
		echo $form->field($model, 'contract_id')->widget(Select2::classname(), [
									'data' => $arrContracts,
									'language' => 'ru',
									'value' => $val_cntr,
									'options' => ['placeholder' => 'Select a state ...'],
									'pluginOptions' => [
										'allowClear' => true,
										'initialize' => true,
									],
									
									'pluginEvents' => [
									   "select2:select" => 'function() { 
									   
									
											   
									   
									   }',
									]
									
									
								]);
	
	
	?>
		</div>
		
		<div class="col-md-6">
		<?= $form->field($model, 'summa_prostoy')->textInput() ?>

		</div>
		
		<div class="col-md-6">
		<?= $form->field($model, 'days_prostoy')->textInput() ?>

		</div>
		
		
		<div class="col-md-6">
		<?= $form->field($model, 'type_opl')->dropdownList($arrStatus,[]) ?>

		</div>
		
		
		
		<div class="col-md-12">
		<?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

		</div>
	</div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
