<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Prostoys */

$this->title = Yii::t('app', 'Обновить простой: {name}', [
    'name' => $model->id_prostoy,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Простои'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_prostoy, 'url' => ['view', 'id' => $model->id_prostoy]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="prostoys-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrContracts' => $arrContracts,
		'arrStatus' => $arrStatus,
    ]) ?>

</div>
