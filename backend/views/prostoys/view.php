<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Logitems;

/* @var $this yii\web\View */
/* @var $model app\models\Prostoys */

$this->title = $model->id_prostoy;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Простои'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="prostoys-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
		<?php
			if(
				Yii::$app->user->can('admin') 
				)  
			{ 
			
				echo	Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_prostoy], ['class' => 'btn btn-primary']);
				echo	Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_prostoy], [
					'class' => 'btn btn-danger',
					'data' => [
						'confirm' => Yii::t('app', 'Вы действительно хотите удалить этот простой?'),
						'method' => 'post',
					],
				]) ;
			} else {
				
				
				$id = $model->id_prostoy;
										
				$model_logitems = Logitems::find()
										->where(
											[									
												'item_id' => $id,
												'type_page' => 'prostoy',
												'user_id' => Yii::$app->user->getId(),
											])
											->andWhere(['>=','date_create', date('Y-m-d', strtotime('-5 days'))])
											->all();
						
						
					
					if (!empty($model_logitems)) {
						echo	Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_prostoy], ['class' => 'btn btn-primary']);
						echo ' ';
						echo	Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_prostoy], [
							'class' => 'btn btn-danger',
							'data' => [
								'confirm' => Yii::t('app', 'Вы действительно хотите удалить этот простой?'),
								'method' => 'post',
							],
						
					]);
						
				} 						
			}
		?>
	
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_prostoy',
            'contract_id',
            'summa_prostoy',
            'days_prostoy',
            'comment:ntext',
        ],
    ]) ?>

</div>
