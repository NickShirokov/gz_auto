<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Prostoys */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prostoys-form">

    <?php $form = ActiveForm::begin(); ?>

	<div class="row">
	
		<div class="col-md-12">
	
		
		<?php 
	if ($_GET['contract_id']) {
		$var_cntr = $_GET['contract_id'];
	} 
	
	
	
	
	?>
	
	
	
<?php 	
		echo $form->field($model, 'contract_id')->widget(Select2::classname(), [
									'data' => $arrContracts,
									'language' => 'ru',
									'value' => $val_cntr,
									'options' => ['placeholder' => 'Select a state ...'],
									'pluginOptions' => [
										'allowClear' => true,
										'initialize' => true,
									],
									
									'pluginEvents' => [
									   "select2:select" => 'function() { 
									   
									
											   
									   
									   }',
									]
									
									
								]);
	
	
	?>

		</div>
		
		<div class="col-md-6">
		<?= $form->field($model, 'summa_prostoy')->textInput() ?>

		</div>
		
		<div class="col-md-6">
		<?= $form->field($model, 'days_prostoy')->textInput() ?>

		</div>
		
		
		<div class="col-md-6">
		<?= $form->field($model, 'type_opl')->dropdownList($arrStatus,[]) ?>

		</div>
		
		
		
		<div class="col-md-12">
		<?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

		</div>
		
		
		
		<div class="clear" style="clear: both"></div>
			 
<h2>Выберите способ внесения денег и на что они должны быть расходаваны</h2>	
	
 <?= $form->field($modelPayments, 'pmnt_variant')->dropDownList(
									[
										
										'5' => 'Простой',
										
									],
									
									[
										'onchange' => '
										
												
												
											
												
										'
									]
									
									
						); 
					?>	

							

<div class="row">	
	
<?php 
	if ($modelPayments->date_payment): 
	?>
	<div class="col-md-4">
		<?= $form->field($modelPayments, 'date_payment')->input('date', ['required' => true]) ?>
	</div>
	
	<?php else: ?>
	<div class="col-md-4">
		<?= $form->field($modelPayments, 'date_payment')->input('date', ['required' => true, 'value' => date('Y-m-d')]) ?>
	</div>
	
	<?php endif; ?>


<div class="col-md-4">

    <?= $form->field($modelPayments, 'type')->dropDownList(
				[ 
					'Наличные' => 'Наличные', 
					'На карту' => 'На карту', 
					'Списано с диспетчерской' => 'Списано с диспетчерской', 
					'Бонус/Простили' => 'Бонус/Простили',
				], 
				[
					'prompt' => 'Выберите ',
					'onchange' => '
									
									if($(this).val() == "Бонус/Простили") {
										
										$("#payments-summa").val(0);
										
									} else {
										
										$("#payments-summa").val("");
									}
									
					
								',
				]
		)
	?>
</div>

<div class="col-md-4">
    <?= $form->field($modelPayments, 'summa')->textInput() ?>
</div>


<div class="col-md-12">


<?= $form->field($modelPayments, 'comission_type')->checkbox([
	'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
])?>

<?= $form->field($modelPayments, 'comission_summa')->textInput() ?>




</div>
		
		
		
		
		
		
		
		
		
		
	</div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
