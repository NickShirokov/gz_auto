<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Prostoys */

$this->title = Yii::t('app', 'Создать простой');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Простои'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prostoys-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_newform', [
        'model' => $model,
        'modelPayments' => $modelPayments,
		'arrContracts' => $arrContracts,
		'arrStatus' => $arrStatus,
    ]) ?>

</div>
