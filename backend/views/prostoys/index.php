<?php

use yii\helpers\Html;
use yii\grid\GridView;


use yii\grid\CheckboxColumn;
use app\models\Logitems;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProstoysSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Простои');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prostoys-index">


<div class="buts">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать простой'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Создать простой на лету'), ['newcreate'], ['class' => 'btn btn-success']) ?>
		
		
		
		<?php
		if(
			Yii::$app->user->can('admin')) 
			{
			
				echo Html::a(Yii::t('app', 'Обновить простои'), ['prostoysupdate'], ['class' => 'btn btn-primary']) .
			
				' <input type="button" class="btn btn-info" value="Удалить выбранные" id="MyButton" >
		
				 <input type="button" class="btn btn-info" value="Очистить всех" id="remove-all" >';
			}
		?>
		
		
		
		
		
		
    </p>

	
</div>	
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			['class' => 'yii\grid\CheckboxColumn'],
			
			[
				'attribute' => 'id_prostoy',
				'format' => 'html',
				'value' => function($data){
					
								$str = '';
							
								$str.= '<a href="/admin/prostoys/view?id='.$data['id_prostoy'].'">'.$data['id_prostoy'].'</a>';
					
							
								return $str;
							}
			],
			[
				'attribute' => 'contract_id',
				'format' => 'html',
				'value' => function($data) {
					
								$str = '';
					
								$str.= '<a href="/admin/contracts/view?id='.$data['contract_id'].'">Договор '.$data['contract_id'].'</a>';
					
								return $str;
							}
            ],
			
			
			'summa_prostoy',
            'days_prostoy',
			
			
			[
				'attribute' => 'type_opl',
				'format' => 'html',
				'filter' => $arrStatus,
				//'value' => 'prostoystatus0.prostoy_status_name',
				'value' => function($data) {
					
								$str = '';
					
								$arr_st = $data['prostoy_status'][0];
					
								if ($arr_st['id_prostoy_status'] == 1) {
									
									$str.= '<span style="color: red">'.$arr_st['prostoy_status_name'].'</span>';
									
								} elseif($arr_st['id_prostoy_status'] == 2) {
									$str.= '<span style="color: green">'.$arr_st['prostoy_status_name'].'</span>';
								} else {
									$str.= '<span style="">'.$arr_st['prostoy_status_name'].'</span>';
								}
					
					
								return $str;
							},
            ],
			
			[
				'attribute' => 'ostatok',
				'format' => 'html',
				'value' => function ($data) {
					
					
									$pmnts = $data['payments'];
					
									$str = '';
					
									foreach ($pmnts as $item) {
										
										$str.= '<a href="/admin/payments/view?id='.$item['id_payment'].'">Платеж №'.$item['id_payment'].' '.$item['prostoy_summa'].'рублей</a><br>'; 
										
									}
					
									return $str;	
								},
			],
			
			'comment:ntext',

            [
				'class' => 'yii\grid\ActionColumn',
				'visibleButtons' => [
									'update' => function ($model) {
										
										$id = $model->id_prostoy;
										
										if (\Yii::$app->user->can('admin', ['post' => $model])) {
											
											return true;
										} else {
											
											$model_logitems = Logitems::find()
																->where(
																	[
																		
																		'item_id' => $id,
																		'type_page' => 'prostoy',
																		'user_id' => Yii::$app->user->getId(),
																	])
																	->andWhere(['>=','date_create', date('Y-m-d', strtotime('-5 days'))])
																	->all();
											
											
											if (!empty($model_logitems)) {
												
												return true;
											} else {
												return false;
											}
											
										}
										
										
									},
									'delete' => function ($model) {
										$id = $model->id_prostoy;
										
										if (\Yii::$app->user->can('admin', ['post' => $model])) {
											
											return true;
										} else {
											
											$model_logitems = Logitems::find()
																->where(
																	[
																		
																		'item_id' => $id,
																		'type_page' => 'prostoy',
																		'user_id' => Yii::$app->user->getId(),
																	])
																	->andWhere(['>=','date_create', date('Y-m-d', strtotime('-5 days'))])
																	->all();
											
											
											if (!empty($model_logitems)) {
												
												return true;
											} else {
												return false;
											}
											
										}
									},
								],
			],
        ],
    ]); ?>
</div>




<?php

$this->registerJs('

$(document).ready(function(){
	$(\'#MyButton\').click(function(){

	var HotId = $(\'.grid-view\').yiiGridView(\'getSelectedRows\');
	
	
	$.ajax({
		type: \'POST\',
		url : \'/admin/prostoys/multipledelete\',
		data : {row_id: HotId},
		success : function() {
			
			$(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
		
		}
	});

	});
	
	
	
	$(\'#remove-all\').click(function(){
	
		$.ajax({
			type: \'POST\',
			url : \'/admin/prostoys/removeall\',
			//data : {row_id: HotId},
			success : function() {
				
				//$(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
			
			}
		});
	
	
	});
	
	
	
	
	
	
	
});', \yii\web\View::POS_READY);

?>
