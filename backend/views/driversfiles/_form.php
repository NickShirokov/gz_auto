<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Driversfiles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="driversfiles-form">

    <?php $form = ActiveForm::begin( ['options' => ['enctype' => 'multipart/form-data']]); ?>

	
	
	
	
	<?//= $form->field($model, 'id_file')->textInput() ?>
	
    <?= $form->field($model, 'id_driver')->dropdownList($arrDrivers, 
	
					[
						'options' => 
							[ 
								$_GET['id'] => ['Selected' => true],
							],
					]
					
						
						
						
						) ?>

    <?//= $form->field($model, 'file')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'file_f')->fileInput() ?>
	
	
    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
