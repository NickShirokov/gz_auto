<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Driversfiles */

$this->title = $model->id_file;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Файлы водителей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="driversfiles-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
		<?php
			if(
				Yii::$app->user->can('admin') 
				)  
			{
				echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_file], ['class' => 'btn btn-primary']);
				echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_file], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите удалить этот файл?'),
                'method' => 'post',
            ],
        ]) ;
			}
			
		?>	
	
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_file',
            'id_driver',
            'file',
            'comment:ntext',
        ],
    ]) ?>

</div>
