<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Driversfiles */

$this->title = Yii::t('app', 'Создать изображение для водителя');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Файлы водителей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driversfiles-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrDrivers' => $arrDrivers,
    ]) ?>

</div>
