<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Driversfiles */

$this->title = Yii::t('app', 'Обновить файл водителя: {name}', [
    'name' => $model->id_file,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Driversfiles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_file, 'url' => ['view', 'id' => $model->id_file]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="driversfiles-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrDrivers' => $arrDrivers,
    ]) ?>

</div>
