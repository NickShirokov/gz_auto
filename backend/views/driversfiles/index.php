<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use yii\grid\CheckboxColumn;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DriversfilesSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Файлы водителей');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driversfiles-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать файл водителя'), ['create'], ['class' => 'btn btn-success']) ?>
		
		
		<?php
		if(
			Yii::$app->user->can('admin')) {
			
			
				
				echo '<input type="button" class="btn btn-info" value="Удалить выбранные" id="MyButton" >
		
				';
				
			}
		?>
		
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			['class' => 'yii\grid\CheckboxColumn'],
            'id_file',
			[
				'attribute' => 'id_driver',
				'format' => 'html',
				'value' => function ($data) {
					
								//print_r ($data);
								
								$driver = $data['drivers'][0];
					
								$str = '';
					
								$str.= $driver->first_name. ' ' .$driver->last_name;
					
								return $str;
					
							},
            ],
			'file',
            'comment:ntext',

            [
				'class' => 'yii\grid\ActionColumn',
				'visibleButtons' => [
									'update' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
									'delete' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
								],
			],
        ],
    ]); ?>

</div>




<?php

$this->registerJs('

$(document).ready(function(){
	$(\'#MyButton\').click(function(){

	var HotId = $(\'.grid-view\').yiiGridView(\'getSelectedRows\');
	
	
	$.ajax({
		type: \'POST\',
		url : \'/admin/driversfiles/multipledelete\',
		data : {row_id: HotId},
		success : function() {
			
			$(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
		
		}
	});

	});
});', \yii\web\View::POS_READY);

?>
