<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Arendatypes */

$this->title = Yii::t('app', 'Обновить тип аренды: {name}', [
    'name' => $model->a_d_type,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Типы аренды '), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->a_d_type, 'url' => ['view', 'id' => $model->a_d_type]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="arendatypes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
