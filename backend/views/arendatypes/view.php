<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Arendatypes */

$this->title = $model->a_d_type;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Типы аренды'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="arendatypes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
		<?php
		if (
			Yii::$app->user->can('admin') ) {
			
				echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->a_d_type], ['class' => 'btn btn-primary']); 
			}
				
		?>
	
	
	
	
        <?// Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->a_d_type], ['class' => 'btn btn-primary']) ?>
        
		
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'a_d_type',
            'name_a_type',
        ],
    ]) ?>

</div>
