<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Arendatypes */

$this->title = Yii::t('app', 'Создать тип аренды');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Типы аренды - день, неделя месяц'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="arendatypes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
