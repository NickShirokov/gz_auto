<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contractsstatus */

$this->title = Yii::t('app', 'Create Contractsstatus');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contractsstatuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contractsstatus-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
