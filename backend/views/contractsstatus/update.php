<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contractsstatus */

$this->title = Yii::t('app', 'Update Contractsstatus: {name}', [
    'name' => $model->c_status_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contractsstatuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->c_status_id, 'url' => ['view', 'id' => $model->c_status_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="contractsstatus-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
