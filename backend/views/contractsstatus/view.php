<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Contractsstatus */

$this->title = $model->c_status_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Статусы договоров'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="contractsstatus-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
	
	
	<?php
			if(
				Yii::$app->user->can('admin') 
				)  
			{
				echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->c_status_id], ['class' => 'btn btn-primary']);
				echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->c_status_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ;
			}	
	
      ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'c_status_id',
            'c_status_name',
            'c_comment:ntext',
        ],
    ]) ?>

</div>
