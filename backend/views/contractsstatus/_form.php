<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contractsstatus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contractsstatus-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'c_status_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'c_comment')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
