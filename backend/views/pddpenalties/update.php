<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pddpenalties */

$this->title = Yii::t('app', 'Обновить штраф ПДД: {name}', [
    'name' => $model->id_shtraf,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Штрафы ПДД'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_shtraf, 'url' => ['view', 'id' => $model->id_shtraf]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="pddpenalties-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrContracts' => $arrContracts,
		'arrPddpenstatus' => $arrPddpenstatus,
		'arrPddpenstatusapi' => $arrPddpenstatusapi,
		'arrAutos' => $arrAutos,
		'arrDrivers' => $arrDrivers,
		'arrParams' => $arrParams,
    ]) ?>

</div>
