<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\models\Pddpenalties */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pddpenalties-form">

    <?php $form = ActiveForm::begin( ['options' => ['enctype' => 'multipart/form-data']]); ?>

	
	
	<?php 	
		echo $form->field($model, 'auto_id')->widget(Select2::classname(), [
									'data' => $arrAutos,
									'language' => 'ru',
									'options' => ['placeholder' => 'Select a state ...'],
									'pluginOptions' => [
										'allowClear' => true
									],
									
									'pluginEvents' => [
									   "select2:select" => 'function() { 
									   
											var auto_id = $(this).val();
													
													
													
													$.post(
														//"/admin/automobiles/lists?mark="+mark, 
														"/admin/pddpenalties/params?id="+auto_id, 
														function(data){
															
														
															//alert(data);
															
															$("select#pddpenalties-param_id").html(data);
															
															
														}
													
													);
													
													
													$.post(
														//"/admin/automobiles/lists?mark="+mark, 
														"/admin/pddpenalties/contracts?id="+auto_id, 
														function(data){
															
														
															//alert(data);
															
															$("select#pddpenalties-contract_id").html(data);
															
															
														}
													
													);
									   
									   
											   
									   
									   }',
									]
									
									
								]);
	
	
	?>
	
		
					
					
					
	<?= $form->field($model, 'param_id')->dropdownList($arrParams, ['prompt' => '']) ?>
	
	
	
	
	<?php 
	
	if ($_GET['contract_id']) :
	
	?>
	
	
	
	 <?= $form->field($model, 'contract_id')->dropdownList($arrContracts,
	 
	 
	 [
		'prompt' => '', 
		'value' => $_GET['contract_id'],
		
		'onchange' => '
										
											var contract_id = $(this).val();
										
										
											$.post(
														//"/admin/automobiles/lists?mark="+mark, 
														"/admin/pddpenalties/drivers?id="+contract_id, 
														function(data){
															
														
															//alert(data);
															
															$("select#pddpenalties-driver_id").html(data);
															
															
														}
													
													);
										
										
										',
		
		
	]
	 
	 
	 
	 
	 
	 ) ?>
	
	
	
	<?php else: ?>
	
	

	<?= $form->field($model, 'contract_id')->dropdownList($arrContracts,
	
	
									[
										'prompt' => '',
										'onchange' => '
										
											var contract_id = $(this).val();
										
										
											$.post(
														//"/admin/automobiles/lists?mark="+mark, 
														"/admin/pddpenalties/drivers?id="+contract_id, 
														function(data){
															
														
															//alert(data);
															
															$("select#pddpenalties-driver_id").html(data);
															
															
														}
													
													);
										
										
										',
									]
									
									
				) ?>
	
	
	<?php endif; ?>
	
	
	<?php 	
		echo $form->field($model, 'driver_id')->widget(Select2::classname(), [
									'data' => $arrDrivers,
									'language' => 'ru',
									'options' => ['placeholder' => 'Select a state ...'],
									'pluginOptions' => [
										'allowClear' => true
									],
									
									'pluginEvents' => [
									   "select2:select" => 'function() { 
									   
									   
									   
											   
									   
									   }',
									]
									
									
								]);
	
	
	?>
  
    <?= $form->field($model, 'n_post')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_post')->input('date') ?>

    <?= $form->field($model, 'date_narush')->input('date') ?>

    <?= $form->field($model, 'type_opl')->dropdownList($arrPddpenstatus,['prompt' => '']) ?>
    <?= $form->field($model, 'type_opl_api')->dropdownList($arrPddpenstatusapi,['prompt' => '']) ?>

    <?= $form->field($model, 'summa_opl')->textInput() ?>
   
   <?= $form->field($model, 'skidka_status')->dropdownList(['0' => 'Работает', '1' => 'Не работает']) ?>
	
		<?= $form->field($model, 'narushitel')->textInput() ?>
	
	
	
	<?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
	
	
	<?= $form->field($model, 'file_f')->fileInput() ?>
	
	
	
	
	
	<?php 
	
	
	
	if ($_GET['contract_id']) {
	
		$val_cntr = $_GET['contract_id'];
	
	} 
	
	
	?>
	
   


<h2>Выберите способ внесения денег и на что они должны быть расходаваны</h2>	
	
 <?= $form->field($modelPayments, 'pmnt_variant')->dropDownList(
									[
										
										
										'4' => 'Штраф ПДД (строго)',
									],
									
									[
										'onchange' => '
										
												
												
												
												$(".pmts-wr").find("input").val("");
												$(".pmts-wr").find("select").val("");
										
												var value_sel = $(this).val();
										
												$(".pmts-wr").hide();
										
												if (value_sel == 6) {
													$(".pmts-wr").show();
												} else {
													$(".payment-box-"+value_sel).show();
												}
												
												
										'
									]
									
									
						); 
					?>	

							

<div class="row">	
	
<?php 
	if ($modelPayments->date_payment): 
	?>
	<div class="col-md-4">
		<?= $form->field($modelPayments, 'date_payment')->input('date', ['required' => true]) ?>
	</div>
	
	<?php else: ?>
	<div class="col-md-4">
		<?= $form->field($modelPayments, 'date_payment')->input('date', ['required' => true, 'value' => date('Y-m-d')]) ?>
	</div>
	
	<?php endif; ?>


<div class="col-md-4">

    <?= $form->field($modelPayments, 'type')->dropDownList(
				[ 
					'Наличные' => 'Наличные', 
					'На карту' => 'На карту', 
					'Списано с диспетчерской' => 'Списано с диспетчерской', 
					'Бонус/Простили' => 'Бонус/Простили',
				], 
				[
					'prompt' => 'Выберите ',
					'onchange' => '
									
									if($(this).val() == "Бонус/Простили") {
										
										$("#payments-summa").val(0);
										
									} else {
										
										$("#payments-summa").val("");
									}
									
					
								',
				]
		)
	?>
</div>

<div class="col-md-4">
    <?= $form->field($modelPayments, 'summa')->textInput() ?>
</div>


<div class="col-md-12">


<?= $form->field($modelPayments, 'comission_type')->checkbox([
	'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
])?>

<?= $form->field($modelPayments, 'comission_summa')->textInput() ?>




</div>





<div style="clear:both"></div>








<div class="col-md-12">
    <?= $form->field($modelPayments, 'comment')->textarea(['rows' => 6]) ?>
</div>









</div>

	
	
	
	
	
	

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>









<?

if ($_GET['contract_id']) {


$this->registerJs(
'
$(document).ready(function(){
	

	var contract_id = $("#pddpenalties-contract_id").val();
										
	//Водитель
	
	$.post(
		//"/admin/automobiles/lists?mark="+mark, 
		"/admin/pddpenalties/drivers?id="+contract_id, 
		function(data){
				
			$("select#pddpenalties-driver_id").html(data);
					
		}
	
	);
	
	
	$.post(
		//"/admin/automobiles/lists?mark="+mark, 
		"/admin/pddpenalties/autos?id="+contract_id, 
		function(data){
				
			$("select#pddpenalties-auto_id").html(data);
					
			
			var auto_id = $("select#pddpenalties-auto_id").val();
			
			
			$.post(
				//"/admin/automobiles/lists?mark="+mark, 
				"/admin/pddpenalties/params?id="+auto_id, 
				function(data){
					
				
					//alert(data);
					
					$("select#pddpenalties-param_id").html(data);
					
					
				}
			
			);
			
					
					
		}
	
	);
	
	
	
});



'



);


}

?>








