<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use app\models\Logitems;

/* @var $this yii\web\View */
/* @var $model app\models\Pddpenalties */

$this->title = 'Штраф '.$model->id_shtraf.' №'.$model->n_post;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Штрафы ПДД'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pddpenalties-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
	
	<?php
			if(
				Yii::$app->user->can('admin') 
				)  
			{
				echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_shtraf], ['class' => 'btn btn-primary']);
				echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_shtraf], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите удалить этот штраф ПДД?'),
                'method' => 'post',
            ],
        ]);
			} else {
				
				
					$id = $model->id_shtraf;
										
				
					
					$model_logitems = Logitems::find()
										->where(
											[									
												'item_id' => $id,
												'type_page' => 'pdd',
												'user_id' => Yii::$app->user->getId(),
											])
											->andWhere(['>=','date_create', date('Y-m-d', strtotime('-5 days'))])
											->all();
						
						
					
					if (!empty($model_logitems)) {
						echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_shtraf], ['class' => 'btn btn-primary']);
						echo ' ';
						echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_shtraf], [
						'class' => 'btn btn-danger',
						'data' => [
							'confirm' => Yii::t('app', 'Вы действительно хотите удалить этот штраф ПДД?'),
							'method' => 'post',
						],
					]);
						
					} 
			
				
				
				
			}
			
		
		
		?>
	
	
		
		
		 <?= Html::a(Yii::t('app', 'Договор'), ['contracts/view', 'id' => $model->contract_id], ['class' => 'btn btn-primary']) ?>
		 <?= Html::a(Yii::t('app', 'Штрафы ПДД по этому договору'), ['contracts/pddshtrafs', 'id' => $model->contract_id], ['class' => 'btn btn-primary']) ?>
       
		
		
		<?php if (!empty($model->image_doc)): ?>
		
		<?= Html::a(Yii::t('app', 'Чек по оплате штрафа'), ['/uploads/'.$model->image_doc],  ['class' => 'btn btn-primary', 'target'=>'_blank',]) ?>
    
		<?php endif; ?>
		
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_shtraf',
            'contract_id',
            
            'n_post',
           
			
			[
				'attribute' => 'date_post',
				'format' => ['date', 'php:d.m.Y'],
			],
			
			[
				'attribute' => 'date_narush',
				'format' => ['date', 'php:d.m.Y'],
			],
			
			
          
            'type_opl',
			'type_opl_api',
            'summa_opl',
			'comment',
			
			
			
			
        ],
    ]) ?>

</div>
