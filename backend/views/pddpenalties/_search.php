<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PddpenaltiesSearchModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pddpenalties-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id_shtraf') ?>

    <?= $form->field($model, 'contract_id') ?>

    <?= $form->field($model, 'n_post') ?>

    <?= $form->field($model, 'date_post') ?>

    <?= $form->field($model, 'date_narush') ?>

    <?php // echo $form->field($model, 'type_opl') ?>

    <?php // echo $form->field($model, 'summa_opl') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
