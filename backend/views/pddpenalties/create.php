<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pddpenalties */

$this->title = Yii::t('app', 'Создать штраф ПДД');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Штрафы ПДД'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pddpenalties-create">


	<?php 
	
	if ($_GET['contract_id']) :
	
	?>

    <h1><?= Html::encode($this->title) ?> для договора <?= $_GET['contract_id'] ?></h1>

	
	<?php else: ?>
	
	<h1><?= Html::encode($this->title) ?></h1>
	<?php endif; ?>
	
    <?= $this->render('_form', [
        'model' => $model,
		
		'arrContracts' => $arrContracts,
		'arrPddpenstatus' => $arrPddpenstatus,
		'arrPddpenstatusapi' => $arrPddpenstatusapi,
		'arrAutos' => $arrAutos,
		'arrDrivers' => $arrDrivers,
		'arrParams' => $arrParams,
    ]) ?>

</div>
