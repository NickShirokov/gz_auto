<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Owners */

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Owners */
/* @var $form yii\widgets\ActiveForm */



$this->title = Yii::t('app', 'Импортировать штрафы ПДД');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Штрафы ПДД'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>




<div class="owners-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

	
	
	<div class="row">
	
	<h2>Штрафы успешно обновлены!</h2>
	<!--
	<div class="col-md-12">
    <?= $form->field($model, 'file_upload')->fileInput() ?>

	</div>
	
	

	 
	 
	<div class="col-md-12"> 
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Загрузить'), ['class' => 'btn btn-success']) ?>
    </div>
	</div>
	-->
	
	</div>
    <?php ActiveForm::end(); ?>

</div>

