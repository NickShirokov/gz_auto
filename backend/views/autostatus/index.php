<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use kartik\date\DatePicker;

use kartik\export\ExportMenu;


/* @var $this yii\web\View */
/* @var $searchModel common\models\AutostatusModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Статусы автомобилей');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="autostatus-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать статус автомобиля'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

	
	
<?php 

$gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],

            [
				'attribute' => 'id_status',
				'format' => 'html',
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/autostatus/view?id='.$data['id_status'].'">'.$data['id_status'].'</a>';
					
								return $str;
					
							},
				
			],
			[
				'attribute' => 'status_name',
				'format' => 'html',
				
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/autostatus/view?id='.$data['id_status'].'">'.$data['status_name'].'</a>';
					
								return $str;
					
							},
				
				
				
			],
            [
				'class' => 'yii\grid\ActionColumn',
				'template' => '{view} {update}',
				
				'visibleButtons' => [
									'update' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
									'delete' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
								],
			],
        ];


// Renders a export dropdown menu
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns
]);


?>	
	
	
	
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		
		'pager' => [
			'class' => \kop\y2sp\ScrollPager::className(),
			'container' => '.grid-view tbody',
			'item' => 'tr',
			'paginationSelector' => '.grid-view .pagination',
			'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',
		],
		
        'columns' => $gridColumns,
    ]); ?>
    <?php //Pjax::end(); ?>
</div>
