<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Autostatus */

$this->title = Yii::t('app', 'Обновить статус машины: {name}', [
    'name' => $model->id_status,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Статусы машин'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_status, 'url' => ['view', 'id' => $model->id_status]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="autostatus-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
