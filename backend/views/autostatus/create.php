<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Autostatus */

$this->title = Yii::t('app', 'Создать статус автомобиля');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Статусы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="autostatus-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
