<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ourpenaltiesstatus */

$this->title = Yii::t('app', 'Создать статус для наших штрафов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Статусы наших штрафов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ourpenaltiesstatus-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
