<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ourpenaltiesstatus */

$this->title = Yii::t('app', 'Обновить статус нашего штрафа: {name}', [
    'name' => $model->status_o_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Статусы наших штрафов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->status_o_id, 'url' => ['view', 'id' => $model->status_o_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="ourpenaltiesstatus-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
