<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Ourpenaltiesstatus */

$this->title = $model->status_o_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Статусы наших штрафов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ourpenaltiesstatus-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>

		<?php
			if(
				Yii::$app->user->can('admin') 
				)  
			{
				
				echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->status_o_id], ['class' => 'btn btn-primary']);
				
			}

		  ?>
      
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'status_o_id',
            'status_o_name',
        ],
    ]) ?>

</div>
