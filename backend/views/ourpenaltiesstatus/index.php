<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\OurpenaltiesstatusSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Статусы наших штрафов');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ourpenaltiesstatus-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать статус'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

			[
				'attribute' => 'status_o_id',
				'format' => 'html',
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/ourpenaltiesstatus/update?id='.$data['status_o_id'].'">'.$data['status_o_id'].'</a>';
					
								return $str;
					
							},
				
            ],
			
			
			[
				'attribute' => 'status_o_name',
				'format' => 'html',
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/ourpenaltiesstatus/update?id='.$data['status_o_id'].'">'.$data['status_o_name'].'</a>';
					
								return $str;
					
							},
				
			],
			
			
			
            [
				'class' => 'yii\grid\ActionColumn',
				'template' => '{view} {update}',
				'visibleButtons' => [
									'update' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
									'delete' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
								],
			],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
