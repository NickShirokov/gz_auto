<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pddpenstatusapi */

$this->title = Yii::t('app', 'Create Pddpenstatusapi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pddpenstatusapis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pddpenstatusapi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
