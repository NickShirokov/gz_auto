<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pddpenstatusapi */

$this->title = Yii::t('app', 'Update Pddpenstatusapi: {name}', [
    'name' => $model->p_status_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pddpenstatusapis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->p_status_id, 'url' => ['view', 'id' => $model->p_status_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pddpenstatusapi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
