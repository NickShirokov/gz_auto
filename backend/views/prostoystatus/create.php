<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Prostoystatus */

$this->title = Yii::t('app', 'Create Prostoystatus');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Статусы простоев'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prostoystatus-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
