<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


use vova07\imperavi\Widget;


/* @var $this yii\web\View */
/* @var $model app\models\Templatecontractstext */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="templatecontractstext-form">

    <?php $form = ActiveForm::begin(); ?>

   
	  
	  <?= 
	  
	  
	  
	   $form->field($model, 'text1')->widget(Widget::className(), [
			'settings' => [
				'lang' => 'ru',
				'minHeight' => 200,
				'plugins' => [
					'clips',
					'fullscreen',
				],
				'clips' => [
					['Lorem ipsum...', 'Lorem...'],
					['red', '<span class="label-red">red</span>'],
					['green', '<span class="label-green">green</span>'],
					['blue', '<span class="label-blue">blue</span>'],
				],
			],
		]);
	  
	  
	  
	  ?>
	  
	  
	  
	   <?= 
	  
	  
	  
	   $form->field($model, 'text2')->widget(Widget::className(), [
			'settings' => [
				'lang' => 'ru',
				'minHeight' => 200,
				'plugins' => [
					'clips',
					'fullscreen',
				],
				'clips' => [
					['Lorem ipsum...', 'Lorem...'],
					['red', '<span class="label-red">red</span>'],
					['green', '<span class="label-green">green</span>'],
					['blue', '<span class="label-blue">blue</span>'],
				],
			],
		]);
	  
	  
	  
	  ?>
	  
	  
	   <?= 
	  
	  
	  
	   $form->field($model, 'text3')->widget(Widget::className(), [
			'settings' => [
				'lang' => 'ru',
				'minHeight' => 200,
				'plugins' => [
					'clips',
					'fullscreen',
				],
				'clips' => [
					['Lorem ipsum...', 'Lorem...'],
					['red', '<span class="label-red">red</span>'],
					['green', '<span class="label-green">green</span>'],
					['blue', '<span class="label-blue">blue</span>'],
				],
			],
		]);
	  
	  
	  
	  ?>
	  
	  
	   <?= 
	  
	  
	  
	   $form->field($model, 'text4')->widget(Widget::className(), [
			'settings' => [
				'lang' => 'ru',
				'minHeight' => 200,
				'plugins' => [
					'clips',
					'fullscreen',
				],
				'clips' => [
					['Lorem ipsum...', 'Lorem...'],
					['red', '<span class="label-red">red</span>'],
					['green', '<span class="label-green">green</span>'],
					['blue', '<span class="label-blue">blue</span>'],
				],
			],
		]);
	  
	  
	  
	  ?>
	  
	  
	   <?= 
	  
	  
	  
	   $form->field($model, 'text5')->widget(Widget::className(), [
			'settings' => [
				'lang' => 'ru',
				'minHeight' => 200,
				'plugins' => [
					'clips',
					'fullscreen',
				],
				'clips' => [
					['Lorem ipsum...', 'Lorem...'],
					['red', '<span class="label-red">red</span>'],
					['green', '<span class="label-green">green</span>'],
					['blue', '<span class="label-blue">blue</span>'],
				],
			],
		]);
	  
	  
	  
	  ?>
	  
	
	


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
