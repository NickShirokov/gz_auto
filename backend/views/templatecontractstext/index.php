<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TemplatecontractstextModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Шаблоны для текста договора');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="templatecontractstext-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать шаблон для текста договора'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

			[
				'attribute' => 'id_text',
				 'contentOptions' => ['style' => 'width:50px; white-space: normal;'],
            ],
			[
				'attribute' => 'text1',
				'format' => 'html',
				 'contentOptions' => ['style' => 'width:200px; white-space: normal;'],
            ],
			
			
			[
				'attribute' => 'text2',
				'format' => 'html',
				 'contentOptions' => ['style' => 'width:200px; white-space: normal;'],
            ],
			
			[
				'attribute' => 'text3',
				'format' => 'html',
				 'contentOptions' => ['style' => 'width:200px; white-space: normal;'],
            ],
			
			[
				'attribute' => 'text4',
				'format' => 'html',
				 'contentOptions' => ['style' => 'width:200px; white-space: normal;'],
            ],
			
			[
				'attribute' => 'text5',
				'format' => 'html',
				 'contentOptions' => ['style' => 'width:200px; white-space: normal;'],
            ],
            [
				'class' => 'yii\grid\ActionColumn',
				'template' => '{view} {update}',
				'visibleButtons' => [
									'update' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
									'delete' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
								],
			],
        ],
    ]); ?>
</div>
