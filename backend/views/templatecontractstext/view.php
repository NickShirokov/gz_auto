<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Templatecontractstext */

$this->title = $model->id_text;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Шаблоны для текста договора'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="templatecontractstext-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
		<?php
			if(
				Yii::$app->user->can('admin') 
				)  
			{ 
				echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_text], ['class' => 'btn btn-primary']);
			
			}
		?>
	
      
        
		
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_text',
			
			[
				'attribute' => 'text1',
				'format' => 'html',
            ],
			
			[
				'attribute' => 'text2',
				'format' => 'html',
            ],
			
			
			[
				'attribute' => 'text3',
				'format' => 'html',
            ],
			
			
			[
				'attribute' => 'text4',
				'format' => 'html',
            ],
			
			
			[
				'attribute' => 'text5',
				'format' => 'html',
            ],
			
			
        ],
		
    ]) ?>

</div>
