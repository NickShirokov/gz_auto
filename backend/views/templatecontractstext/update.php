<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Templatecontractstext */

$this->title = Yii::t('app', 'Обновить шблон для текста договора: {name}', [
    'name' => $model->id_text,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Шаблоны для текста договора'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_text, 'url' => ['view', 'id' => $model->id_text]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="templatecontractstext-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
