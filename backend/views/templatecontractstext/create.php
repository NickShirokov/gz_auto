<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Templatecontractstext */

$this->title = Yii::t('app', 'Создать шаблон для текста договора');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Шаблоны для текста договора'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="templatecontractstext-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
