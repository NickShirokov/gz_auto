<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pddpenstatus */

$this->title = $model->p_status_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pddpenstatuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pddpenstatus-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
		<?php
			if(
				Yii::$app->user->can('admin') 
				)  
			{ 
				echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->p_status_id], ['class' => 'btn btn-primary']);
				echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->p_status_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]);
			}
	?>
	
	
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'p_status_id',
            'p_status_name',
        ],
    ]) ?>

</div>
