<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\PddpenstatusSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Статусы штрафов ПДД');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pddpenstatus-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать статус штрафа ПДД'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		
		
		'pager' => [
			'class' => \kop\y2sp\ScrollPager::className(),
			'container' => '.grid-view tbody',
			'item' => 'tr',
			'paginationSelector' => '.grid-view .pagination',
			'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',
		],
		
		
		
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

         	
			
			[
				'attribute' => 'p_status_id',
				'format' => 'html',
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/pddpenstatus/update?id='.$data['p_status_id'].'">'.$data['p_status_id'].'</a>';
					
								return $str;
					
							},
				
            ],
			
			
			[
				'attribute' => 'p_status_name',
				'format' => 'html',
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/pddpenstatus/update?id='.$data['p_status_id'].'">'.$data['p_status_name'].'</a>';
					
								return $str;
					
							},
				
            ],
			
		

            [
				'class' => 'yii\grid\ActionColumn',
				'visibleButtons' => [
									'update' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
									'delete' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
								],
			],
        ],
    ]); ?>
    <?php //Pjax::end(); ?>
</div>
