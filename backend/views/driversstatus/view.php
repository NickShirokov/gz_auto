<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Driversstatus */

$this->title = $model->id_status;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Статусы водителей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="driversstatus-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
		<?php
			if(
				Yii::$app->user->can('admin') 
				)  
			{ 
				echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_status], ['class' => 'btn btn-primary']);
			
			}
			
		?>	 
       
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_status',
            'status_name',
        ],
    ]) ?>

</div>
