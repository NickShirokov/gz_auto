<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;



use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DriversstatusModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Статусы водителей');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driversstatus-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать статус водителя'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	
<?php 







// Renders a export dropdown menu
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns
]);


?>	
	
	

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

			
			[
				'attribute' =>'id_status',
				'format' => 'html',
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/driversstatus/update?id='.$data['id_status'].'">'.$data['id_status'].'</a>';
					
								return $str;
					
							},
			],
			
			
			[
				'attribute' =>'status_name',
				'format' => 'html',
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/driversstatus/update?id='.$data['id_status'].'">'.$data['status_name'].'</a>';
					
								return $str;
					
							},
			],
			
			

			
			
			
            [
				'class' => 'yii\grid\ActionColumn',
			
				'template' => '{view} {update}',
				'visibleButtons' => [
									'update' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
									'delete' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
								],
			
			],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
