<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Driversstatus */

$this->title = Yii::t('app', 'Создать статус водителя');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Статусы водителей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driversstatus-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
