<?php

use yii\helpers\Html;
use yii\grid\GridView;


use yii\widgets\ActiveForm;

use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PutevielistiSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Путевые листы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="putevielisti-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	
	
	<p>
        <?= Html::a(Yii::t('app', 'На неделю'), ['week'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'На месяц'), ['month'], ['class' => 'btn btn-primary']) ?>
    </p>
	
	
 
	

	<?php if ($model_contracts): ?>

	
		<h3>Форма для совместной печати</h3>
	
		<?php $form = ActiveForm::begin(['action' => 'put']); ?>

		<table class="table table-striped table-bordered">
		
		<thead>
		
		<th>Договор</th>
		<th>ФИО</th>
		<th>Машина</th>
		<th>Печатать для договора</th>
		<th>С датами/Без</th>
		
		</thead>
		
		<tbody>
		
		
		
		
		
		<?php foreach ($model_contracts as $item): ?>
		
		
			
		
			<tr>
			
			<td>
				<?php echo $item->id_contract; ?>
			</td>
			
			
			<td>
			
				<?php 
				
				
				echo $item['drivers'][0]['first_name']. ' '. $item['drivers'][0]['last_name'];
				
				
				?>
			
			</td>
			<td>
			
				<?php 
				
				
				echo $item['automobiles'][0]['mark']. ' '. $item['automobiles'][0]['model'].' ' . $item['automobiles'][0]['gosnomer'];
				
				
				?>
			
			
			</td>
			<td>
				<input class="" type="checkbox" name="test[<?php echo $item->id_contract; ?>][name]" value="<?php echo $item->id_contract; ?>">
			
			</td>
			
			<td>
			
			<?php 
			if ($item->putevoy_type) :
			
			?>
			
				<input type="checkbox" checked name="test[<?php echo $item->id_contract; ?>][type]" value="1">
			
			<?php else: ?>
			
				<input type="checkbox" name="test[<?php echo $item->id_contract; ?>][type]" value="1">
			
			
			<?php endif; ?>
			</td>
			</tr>
	 
		<?php endforeach; ?>
	 
	 
		</tbody>
	 
		</table>
	 
	 
		<div style="clear: both;">
	 
		<div class="form-group">
			<?= Html::submitButton(Yii::t('app', 'Печать'), ['class' => 'btn btn-success']) ?>
		</div>

		<?php ActiveForm::end(); ?>
	
	
	<?php endif; ?>
	
	
	
	<p>
        <?= Html::a(Yii::t('app', 'Создать путевой лист'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


			[
				'attribute' => 'id_putevoy',
				'format' => 'html',
				'value' => function($data) {
					
					
								$str = '';
								
								$str.= '<a href="/admin/putevielisti/view?id='.$data['id_putevoy'].'">'.$data['id_putevoy'].'</a>';
					
								return $str;
					
							},
			],


			[
				'attribute' => 'gosnomer',
				'label' => 'Машина (госномер)',
				'format' => 'html',
				'value' => function($data) {
					
								//print_r ($data);
								
								$str = '';
								$str.= '<a href="/admin/automobiles/view?id='.$data['automobiles'][0]['id_auto'].'">'.$data['automobiles'][0]['mark']. 
											' '. $data['automobiles'][0]['model']. ' '
											. $data['automobiles'][0]['gosnomer'].'</a>';
								
								return $str;
					
							},
            ],
			
			[
				'attribute' =>'last_name',
				'format' => 'html',
				'label' => 'Водитель (фамилия)',
				'value' => function($data) {
					
								//print_r ($data);
								
								$str = '';
								$str.= '<a href="/admin/drivers/view?id='.$data['drivers'][0]['id_driver'].'">'.$data['drivers'][0]['first_name']. 
											' '. $data['drivers'][0]['last_name'].'</a>';
								
								return $str;
					
							},
            ],
			
			
			
			
			[   
				'attribute' => 'date_start',
                'value' => 'date_start',
                'format' => ['date', 'php:d.m.Y'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'PutevielistiModelSearch[date_start]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['PutevielistiModelSearch']['date_start'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],
			
			
			[   
				'attribute' => 'date_end',
                'value' => 'date_end',
                'format' => ['date', 'php:d.m.Y'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'PutevielistiModelSearch[date_end]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['PutevielistiModelSearch']['date_end'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],
	
			
	
			
			[
				'attribute' =>'put_type',
				'format' => 'html',
				'filter' => $arrTypes,
				'value' => function ($data) {
					
								$str = '';
						
								if ($data['put_type'] == 1) {
									$str.= 'С проставлением дат';
								} else {
									
									$str.= 'Без дат';
								}
					
					
								return $str;
							}
			],
			
			[
			
				'attribute' => 'link',
				'format' => 'html',
				
				'value' => function($data) {
					
								
								$str = '';
						
								
								$str.= '<a href="/admin/putevielisti/putevoy?id='.$data['id_putevoy'].'">Печать</a>';
					
					
								return $str;

							},
			],
			
			
			
            [
				'class' => 'yii\grid\ActionColumn',
				'visibleButtons' => [
									'update' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
									'delete' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
								],
			],
        ],
    ]); ?>
</div>
