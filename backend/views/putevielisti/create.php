<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Putevielisti */

$this->title = Yii::t('app', 'Создать путевой лист');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Путевые листы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="putevielisti-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrAutos' => $arrAutos,
		'arrDrivers' => $arrDrivers,
    ]) ?>

</div>
