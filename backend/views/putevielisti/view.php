<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Putevielisti */

$this->title = $model->id_putevoy;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Putevielistis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="putevielisti-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_putevoy], ['class' => 'btn btn-primary']) ?>
     
		 <?= Html::a(Yii::t('app', 'Печать'), ['putevoy', 'id' => $model->id_putevoy], ['class' => 'btn btn-primary']) ?>
      
	 
		<?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_putevoy], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите удалить путевой лист?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_putevoy',

            'auto_id',
            'driver_id',
            'date_start',
            'date_end',
            'put_type',
        ],
    ]) ?>

</div>
