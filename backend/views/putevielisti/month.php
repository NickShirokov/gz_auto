<?php

use yii\helpers\Html;
use yii\grid\GridView;


use yii\widgets\ActiveForm;

use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PutevielistiSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Путевые листы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="putevielisti-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	
	
	<p>
		<?= Html::a(Yii::t('app', 'Все путевые'), ['index'], ['class' => 'btn btn-success']) ?>
	
        <?= Html::a(Yii::t('app', 'На неделю'), ['week'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'На месяц'), ['month'], ['class' => 'btn btn-primary']) ?>
    </p>
	
	
 
	

	<?php if ($model_contracts): ?>

	
		<h3>Форма для совместной печати</h3>
	
		<h4>
		
		<?php 
		
		echo date('d.m.Y').' - '.date('d.m.Y', strtotime(date("d.m.Y")." + 30 days")). ' Без дат';
		
		?>
		
		</h4>
	
	
	
		<?php $form = ActiveForm::begin(['action' => 'put']); ?>

		<table class="table table-striped table-bordered">
		
		<thead>
		
		<th>Договор</th>
		<th>ФИО</th>
		<th>Машина</th>
		<th>Печатать для договора</th>
		<th>С датами/Без</th>
		
		</thead>
		
		<tbody>
		
		
		
		
		
		<?php foreach ($model_contracts as $item): ?>
		
		
		
			<tr>
			
			<td>
				<?php echo $item->id_contract; ?>
			</td>
			
			
			<td>
			
				<?php 
				
				
				echo $item['drivers'][0]['first_name']. ' '. $item['drivers'][0]['last_name'];
				
				
				?>
			
			</td>
			<td>
			
				<?php 
				
				
				echo $item['automobiles'][0]['mark']. ' '. $item['automobiles'][0]['model'].' ' . $item['automobiles'][0]['gosnomer'];
				
				
				?>
			
			
			</td>
			
				
			<? $ch = '' ?>
			
			<?php if ($item->putevoy_type != '0'): ?>
						<? $ch = 'checked'; ?>
				<?php endif; ?>
			
			<td>
				<input class="form-control" <?= $ch ?> type="checkbox" name="test[<?php echo $item->id_contract; ?>][name]" value="<?php echo $item->id_contract; ?>">
			
			</td>
			
				
			
			<td>
			
			
			<?= Html::a(Yii::t('app', 'Печать'), ['putik', 'contract_id' => $item->id_contract, 'put_type' => 2], ['class' => 'btn btn-primary']) ?>
			<input type="hidden" name="test[<?php echo $item->id_contract; ?>][type]" value="1">
			
			</td>
			</tr>
	 
	 
			
	 
		<?php endforeach; ?>
	 
	 
		</tbody>
	 
		</table>
	 
	 
		<div style="clear: both;">
	 
		<div class="form-group">
			<?= Html::submitButton(Yii::t('app', 'Печать'), ['class' => 'btn btn-success']) ?>
		</div>

		<?php ActiveForm::end(); ?>
	
	
	<?php endif; ?>
	

</div>
