<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\Putevielisti */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="putevielisti-form">

    <?php $form = ActiveForm::begin(); ?>


	
		
	
<?php 	
		echo $form->field($model, 'auto_id')->widget(Select2::classname(), [
									'data' => $arrAutos,
									'language' => 'ru',
									'options' => ['placeholder' => 'Select a state ...'],
									'pluginOptions' => [
										'allowClear' => true,
										'initialize' => true,
									],
									
									'pluginEvents' => [
									   "select2:select" => 'function() { 
									   
									   
											   
									   
									   }',
									]
									
									
								]);
	
	
	?>
	

	
		
	
<?php 	
		echo $form->field($model, 'driver_id')->widget(Select2::classname(), [
									'data' => $arrDrivers,
									'language' => 'ru',
									'options' => ['placeholder' => 'Select a state ...'],
									'pluginOptions' => [
										'allowClear' => true,
										'initialize' => true,
									],
									
									'pluginEvents' => [
									   "select2:select" => 'function() { 
									   
									   
											   
									   
									   }',
									]
									
									
								]);
	
	
	?>
	



   
    <?= $form->field($model, 'date_start')->input('date') ?>

   
	<?= $form->field($model, 'date_end')->input( 'date' ) ?>

    <?= $form->field($model, 'put_type')->dropDownList([ 1 => 'с проставлением дат', 2 => 'без дат', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
