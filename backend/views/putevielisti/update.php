<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Putevielisti */

$this->title = Yii::t('app', 'Обновить путевой: {name}', [
    'name' => $model->id_putevoy,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Путевые листы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_putevoy, 'url' => ['view', 'id' => $model->id_putevoy]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="putevielisti-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
				'arrAutos' => $arrAutos,
			'arrDrivers' => $arrDrivers,
    ]) ?>

</div>
