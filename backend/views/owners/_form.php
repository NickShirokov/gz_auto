<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Owners */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="owners-form">

    <?php $form = ActiveForm::begin(); ?>

	
	
	<div class="row">
	
	<div class="col-md-4">
    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

	</div>
	
	
	<div class="col-md-4">
	
    <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true]) ?>

	</div>
	
	
	<div class="col-md-4">
					
    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

	</div>
	
	<div class="col-md-6">
	
    <?= $form->field($model, 'p_series')->textInput(['maxlength' => true]) ?>

	</div>
	
	<div class="col-md-6">
    <?= $form->field($model, 'p_number')->textInput(['maxlength' => true]) ?>

	</div>
	
	
	<div class="col-md-12">
    <?= $form->field($model, 'p_who')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'address_reg')->textarea(['rows' => 6]) ?>

	</div>
	
	<div class="col-md-4">
    <?= $form->field($model, 'phone_1')->textInput(['maxlength' => true]) ?>

	</div>
    
	<div class="col-md-4">
	<?= $form->field($model, 'phone_2')->textInput(['maxlength' => true]) ?>

	</div>
	
	<div class="col-md-4">
    <?= $form->field($model, 'phone_3')->textInput(['maxlength' => true]) ?>
	
	</div>
	
	
	
	
	 
	 
	 
	<div class="col-md-12"> 
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>
	</div>
	
	
	</div>
    <?php ActiveForm::end(); ?>

</div>
