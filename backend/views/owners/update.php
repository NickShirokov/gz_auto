<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Owners */

$this->title = Yii::t('app', 'Обновить владельца: {name} ' . $model->first_name . ' ' .$model->middle_name. ' '.$model->last_name, [
    'name' => $model->id_owner,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Владельцы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' =>  $model->first_name . ' ' .$model->middle_name. ' '.$model->last_name, 'url' => ['view', 'id' => $model->id_owner]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="owners-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
