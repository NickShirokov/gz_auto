<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;


use yii\grid\CheckboxColumn;

use kartik\export\ExportMenu;
use kartik\form\ActiveForm;
use kartik\builder\TabularForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OwnersSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Владельцы авто');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
/*
$form = ActiveForm::begin();
echo TabularForm::widget([
    'dataProvider'=>$dataProvider,
    'form'=>$form,
    'attributes'=>$model->formAttribs,
    'gridSettings'=>['condensed'=>true]
]);
// Add other fields if needed or render your submit button
echo '<div class="text-right">' . 
     Html::submitButton('Submit', ['class'=>'btn btn-primary']) .
     '<div>';
ActiveForm::end();

*/
?>


<div class="owners-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать владельца'), ['create'], ['class' => 'btn btn-success']) ?>
        <?// Html::a(Yii::t('app', 'Загрузить владельцев'), ['import'], ['class' => 'btn btn-success']) ?>
		
		
		
		<?php
		if(
			Yii::$app->user->can('admin')) 
			
			
				echo  Html::a(Yii::t('app', 'Загрузить владельцев'), ['import'], ['class' => 'btn btn-success']) .
			
				' <input type="button" class="btn btn-info" value="Удалить выбранные" id="MyButton" >
		
				';
		?>
		
		
		
		
		
		
    </p>
	
	
	
	
<?php 


$gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],
			['class' => 'yii\grid\CheckboxColumn'],
            
			[
				'attribute'=> 'id_owner',
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/owners/view?id='.$data['id_owner'].'">'.$data['id_owner'].'</a>';
					
								return $str;
					
							},
				'format' => 'html',
			],
			
			
			[
				'attribute'=> 'first_name',
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/owners/view?id='.$data['id_owner'].'">'.$data['first_name'].'</a>';
					
								return $str;
					
							},
				'format' => 'html',
			],
			
			
			[
				'attribute'=> 'middle_name',
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/owners/view?id='.$data['id_owner'].'">'.$data['middle_name'].'</a>';
					
								return $str;
					
							},
				'format' => 'html',
			],
			
			[
				'attribute'=> 'last_name',
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/owners/view?id='.$data['id_owner'].'">'.$data['last_name'].'</a>';
					
								return $str;
					
							},
				'format' => 'html',
			],
			
			
			
			'p_series',
            'p_number',
            //'p_who:ntext',
            //'address_reg:ntext',
            //'phone_1',
            //'phone_2',
            //'phone_3',

            [
				'class' => 'yii\grid\ActionColumn',
				'visibleButtons' => [
									'update' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
									'delete' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
								],
			],
        ];

// Renders a export dropdown menu
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns
]);

?>	
	
	
	
	
	
	

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]); ?>
    <?php //Pjax::end(); ?>
</div>








<?php

$this->registerJs('

$(document).ready(function(){
	$(\'#MyButton\').click(function(){

	var HotId = $(\'#w4\').yiiGridView(\'getSelectedRows\');
	
	
	$.ajax({
		type: \'POST\',
		url : \'/admin/owners/multipledelete\',
		data : {row_id: HotId},
		success : function() {
			
			$(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
		
		}
	});

	});
});', \yii\web\View::POS_READY);

?>











