<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Owners */

$this->title = $model->id_owner . ' '. $model->first_name  . ' ' . $model->middle_name . ' ' . $model->last_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Владельцы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="owners-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
		<?php
			if(
				Yii::$app->user->can('admin') 
				)  
			{ 
				echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_owner], ['class' => 'btn btn-primary']);
				echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_owner], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы точно хотите удалить этого владельца авто?'),
                'method' => 'post',
            ],
        ]);
			}
		?>
	
       
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_owner',
            'first_name',
            'middle_name',
            'last_name',
            'p_series',
            'p_number',
            'p_who:ntext',
            'address_reg:ntext',
            'phone_1',
            'phone_2',
            'phone_3',
			
        ],
    ]) ?>

</div>
