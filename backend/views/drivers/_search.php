<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DriversModelSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="drivers-model-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id_driver') ?>

    <?= $form->field($model, 'first_name') ?>

    <?= $form->field($model, 'middle_name') ?>

    <?= $form->field($model, 'last_name') ?>

    <?= $form->field($model, 'date_birth') ?>

    <?php // echo $form->field($model, 'p_series') ?>

    <?php // echo $form->field($model, 'p_number') ?>

    <?php // echo $form->field($model, 'lic_series') ?>

    <?php // echo $form->field($model, 'lic_number') ?>

    <?php // echo $form->field($model, 'main_summa') ?>

    <?php // echo $form->field($model, 'driver_cat') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'penalty_type') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
