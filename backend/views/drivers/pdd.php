<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use backend\assets\AppAsset;

use yii\widgets\Pjax;

use kartik\date\DatePicker;

use app\models\Logitems;

use yii\grid\CheckboxColumn;



use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $model common\models\DriversModel */

$this->title = 'Штрафы ПДД по водителю '.$model->id_driver . ' ' 
. $model->last_name . ' ' 
. $model->first_name . ' '
. $model->middle_name . ' ';



$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Водители'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);





?>



<div class="drivers-model-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
	
	
	
	
		<ul class="nav nav-tabs">
			<li role="presentation"><a href="#" id="karta-vod" class="btn">Карточка</a>
		</li>
		
		
		<li role="presentation"><?= Html::a(Yii::t('app', 'Сводка по водителю'), ['svodka', 'id' => $model->id_driver],  ['class' => 'btn',]) ?>
		</li>
	  
	  
	  <li role="presentation"><?= Html::a(Yii::t('app', 'Штрафы ПДД'), ['pdd', 'id' => $model->id_driver],  ['class' => 'btn',]) ?>
		</li>
	  
	  
	  
	  <li role="presentation"><?= Html::a(Yii::t('app', 'Платежи'), ['payments', 'id' => $model->id_driver],  ['class' => 'btn',]) ?>
		</li>
	  
	  
	</ul>
	
	</p>
	
	<p>
	
		<?php 
		
		if(
				Yii::$app->user->can('admin') 
				)  
			{ 
			
				echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_driver], ['class' => 'btn btn-primary']);
				echo ' ';
				echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_driver], [
					'class' => 'btn btn-danger',
					'data' => [
						'confirm' => Yii::t('app', 'Вы действительно хотите удалить этого водителя?'),
						'method' => 'post',
					],
				]);
			} else {
				
				$id = $model->id_driver;
										
				
					
				$model_logitems = Logitems::find()
									->where(
										[									
											'item_id' => $id,
											'type_page' => 'driver',
											'user_id' => Yii::$app->user->getId(),
										])
										->andWhere(['>=','date_create', date('Y-m-d', strtotime('-5 days'))])
										->all();
					
					
				
				if (!empty($model_logitems)) {
					echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_driver], ['class' => 'btn btn-primary']);
					echo ' ';
					echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_driver], [
						'class' => 'btn btn-danger',
						'data' => [
							'confirm' => Yii::t('app', 'Вы действительно хотите удалить этого водителя?'),
							'method' => 'post',
						],
					]);
					
				} 
				
				
			}
		
		
		?>
	
       
		<?= Html::a(Yii::t('app', 'Добавить сканы документов'), ['driversfiles/create', 'id' => $model->id_driver], ['class' => 'btn btn-primary']) ?>
       
		
		
			
    </p>

	<p>
	<?php 
	
		if (!empty($model_contracts)) {
		
			echo '<h3>Договора водителя</h3>';
			echo '<p>';
			foreach ($model_contracts as $contract) {
				
				echo Html::a(Yii::t('app', 'Договор '.$contract->id_contract), ['contracts/view', 'id' => $contract->id_contract], ['class' => 'btn btn-primary']);		
				
			}
			
			echo '</p>';
			
		}	
	?>
	
	
	
	
	
	
	
	
	
	</p>
	
	
	
	
	
	
	
	
	<?php echo \nterms\pagesize\PageSize::widget(); ?>	
	
<?php 



$gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],
			['class' => 'yii\grid\CheckboxColumn'],
			
			'driver_id',
			
			[
				'attribute' => 'id_shtraf',
				'format' => 'html',
				'value' => function($data) {
					
					
					
					
								$str = '';
								$str.= '<a href="/admin/pddpenalties/view?id='.$data->id_shtraf.'">'.$data->id_shtraf.'</a>';
					
					
								return $str;
							}
				
				
			],
			[
				'attribute' => 'contract_id',
				'format' => 'html',
				'value' => function($data) {
					
					
					
					
								$str = '';
								$str.= '<a href="/admin/contracts/view?id='.$data->contract_id.'">Договор '.$data->contract_id.'</a>';
					
					
								return $str;
							}
			],
			
			
			
			[
				'attribute' => 'sts',
				'value' => function($data) {
							

					

							
							$str ='';
							
				
							$str.=$data['autoparams'][0]['param_sts'];
							
							return $str;
                },
			],
			
			
				
			[
				'attribute' => 'gosnomer',
				'value' => function($data) {
														
							$str ='';
							
				
							$str.=$data['autoparams'][0]['param_gosnomer'];
							
							return $str;
                },
			],
			
			
			
	
			
			
			[
				'attribute' => 'last_name',
				'format' => 'html',
				'value' => function($data) {
													
							$str ='';
							
							$str.='<a href="/admin/drivers/view?id='.$data['drivers'][0]['id_driver'].'">'.$data['drivers'][0]['last_name'].' '.$data['drivers'][0]['first_name']. '</a>';
							
							return $str;
                },
				'contentOptions' => ['style' => 'width:150px; white-space: normal;'],
			],
			
			
			
			
			
			[
				'attribute' => 'auto_item',
				'format' => 'html',
				'value' => function($data) {
					
			
							$str ='';
										
							$str.='<a href="/admin/automobiles/view?id='.$data['automobiles'][0]['id_auto'].'">'.$data['automobiles'][0]['mark'].' '.$data['automobiles'][0]['model'].' '.$data['automobiles'][0]['VIN'].'</a>';
							
							return $str;
							
							
                },
				'contentOptions' => ['style' => 'width:150px; white-space: normal;'],
						
			],
			
	
			
            'n_post',

	[   
				'attribute' => 'date_narush',
                'value' => 'date_narush',
                'format' => ['date', 'php:d.m.Y'],
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'PddpenaltiesSearchModel[date_narush]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['PddpenaltiesSearchModel']['date_narush'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],
			
			
			[   
				'attribute' => 'date_post',
                'value' => 'date_post',
                'format' => ['date', 'php:d.m.Y'],
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'PddpenaltiesSearchModel[date_post]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['PddpenaltiesSearchModel']['date_post'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],
			
            
		

			
			[
				'attribute' => 'type_opl',
				
				'format' => 'html',
				//'value' => 'pddpenaltiesstatus0.p_status_name',
				'value' => function($data){
					
								$str = '';
								
								$arr_penst = $data['pdd_pen_status'][0];
								
								if ($arr_penst['p_status_id'] == 1) {
									
									$str.= '<span style="color: red">'.$arr_penst['p_status_name'].'</span>';
									
								} elseif ($arr_penst['p_status_id'] == 2) {
									
									$str.= '<span style="color: green">'.$arr_penst['p_status_name'].'</span>';
									
								} else {
									$str.= '<span style="">'.$arr_penst['p_status_name'].'</span>';
									
								}
						
					
								return $str;
							},
				'filter' => $arrPddpenstatus,
			],
			
			
			
			[
				'attribute' => 'type_opl_api',
				'format' => 'html',
				'value' => function($data){
					
					
					
								$str = '';
								
								$arr_penst = $data['pdd_pen_status_api'][0];
								
								if ($arr_penst['p_status_id'] == 1) {
									
									$str.= '<span style="color: red">'.$arr_penst['p_status_name_api'].'</span>';
									
								} elseif ($arr_penst['p_status_id'] == 3) {
									
									$str.= '<span style="color: green">'.$arr_penst['p_status_name_api'].'</span>';
									
								} else {
									$str.= '<span style="">'.$arr_penst['p_status_name_api'].'</span>';
									
								}
						
					
								return $str;
					
					
					
					
				},
				'filter' => $arrPddpenstatusapi,
			],
			
			
			
			
			
			
			
           // 'summa_opl',
			
			
			[
				'attribute' => 'test_sum',
				'format' => 'html',
				'value' => function($data) {
					
						//Здесь будем программировать сумму к оплате
						//Пока прокостылим, потом приведем в норму
						
							$summa = $data['summa_opl'];	
							
							
							$contracts = $data['contracts'];
							
							$pen_com = $contracts[0]['penalty_comission']; 
							$dsc_type = $contracts[0]['discount_type']; 
							
							
							$date_raznost = date_diff(new DateTime($data['date_post']), new DateTime())->days;
							
							
							if ( $date_raznost <= 19 ) {
							
								//skidka_status
							
								//Если есть галочка скидка, то ебашим скидку 50%
								if ($data['skidka_status'] == 0) {
									if ($dsc_type == 1) {					
										$summa = $summa / 2;
									}
								}
								
							} elseif ($date_raznost >= 59) {
								
								$summa = $summa * 2;
								
							} else {
								//
							}
							
							//Если есть галочка комиссия, то добавляем 50 рублей
							if ($pen_com == 1) {
								
								$summa = $summa + 50;
							}
							
							
							return '<strike>'. $data['summa_opl'] . '</strike> '.$summa;
					
				},
				
			],
			
			'narushitel',
			
			/*
			[
				'attribute' => 'comment',
				'contentOptions' => ['style' => 'width:150px; white-space: normal;'],
			],
			*/
			
			

            [
				'class' => 'yii\grid\ActionColumn',
				'visibleButtons' => [
									'update' => function ($model) {
										$id = $model->id_shtraf;
										
										if (\Yii::$app->user->can('admin', ['post' => $model])) {
											
											return true;
										} else {
											
											$model_logitems = Logitems::find()
																->where(
																	[
																		
																		'item_id' => $id,
																		'type_page' => 'pdd',
																		'user_id' => Yii::$app->user->getId(),
																	])
																	->andWhere(['>=','date_create', date('Y-m-d', strtotime('-5 days'))])
																	->all();
											
											
											if (!empty($model_logitems)) {
												
												return true;
											} else {
												return false;
											}
											
										}
									},
									'delete' => function ($model) {
										$id = $model->id_shtraf;
										
										if (\Yii::$app->user->can('admin', ['post' => $model])) {
											
											return true;
										} else {
											
											$model_logitems = Logitems::find()
																->where(
																	[
																		
																		'item_id' => $id,
																		'type_page' => 'pdd',
																		'user_id' => Yii::$app->user->getId(),
																	])
																	->andWhere(['>=','date_create', date('Y-m-d', strtotime('-5 days'))])
																	->all();
											
											
											if (!empty($model_logitems)) {
												
												return true;
											} else {
												return false;
											}
											
										}
									},
								],
			],
        ];




// Renders a export dropdown menu
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns
]);

?>	
	
	
	</div>
	
	
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		
		
		'pager' => [
			'class' => \kop\y2sp\ScrollPager::className(),
			'container' => '.grid-view tbody',
			'item' => 'tr',
			'paginationSelector' => '.grid-view .pagination',
			'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',
		],
		'filterSelector' => 'select[name="per-page"]',
		
        'columns' => $gridColumns,
    ]); ?>
    <?php //Pjax::end(); ?>


	
	
	
<?php

$this->registerJs('

$(document).ready(function(){
	$(\'#MyButton\').click(function(){

	var HotId = $(\'.grid-view\').yiiGridView(\'getSelectedRows\');
	
	
	$.ajax({
		type: \'POST\',
		url : \'/admin/driverscomments/multipledelete\',
		data : {row_id: HotId},
		success : function() {
			
			$(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
		
		}
	});

	});
});', \yii\web\View::POS_READY);

?>
	
	