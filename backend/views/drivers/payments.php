<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use backend\assets\AppAsset;

use yii\widgets\Pjax;

use kartik\date\DatePicker;

use app\models\Logitems;

use yii\grid\CheckboxColumn;



use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $model common\models\DriversModel */

$this->title = 'Платежи по водителю '.$model->id_driver . ' ' 
. $model->last_name . ' ' 
. $model->first_name . ' '
. $model->middle_name . ' ';



$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Водители'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);





?>



<div class="drivers-model-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
	
	
	
	
		<ul class="nav nav-tabs">
			<li role="presentation"><a href="#" id="karta-vod" class="btn">Карточка</a>
		</li>
		
		
		<li role="presentation"><?= Html::a(Yii::t('app', 'Сводка по водителю'), ['svodka', 'id' => $model->id_driver],  ['class' => 'btn',]) ?>
		</li>
	  
	  
	  <li role="presentation"><?= Html::a(Yii::t('app', 'Штрафы ПДД'), ['pdd', 'id' => $model->id_driver],  ['class' => 'btn',]) ?>
		</li>
	  
	  
	  
	  <li role="presentation"><?= Html::a(Yii::t('app', 'Платежи'), ['payments', 'id' => $model->id_driver],  ['class' => 'btn',]) ?>
		</li>
	  
	  
	</ul>
	
	</p>
	
	<p>
	
		<?php 
		
		if(
				Yii::$app->user->can('admin') 
				)  
			{ 
			
				echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_driver], ['class' => 'btn btn-primary']);
				echo ' ';
				echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_driver], [
					'class' => 'btn btn-danger',
					'data' => [
						'confirm' => Yii::t('app', 'Вы действительно хотите удалить этого водителя?'),
						'method' => 'post',
					],
				]);
			} else {
				
				$id = $model->id_driver;
										
				
					
				$model_logitems = Logitems::find()
									->where(
										[									
											'item_id' => $id,
											'type_page' => 'driver',
											'user_id' => Yii::$app->user->getId(),
										])
										->andWhere(['>=','date_create', date('Y-m-d', strtotime('-5 days'))])
										->all();
					
					
				
				if (!empty($model_logitems)) {
					echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_driver], ['class' => 'btn btn-primary']);
					echo ' ';
					echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_driver], [
						'class' => 'btn btn-danger',
						'data' => [
							'confirm' => Yii::t('app', 'Вы действительно хотите удалить этого водителя?'),
							'method' => 'post',
						],
					]);
					
				} 
				
				
			}
		
		
		?>
	
       
		<?= Html::a(Yii::t('app', 'Добавить сканы документов'), ['driversfiles/create', 'id' => $model->id_driver], ['class' => 'btn btn-primary']) ?>
       
		
		
			
    </p>

	<p>
	<?php 
	
		if (!empty($model_contracts)) {
		
			echo '<h3>Договора водителя</h3>';
			echo '<p>';
			foreach ($model_contracts as $contract) {
				
				echo Html::a(Yii::t('app', 'Договор '.$contract->id_contract), ['contracts/view', 'id' => $contract->id_contract], ['class' => 'btn btn-primary']);		
				
			}
			
			echo '</p>';
			
		}	
	?>
	
	
	
	
	
	
	
	
	
	</p>
	
	
	
	

	
	
	
	
<?php echo \nterms\pagesize\PageSize::widget(); ?>	


<?php 




$gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],
			['class' => 'yii\grid\CheckboxColumn'],

			[
			    'attribute' => 'id_payment',
				'format' => 'html',
				'value' => function($data) {
					
								$str = '';
								$str.= '<a href="/admin/payments/view?id='.$data->id_payment.'">'.$data->id_payment.'</a>';
								return $str;
							}
			],
            
			[   
				'attribute' => 'date_payment',
                'value' => 'date_payment',
                'format' => ['date', 'php:d.m.Y'],
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'PaymentsSearchModel[date_payment]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['PaymentsSearchModel']['date_payment'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],
			
			
			
			[
				'attribute' => 'contract_id',
				'label' => 'Номер договора (число)',
				'format' => 'html',
				'contentOptions' => ['style' => 'width:50px; white-space: normal;'],
				'value' => function($data) {
					
								$str = '';
								
								$str.= '<a href="/admin/contracts/view?id='.$data->contract_id.'">Договор '.$data->contract_id.'</a>';
								
								return $str;
							}
			],
           
			
			
			
			
			
			[
				'attribute' => 'last_name',
				'format' => 'html',
				'value' => function($data) {
					
					
							
							$str ='';
							foreach($data['contracts'] as $item)
							{
											
								$str.= '<a href="/admin/drivers/view?id='.$item->driver_id.'">
													'. $item['drivers'][0]['last_name'].' '
													.$item['drivers'][0]['first_name'].' ' 
													.$item['drivers'][0]['middle_name']. 
													'</a>';
							}
							return $str;
							
							
                },
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
				
				
			],
			
			
			
			[
				'attribute' => 'gosnomer',
				'format' => 'html',
				'value' => function($data) {
					
					
							//print_r ($data);
							
							
							$str ='';
							foreach($data['contracts'] as $item)
							{
								//$str.='<a href="/admin/automobiles/view?id='.$item->auto_id.'">' .$item['automobiles'][0]['gosnomer'].'</a>';
						
								
								$str.='<a href="/admin/automobiles/view?id='.$item->auto_id.'">'.$item['automobiles'][0]['mark'].' '.$item['automobiles'][0]['model']. ' ' .$item['automobiles'][0]['gosnomer'].'</a>';
							}
							return $str;
							
							
                },
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
				
				
				
			],
			
			
			
			
            //'type',
			
			[   
				'attribute' => 'type',
                'value' => 'type',
				'filter' => [ 
								'Наличные' => 'Наличные', 
								'На карту' => 'На карту', 
								'Списано с диспетчерской' => 'Списано с диспетчерской', 
								'Бонус/Простили' => 'Бонус/Простили',
							],
				 'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
			],
			
			
			[
				'attribute' => 'pmnt_variant',
				'filter' => [
										'1' => 'Арендный платеж',
										'2' => 'Платеж по договору (взнос и и выкуп)',
										'3' => 'Наши штрафы по договору',
										'4' => 'Штраф ПДД (строго)',
										'5' => 'Простой',
										//'6' => 'Комплексный платеж'
									],
				 'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
				'value' => function ($data) {
						
									$arrr = [
										'1' => 'Арендный платеж',
										'2' => 'Платеж по договору (взнос и и выкуп)',
										'3' => 'Наши штрафы по договору',
										'4' => 'Штраф ПДД (строго)',
										'5' => 'Простой',
										//'6' => 'Комплексный платеж'
									];
						
					
									$str = '';
									$str.=  $arrr[$data['pmnt_variant']];
									
									return $str; 
					
					
								}
			],
			
			
            //'summa',
			
			
			[
				'attribute' => 'summa',
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
			],
            //'p_vznos',
           
			
			[
				'attribute' => 'arenda',
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
			],
			
			'pay_dogovor',
			'id_pdd_shtraf',
			'pay_pdd',
			'id_ourpen',
			'pay_ourpen',
            
			/*
			[
				'attribute' => 'pdd',
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
			],
			*/
			
            //'prosrochka',
            //'peredacha_rulya',
            //'p_vyezd_200',
            //'p_atrib',
            //'p_put_list',
            //'p_osago',
            //'p_uterya_doc',
            //'p_uterya_key',
            //'p_diag_card',
            //'vyezd_sotr',
			
			[	
			
				'attribute' => 'comment',
				  'contentOptions' => ['style' => 'width:200px; white-space: normal;'],
			],
			
            //'comment:ntext',

            [
				'class' => 'yii\grid\ActionColumn',
				'visibleButtons' => [
									'update' => function ($model) {
										$id = $model->id_payment;
										
										if (\Yii::$app->user->can('admin', ['post' => $model])) {
											
											return true;
										} else {
											
											$model_logitems = Logitems::find()
																->where(
																	[
																		
																		'item_id' => $id,
																		'type_page' => 'payment',
																		'user_id' => Yii::$app->user->getId(),
																	])
																	->andWhere(['>=','date_create', date('Y-m-d', strtotime('-5 days'))])
																	->all();
											
											
											if (!empty($model_logitems)) {
												
												return true;
											} else {
												return false;
											}
											
										}
									},
									'delete' => function ($model) {
										$id = $model->id_payment;
										
										if (\Yii::$app->user->can('admin', ['post' => $model])) {
											
											return true;
										} else {
											
											$model_logitems = Logitems::find()
																->where(
																	[
																		
																		'item_id' => $id,
																		'type_page' => 'payment',
																		'user_id' => Yii::$app->user->getId(),
																	])
																	->andWhere(['>=','date_create', date('Y-m-d', strtotime('-5 days'))])
																	->all();
											
											
											if (!empty($model_logitems)) {
												
												return true;
											} else {
												return false;
											}
											
										}
									},
								],
			],
			
        ];

// Renders a export dropdown menu
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns
]);




?>






    <h1><?= Html::encode($this->title) ?></h1>
   
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать платеж'), ['create'], ['class' => 'btn btn-success']) ?>
		
		<?// Html::a(Yii::t('app', 'Загрузить платежи'), ['import'], ['class' => 'btn btn-success']) ?>
		
		
		
		
		
		
		
		
		
		
    </p>

   
	</div>
	
	
	
	
<?php 


echo GridView::widget([
        'dataProvider' => $dataProvider,
		
		
		'pager' => [
			'class' => \kop\y2sp\ScrollPager::className(),
			'container' => '.grid-view tbody',
			'item' => 'tr',
			'paginationSelector' => '.grid-view .pagination',
			'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',
		],
	
	 
		'filterSelector' => 'select[name="per-page"]',
	    'filterModel' => $searchModel,
        'columns' => $gridColumns,
	 
	 
	 
	 
	 
]);




?>

	
	
	
	</div>
	
	
	
	
	