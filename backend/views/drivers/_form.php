<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DriversModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="drivers-model-form">




    <?php $form = ActiveForm::begin(); ?>
	


	<div class="row">
	
	<div class="col-md-4">
	 <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
	</div>

	
	<div class="col-md-4">
	
    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
</div>
	
	<div class="col-md-4">
	
    <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true]) ?>
</div>
	
	
	
		
		<div class="col-md-12"><h2>Паспортные данные</h2></div>
	
		
	
		<div class="col-md-4">
	
		 <?= $form->field($model, 'date_birth')->input('date') ?>
		</div>
	
		<div class="col-md-4">
			<?= $form->field($model, 'p_series')->textInput(['maxlength' => true]) ?>
		</div>
		
		<div class="col-md-4">
		
			<?= $form->field($model, 'p_number')->textInput(['maxlength' => true]) ?>
		</div>
			
		<div class="col-md-12">
		
			<?= $form->field($model, 'p_who')->textArea(['rows' => 6])  ?>
		</div>
		
		
		
		<div class="col-md-12">
		
			<?= $form->field($model, 'address_reg')->textArea(['rows' => 6])  ?>
		</div>
		
		
		<div class="col-md-12">
		
			<?= $form->field($model, 'address_fact')->textArea(['rows' => 6])  ?>
		</div>
		
		<div class="col-md-6">
		<?= $form->field($model, 'phone_1')->textInput(['maxlength' => true]) ?>
		</div>
		
		<div class="col-md-6">
		<?= $form->field($model, 'phone_2')->textInput(['maxlength' => true]) ?>
		</div>
	
		
		
		
		
		
		<div class="col-md-6">
		<?= $form->field($model, 'lic_series')->textInput(['maxlength' => true]) ?>
		</div>
		
		<div class="col-md-6">
		<?= $form->field($model, 'lic_number')->textInput(['maxlength' => true]) ?>
		</div>
	
	
		<div class="col-md-6">
		<?= $form->field($model, 'lic_type')->textInput(['maxlength' => true]) ?>
		</div>
		
		<div class="col-md-6">
		<?= $form->field($model, 'lic_dates')->textInput(['maxlength' => true]) ?>
		</div>
	
		
	
		
		<div class="col-md-4">
			<?= $form->field($model, 'driver_cat')->dropdownList($arrCat, ['prompt' => '']) ?>
	
	
		</div>
   
		<div class="col-md-4">
			<?= $form->field($model, 'status')->dropdownList($arrStatus, ['prompt' => '']) ?>
	
		</div>
		<div class="col-md-4">
			<?= $form->field($model, 'penalty_type')->dropdownList($arrPenaltytype, []) ?>
	
		</div>
	
	
	
	</div>
	


	
	<?= $form->field($model, 'comment')->textArea(['rows' => 6]) ?>
	
	
	<?= $form->field($model, 'photo_f')->fileInput() ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
