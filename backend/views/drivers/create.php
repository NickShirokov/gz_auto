<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DriversModel */

$this->title = Yii::t('app', 'Создать нового водителя');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Водители'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="drivers-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrCat' => $arrCat,
		'arrStatus' => $arrStatus,
		'arrPenaltytype' => $arrPenaltytype,
    ]) ?>

</div>
