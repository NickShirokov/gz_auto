<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Owners */

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Owners */
/* @var $form yii\widgets\ActiveForm */



$this->title = Yii::t('app', 'Импортировать водителей только фио');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Водители'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>




<div class="owners-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

	
	
	<div class="row">
	
	<div class="col-md-12">
    <?= $form->field($model, 'file_f')->fileInput() ?>

	</div>
	
	

	 
	 
	<div class="col-md-12"> 
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Загрузить'), ['class' => 'btn btn-success']) ?>
    </div>
	</div>
	
	
	</div>
    <?php ActiveForm::end(); ?>

</div>

