<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use kartik\date\DatePicker;

use yii\grid\CheckboxColumn;

use app\models\Logitems;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DriversModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Водители');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="drivers-model-index">



<div class="buts">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php //Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать водителя'), ['create'], ['class' => 'btn btn-success']) ?>  	
		
		
		<?// Html::a(Yii::t('app', 'Загрузить водителей'), ['import'], ['class' => 'btn btn-success']) ?>
		
		
		<?php
		if(
			Yii::$app->user->can('admin')) {
			
			
				echo Html::a(Yii::t('app', 'Загрузить водителей'), ['import'], ['class' => 'btn btn-success']) . ' '.
			
				'<input type="button" class="btn btn-info" value="Удалить выбранные" id="MyButton" >
		
				<input type="button" class="btn btn-info" value="Очистить всех" id="remove-all" >';
		
		
		}
		
		?>
		
		
		
		
		
  </p>


<?php echo \nterms\pagesize\PageSize::widget(); ?>	
	
<?php 

$gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],
			['class' => 'yii\grid\CheckboxColumn'],
			
			[
				'attribute' => 'id_driver',
				'format' => 'html',
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/drivers/view?id='.$data['id_driver'].'">'.$data['id_driver'].'</a>';
					
								return $str;
					
							},
			],
			[
				'attribute' => 'driver_cat',
				'value' => 'cat0.category_name',
				'filter' => $arrCat
			],
			
			[
				'attribute' => 'status',
				'value' => 'status0.status_name',
				'filter' => $arrStatus
			],
			
            //'id_driver',
			
			[
				'attribute' => 'last_name',
				'format' => 'html',
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/drivers/view?id='.$data['id_driver'].'">'.$data['last_name'].'</a>';
					
								return $str;
					
							},
			],
			
			
			
			
			[
				'attribute' => 'first_name',
				'format' => 'html',
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/drivers/view?id='.$data['id_driver'].'">'.$data['first_name'].'</a>';
					
					
								return $str;
					
							},
			],
			
			
			[
				'attribute' => 'middle_name',
				'format' => 'html',
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/drivers/view?id='.$data['id_driver'].'">'.$data['middle_name'].'</a>';
					
								return $str;
					
							},
			],
			
			
					
			
		    [   
				'attribute' => 'date_birth',
                'value' => 'date_birth',
                'format' => ['date', 'php:d.m.Y'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'DriversModelSearch[date_birth]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['DriversModelSearch']['date_birth'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],
	
			
			
			[
				'attribute' => 'penalty_type',
				'value' => 'penalty0.name_type',
				'filter' => $arrPenaltytype,
			],
			
			
            //'comment:ntext',

            [
				'class' => 'yii\grid\ActionColumn',
				'visibleButtons' => [
									'update' => function ($model) {
										
										$id = $model->id_driver;
										
										if (\Yii::$app->user->can('admin', ['post' => $model])) {
											
											return true;
										} else {
											
											$model_logitems = Logitems::find()
																->where(
																	[
																		
																		'item_id' => $id,
																		'type_page' => 'driver',
																		'user_id' => Yii::$app->user->getId(),
																	])
																	->andWhere(['>=','date_create', date('Y-m-d', strtotime('-5 days'))])
																	->all();
											
											
											if (!empty($model_logitems)) {
												
												return true;
											} else {
												return false;
											}
											
										}
									
									},
									'delete' => function ($model) {
									
										$id = $model->id_driver;
										
										if (\Yii::$app->user->can('admin', ['post' => $model])) {
											
											return true;
										} else {
											
											$model_logitems = Logitems::find()
																->where(
																	[
																		
																		'item_id' => $id,
																		'type_page' => 'driver',
																		'user_id' => Yii::$app->user->getId(),
																	])
																	->andWhere(['>=','date_create', date('Y-m-d', strtotime('-5 days'))])
																	->all();
											
											
											if (!empty($model_logitems)) {
												
												return true;
											} else {
												return false;
											}
											
										}
									
									
									},
								],
			],
        ];


echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns
]);



?>	
	
	</div>
	
		
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		
		
		'pager' => [
			'class' => \kop\y2sp\ScrollPager::className(),
			'container' => '.grid-view tbody',
			'item' => 'tr',
			'paginationSelector' => '.grid-view .pagination',
			'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',
		 ],
		 'filterSelector' => 'select[name="per-page"]',
		
		
		
        'columns' => $gridColumns,
    ]); ?>
    <?php //Pjax::end(); ?>
</div>

<?php

$this->registerJs('

$(document).ready(function(){
	$(\'#MyButton\').click(function(){

	var HotId = $(\'.grid-view\').yiiGridView(\'getSelectedRows\');
	
	
	$.ajax({
		type: \'GET\',
		url : \'/admin/drivers/multipledelete\',
		data : {row_id: HotId},
		success : function(data) {
			
			
			$(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
		
		}
	});

	});
	
	
	
	
	$(\'#remove-all\').click(function(){
	
		$.ajax({
			type: \'POST\',
			url : \'/admin/drivers/removeall\',
			//data : {row_id: HotId},
			success : function() {
				
				//$(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
			
			}
		});
	
	
	});
	
	
	
	
	
});', \yii\web\View::POS_READY);

?>



