<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use backend\assets\AppAsset;

use yii\widgets\Pjax;

use kartik\date\DatePicker;

use app\models\Logitems;

use yii\grid\CheckboxColumn;



use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $model common\models\DriversModel */

$this->title = 'Сводка по водителю '.$model->id_driver . ' ' 
. $model->last_name . ' ' 
. $model->first_name . ' '
. $model->middle_name . ' ';



$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Водители'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);





?>



<div class="drivers-model-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
	
	
	
	
		<ul class="nav nav-tabs">
			<li role="presentation"><a href="#" id="karta-vod" class="btn">Карточка</a>
		</li>
		
		
		<li role="presentation"><?= Html::a(Yii::t('app', 'Сводка по водителю'), ['svodka', 'id' => $model->id_driver],  ['class' => 'btn',]) ?>
		</li>
	  
	  
	  <li role="presentation"><?= Html::a(Yii::t('app', 'Штрафы ПДД'), ['pdd', 'id' => $model->id_driver],  ['class' => 'btn',]) ?>
		</li>
	  
	  
	  
	  <li role="presentation"><?= Html::a(Yii::t('app', 'Платежи'), ['payments', 'id' => $model->id_driver],  ['class' => 'btn',]) ?>
		</li>
	  
	  
	</ul>
	
	</p>
	
	<p>
	
		<?php 
		
		if(
				Yii::$app->user->can('admin') 
				)  
			{ 
			
				echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_driver], ['class' => 'btn btn-primary']);
				echo ' ';
				echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_driver], [
					'class' => 'btn btn-danger',
					'data' => [
						'confirm' => Yii::t('app', 'Вы действительно хотите удалить этого водителя?'),
						'method' => 'post',
					],
				]);
			} else {
				
				$id = $model->id_driver;
										
				
					
				$model_logitems = Logitems::find()
									->where(
										[									
											'item_id' => $id,
											'type_page' => 'driver',
											'user_id' => Yii::$app->user->getId(),
										])
										->andWhere(['>=','date_create', date('Y-m-d', strtotime('-5 days'))])
										->all();
					
					
				
				if (!empty($model_logitems)) {
					echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_driver], ['class' => 'btn btn-primary']);
					echo ' ';
					echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_driver], [
						'class' => 'btn btn-danger',
						'data' => [
							'confirm' => Yii::t('app', 'Вы действительно хотите удалить этого водителя?'),
							'method' => 'post',
						],
					]);
					
				} 
				
				
			}
		
		
		?>
	
       
		<?= Html::a(Yii::t('app', 'Добавить сканы документов'), ['driversfiles/create', 'id' => $model->id_driver], ['class' => 'btn btn-primary']) ?>
       
		
		
			
    </p>

	<p>
	<?php 
	
		if (!empty($model_contracts)) {
		
			echo '<h3>Договора водителя</h3>';
			echo '<p>';
			foreach ($model_contracts as $contract) {
				
				echo Html::a(Yii::t('app', 'Договор '.$contract->id_contract), ['contracts/view', 'id' => $contract->id_contract], ['class' => 'btn btn-primary']);		
				
			}
			
			echo '</p>';
			
		}	
	?>
	
	
	
	
	
	
	
	
	
	</p>
	
	
	
	
	
	
	
	
	
	<div class="karta-vod">
    <?= DetailView::widget([
        'model' => $model,
		
		
		
        'attributes' => [
            'id_driver',
			 'last_name',
            'first_name',
            'middle_name',
           
			
            
			[
				'attribute' => 'date_birth',
				'format' => ['date', 'php:d.m.Y'],
			],
			
			'p_series',
            'p_number',
			'p_who',
            
			'address_reg',
			'address_fact',
			
			'phone_1',
			'phone_2',
			
			
			
			'lic_series',
            'lic_number',
			
			'lic_type',
			
			'lic_dates',

            //'driver_cat',
			
			[
				'attribute' => 'driver_cat',
				'value' => $driver_categ,
			],
			
            //'status',
            
			[
				'attribute' => 'status',
				'value' => $driver_stat,
			],
			[
				'attribute' => 'penalty_type',
				'value' => $pdd_st_name,
            ],
			
			
			'comment:ntext',
			
			
			'photo',
			
			
			[
				'attribute' => 'photo',
				'format' => 'html',
				'value' => function ($data){
					
					
					//print_r ($data);
					
					return '<img width=150 src="/admin/uploads/'.$data->photo.'">';
					
					
				}
				//
				
			],
			
        ],
    ]) ?>
	
	
	</div>
	
	<?php if (!empty($arrFiles)): ?>
	
	
	<div class="row">
	
	
	<div class="container">
	<h2>Сканы документов</h2>
	</div>

	<?php 
	
	
		foreach ($arrFiles as $key => $value):
		?>	
		
			<div class="col-md-4" style="margin-bottom: 20px;">
				<a href="<?php echo '/admin/uploads/'.$value; ?>"  target="_blank" >
			
				<img  style="width: 100%" src="<?php echo '/admin/uploads/'.$value; ?>">
			</a>
			</div>
			
			
			
		
	
	<?php endforeach; ?>
	
	<?php endif; ?>
	
	</div>


	
	
	
	
	
	
	<?php echo \nterms\pagesize\PageSize::widget(); ?>	
	
<?php 

$gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],

			
			[
				'attribute' => 'id_contract',
				'format' => 'html',
				'value' => function ($data) {
					
								//print_r ($data);
								
								$str.= '<a href="/admin/contracts/view?id='.$data->id_contract.'">Договор '.$data->id_contract.'</a>'; 
								
								return $str;
				},
			],
			[
				'attribute' => 'status_name',
				'contentOptions' => ['style' => 'width:20px; white-space: normal;'],
				'filter' => $arrStatuses,
            ],
			
			//'id_auto',
            //'id_driver',
            
            //'AutoMobile',
			
			
			
			[
				'attribute' => 'AutoMobile',
				'format' => 'html',
				'value' => function($data) {
					
					
								$str = '';
								
								$str.= '<a href="/admin/automobiles/view?id='.$data['id_auto'].'">'.$data['AutoMobile'].'</a>';
					
					
								return $str;
					
						},
				
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
			],
			
			
           // 'date_start',
			
				
		    [   
				'attribute' => 'date_start',
                'value' => 'date_start',
                'format' => ['date', 'php:d.m.Y'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'MaintableModelSearch[date_start]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['MaintableModelSearch']['date_start'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],
	
			
			
            
			
			[
				'attribute' => 'FIO',
				'format' => 'html',
				
				'value' => function($data) {
								
								$str = '';
								
								$str.= '<a href="/admin/drivers/view?id='.$data['id_driver'].'">'.$data['FIO'].'</a>';
					
								return $str;
							},
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
			],
			
			
            //'arenda_stoim',
            //'ArendaSumma',
            //'MainSumma',
            //'ArendaDays',
           // 'OplachenDo',
			
		    [   
				'attribute' => 'OplachenDo',
                'value' => 'OplachenDo',
                'format' => ['date', 'php:d.m.Y'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'MaintableModelSearch[OplachenDo]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['MaintableModelSearch']['OplachenDo'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],
			
			
            //'FullDays',
            'DolgPoArende',
            
			//'DolgPddSumma',
			[
					'attribute' => 'DolgPddSummaReal',
					'format' => 'html',
					'value' => function($data) {
						
							$str.= '';
							
							if ($data['DolgPddSummaReal'] < 0) {
								$str.= '<span style="color: green;">'.(int)$data['DolgPddSummaReal'].'</span>';
							} else {
								$str.= '<span style="color: red;">'.(int)$data['DolgPddSummaReal'].'</span>';
							}
							
							return $str;
							
					},
			],
			
			'DolgPoDogovoru',
			
			//'PayDogovorSumma',
            //'SummaOurPenalties',
            //'SummaOurPenaltiesPvznosVykup',
            
            //'SummaPddPenalties',
            //'PayPddSumma',
            
			[
				'attribute' => 'MainDolg',
				'format' => 'html',
				'value' => function($data) { 
				
								$str = (int) $data['MainDolg'];
				
								return $str;
				
							}
			],
			//'NewOstatok',
			'WeekPayArenda',
			'MonthPayArenda',

            //['class' => 'yii\grid\ActionColumn'],
        ];



// Renders a export dropdown menu
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns
]);



?>	
	
	
	
	
	
  

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		
		'pager' => [
			'class' => \kop\y2sp\ScrollPager::className(),
			'container' => '.grid-view tbody',
			'item' => 'tr',
			'paginationSelector' => '.grid-view .pagination',
			'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',
		],
		'filterSelector' => 'select[name="per-page"]',
		
        'columns' => $gridColumns,
    ]); ?>
	
	
<h3>
ОБщий долг
	<?=		$arrMainDolg ?>
	
</h3>	
<h3>	

Прочие долги
		<?=	$arrOtherDolg ?>
	
</h3>
<h3>
Арендный долг
	<?=	$arrArendaDolg ?>
</h3>
<h3> Долг ПДД
		<?=	$arrPddDolg ?>
			
</h3>


	
	
	
<?php

$this->registerJs('

$(document).ready(function(){
	$(\'#MyButton\').click(function(){

	var HotId = $(\'.grid-view\').yiiGridView(\'getSelectedRows\');
	
	
	$.ajax({
		type: \'POST\',
		url : \'/admin/driverscomments/multipledelete\',
		data : {row_id: HotId},
		success : function() {
			
			$(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
		
		}
	});

	});
});', \yii\web\View::POS_READY);

?>
	
	