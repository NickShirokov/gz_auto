<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DriversModel */

$this->title = Yii::t('app', 'Обновить водителя: {name}', [
    'name' => $model->id_driver.' '.$model->first_name.' '.$model->last_name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Водители'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_driver.' '.$model->first_name.' '.$model->last_name, 
							'url' => ['view', 'id' => $model->id_driver]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="drivers-model-update">


    <h1><?= Html::encode($this->title) ?></h1>

	
	
	<p>
	
	
	
	<?php 
	
		
	
		if (!empty($model_contracts)) {
		
			echo '<h3>Договора водителя</h3>';
		
			foreach ($model_contracts as $contract) {
				
				echo Html::a(Yii::t('app', 'Договор '.$contract->id_contract), ['contracts/update', 'id' => $contract->id_contract], ['class' => 'btn btn-primary']);		
				
			}
			
		}
	
	
	
	?>
	</p>
	
	
	
	
    <?= $this->render('_form', [
        'model' => $model,
		'arrCat' => $arrCat,
		'arrStatus' => $arrStatus,
		'arrPenaltytype' => $arrPenaltytype,
    ]) ?>
	
	
	
	
	
	
	

</div>
