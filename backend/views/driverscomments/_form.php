<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Driverscomments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="driverscomments-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'driver_id')->dropdownList(
							$arrDrivers,
							
							[
								'options' => 
									[ 
										$_GET['id'] => ['Selected' => true],
									],
							]
							
							
							
							) ?>
	
	<?= $form->field($model, 'date_comment')->input('date') ?>

    <?= $form->field($model, 'comments')->textarea(['rows' => 6]) ?>
	
	
	<?= $form->field($model, 'file_f')->fileInput() ?>
	

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
