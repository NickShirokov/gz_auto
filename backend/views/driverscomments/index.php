<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use kartik\date\DatePicker;


use yii\grid\CheckboxColumn;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\DriverscommentsSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Комментарии для водителя');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driverscomments-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать комментарий для водителя'), ['create'], ['class' => 'btn btn-success']) ?>
    
		<?php
		if(
			Yii::$app->user->can('user')){ 
			
				
				echo '<input type="button" class="btn btn-info" value="Удалить выбранные" id="MyButton" >';
		
			}	
		?>
	
		
	
	</p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		
		'pager' => [
			'class' => \kop\y2sp\ScrollPager::className(),
			'container' => '.grid-view tbody',
			'item' => 'tr',
			'paginationSelector' => '.grid-view .pagination',
			'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',
		],
		
		
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			['class' => 'yii\grid\CheckboxColumn'],
            'id_comment',
            //'driver_id',
			
			[
				'attribute' => 'driver_id',
				'value' => 'driver.last_name',
				'filter' => $arrDrivers,
			],
			
			
			
			//'date_comment',
			
			[   
				'attribute' => 'date_comment',
                'value' => 'date_comment',
                'format' => ['date', 'php:d.m.Y'],
				'contentOptions' => ['style' => 'width:100px; white-space: normal;'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'DriverscommentsSearchModel[date_comment]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['DriverscommentsSearchModel']['date_comment'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],
			
            'comments:ntext',
			
			
			'comment_file',
			

            [
				'class' => 'yii\grid\ActionColumn',
				'visibleButtons' => [
									'update' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
									'delete' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
								],
			],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>



<?php

$this->registerJs('

$(document).ready(function(){
	$(\'#MyButton\').click(function(){

	var HotId = $(\'.grid-view\').yiiGridView(\'getSelectedRows\');
	
	
	$.ajax({
		type: \'POST\',
		url : \'/admin/driverscomments/multipledelete\',
		data : {row_id: HotId},
		success : function() {
			
			$(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
		
		}
	});

	});
});', \yii\web\View::POS_READY);

?>
