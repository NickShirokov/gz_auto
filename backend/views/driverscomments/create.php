<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Driverscomments */

$this->title = Yii::t('app', 'Создать комментарий для водителя');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Комментарии для водителей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driverscomments-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrDrivers' => $arrDrivers,
    ]) ?>

</div>
