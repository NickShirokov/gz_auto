<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Driverscomments */

$this->title = $model->id_comment;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Комментарии для водителей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="driverscomments-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
		<?php
			if(
				Yii::$app->user->can('admin') 
				)  
			{ 
			
				echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_comment], ['class' => 'btn btn-primary']);
				echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_comment], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите удалить этот комментарий?'),
                'method' => 'post',
            ],
        ]) ;
			}
	?>
	
       
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_comment',
            'driver_id',
			'date_comment',
            'comments:ntext',
			'comment_file',
        ],
    ]) ?>

</div>
