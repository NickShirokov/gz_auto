<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Driverscomments */

$this->title = Yii::t('app', 'Обновить комментрарий: {name}', [
    'name' => $model->id_comment,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Комментарии для водителей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_comment, 'url' => ['view', 'id' => $model->id_comment]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="driverscomments-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrDrivers' => $arrDrivers,
    ]) ?>

</div>
