<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\RazresheniyaSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Razresheniyas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="razresheniya-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Razresheniya'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_razresh',
			[
				'attribute' => 'licen_id',
				'format' => 'html',
				'filter' => $arrLicens,
				'value' => function($data) {
					
								$str = '';
								
								if ($data['licen'][0]) {
								
									$str.= '<a href="/admin/licen/view?id='.$data['licen'][0]['id_lic'].'">'.$data['licen'][0]['name_lic'].'</a>';
					
								} 
					
								return $str;
							},
            ],
			'r_n',
            'series',
            'number',

            [
				'class' => 'yii\grid\ActionColumn',
				'visibleButtons' => [
									'update' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
									'delete' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
								],
			],
        ],
    ]); ?>
</div>
