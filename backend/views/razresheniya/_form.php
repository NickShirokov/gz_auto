<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Razresheniya */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="razresheniya-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'licen_id')->dropdownList($arrLicens, ['prompt' => '']) ?>

    <?= $form->field($model, 'r_n')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'series')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
