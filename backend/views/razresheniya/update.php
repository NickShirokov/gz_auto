<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Razresheniya */

$this->title = Yii::t('app', 'Update Razresheniya: {name}', [
    'name' => $model->id_razresh,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Razresheniyas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_razresh, 'url' => ['view', 'id' => $model->id_razresh]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="razresheniya-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrLicens' => $arrLicens,
    ]) ?>

</div>
