<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ttty */

$this->title = Yii::t('app', 'Create Ttty');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ttties'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttty-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
