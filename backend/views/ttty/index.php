<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\TttySearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ttties');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ttty-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Ttty'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',

            [
				'class' => 'yii\grid\ActionColumn'
				'template' => (
								Yii::$app->user->can('user') 
								or 
								Yii::$app->user->can('manager')
								or 
								Yii::$app->user->can('payer'))  ? '{view}' : '{view}{update}{delete}',
			],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
