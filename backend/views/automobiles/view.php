<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use kartik\date\DatePicker;
use app\models\Logitems;

/* @var $this yii\web\View */
/* @var $model common\models\AutomobilesNewModel */

$this->title = $model->id_auto . ' ' . $model->mark . ' ' . $model->model;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Машины'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title . ' ' . $model->mark . ' ' . $model->model; 
\yii\web\YiiAsset::register($this);
?>
<div class="automobiles-new-model-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
	
	<?php
		if(
				Yii::$app->user->can('admin') 
				)  
			{
				
				echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_auto], ['class' => 'btn btn-primary']);
				echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_auto], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы готовы удалить эту машину?'),
                'method' => 'post',
            ],
        ]);
				
				
			} else {
				
				
				
				
				$id = $model->id_auto;
										
				
					
				$model_logitems = Logitems::find()
									->where(
										[									
											'item_id' => $id,
											'type_page' => 'auto',
											'user_id' => Yii::$app->user->getId(),
										])
										->andWhere(['>=','date_create', date('Y-m-d', strtotime('-5 days'))])
										->all();
					
					
				
				if (!empty($model_logitems)) {
					echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_auto], ['class' => 'btn btn-primary']);
					echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_auto], [
						'class' => 'btn btn-danger',
						'data' => [
							'confirm' => Yii::t('app', 'Вы готовы удалить эту машину?'),
							'method' => 'post',
						],
					]);
					
				} 
				
				
			}	
	
	?>

	
        
		
		 <?= Html::a(Yii::t('app', 'Добавить сканы документов'), ['automobilefiles/create', 'id' => $model->id_auto], ['class' => 'btn btn-primary']) ?>
       
		 
		<a href="#" id="karta-auto" class="btn btn-primary">Карточка</a>
		
		
		
		<?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_auto], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы готовы удалить эту машину?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

	

	
	<?php 
	
		if (!empty($model_contracts)) {
		
			echo '<h3>Договор машины</h3>';
			echo '<p>';
			foreach ($model_contracts as $contract) {
				
				echo Html::a(Yii::t('app', 'Договор '.$contract->id_contract), ['contracts/view', 'id' => $contract->id_contract], ['class' => 'btn btn-primary']);		
				
			}
			
			echo '</p>';
			
		}	
	?>
		
	
	
	
	
	
	
	
	
	
	
	
	<div class="karta-auto">
	
	
    <?= DetailView::widget([
	
		//'fo' => 'MM/dd/yyyy',
		
	
	
        'model' => $model,
        'attributes' => [
		
		
		
		
		
            'id_auto',
            'mark',
            'model',
            //'alias',
            'VIN',
			'PTS',
            'sts',
            'gosnomer',
            'year',
            'color',
            'transmission',
			
			'odometr_km',
            
			[
				'attribute' => 'status',
				'value' => $status,
            ],
			//'osago',
			
			
			
			[
				'attribute' => 'osago',
				'format' => ['date', 'php:d.m.Y'],
			],
			
			
			'ins_company',
			
			[
				'attribute' => 'diagnostic_card',
				'format' => ['date', 'php:d.m.Y'],
			],
			
            'vialon',
            'avtofon',
            'comment:ntext',
           // 'licency_org:ntext',
        ],
    ]) ?>

	
	</div>
	
</div>



<?php if (!empty($arrFiles)): ?>
	
	
	<div class="row">
	
	
	<div class="container">
	<h2>Сканы документов</h2>
	</div>

	<?php 
	
		
		
	
		foreach ($arrFiles as $key => $value):
		?>	
		
			<div class="col-md-4" style="margin-bottom: 20px;">
				<a href="<?php echo '/admin/uploads/'.$value; ?>"  target="_blank" >
			
				<img  style="width: 100%" src="<?php echo '/admin/uploads/'.$value; ?>">
			</a>
			</div>
			
			
			
		
	
	<?php endforeach; ?>
	
		</div>
	
	<?php endif; ?>
	

	
	
	
	
	
	
	
	<h2>Ранние пары СТС-Госномер</h2>
	
	
	 <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'pager' => [
			'class' => \kop\y2sp\ScrollPager::className(),
			'container' => '.grid-view tbody',
			'item' => 'tr',
			'paginationSelector' => '.grid-view .pagination',
			'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',
		],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_param',
			
			[
				'attribute' => 'auto_id',
				'format' => 'html',
				'value' => function($data) {
					
								$str = '';
								
								$str.= '<a href="/admin/automobiles/view?id='.$data['auto_id'].'">'
										.$data['automobiles'][0]['mark'].' '
										.$data['automobiles'][0]['model'].' '
										.$data['automobiles'][0]['VIN']
										
									.'</a>';
								return $str;
							}
            ],
			
			'param_sts',
            'param_gosnomer',


			 [   
				'attribute' => 'date_param',
                'value' => 'date_param',
                'format' => ['date', 'php:d.m.Y'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'DriversModelSearch[date_param]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['AutoparamsSearchModel']['date_param'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],
			
			
			
             
			 [
				'class' => 'yii\grid\ActionColumn',
				'template' => '{view} {update}',
				'buttons' => [
				   
				   
				   'update' => function ($url, $model, $key) {
						
						
					//echo $url;	
						
					  return Html::a('', '/admin/autoparams/update?id='.$key, ['class' => 'glyphicon glyphicon-pencil']);

				   },
				   
				   
				   
				   
				   
				   'view' => function ($url, $model, $key) {			
					  return Html::a('', '/admin/autoparams/view?id='.$key, ['class' => 'glyphicon glyphicon-eye-open']);

				   },
				   
				   
				   
				   
				],
			
			],
        ],
    ]); ?>
	
	
	
	
	
	<?= $this->render('/autoparams/_form', [
        'model' => $modelAutoparams,
		'arrAutomobiles' => $arrAutomobiles,
    ]) ?>
	
	
	
