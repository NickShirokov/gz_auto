<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AutomobilesNewModel */

$this->title = Yii::t('app', 'Обновить машину: {name}', [
    'name' => $model->id_auto . ' ' . $model->mark . ' ' . $model->model,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Машины'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_auto . ' '. $model->mark . ' ' . $model->model, 'url' => [
					'view', 'id' => $model->id_auto]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="automobiles-new-model-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrMarks' => $arrMarks,
		'arrModels' => $arrModels,
		'arrIns' => $arrIns,
		'arrStatus' => $arrStatus,
		'arrOwners' => $arrOwners,
		'arrLicen' => $arrLicen,
		'arrRazresh' => $arrRazresh,
    ]) ?>

</div>
