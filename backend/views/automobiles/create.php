<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AutomobilesNewModel */

$this->title = Yii::t('app', 'Создать новую машину');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Машины'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="automobiles-new-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrMarks' => $arrMarks,
		'arrModels' => $arrModels,
		'arrIns' => $arrIns,
		'arrStatus' => $arrStatus,
		'arrOwners' => $arrOwners,
		'arrLicen' => $arrLicen,
		'arrRazresh' => $arrRazresh,
    ]) ?>

</div>
