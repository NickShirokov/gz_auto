<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;


use yii\grid\CheckboxColumn;

use kartik\export\ExportMenu;

use app\models\Logitems;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\AutomobilesNewSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Машины');
$this->params['breadcrumbs'][] = $this->title;




?>



<div class="automobiles-new-model-index">

<div class="buts">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать новое авто'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Все'), ['', 'AutomobilesNewSearchModel' => ['status' => '']], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Свободные'), ['', 'AutomobilesNewSearchModel' => ['status' => '1']], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Выкупаются'), ['', 'AutomobilesNewSearchModel' => ['status' => '2']], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Архив'), ['', 'AutomobilesNewSearchModel' => ['status' => '6']], ['class' => 'btn btn btn-primary']) ?>
	
	
		<?// Html::a(Yii::t('app', 'Загрузить машины'), ['import'], ['class' => 'btn btn-success']) ?>
	
		<?php
		if(
			Yii::$app->user->can('admin')){ 
			
			
				echo Html::a(Yii::t('app', 'Загрузить машины'), ['import'], ['class' => 'btn btn-success']) . ' '.
			
				'<input type="button" class="btn btn-info" value="Удалить выбранные" id="MyButton" >
		
				<input type="button" class="btn btn-info" value="Очистить всех" id="remove-all" >';
			}
		
		?>
	</p>

	

<?php echo \nterms\pagesize\PageSize::widget(); ?>	
	
<?php 




$gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],
			['class' => 'yii\grid\CheckboxColumn'],
           


		   'id_auto',
            //'mark',
			
			[
				'attribute' => 'mark',
				'value' => 'mark0.name_mark',
				'filter' => $arrMarks
			],
			
			
           // 'model',
			
			
			[
				'attribute' => 'model',
				'value' => 'model0.name',
				'filter' => $arrModels
			],
			
			
            //'alias',
			[
				'attribute' => 'VIN',
				'format' => 'html',
				'value' => function($data) {
					
								$str = '';
					
								$str.= '<a href="/admin/automobiles/view?id='.$data['id_auto'].'">'.$data['VIN'].'</a>';
					
								return $str;
								
					
							},
			],
			
			[
				
				
				'attribute' => 'PTS',
				'format' => 'html',
				'value' => function($data) {
					
								$str = '';
					
								$str.= '<a href="/admin/automobiles/view?id='.$data['id_auto'].'">'.$data['PTS'].'</a>';
					
								return $str;
								
					
							},
            ],
			
			[
				'attribute' => 'sts',
				'format' => 'html',
				'value' => function($data) {
					
								$str = '';
					
								$str.= '<a href="/admin/automobiles/view?id='.$data['id_auto'].'">'.$data['sts'].'</a>';
					
								return $str;
								
					
							},
            ], 
			[
				'attribute' => 'gosnomer',
				'format' => 'html',
				'value' => function($data) {
					
								$str = '';
					
								$str.= '<a href="/admin/automobiles/view?id='.$data['id_auto'].'">'.$data['gosnomer'].'</a>';
					
								return $str;
								
					
							},
			],
			
			[
				'attribute' => 'status',
				'value' => 'autostatus0.status_name',
				'filter' => $arrStsts,
			],
          /*  'year',
            'color',
            'transmission',
            'status',
           'osago',
            'diagnostic_card',
            'vialon',
            'avtofon',
            
			*/
			[
				'attribute' => 'galka_vserv',
				'format' => 'html',
				'value' => function ($data) {
					
								$str = '';
								
								if ($data->galka_vserv) {
									
									$str.= '<span class="glyphicon glyphicon-ok"></span>';
								}
								
					
								return $str;
							},
			],
			
			
			
			[
				'attribute' => 'galka_fssp',
				'format' => 'html',
				'value' => function ($data) {
					
								$str = '';
								
								if ($data->galka_fssp) {
									
									$str.= '<span class="glyphicon glyphicon-ok"></span>';
								}
								
					
								return $str;
							},
			],
			
			
			
			
			
			
			[
				'attribute' => 'comment',
				'contentOptions' => ['style' => 'width:200px; white-space: normal;'],
			],
			
			[
				'attribute' => 'owner_id',
				'format' => 'html',
				'value' => function($data) {
				    
				                
				                $str = '';
				                
				                $str.= $data['owners'][0]['last_name'];
				                return $str;
				            },
				'contentOptions' => ['style' => 'width:200px; white-space: normal;'],
			],
			
			

            [
				'class' => 'yii\grid\ActionColumn',
				//'template' => Yii::$app->user->can('admin') ? '{view}' : '{view}{update}{delete}',
				
				
				'visibleButtons' => [
									'update' => function ($model) {
										
										
										$id = $model->id_auto;
										
										if (\Yii::$app->user->can('admin', ['post' => $model])) {
											
											return true;
										} else {
											
											$model_logitems = Logitems::find()
																->where(
																	[
																		
																		'item_id' => $id,
																		'type_page' => 'auto',
																		'user_id' => Yii::$app->user->getId(),
																	])
																	->andWhere(['>=','date_create', date('Y-m-d', strtotime('-5 days'))])
																	->all();
											
											
											if (!empty($model_logitems)) {
												
												return true;
											} else {
												return false;
											}
											
										}
										
										
										
										
										
									
									},
									'delete' => function ($model) {
										$id = $model->id_auto;
										
										if (\Yii::$app->user->can('admin', ['post' => $model])) {
											
											return true;
										} else {
											
											$model_logitems = Logitems::find()
																->where(
																	[
																		
																		'item_id' => $id,
																		'type_page' => 'auto',
																		'user_id' => Yii::$app->user->getId(),
																	])
																	->andWhere(['>=','date_create', date('Y-m-d', strtotime('-5 days'))])
																	->all();
											
											
											if (!empty($model_logitems)) {
												
												return true;
											} else {
												return false;
											}
											
										}
									},
								],
				
			],
			
			
			
			
        ];

		// Renders a export dropdown menu
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns
]);

?>	
	
	
	</div>
	
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		
		
		 'pager' => [
			'class' => \kop\y2sp\ScrollPager::className(),
			'container' => '.grid-view tbody',
			'item' => 'tr',
			'paginationSelector' => '.grid-view .pagination',
			'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',
		 ],
		
		'filterSelector' => 'select[name="per-page"]',
		
        'columns' => $gridColumns,
    ]); ?>
    <?php //Pjax::end(); ?>
</div>



<?php

$this->registerJs('

$(document).ready(function(){
	$(\'#MyButton\').click(function(){

	var HotId = $(\'.grid-view\').yiiGridView(\'getSelectedRows\');
	

	
	$.ajax({
		type: \'POST\',
		url : \'/admin/automobiles/multipledelete\',
		data : {row_id: HotId},
		success : function(data) {
		
		
			
			$(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
		
		}
	});

	});
	
	
	
	
	$(\'#remove-all\').click(function(){
	
		$.ajax({
			type: \'POST\',
			url : \'/admin/automobiles/removeall\',
			//data : {row_id: HotId},
			success : function() {
				
				//$(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
			
			}
		});
	
	
	});
	
	
});', \yii\web\View::POS_READY);

?>




