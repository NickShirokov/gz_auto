<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AutomobilesNewModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="automobiles-new-model-form">

    <?php $form = ActiveForm::begin(); ?>

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<div class="row">
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
    <div class="col-md-12">

    <?= $form->field($model, 'owner_id')->dropdownList($arrOwners, 
				[
					'prompt' => '',
					'onchange' => '
					
					
						var owner_id = $(this).val();	
					
						$.post(
							"/admin/automobiles/licens?id="+owner_id, 
							function(data){
									
								$("select#automobilesnewmodel-licen_id").html(data);		
							}
						
						);
						
					
					
					
					
					',
					
				]
	
			) ?>
	
	
	
	
		
	
	<?= $form->field($model, 'licen_id')->dropDownList($arrLicen,
	
				[
					'prompt' => '',
					'onchange' => '
					
							var lic_id = $(this).val();
					
							$.post(
								"/admin/automobiles/razresh?id="+lic_id, 
								function(data){
										
									$("select#automobilesnewmodel-razresh_id").html(data);		
								}
							
							);
							
					
					
					',
				]
														
	); ?>
	<?= $form->field($model, 'razresh_text')->textarea(['rows' => 6, ['required' => false]])?>

	</div>
	
	
 
	<div class="col-md-6">
 
 
 
 
 
 
 
 
 
 
	<?= $form->field($model, 'mark')->dropdownList($arrMarks,
						[
							'prompt' => 'выбрать',
							'onchange' => '
								
								var mark = $(this).val();
								
								$.post(
									"/admin/automobiles/lists?mark="+mark, 
									function(data){
										
										
			
										
										$("select#automobilesnewmodel-model").html(data);
									}
								
								);
							',
				
						]
						
				) ?>
	
	</div>
	
	
	<div class="col-md-6">
	
   <?= $form->field($model, 'model')->dropdownList($arrModels, 
				[
					'prompt' => 'выбрать'
					
					
				]
	
			) ?>

	</div>
	
	
	
	
	
   
   
   <div class="col-md-4">

    <?= $form->field($model, 'VIN')->textInput(['maxlength' => true]) ?>
	
	</div>
	
	  <div class="col-md-4">
	
	 <?= $form->field($model, 'PTS')->textInput(['maxlength' => true]) ?>
	</div>
	
<div class="col-md-4">
    <?= $form->field($model, 'sts')->textInput(['maxlength' => true]) ?>

	</div>
	
	<div class="col-md-4">
    <?= $form->field($model, 'gosnomer')->textInput(['maxlength' => true]) ?>
</div>
   

	<div class="col-md-4">
	
	<?= $form->field($model, 'year')->input('number', ['required' => false, 'min' => 1950, 'max' => 2099])  ?>
	
</div>

<div class="col-md-4">
    <?= $form->field($model, 'color')->textInput(['maxlength' => true]) ?>

</div>	



<div class="col-md-4">
    <?= $form->field($model, 'transmission')->dropDownList([ 'Ручная' => 'Ручная', 'Автомат' => 'Автомат', '' => '', ], ['prompt' => '']) ?>
</div>


<div class="col-md-4">
<?= $form->field($model, 'odometr_km')->textInput(['maxlength' => true]) ?>

</div>


<div class="col-md-4">
   <?= $form->field($model, 'status')->dropdownList($arrStatus, 
				[
					'prompt' => 'выбрать'				
				]
	
			) ?>
   
   
</div>

	
	
	
	
<div class="col-md-4">	
	<?= $form->field($model, 'osago')->input('date', ['required' => false])  ?>
</div>

<div class="col-md-4">	
	<?= $form->field($model, 'ins_company')->dropdownList($arrIns, 
				[
					'prompt' => 'выбрать'				
				]
	
			) ?>
</div>
	

<div class="col-md-4">
	<?= $form->field($model, 'diagnostic_card')->input('date', ['required' => false])  ?>
</div>

</div>



	
<div class="col-md-12">
<div class="row">
	<h3>Ограничения</h3>

	<?= $form->field($model, 'ogranichenia')->checkbox([
		'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
	])?>
		
	</div>	
	<br><br>
</div>	
	
	<div class="">
		<h3>Мониторинг</h3>
	</div>
	
	<div class="row">
	
	<div class="col-md-4">
	<?= $form->field($model, 'vialon')->checkbox([
		'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
	])?>
		
	</div>	
		
	<div class="col-md-4">

		
	<?= $form->field($model, 'vialon_status')->dropdownList(['РАБОТАЕТ' => 'РАБОТАЕТ', 'ГЛЮЧИТ' => 'ГЛЮЧИТ', 'НЕ РАБОТАЕТ' => 'НЕ РАБОТАЕТ'], 
				[
					'prompt' => ''				
				]
	
			) ?>	
		
		

	</div>


	<div class="col-md-4">

		 <?= $form->field($model, 'vialon_comment')->textarea(['rows' => 6, ['required' => false]]) ?>
	

	</div>
	
	
	
	
	<div class="col-md-4">
	
	<?= $form->field($model, 'avtofon')->checkbox([
		'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
	])?>
	
	</div>
	
	<div class="col-md-4">

		
	<?= $form->field($model, 'avtofon_status')->dropdownList(['РАБОТАЕТ' => 'РАБОТАЕТ', 'ГЛЮЧИТ' => 'ГЛЮЧИТ', 'НЕ РАБОТАЕТ' => 'НЕ РАБОТАЕТ'], 
				[
					'prompt' => ''				
				]
	
			) ?>	
		
		

	</div>


	<div class="col-md-4">

		 <?= $form->field($model, 'avtofon_comment')->textarea(['rows' => 6, ['required' => false]]) ?>
	

	</div>
	
	
	
	</div>
	
	
    <?= $form->field($model, 'comment')->textarea(['rows' => 6, ['required' => false]]) ?>
    
	
	<?= $form->field($model, 'galka_vserv')->checkbox([
		'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
	])?>
	
	<?= $form->field($model, 'galka_fssp')->checkbox([
		'template' => '<div class="col-md-1">{label}</div><div class="col-md-5">{input}</div><div class="col-md-6">{error}</div>'
	])?>
	
	
	
	
	
	
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>





<?

$this->registerJs(
'
$(document).ready(function(){
	

	/*
		var owner_id = $(this).val();	
	
		$.post(
			"/admin/automobiles/licens?id="+owner_id, 
			function(data){
					
				$("select#automobilesnewmodel-licen_id").html(data);		
			}
		
		);
	
	
		var lic_id = $(this).val();
					
			$.post(
				"/admin/automobiles/razresh?id="+lic_id, 
				function(data){
						
					$("select#automobilesnewmodel-razresh_id").html(data);		
				}
			
			);
	
	
	*/
	
	
	
});



'



);

?>














