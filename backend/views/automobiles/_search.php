<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\AutomobilesNewSearchModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="automobiles-new-model-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'mark') ?>

    <?= $form->field($model, 'model') ?>

    <?= $form->field($model, 'alias') ?>

    <?= $form->field($model, 'VIN') ?>

    <?php // echo $form->field($model, 'sts') ?>

    <?php // echo $form->field($model, 'gosnomer') ?>

    <?php // echo $form->field($model, 'year') ?>

    <?php // echo $form->field($model, 'color') ?>

    <?php // echo $form->field($model, 'transmission') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'osago') ?>

    <?php // echo $form->field($model, 'diagnostic_card') ?>

    <?php // echo $form->field($model, 'vialon') ?>

    <?php // echo $form->field($model, 'avtofon') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
