<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Penaltytypes */

$this->title = Yii::t('app', 'Обновить значение кому платит штрафы ГИБДД: {name}', [
    'name' => $model->id_type,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Кому платит штрафы ГИБДД'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_type, 'url' => ['view', 'id' => $model->id_type]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="penaltytypes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
