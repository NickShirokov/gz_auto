<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Penaltytypes */

$this->title = $model->id_type;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Кому платит штрафы ГИБДД'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="penaltytypes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
		<?php
			if(
				Yii::$app->user->can('admin') 
				)  
			{
				
				echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_type], ['class' => 'btn btn-primary']);
				echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_type], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ;
			}
		?>
	
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_type',
            'name_type',
        ],
    ]) ?>

</div>
