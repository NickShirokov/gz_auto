<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Licen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="licen-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name_lic')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'owner_id')->dropdownList($arrOwners, ['prompt' => '']); ?>
    <?= $form->field($model, 'ogrn')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'inn')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'okpo')->textInput(['maxlength' => true]) ?>
    
	<!--
	<?= $form->field($model, 'razreshenie')->textInput(['maxlength' => true]) ?>

	-->
	
    <?= $form->field($model, 'mechanic')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'medic')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
