<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Licen */

$this->title = $model->id_lic;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Лицензии'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="licen-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
	<?php
			if(
				Yii::$app->user->can('admin') 
				)  
			{ 
				echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_lic], ['class' => 'btn btn-primary']);
				echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_lic], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите удалить эту лицензию?'),
                'method' => 'post',
            ],
        ]);
			}
	
	?>
	
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_lic',
            'owner_id',
			
            'name_lic',
            'ogrn',
            'inn',
            'okpo',
			//'razreshenie',
            'mechanic',
            'medic',
        ],
    ]) ?>

</div>






<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_razresh',
			[
				'attribute' => 'licen_id',
				'format' => 'html',
				'filter' => $arrLicens,
				'value' => function($data) {
					
								$str = '';
								
								if ($data['licen'][0]) {
								
									$str.= '<a href="/admin/licen/view?id='.$data['licen'][0]['id_lic'].'">'.$data['licen'][0]['name_lic'].'</a>';
					
								} 
					
								return $str;
							},
            ],
			'r_n',
            'series',
            'number',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	
	
	
	
	
	
	
	  <?= $this->render('/razresheniya/_form', [
        'model' => $modelRazresh,
		'arrLicens' => $arrLicens,
    ]) ?>
	
	
