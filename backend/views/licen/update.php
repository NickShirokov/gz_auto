<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Licen */

$this->title = Yii::t('app', 'Обновить лицензию: {name}', [
    'name' => $model->id_lic,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Лицензии'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_lic, 'url' => ['view', 'id' => $model->id_lic]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="licen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrOwners' => $arrOwners,
    ]) ?>

</div>
