<?php

use yii\helpers\Html;
use yii\grid\GridView;


use kartik\date\DatePicker;

use kartik\export\ExportMenu;

use yii\grid\CheckboxColumn;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\LicenSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Лицензии');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="licen-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать лицензию'), ['create'], ['class' => 'btn btn-success']) ?>
		
		
		<?php
		if(
			Yii::$app->user->can('admin')) 
			
			{
				
				echo '<input type="button" class="btn btn-info" value="Удалить выбранные" id="MyButton" >
		
				';
			}
		?>
		
		
    </p>
	
	
	
<?php 


$gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],

			['class' => 'yii\grid\CheckboxColumn'],
			
            'id_lic',
			
			[
				'attribute' => 'owner_id',
				'format' => 'html',
				'filter' => $arrOwners,
				'value' => function($data) {
					
								
								$str = '';
								
								if ($data['owners'][0]) {
								
									$str.= '<a href="/admin/owners/view?id='.$data['owners'][0]['id_owner'].'">'.$data['owners'][0]['first_name'].' '.$data['owners'][0]['last_name'].'</a>';
					
								}
								
								return $str;
							}
            ],
			
			
			'name_lic',
            'ogrn',
            'inn',
            'okpo',
			//'razreshenie',
            'mechanic',
            'medic',

            [
				'class' => 'yii\grid\ActionColumn',
				'visibleButtons' => [
									'update' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
									'delete' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
								],
			],
        ];


// Renders a export dropdown menu
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns
]);


?>	
	
	
	
	
	
	
	

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]); ?>
</div>




<?php

$this->registerJs('

$(document).ready(function(){
	$(\'#MyButton\').click(function(){

	var HotId = $(\'.grid-view\').yiiGridView(\'getSelectedRows\');
	

	
	$.ajax({
		type: \'POST\',
		url : \'/admin/licen/multipledelete\',
		data : {row_id: HotId},
		success : function() {
			
			$(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
		
		}
	});

	});
});', \yii\web\View::POS_READY);

?>


