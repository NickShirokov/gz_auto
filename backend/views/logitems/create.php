<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Logitems */

$this->title = 'Create Logitems';
$this->params['breadcrumbs'][] = ['label' => 'Logitems', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logitems-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
