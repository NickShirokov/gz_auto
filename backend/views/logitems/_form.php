<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Logitems */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="logitems-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type_user')->dropDownList([ 'admin' => 'Admin', 'manager' => 'Manager', 'payer' => 'Payer', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'type_page')->dropDownList([ 'contract' => 'Contract', 'driver' => 'Driver', 'auto' => 'Auto', 'payment' => 'Payment', 'pdd' => 'Pdd', 'ourpenalty' => 'Ourpenalty', 'prostoy' => 'Prostoy', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'item_id')->textInput() ?>

    <?= $form->field($model, 'date_create')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
