<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Autoparams */

$this->title = Yii::t('app', 'Обновить пару СТС и Госномер: {name}', [
    'name' => $model->id_param,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Пары СТС и Госномера'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_param, 'url' => ['view', 'id' => $model->id_param]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="autoparams-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrAutomobiles' => $arrAutomobiles, 
    ]) ?>

</div>
