<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Autoparams */

$this->title = $model->id_param.' '.$model->param_sts. ' '.$model->param_gosnomer;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'СТС и ГОСНОМЕР'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="autoparams-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	
		<?php 
		
		if(
				Yii::$app->user->can('admin') 
				)  
			{
				echo Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_param], ['class' => 'btn btn-primary']);
				echo Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_param], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите удалить эту запись?'),
                'method' => 'post',
            ],
        ]);
			}
		
		
		?>
	
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_param',
            'auto_id',
            'param_sts',
            'param_gosnomer',
            'date_param',
        ],
    ]) ?>

</div>
