<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Autoparams */

$this->title = Yii::t('app', 'Создать пару СТС - Госномер');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'СТС и Госномера'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="autoparams-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrAutomobiles' => $arrAutomobiles, 
    ]) ?>

</div>
