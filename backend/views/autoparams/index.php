<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;

use yii\grid\CheckboxColumn;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\AutoparamsSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'СТС и Госномер');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="autoparams-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать пару СТС и Госномер'), ['create'], ['class' => 'btn btn-success']) ?>
		
		<?php
		if(
			Yii::$app->user->can('admin') ) {
			
			
				
				echo '<input type="button" class="btn btn-info" value="Удалить выбранные" id="MyButton" >
		
				<input type="button" class="btn btn-info" value="Очистить всех" id="remove-all" >';
		
			}
		?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'pager' => [
			'class' => \kop\y2sp\ScrollPager::className(),
			'container' => '.grid-view tbody',
			'item' => 'tr',
			'paginationSelector' => '.grid-view .pagination',
			'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',
		],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			['class' => 'yii\grid\CheckboxColumn'],
            'id_param',
			
			[
				'attribute' => 'auto_id',
				'format' => 'html',
				'value' => function($data) {
					
								$str = '';
								
								$str.= '<a href="/admin/automobiles/view?id='.$data['auto_id'].'">'
										.$data['automobiles'][0]['mark'].' '
										.$data['automobiles'][0]['model'].' '
										.$data['automobiles'][0]['VIN']
										
									.'</a>';
								return $str;
							}
            ],
			
			'param_sts',
            'param_gosnomer',


			 [   
				'attribute' => 'date_param',
                'value' => 'date_param',
                'format' => ['date', 'php:d.m.Y'],
                'filter' =>   DatePicker::widget([
                   
				    'name' => 'DriversModelSearch[date_param]',
					'type' => DatePicker::TYPE_INPUT,
					
					'value' => $_GET['AutoparamsSearchModel']['date_param'], 
					
					'pluginOptions' => [
						'autoclose'=> true,
						'format' => 'yyyy-mm-dd',
					],
			   
                ]),

            ],
			
			
			
            [
				'class' => 'yii\grid\ActionColumn',
				'visibleButtons' => [
									'update' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
									'delete' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
								],
			],
        ],
    ]); ?>
</div>



<?php

$this->registerJs('

$(document).ready(function(){
	$(\'#MyButton\').click(function(){

	var HotId = $(\'.grid-view\').yiiGridView(\'getSelectedRows\');
	
	
	$.ajax({
		type: \'POST\',
		url : \'/admin/autoparams/multipledelete\',
		data : {row_id: HotId},
		success : function() {
			
			$(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
		
		}
	});

	});
	
	
	$(\'#remove-all\').click(function(){
	
		$.ajax({
			type: \'POST\',
			url : \'/admin/autoparams/removeall\',
			//data : {row_id: HotId},
			success : function() {
				
				//$(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
			
			}
		});
	
	
	});
	
	
	
	
});', \yii\web\View::POS_READY);

?>

