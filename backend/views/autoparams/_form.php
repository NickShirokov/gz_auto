<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Autoparams */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="autoparams-form">

    <?php $form = ActiveForm::begin(); ?>

	
	
	<?= $form->field($model, 'auto_id')->dropdownList( $arrAutomobiles ) ?>

	
    <?= $form->field($model, 'param_sts')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'param_gosnomer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_param')->input('date', ['value' => date("Y-m-d")]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
