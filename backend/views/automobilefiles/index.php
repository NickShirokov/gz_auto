<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use yii\grid\CheckboxColumn;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AutomobilefilesModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Все сканы по автомобилям');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="automobilefiles-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать скан доки авто'), ['create'], ['class' => 'btn btn-success']) ?>
		
		<?php
		if(
			Yii::$app->user->can('admin')) 
			{
					echo '<input type="button" class="btn btn-info" value="Удалить выбранные" id="MyButton" >';
			}
		?>
		
		
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		
		'pager' => [
			'class' => \kop\y2sp\ScrollPager::className(),
			'container' => '.grid-view tbody',
			'item' => 'tr',
			'paginationSelector' => '.grid-view .pagination',
			'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',
		],
		
		
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			['class' => 'yii\grid\CheckboxColumn'],
          
			
			
			[
				'attribute' => 'id_file',
				'format' => 'html',
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/automobilefiles/view?id='.$data['id_file'].'">'.$data['id_file'].'</a>';
					
								return $str;
					
							},
				
            ],
			
			
			
			[
				'attribute' => 'id_auto',
				'filter' => $arrAutomobiles,
				'format' => 'html',
				'value' => function ($data) {
					
							//print_r ($data);
					
							$str = '';
					
							if (!empty($data['automobiles'])) {
								$str.= '<a href="/admin/automobiles/view?id='.$data['automobiles'][0]['id_auto'].'">'
								
											.$data['automobiles'][0]['mark'].' '.$data['automobiles'][0]['model'].' '.$data['automobiles'][0]['gosnomer'].
											
										'</a>';
					
								
							}
					
							
							return $str;
					
				},
            ],
			
			
			[
				'attribute' => 'file',
				'format' => 'html',
				'value' => function($data) {
					
					
								//print_r ($data);
					
								$str = '';
					
								$str.= '<a href="/admin/automobilefiles/view?id='.$data['id_file'].'">'.$data['file'].'</a>';
					
								return $str;
					
							},
				
            ],
			
			
			
			
            'comment:ntext',

            [
				'class' => 'yii\grid\ActionColumn',
				'visibleButtons' => [
									'update' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
									'delete' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
								],
			],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>






<?php

$this->registerJs('

$(document).ready(function(){
	$(\'#MyButton\').click(function(){

	var HotId = $(\'.grid-view\').yiiGridView(\'getSelectedRows\');
	
	
	$.ajax({
		type: \'POST\',
		url : \'/admin/automobilefiles/multipledelete\',
		data : {row_id: HotId},
		success : function() {
			
			$(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
		
		}
	});

	});
});', \yii\web\View::POS_READY);

?>





