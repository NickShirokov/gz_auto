<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Automobilefiles */

$this->title = Yii::t('app', 'Создать изображение');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Изображения для машин'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="automobilefiles-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrAutomobiles' => $arrAutomobiles,
    ]) ?>

</div>
