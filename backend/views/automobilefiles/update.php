<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Automobilefiles */

$this->title = Yii::t('app', 'Обновить изображение: {name}', [
    'name' => $model->id_file,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Изображения авто'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_file, 'url' => ['view', 'id' => $model->id_file]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="automobilefiles-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'arrAutomobiles' => $arrAutomobiles,
    ]) ?>

</div>
