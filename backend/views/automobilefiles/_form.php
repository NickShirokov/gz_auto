<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Automobilefiles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="automobilefiles-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_auto')->dropdownList($arrAutomobiles,
	
					[
						'options' => 
							[ 
								$_GET['id'] => ['Selected' => true],
							],
					]
	
						) ?>

    <?= $form->field($model, 'file_f')->fileInput() ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
