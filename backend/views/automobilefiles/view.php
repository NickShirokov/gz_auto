<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Automobilefiles */

$this->title = $model->id_file.' '.$model->file;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Сканы документов по авто'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="automobilefiles-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?// Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_file], ['class' => 'btn btn-primary']) ?>
        <?/* Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_file], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы действительно хотите удалить этот документ?'),
                'method' => 'post',
            ],
        ])*/ ?>
		
		
		
		
		<?php
		if(
			Yii::$app->user->can('admin')) { 
			
			
			echo	Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id_file], ['class' => 'btn btn-primary']). ' '.
				Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id_file], [
					'class' => 'btn btn-danger',
					'data' => [
						'confirm' => Yii::t('app', 'Вы действительно хотите удалить этот документ?'),
						'method' => 'post',
					],
				]);
			
				;
				}
		?>
		
		
		
		
		
		
		
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_file',
			
            'id_auto',
            
			[
				'attribute' => 'file',
				'format' => 'html',
				'value' => function($data){
					
					$str = '<a href="/admin/uploads/'.$data->file.'" target="_blank">'.$data->file.'</a>';
					return $str;
				},
            ],
			
			
			'comment:ntext',
        ],
    ]) ?>

	<a href="/admin/uploads/<?= $model->file?>" target="_blank">
	
		<img style="width: 50%" src="/admin/uploads/<?= $model->file?>">
	</a>
	
</div>
