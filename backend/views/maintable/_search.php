<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MaintableModelSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="maintable-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id_contract') ?>

    <?= $form->field($model, 'id_auto') ?>

    <?= $form->field($model, 'id_driver') ?>

    <?= $form->field($model, 'status_name') ?>

    <?= $form->field($model, 'AutoMobile') ?>

    <?php // echo $form->field($model, 'date_start') ?>

    <?php // echo $form->field($model, 'FIO') ?>

    <?php // echo $form->field($model, 'arenda_stoim') ?>

    <?php // echo $form->field($model, 'ArendaSumma') ?>

    <?php // echo $form->field($model, 'MainSumma') ?>

    <?php // echo $form->field($model, 'ArendaDays') ?>

    <?php // echo $form->field($model, 'OplachenDo') ?>

    <?php // echo $form->field($model, 'FullDays') ?>

    <?php // echo $form->field($model, 'DolgPoArende') ?>

    <?php // echo $form->field($model, 'PayDogovorSumma') ?>

    <?php // echo $form->field($model, 'SummaOurPenalties') ?>

    <?php // echo $form->field($model, 'SummaOurPenaltiesPvznosVykup') ?>

    <?php // echo $form->field($model, 'DolgPoDogovoru') ?>

    <?php // echo $form->field($model, 'SummaPddPenalties') ?>

    <?php // echo $form->field($model, 'PayPddSumma') ?>

    <?php // echo $form->field($model, 'DolgPddSumma') ?>

    <?php // echo $form->field($model, 'MainDolg') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
