<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Maintable */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="maintable-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_contract')->textInput() ?>

    <?= $form->field($model, 'id_auto')->textInput() ?>

    <?= $form->field($model, 'id_driver')->textInput() ?>

    <?= $form->field($model, 'status_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'AutoMobile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_start')->textInput() ?>

    <?= $form->field($model, 'FIO')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'arenda_stoim')->textInput() ?>

    <?= $form->field($model, 'ArendaSumma')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MainSumma')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ArendaDays')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'OplachenDo')->textInput() ?>

    <?= $form->field($model, 'FullDays')->textInput() ?>

    <?= $form->field($model, 'DolgPoArende')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PayDogovorSumma')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SummaOurPenalties')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SummaOurPenaltiesPvznosVykup')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DolgPoDogovoru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SummaPddPenalties')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PayPddSumma')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DolgPddSumma')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MainDolg')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
