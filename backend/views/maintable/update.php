<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Maintable */

$this->title = Yii::t('app', 'Update Maintable: {name}', [
    'name' => $model->view_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Maintables'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->view_id, 'url' => ['view', 'id' => $model->view_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="maintable-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
