<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Maintable */

$this->title = Yii::t('app', 'Create Maintable');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Maintables'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maintable-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
