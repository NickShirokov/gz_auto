<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\MaintableModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Maintables');
$this->params['breadcrumbs'][] = $this->title;
?>


<?php echo \nterms\pagesize\PageSize::widget(); ?>	

<div class="maintable-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Maintable'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'filterSelector' => 'select[name="per-page"]',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_contract',
			'status_name',
            //'id_auto',
            //'id_driver',
            
            'AutoMobile',
            'date_start',
            'FIO:ntext',
            //'arenda_stoim',
            //'ArendaSumma',
            //'MainSumma',
            //'ArendaDays',
            'OplachenDo',
            //'FullDays',
            'DolgPoArende',
            
			'DolgPddSumma',
			
			'DolgPoDogovoru',
			
			//'PayDogovorSumma',
            //'SummaOurPenalties',
            //'SummaOurPenaltiesPvznosVykup',
            
            //'SummaPddPenalties',
            //'PayPddSumma',
            
            'MainDolg',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php //Pjax::end(); ?>
</div>
