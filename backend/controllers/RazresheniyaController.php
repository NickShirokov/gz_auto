<?php

namespace backend\controllers;

use Yii;
use app\models\Razresheniya;
use app\models\Licen;
use backend\models\RazresheniyaSearchModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * RazresheniyaController implements the CRUD actions for Razresheniya model.
 */
class RazresheniyaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
		
			[
				   'class' => AccessControl::className(),
				   'only' => ['index','create', 'update', 'delete'],
				   'rules' => [
						   [
								   'actions' => ['index','create', 'update', 'delete'],
								   'allow' => true,
								   'roles' => ['admin'],
						   ],
						   [
								   'actions' => [
												'index', 
												'view',
												],
								   'allow' => true,
								   'roles' => ['user', 'manager', 'payer'],
						   ],
				   ],

		   ],
			
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'create', 'view', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
		
		
		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Razresheniya models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RazresheniyaSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		
		$model_licens = Licen::find()->all();
		
		
		$arrLicens = array ();
		
		foreach ($model_licens as $value) {
			
			$arrLicens[$value->id_lic] = $value->name_lic;
		}
		
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrLicens' => $arrLicens,
        ]);
    }

    /**
     * Displays a single Razresheniya model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Razresheniya model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Razresheniya();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_razresh]);
        }
		
		
		$model_licens = Licen::find()->all();
		
		
		$arrLicens = array ();
		
		foreach ($model_licens as $value) {
			
			$arrLicens[$value->id_lic] = $value->name_lic;
		}
		
		

        return $this->render('create', [
            'model' => $model,
			'arrLicens' => $arrLicens,
        ]);
    }

    /**
     * Updates an existing Razresheniya model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_razresh]);
        }
		
		
		$model_licens = Licen::find()->all();
		
		
		$arrLicens = array ();
		
		foreach ($model_licens as $value) {
			
			$arrLicens[$value->id_lic] = $value->name_lic;
		}
		

        return $this->render('update', [
            'model' => $model,
			'arrLicens' => $arrLicens,
        ]);
    }

    /**
     * Deletes an existing Razresheniya model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Razresheniya model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Razresheniya the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Razresheniya::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
