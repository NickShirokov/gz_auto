<?php

namespace backend\controllers;

use Yii;
use app\models\Maintable;
use common\models\MaintableModelSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\Contracts;

use yii\filters\AccessControl;
use common\models\AutomobilesNewModel;

/**
 * MaintableController implements the CRUD actions for Maintable model.
 */
class MaintableController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
		
			[
				   'class' => AccessControl::className(),
				   'only' => ['index','create', 'view', 'update', 'delete', 'morearenda'],
				   'rules' => [
						   [
								   'actions' => ['index','create', 'view', 'update', 'delete', 'morearenda'],
								   'allow' => true,
								   'roles' => ['admin'],
						   ],
						   [
								   'actions' => [
												'index', 
												'view',
												'morearenda',
												],
								   'allow' => true,
								   'roles' => ['user', 'manager', 'payer'],
						   ],
				   ],

		   ],
			
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'create', 'view', 'update', 'delete', 'morearenda'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
		
		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Maintable models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MaintableModelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	
	
	
	
	
	
	   /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionSotrudniki()
    {
		
		//Выведем все авто со статусами
		
		$model_autos_statuses = [];
		
		
		$model_contracts = Contracts::find()->all();
		
		
		
		//print_r (count($model_contracts));
		
		
		
		$model_autos_statuses = AutomobilesNewModel::find()
						->select(['COUNT(*) AS aa', 'automobiles.*'])
						->joinWith(['auto_status'])
						->groupBy(['automobiles.status'])->all();
		

		
		

		
		//print_r ($model_autos_statuses);
		
		//exit;
		
		
		$searchModel = new MaintableModelSearch();
        $dataProvider = $searchModel->searchZP(Yii::$app->request->queryParams);

        return $this->render('sotrudniki', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'model_autos_statuses' => $model_autos_statuses,
        ]);
		
		
		
        
    }
	
	
	
	
	
	   /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionMorearenda()
    {
		
		//Выведем все авто со статусами
		
		$model_autos_statuses = [];
		
		
		$model_contracts = Contracts::find()->all();
		
		
		
		//print_r (count($model_contracts));
		
		
		
		$model_autos_statuses = AutomobilesNewModel::find()
						->select(['COUNT(*) AS aa', 'automobiles.*'])
						->joinWith(['auto_status'])
						->groupBy(['automobiles.status'])->all();
		

		
		

		
		//print_r ($model_autos_statuses);
		
		//exit;
		
		
		$searchModel = new MaintableModelSearch();
        $dataProvider = $searchModel->searchDolgARenda(Yii::$app->request->queryParams);

        return $this->render('morearenda', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'model_autos_statuses' => $model_autos_statuses,
        ]);
		
		
		
        
    }
	
	
	
	
	   /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionBigdolg()
    {
		
		//Выведем все авто со статусами
		
		$model_autos_statuses = [];
		
		
		$model_contracts = Contracts::find()->all();
		
		
		
		//print_r (count($model_contracts));
		
		
		
		$model_autos_statuses = AutomobilesNewModel::find()
						->select(['COUNT(*) AS aa', 'automobiles.*'])
						->joinWith(['auto_status'])
						->groupBy(['automobiles.status'])->all();
		

		
		

		
		//print_r ($model_autos_statuses);
		
		//exit;
		
		
		$searchModel = new MaintableModelSearch();
        $dataProvider = $searchModel->searchDolgFull(Yii::$app->request->queryParams);

        return $this->render('bigdolg', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'model_autos_statuses' => $model_autos_statuses,
        ]);
		
		
		
        
    }
	
	
	
	
	
	
	
	
	
	
	
	
	

    /**
     * Displays a single Maintable model.
     * @param  $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Maintable model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Maintable();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->view_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Maintable model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param  $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->view_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Maintable model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param  $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Maintable model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param  $id
     * @return Maintable the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Maintable::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
