<?php

namespace backend\controllers;

use Yii;
use app\models\Owners;
use backend\models\OwnersSearchModel;
use app\models\Licen;



use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use yii\web\UploadedFile;
use kartik\builder\TabularForm;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/**
 * OwnersController implements the CRUD actions for Owners model.
 */
class OwnersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
	{
		return [
			

			[
				   'class' => AccessControl::className(),
				   'only' => ['index', 
										'create', 
										'view', 
										'update', 
										'delete', 
										'import', 
										'multipledelete'],
				   'rules' => [
						   [
								   'actions' => ['index', 
										'create', 
										'view', 
										'update', 
										'delete', 
										'import', 
										'multipledelete'],
								   'allow' => true,
								   'roles' => ['admin'],
						   ],
						   [
								   'actions' => [
												'index', 
												'view',
												'create',
												],
								   'allow' => true,
								   'roles' => ['manager', 'payer'],
						   ],
						   
						   [
								   'actions' => [
												'index', 
												'view',
												
												],
								   'allow' => true,
								   'roles' => ['user'],
						   ],
				   ],

		   ],
			
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
										'logout', 
										'index', 
										'create', 
										'view', 
										'update', 
										'delete', 
										'import', 
										'multipledelete'
									],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
	  
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}
	
	
	
	
	
	
	
	
	public function getFormAttribs() {
		return [
			// primary key column
			'id'=>[ // primary key attribute
				'type'=>TabularForm::INPUT_HIDDEN, 
				'columnOptions'=>['hidden'=>true]
			], 
			'name'=>['type'=>TabularForm::INPUT_TEXT],
			'publish_date'=>[
				'type' => function($model, $key, $index, $widget) {
					return ($key % 2 === 0) ? TabularForm::INPUT_HIDDEN : TabularForm::INPUT_WIDGET;
				},
				'widgetClass'=>\kartik\widgets\DatePicker::classname(), 
				'options'=> function($model, $key, $index, $widget) {
					return ($key % 2 === 0) ? [] :
					[ 
						'pluginOptions'=>[
							'format'=>'yyyy-mm-dd',
							'todayHighlight'=>true, 
							'autoclose'=>true
						]
					];
				},
				'columnOptions'=>['width'=>'170px']
			],
			'color'=>[
				'type'=>TabularForm::INPUT_WIDGET, 
				'widgetClass'=>\kartik\color\ColorInput::classname(), 
				'options'=>[ 
					'showDefaultPalette'=>false,
					'pluginOptions'=>[
						'preferredFormat'=>'name',
						'palette'=>[
							[
								"white", "black", "grey", "silver", "gold", "brown", 
							],
							[
								"red", "orange", "yellow", "indigo", "maroon", "pink"
							],
							[
								"blue", "green", "violet", "cyan", "magenta", "purple", 
							],
						]
					]
				],
				'columnOptions'=>['width'=>'150px'],
			],
			
			'author_id'=>[
				'type'=>TabularForm::INPUT_DROPDOWN_LIST, 
				'items'=>ArrayHelper::map(Author::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
				'columnOptions'=>['width'=>'185px']
			],
			/*
			'buy_amount'=>[
				'type'=>TabularForm::INPUT_TEXT, 
				'label'=>'Buy',
				'options'=>['class'=>'form-control text-right'], 
				'columnOptions'=>['hAlign'=>GridView::ALIGN_RIGHT, 'width'=>'90px']
			],
			*/
			'sell_amount'=>[
				'type'=>TabularForm::INPUT_STATIC, 
				'label'=>'Sell',
				'columnOptions'=>['hAlign'=>GridView::ALIGN_RIGHT, 'width'=>'90px']
			],
		];
	}
	
	
	
	
	
	
	
	
	
	

    /**
     * Lists all Owners models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OwnersSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		
		
		
		//Категории лицензий
		$model_lics = Licen::find()->orderBy('name_lic ASC')->all();
		
		foreach ($model_lics as $value) {
			$arrLics[$value->id_lic] = $value->name_lic;
		}

		
		
		
		
		

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrLics' => $arrLics,
        ]);
    }

    /**
     * Displays a single Owners model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Owners model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Owners();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_owner]);
        }
		
		
			//Категории лицензий
		$model_lics = Licen::find()->orderBy('name_lic ASC')->all();
		
		foreach ($model_lics as $value) {
			$arrLics[$value->id_lic] = $value->name_lic;
		}
		
		

        return $this->render('create', [
            'model' => $model,
			'arrLics' => $arrLics,
        ]);
    }
	
	
	
	public function actionImport()
    {
        $model = new Owners();

        if ($model->load(Yii::$app->request->post())) {
            
			
			$t_m = 	$model;	
					
					
			$t_m->file_f = UploadedFile::getInstance($t_m,'file_f');
			
			$fileName = $t_m->file_f->name;
			
			
			//$post = Yii::$app->request->post();
			
			//$post['Driversfiles']['file_f'] = $t_m->file_f->name;
			
			$model->file_f->saveAs('uploads/'.$t_m->file_f->baseName.'.'.$t_m->file_f->extension);
			
			
			
			$file_path = 'uploads/'.$t_m->file_f->baseName.'.'.$t_m->file_f->extension;
			
			
			
			$filename = $file_path;

			// The nested array to hold all the arrays
			$the_big_array = []; 

			// Open the file for reading
			if (($h = fopen("{$filename}", "r")) !== FALSE) 
			{
			  // Each line in the file is converted into an individual array that we call $data
			  // The items of the array are comma separated
			  while (($data = fgetcsv($h, 1000, ";")) !== FALSE) 
			  {
				// Each individual array is being pushed into the nested array
				$the_big_array[] = $data;		
			  }

			  // Close the file
			  fclose($h);
			}

			
			
			
			
			unset ($the_big_array[0]);
			
			
			
			
			
			
			foreach ($the_big_array as $item) {
				
				
				
				
				$it = Owners::findOne($item[0]);
				
				//if ($it) {
				
					$model = new Owners();
				
					$model->id_owner = $item[0];
					$model->first_name = $item[1];
					$model->middle_name = $item[2];
					$model->last_name = $item[3];
					$model->p_series = $item[4];
					$model->p_number = $item[5];
					$model->p_who = $item[6];
					$model->address_reg = $item[7];
					$model->phone_1 = $item[8];
					$model->phone_2 = $item[9];
					$model->phone_3 = $item[10];
					
					if (empty($it)) {
					
						$model->save(false);
				//}
					}
				
				
				
			}
			
			
			
		
			
			return $this->redirect(['index']);
        }
		
		
	
		

        return $this->render('import', [
            'model' => $model,
			
        ]);
    }
	
	

    /**
     * Updates an existing Owners model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_owner]);
        }
		
		
		//Категории лицензий
		$model_lics = Licen::find()->orderBy('name_lic ASC')->all();
		
		foreach ($model_lics as $value) {
			$arrLics[$value->id_lic] = $value->name_lic;
		}
		
		

        return $this->render('update', [
            'model' => $model,
			'arrLics' => $arrLics,
        ]);
    }

    /**
     * Deletes an existing Owners model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Owners model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Owners the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
	 
	 
	 
	public function actionMultipledelete()
	{
		$pk = Yii::$app->request->post('row_id');
		
		

		foreach ($pk as $key => $value) {
			$sql = "DELETE FROM owners WHERE id_owner = $value";
			$query = Yii::$app->db->createCommand($sql)->execute();
		}

		return $this->redirect(['index']);

	} 
	 
	 
	 
	 
	 
	 
	 
	 
    protected function findModel($id)
    {
        if (($model = Owners::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
