<?php

namespace backend\controllers;

use Yii;
use common\models\Models;
use common\models\Marks;
use common\models\ModelsModelSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * ModelsController implements the CRUD actions for Models model.
 */
class ModelsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
			[
				   'class' => AccessControl::className(),
				   'only' => ['index','create', 'update', 'delete'],
				   'rules' => [
						   [
								   'actions' => ['index','create', 'update', 'delete'],
								   'allow' => true,
								   'roles' => ['admin'],
						   ],
						   [
								   'actions' => [
												'index', 
												'view',
												'create',
												],
								   'allow' => true,
								   'roles' => ['manager', 'payer'],
						   ],
						   
						   [
								   'actions' => [
												'index', 
												'view',
												
												],
								   'allow' => true,
								   'roles' => ['user'],
						   ],
				   ],

		   ],
			
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'create', 'view', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
		
		
		
		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Models models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ModelsModelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		$model_marks = Marks::find()->orderBy('name_mark ASC')->all();
		
		
		foreach ($model_marks as $value) {
			$arrMarks[$value->name_mark] = $value->name_mark;
		}
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrMarks' => $arrMarks,
        ]);
    }

    /**
     * Displays a single Models model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Models model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Models();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_model]);
        }
		
		$model_marks = Marks::find()->orderBy('name_mark ASC')->all();
		
		
		foreach ($model_marks as $value) {
			$arrMarks[$value->name_mark] = $value->name_mark;
		}
		
		

        return $this->render('create', [
            'model' => $model,
			'arrMarks' => $arrMarks,
        ]);
    }

    /**
     * Updates an existing Models model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_model]);
        }

        $model_marks = Marks::find()->orderBy('name_mark ASC')->all();
		
		
		foreach ($model_marks as $value) {
			$arrMarks[$value->name_mark] = $value->name_mark;
		}
		


		
		
		

        return $this->render('update', [
            'model' => $model,
			'arrMarks' => $arrMarks,
        ]);
    }

    /**
     * Deletes an existing Models model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Models model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Models the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
	 
	 
	 
	 

	 
    protected function findModel($id)
    {
        if (($model = Models::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
