<?php


//require_once '\vendor\mpdf\mpdf\src';

namespace backend\controllers;

use Mpdf\Mpdf;

use Yii;
use app\models\Contracts;
use app\models\Contractsstatus;
use backend\models\ContractsSearchModel;

use backend\models\PaymentsSearchModel;


use common\models\Marks;
use common\models\Models;
use backend\models\OurpenaltiesSearchModel;

use backend\models\Pddpenalties;

use backend\models\PddpenaltiesSearchModel;

use app\models\Payments;


use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\Owners;
use common\models\DriversModel;

use common\models\Driverscategory;
use common\models\Driversstatus;
use common\models\Penaltytypes;

use app\models\Driversfiles;

use common\models\AutomobilesNewModel;

use common\models\Autostatus;

use app\models\Arendatypes;
use app\models\Pddpenstatus;
use app\models\Pddpenstatusapi;
use app\models\Logitems;

use app\models\Ourpenaltiesstatus;
use app\models\Licen;
use DateTime;
use common\models\Insurancecompany;

use app\models\Templatecontractstext;

use app\models\Razresheniya;
use app\models\Prostoys;
use yii\filters\AccessControl;

use backend\models\PutevielistiSearchModel;




use yii\web\UploadedFile;

/**
 * ContractsController implements the CRUD actions for Contracts model.
 */
class ContractsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
		
		
		
			
                [
                       'class' => AccessControl::className(),
                       'only' => ['index', 
										'newcreate', 
										'view', 
										'create',
										'update',
										'delete',
										'card', 
										'payments',
										'pdfcontract',
										'pddshtrafs',
										'shtrafs',
										'owner',
										'auto',
										'contractsarchive',
										'autos',
										'contractsprepared',
										'putevoy',
										'putevie',
										'import',
										'multipledelete',
										'removeall',
										'pdfcard',
										'ownernew',
										'prodlenie',
										'peresadka',
										],
                       'rules' => [
                               [
                                       'actions' => [
									   'update',
									   'delete',
									   'index', 
										'newcreate', 
										'payments',
										'view', 
										'create', 
										'card', 
										'pdfcontract',
										'pddshtrafs',
										'shtrafs',
										'owner',
										'auto',
										'contractsarchive',
										'autos',
										'contractsprepared',
										'putevoy',
										'putevie',
										'import',
										'multipledelete',
										'removeall',
										'cardnaletu',
										'finish',
										'terminate',
										'pdfcard',
									   'ownernew',
									   'prodlenie',
									   'peresadka',
									   ],
                                       'allow' => true,
                                       'roles' => ['admin'],
                               ],
							   [
								   'actions' => [
												'index', 
												'view',
												'create',
												'payments',
												'update',
												'newcreate',
												'card',
												'putevoy',
												'putevie',
												'pdfcontract',
												'pddshtrafs',
												'shtrafs',
												'owner',
												'auto',
												'contractsarchive',
												'autos',
												'contractsprepared',
												'cardnaletu',
												'finish',
												'terminate',
												'pdfcard',
												'ownernew',
												'prodlenie',
												'peresadka',
												],
								   'allow' => true,
								   'roles' => ['manager', 'payer'],
								],
								
								 [
								   'actions' => [
												'index', 
												'view',
												
												
												
												],
								   'allow' => true,
								   'roles' => ['user'],
								],
                       ],

               ],
			
                'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
										'logout', 
										'index', 
										'newcreate', 
										'view', 
										'delete',
										'payments',
										'update',
										'create', 
										'card', 
										'pdfcontract',
										'pddshtrafs',
										'shtrafs',
										'owner',
										'auto',
										'contractsarchive',
										'autos',
										'contractsprepared',
										'putevoy',
										'putevie',
										'import',
										'multipledelete',
										'removeall',
										'cardnaletu',
										'finish',
										'terminate',
										'pdfcard',
										'ownernew',
										'prodlenie',
										'peresadka',
										],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
				
		
		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contracts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContractsSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
			
		//$dataProvider->pagination->pageSize=1000;
			
		//Категории владельцев
		$model_owners = Owners::find()->orderBy('first_name ASC')->all();
		
		foreach ($model_owners as $value) {
			$arrOwners[$value->id_owner] = $value->last_name.' '.$value->first_name.' '.$value->middle_name;
		}
		
		
		$model_contr_statuses = Contractsstatus::find()->orderBy('c_status_id ASC')->all();
		
		
		foreach ($model_contr_statuses as $value) {
			$arrCStatuses[$value->c_status_id] = $value->c_status_name;
		}
		
		
		
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrOwners' => $arrOwners,
			'arrCStatuses' => $arrCStatuses,
        ]);
    }

	
	
	public function actionPayments ($id) {
		
		
		$model = $this->findModel($id);
		
		//Прицепляем платежи
		$request = Yii::$app->request->queryParams;
		
		$request['PaymentsSearchModel']['contract_id'] = $id;
				
		$searchModel = new PaymentsSearchModel();
        $dataProvider = $searchModel->search($request);
		
		
		$par['PaymentsSearchModel']['contract_id'] = $id;

		$ttt = $searchModel->searchAll($par);
	
		
		return $this->render('payments', [
            'model' => $model,
			'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			
			'searchModel' => $searchModel,
	
			'arrAllPays' => $ttt,
			'arrAllShtrafs' => $sss,
		
		]);
		
		
		
	} 
	
	
	
	
	public function actionCard ($id) {
		
		$model = $this->findModel($id);
		
		
		$auto_id = $model->auto_id;
		
		
		$auto = new AutomobilesNewModel();
		
		$auto = $auto->findOne($auto_id);
		
		$driver_id = $model->driver_id;
		
		$driver = new DriversModel();
		
		$driver = $driver->findOne($driver_id);
		
		
		$licen_id = $auto->licen_id;
		
		
		$licen = new Licen();
		
		$licen = $licen->findOne($licen_id);
		
		
		
		return $this->render('card', [
            'model' => $model,
			'auto' => $auto,
			'driver' => $driver,
			'licen' => $licen,
		]);
	}
	
	
	
	
    /**
     * Displays a single Contracts model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
		
		//Прицепляем платежи
		$request = Yii::$app->request->queryParams;
		
		$request['PaymentsSearchModel']['contract_id'] = $id;
				
		$searchModel = new PaymentsSearchModel();
        $dataProvider = $searchModel->search($request);
		
		
		$par['PaymentsSearchModel']['contract_id'] = $id;

		$ttt = $searchModel->searchAll($par);
		
		$request['OurpenaltiesSearchModel']['p_contract_id'] = $id;
		
		//Прицепляем штрафы
		$par['OurpenaltiesSearchModel']['p_contract_id'] = $id;
		
		$searchModelP = new OurpenaltiesSearchModel();
        $dataProviderP = $searchModel->search($request);
		
		$sss = $searchModelP->searchAll($par);
		
		
		
		//Прицепляем штрафы ПДД
		$par['PddpenaltiesSearchModel']['contract_id'] = $id;
		
		$searchModelPdd = new PddpenaltiesSearchModel();
        $dataProviderPdd = $searchModel->search($request);
		
		$pdd = $searchModelPdd->searchAll($par);
		
		
		//print_r ($pdd);
		
		
		
		//Владелец
		$owners = Owners::find()->all();	
		$owner_id = $this->findModel($id)->owner_id;
		
		foreach ($owners as $item) {
			
			if ($item->id_owner == $owner_id) {
					
				$cur_owner = $item;
				break;
			}
			
		}
		
		
		//Водитель
		$drivers = DriversModel::find()->all();	
		$driver_id = $this->findModel($id)->driver_id;
		
		foreach ($drivers as $item) {
			
			if ($item->id_driver == $driver_id) {
					
				$cur_driver = $item;
				break;
			}
			
		}
		
		
		//Машина
		$autos = AutomobilesNewModel::find()->all();	
		$auto_id = $this->findModel($id)->auto_id;
		
		foreach ($autos as $item) {
			
			if ($item->id_auto == $auto_id) {
					
				$cur_auto = $item;
				break;
			}
			
		}
		
		$auto_status = $cur_auto->status;
		$asts = Autostatus::find()->where(['id_status'=> $auto_status])->all();	
		
		
		$model = $this->findModel($id);
		
		
		$ar_types = Arendatypes::find()->where(['a_d_type' => $this->findModel($id)->arenda_type])->all();
		
		$arType = $ar_types[0]->name_a_type;
		
		//print_r ($arType);
		//exit;
		
		$model->date_pay_arenda_new = $model->date_pay_arenda;
		
		//print_r ($model);
		
		$arrAllPays = $ttt;
		
		//Копипаста пока
				
		$full_summa = 0;

		//Стоимость аренды в день
		$arenda_item_summ = $model->arenda_stoim;
		//Количество дней, оплаченных по аренде

		$arenda_summa = 0;

		$dog_summa = 0;
		
		$prostoy_main = 0;
			
		$prostoy_days = 0;
		
		$prostoy_ful_summa = 0;
		
		//Добавляем простои
		
		
		$model_prostoys = Prostoys::find()->where(['contract_id' => $id])->all();
		
		
		if (!empty($model_prostoys)) {
			
			foreach ($model_prostoys as $item) {
				
				
				if ($item->type_opl == 2) {
					
					$prostoy_days = $prostoy_days + $item->days_prostoy;
				
				}
				
				
				
				$prostoy_ful_summa = $prostoy_ful_summa + $item->summa_prostoy * $item->days_prostoy;
				
			}
			
			
		}
		
	
		

		if (!empty($arrAllPays)) {
			
			
			$summa_week = 0;
			$summa_month = 0;
			
		
			
			foreach ($arrAllPays as $item) {
						
				$full_summa = $full_summa + $item['summa'];
				$arenda_summa = $arenda_summa + $item['arenda'];
				$dog_summa = $dog_summa + $item['pay_dogovor'];
				
				//Пробуем просуммировать за неделю и за месяц
				
				
				$date_raznost = date_diff(new DateTime($item['date_payment']), new DateTime())->days;
				
				if ($date_raznost < 7) {
					
					$summa_week = $summa_week + $item['arenda'];
				}
				
				if ($date_raznost < 30) {
					
					$summa_month = $summa_month + $item['arenda'];
				}
				
				
				
			
			}
			
		}
		
	
		$model->date_pay_arenda_new = date('d.m.Y', strtotime($model->date_pay_arenda_new. ' + '.$prostoy_days.' days'));
		
		$request = Yii::$app->request->queryParams;
		
		$request['PddpenaltiesSearchModel']['contract_id'] = $id;
			
		$searchModelPdd = new PddpenaltiesSearchModel();
		$dataProvider = $searchModel->search($request);
		
		
		$par = [];
		
		$par['PddpenaltiesSearchModel']['contract_id'] = $id;
		
		
		
		//Пробуем результатик
		$pdd_shtrafs = $searchModelPdd->searchAll($par);
		
	
		
		
		$summa = 0;
		
		$summa_res = 0;
		
		$summa_res_opl = 0;
		
		
		
		
		$summa_allll = 0;
		
		
		
		
		
		
		
		//Пробуем суммировать - для итого
		if (!empty($pdd_shtrafs)) {
			
			foreach ($pdd_shtrafs as $item) {
				
				if ($item['type_opl'] == 1) {
					
					
					$summa_opl_item = $item['summa_opl'];
					
					$summa = $summa + (int) ($item['summa_opl']);
					
					$skidka_status = $item['skidka_status'];
					$pen_com = $this->findModel($id)->penalty_comission;
					$dsc_type = $this->findModel($id)->discount_type;
					
					
					$date_raznost = date_diff(new DateTime($item['date_post']), new DateTime())->days;
					
					if ( $date_raznost <= 19 ) {
							
						//skidka_status
					
						//Если есть галочка скидка, то ебашим скидку 50%
						if ($skidka_status == 0) {
							if ($dsc_type == 1) {					
								$summa_opl_item = $summa_opl_item / 2;
							}
						}
						
					} elseif ($date_raznost >= 59) {
						
						$summa_opl_item = $summa_opl_item * 2;
						
					} else {
						//
					}
						
					//Если есть галочка комиссия, то добавляем 50 рублей
					if ($pen_com == 1) {
						
						$summa_opl_item = $summa_opl_item + 50;
					}
								
					$summa_res = $summa_res + $summa_opl_item;
				
				} else {
					
					$summa_opl_item = $item['summa_opl'];
					
					$summa = $summa + (int) ($item['summa_opl']);
					
					$skidka_status = $item['skidka_status'];
					$pen_com = $this->findModel($id)->penalty_comission;
					$dsc_type = $this->findModel($id)->discount_type;
					
					
					$date_raznost = date_diff(new DateTime($item['date_post']), new DateTime())->days;
					
					if ( $date_raznost <= 19 ) {
							
						//skidka_status
					
						//Если есть галочка скидка, то ебашим скидку 50%
						if ($skidka_status == 0) {
							if ($dsc_type == 1) {					
								$summa_opl_item = $summa_opl_item / 2;
							}
						}
						
					} elseif ($date_raznost >= 59) {
						
						$summa_opl_item = $summa_opl_item * 2;
						
					} else {
						//
					}
						
					//Если есть галочка комиссия, то добавляем 50 рублей
					if ($pen_com == 1) {
						
						$summa_opl_item = $summa_opl_item + 50;
					}
								
					$summa_res_opl = $summa_res_opl + $summa_opl_item;
					
				}
		
			}
			
		}
		
		
		
		
		
		if (!empty($pdd_shtrafs)) {
			
			foreach ($pdd_shtrafs as $item) {
				
				
				$summa_opl_item = $item['summa_opl'];
				
				$summa = $summa + (int) ($item['summa_opl']);
				
				$skidka_status = $item['skidka_status'];
				$pen_com = $this->findModel($id)->penalty_comission;
				$dsc_type = $this->findModel($id)->discount_type;
				
				
				$date_raznost = date_diff(new DateTime($item['date_post']), new DateTime())->days;
				
				if ( $date_raznost <= 19 ) {
						
					//skidka_status
				
					//Если есть галочка скидка, то ебашим скидку 50%
					if ($skidka_status == 0) {
						if ($dsc_type == 1) {					
							$summa_opl_item = $summa_opl_item / 2;
						}
					}
					
				} elseif ($date_raznost >= 59) {
					
					$summa_opl_item = $summa_opl_item * 2;
					
				} else {
					//
				}
					
				//Если есть галочка комиссия, то добавляем 50 рублей
				if ($pen_com == 1) {
					
					$summa_opl_item = $summa_opl_item + 50;
				}
							
				$summa_allll = $summa_allll + $summa_opl_item;

			
		
			}
		}
		
		//echo $summa_allll;

				
		
		///echo $summa_res_opl;
		
				
			
			
        return $this->render('view', [
            'model' => $model,
			'current_owner' => $cur_owner,
			'current_driver' => $cur_driver,
			'current_auto' => $cur_auto,
			'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			
			'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			
			'searchModelP' => $searchModelP,
			'dataProviderP' => $dataProviderP,
			
			'arrAllPays' => $ttt,
			'arrAllShtrafs' => $sss,
			'auto_status' => $asts[0]->status_name,
			'arrAllpdd' => $pdd, 
			'arType' => $arType,
			'sumRealAl' => $summa_res,
			'sumRealOpl' => $summa_res_opl, 
			'prostoy_ful_summa' => $prostoy_ful_summa,
			'prostoy_days' => $prostoy_days,
			'summa_allll' => $summa_allll,
		
		]);
    }

    /**
     * Creates a new Contracts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
	 
	 
	public function actionPddshtrafs($id) {
		
		$model = new Contracts();
		
		$request = Yii::$app->request->queryParams;
		
		$request['PddpenaltiesSearchModel']['contract_id'] = $id;
			
		$searchModel = new PddpenaltiesSearchModel();
		$dataProvider = $searchModel->search($request);
		
		
		$par = [];
		
		$par['PddpenaltiesSearchModel']['contract_id'] = $id;
		
		
		
		//Пробуем результатик
		$pdd_shtrafs = $searchModel->searchAll($par);
		

		
		$summa = 0;
		
		$summa_res = 0;
		
		//Пробуем суммировать - для итого
		if (!empty($pdd_shtrafs)) {
			
			foreach ($pdd_shtrafs as $item) {
				
				if ($item['type_opl'] == 1) {
					
					
					$summa_opl_item = $item['summa_opl'];
					
					$summa = $summa + (int) ($item['summa_opl']);
					
					$skidka_status = $item['skidka_status'];
					$pen_com = $this->findModel($id)->penalty_comission;
					$dsc_type = $this->findModel($id)->discount_type;
					
					
					$date_raznost = date_diff(new DateTime($item['date_post']), new DateTime())->days;
					
					if ( $date_raznost <= 19 ) {
							
						//skidka_status
					
						//Если есть галочка скидка, то ебашим скидку 50%
						if ($skidka_status == 0) {
							if ($dsc_type == 1) {					
								$summa_opl_item = $summa_opl_item / 2;
							}
						}
						
					} elseif ($date_raznost >= 59) {
						
						$summa_opl_item = $summa_opl_item * 2;
						
					} else {
						//
					}
						
					//Если есть галочка комиссия, то добавляем 50 рублей
					if ($pen_com == 1) {
						
						$summa_opl_item = $summa_opl_item + 50;
					}
								
					$summa_res = $summa_res + $summa_opl_item;
				}
			}
			
		}
		
		
		
		$model_pddpentls = Pddpenstatus::find()->all();
		
		foreach ($model_pddpentls as $value) {
			$arrPddpenstatus[$value->p_status_id] = $value->p_status_name;
		}
		
		
		$model_pddpentlsapi = Pddpenstatusapi::find()->all();
		
		foreach ($model_pddpentlsapi as $value) {
			$arrPddpenstatusapi[$value->p_status_id] = $value->p_status_name_api;
		}
		
		
		
		return $this->render('pddshtrafs', [
            'model' => $this->findModel($id),
			'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrPddpenstatus' => $arrPddpenstatus,
			'arrPddpenstatusapi' => $arrPddpenstatusapi,
			'sumAl' => $summa,
			'sumRealAl' => $summa_res,
			//'arrOwners' => $arrOwners,
			//'arrDrivers' => $arrDrivers,
			//'arrAutomobiles' => $arrAutomobiles,
			//'arrArendatypes' => $arrArendatypes,
        ]);
		
	} 
	
	
	
	//Тестовый метод для оплаты штрафа
	public function actionPddshtraftest($id) {
	
	
		set_time_limit(500); 
	
		$searchModel = new PddpenaltiesSearchModel();
		//$dataProvider = $searchModel->search($request);
		
		$par = [];
		
		$par['PddpenaltiesSearchModel']['contract_id'] = $id;
		
		//Пробуем результатик
		$pdd_shtrafs = $searchModel->searchAll($par);
		
		$pdd_shtrafs_n_post_arr = [];
		
		foreach ($pdd_shtrafs as $item) {
			
			
			$pdd_shtrafs_n_post_arr[] = $item->n_post;
		}
			
		
		$host = 'https://parser-api.com/parser/fines_api/';
		//$host = 'http://parser-api.com/parser/fines_api/';
		
		$key = '83dcccbc2e20d1ec536acdbb2e2b2db2';
		
		$regNumber = 'Е732ТТ750';
		//Поднять
		$regNumber = mb_strtoupper($regNumber);
		
		
		$sts = '9906383141';
		
		$url = $host.'?key='.$key.'&regNumber='.$regNumber.'&sts='.$sts;
		
		$res = json_decode(file_get_contents($url), true);

		$api_pdd_arr = $res['fines'];
		
		if (!empty($res['fines'])){
	
			$api_pdd_arr = $res['fines'];
			
			$connection = Yii::$app->db;
			
			$new_num_post_arr = [];
			
			//Сначала пройдемся по апишным штрафам и вставим их в базу
			foreach ($api_pdd_arr as $api_sh) {
				

				$new_num_post_arr[] = $api_sh['num_post'];
				
				
				//Проверочка, чтобы не делать лишних вставок
				if (!in_array($api_sh['num_post'], $pdd_shtrafs_n_post_arr)) {
					
						// выполняем команду вставки (параметры таблица Country, массив с данными) и выполняем запрос
						$connection->createCommand()->insert('pdd_penalties', 
										[
											'contract_id' => $id, 
											'real_sts' => $sts, 
											'n_post' => $api_sh['num_post'],
											'date_post' => $api_sh['date_post'],
											'date_narush' => $api_sh['date_decision'],
											
											'type_opl_api' => 1, 
											'summa_opl' => $api_sh['sum'], 
											'skidka_status' => 0, 
											'comment' => $api_sh['koap_text'], 
											
										])->execute();
					
					
					
				}
				
				

			}
			
			
			//Далее пройдемся по нашему массиву и проверим наличие, если его нет, то выставим статус
			
			if (!empty($pdd_shtrafs)) {
				
				foreach ($pdd_shtrafs as $item) {
					
					if (!in_array($item->n_post, $new_num_post_arr)) {
						
						$connection->createCommand()->update('pdd_penalties', ['type_opl_api' => 3], 'n_post = '.$item->n_post)->execute();
					}
					
				}
				
			}
			
		}
	
		return $this->redirect(['pddshtrafs', 'id' => $id]);
	
		//return 1;
	}
	
	 
	//Наши штрафы 
	public function actionShtrafs($id) {
		
		$model = new Contracts();
			
		$request = Yii::$app->request->queryParams;
		
		$request['OurpenaltiesSearchModel']['p_contract_id'] = $id;
		
		$searchModel = new OurpenaltiesSearchModel();
			
        $dataProvider = $searchModel->search($request);
			
		$model_ourpentls = Ourpenaltiesstatus::find()->all();
		
		foreach ($model_ourpentls as $value) {
			$arrOurpenstatus[$value->status_o_id] = $value->status_o_name;
		}
		
		
		$op = $searchModel->searchAll($request);
		
			
		
		return $this->render('shtrafs', [
            'model' => $this->findModel($id),
			'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrOurpenstatus' => $arrOurpenstatus,
			'arrOpen' => $op,
			//'arrOwners' => $arrOwners,
			//'arrDrivers' => $arrDrivers,
			//'arrAutomobiles' => $arrAutomobiles,
			//'arrArendatypes' => $arrArendatypes,
        ]);
		
	}  
	 
	 
	 
    public function actionCreate()
    {
        $model = new Contracts();

        if ($model->load(Yii::$app->request->post())) {
           
		    //Обновим статус авто
			if ($model->status_itog == 3) {
			

				$auto = AutomobilesNewModel::find()->where(['id_auto' => $model->auto_id])->all();

				if (!empty($auto[0])) {
			
			
					//print_r ($auto[0]);
			
					$auto[0]->status = 5;
					$auto[0]->save();
				}
			
			} elseif ($model->status_itog == 4) { 
			
				$auto = AutomobilesNewModel::find()->where(['id_auto' => $model->auto_id])->all();

				if (!empty($auto[0])) {
			
			
					//print_r ($auto[0]);
			
					$auto[0]->status = 6;
					$auto[0]->save();
				}
			
			
			
			} elseif ($model->status_itog == 7) {

				$auto = AutomobilesNewModel::find()->where(['id_auto' => $model->auto_id])->all();

				if (!empty($auto[0])) {
			
			
					//print_r ($auto[0]);
			
					$auto[0]->status = 9;
					$auto[0]->save();
				}

			} elseif ($model->status_itog == 1) {
				
				
				$auto = AutomobilesNewModel::find()->where(['id_auto' => $model->auto_id])->all();

				if (!empty($auto[0])) {
			
			
					//print_r ($auto[0]);
			
					$auto[0]->status = 4;
					$auto[0]->save();
				}
				
				
			} elseif ($model->status_itog == 8) {
			    
		    	$auto = AutomobilesNewModel::find()->where(['id_auto' => $model->auto_id])->all();

				if (!empty($auto[0])) {
			
			
					//print_r ($auto[0]);
			
					$auto[0]->status = 10;
					$auto[0]->save();
				}
			    
			    
			} else {
			    $auto = AutomobilesNewModel::find()->where(['id_auto' => $model->auto_id])->all();

				if (!empty($auto[0])) {
			
			
					//print_r ($auto[0]);
			
					$auto[0]->status = 1;
					$auto[0]->save();
				}
			}
			
			
			if (empty($model->arenda_type)) {
			    $model->arenda_type = 1;
			}
			
			
			if (empty($model->arenda_items)) {
			    $model->arenda_items = 365;
			}
			
			
			$new_date_end = 0;
							
							//$arenda_items
							
							//$arenda_type
							
						
							
			if ($arenda_type == 1) {
				
				$koeff  = 1;
				
			} elseif ($arenda_type == 2) {
				
				$koeff = 7; 
				
			} elseif ( $arenda_type == 3) {
				$koeff = 30; 
			} else {
				$koeff = 1; 
			}
			
			$dddays = $model->arenda_items * $koeff;
				
			if (empty($model->date_pay_arenda)) {
			    $model->date_pay_arenda = date('Y-m-d');
			}	
				
				
			$new_date_end = date('Y-m-d',strtotime($model->date_pay_arenda. ' + '.$dddays.' days'));

			
			//Дата окончания новая
			$model->date_end = $new_date_end;
			
			if ($model->status_itog == 8 or $model->status_itog == 7) {
				$model->date_end = '2099-12-31';
			}
			
			
			$model->save();
			
			
			$modelLogitems = new Logitems ();
			
			$user_id = Yii::$app->user->identity->id;
			
			//Yii::$app->authManager->getRolesByUser($user_id);
			$type_user = current(Yii::$app->authManager->getRolesByUser( Yii::$app->user->identity->id))->name;
			$modelLogitems->type_user = $type_user;
			$modelLogitems->user_id = $user_id;
			$modelLogitems->type_page = 'contract';
			$modelLogitems->item_id = $model->id_contract;
			$modelLogitems->date_create = date('Y-m-d');
						
			$modelLogitems->save();
			
			
			
            return $this->redirect(['view', 'id' => $model->id_contract]);
		   
		   
        }
		
		
		//Владелец
		$model_owner = Owners::find()->orderBy('first_name ASC')->all();
		
		foreach ($model_owner as $value) {
			$arrOwners[$value->id_owner] = $value->last_name.' '.$value->first_name.' '.$value->middle_name;
		}
		
		//Водитель
		$model_driver = DriversModel::find()->where(['not', ['status' => 5]])->orderBy('first_name ASC')->all();
		

		foreach ($model_driver as $value) {
			$arrDrivers[$value->id_driver] = $value->last_name.' '.$value->first_name.' '.$value->middle_name;
		}
		
		
		//Авто
		$model_auto = AutomobilesNewModel::find()->where(['status' => 1])->orderBy('mark, model ASC')->all();
		
		foreach ($model_auto as $value) {
			$arrAutomobiles[$value->id_auto] = $value->mark.' '.$value->model.' '.$value->gosnomer;
		}
		
		
		
		//Тип аренды 
		$model_arendatypes = Arendatypes::find()->orderBy('a_d_type ASC')->all();
		
		foreach ($model_arendatypes as $value) {
			$arrArendatypes[$value->a_d_type] = $value->name_a_type;
		}
		
		
		
		
			
		$model_contr_statuses = Contractsstatus::find()->orderBy('c_status_id ASC')->all();
		
		
		foreach ($model_contr_statuses as $value) {
			$arrCStatuses[$value->c_status_id] = $value->c_status_name;
		}
		
		
		
		$model_texts = Templatecontractstext::find()->all();
		
		
	
		$arrTexts = $model_texts[0];
	
		if (!$model->text1) {
			$model->text1 = $arrTexts->text1;
		}
		
		if (!$model->text2) {
			$model->text2 = $arrTexts->text2;
		}
		
		if (!$model->text3) {
			$model->text3 = $arrTexts->text3;
		}
		
		if (!$model->text4) {
			$model->text4 = $arrTexts->text4;
		}
		
		if (!$model->text5) {
			$model->text5 = $arrTexts->text5;
		}
	
		
		

        return $this->render('create', [
            'model' => $model,
			'arrOwners' => $arrOwners,
			'arrDrivers' => $arrDrivers,
			'arrAutomobiles' => $arrAutomobiles,
			'arrArendatypes' => $arrArendatypes,
			'arrCStatuses' => $arrCStatuses,
			'arrTexts' => $arrTexts,
        ]);
    }
	
	
	
	
	
	 public function actionNewcreate()
    {
        $model = new Contracts();
		
		
		
		
		$model->arenda_stoim = '';
		$model->p_vznos = 0;
		
		$model->date_start = date('Y-m-d');
		
		

		
		$model->arenda_items = 365;
							
		
		
		$model->arenda_type = 1;
		$model->prosrochka = 5000;
		$model->peredacha_drugomu = 100000;
		$model->vyezd_bez_sogl_200 = 100000;
		$model->no_put_list = 100000;
		$model->shtraf_pdd = 2;
		$model->snyat_atrib = 100000;
		$model->no_osago = 50000;
		$model->poterya_docs = 50000;
		$model->poterya_keys = 10000;
		$model->no_prodl_diag  = 50000;
		$model->vyezd_sotrudnikov = 10000;
		$model->penalty_comission = 1;
		$model->discount_type = 1;
								
		
		

		$modelAuto = new AutomobilesNewModel();

		$modelDriver = new DriversModel();

		$model_marks = Marks::find()->orderBy('name_mark ASC')->all();
		
		
		foreach ($model_marks as $value) {
			$arrMarks[$value->name_mark] = $value->name_mark;
		}
		
		
		
		//Модели
		$model_models = Models::find()->orderBy('name ASC')->all();
		
		
		foreach ($model_models as $value) {
			$arrModels[$value->name] = $value->name;
		}
		
		//Страховые
		$model_insurance = Insurancecompany::find()->orderBy('name ASC')->all();
		
		
		foreach ($model_insurance as $value) {
			$arrIns[$value->id_company] = $value->name;
		}
			
		
		//Статусы
		$model_status = Autostatus::find()->orderBy('status_name ASC')->all();
					
		foreach ($model_status as $value) {
			$arrStatus[$value->id_status] = $value->status_name;
		}
		
		
		// Владельцы
		$arrOwners = [];
		
		
		$model_owners = Owners::find()->orderBy('id_owner ASC')->all();
					
		foreach ($model_owners as $value) {
			$arrOwners[$value->id_owner] = $value->first_name.' '.$value->middle_name.' '.$value->last_name;
		}
		
		
		$arrLicen = [];
		
		$model_licens = Licen::find()->all();
		
		
		foreach ($model_licens as $value) {
			
			
			
			$arrLicen[$value->id_lic] = $value->name_lic;
			
		}
		
		
		
		$arrRazresh = [];
		
		$model_razresh = Razresheniya::find()->all();
		
		
		
		foreach ($model_razresh as $value) {
			
			
			
			
			$arrRazresh[$value->id_razresh] = 'Лицензия №'.$value->licen_id.' '.$value->r_n.' Серия '.$value->series.' № ' .$value->number; 
			
		}
		
		
		



		$model_drvrcat = Driverscategory::find()->orderBy('id_cat ASC')->all();
		
		foreach ($model_drvrcat as $value) {
			$arrCat[$value->id_cat] = $value->category_name;
		}
		
		$model_drvrstatus = Driversstatus::find()->orderBy('id_status ASC')->all();
		
		foreach ($model_drvrstatus as $value) {
			$arrStatusDr[$value->id_status] = $value->status_name;
		}
				
		//Типы штрафов
		$model_penaltytype = Penaltytypes::find()->orderBy('id_type ASC')->all();
		
		foreach ($model_penaltytype as $value) {
			$arrPenaltytype[$value->id_type] = $value->name_type;
		}
		
				
		
		
        if ($model->load(Yii::$app->request->post())) {
			
			
			
			$postBigDats = Yii::$app->request->post();
			
			
			//print_r ($postBigDats);
			
			//exit;
			
			if ($postBigDats['Contracts']['newcreateauto'] == 1) {
				
				$modelAuto->load($postBigDats);
				
				$modelAuto->save(false);
				
				$modelAutosss = AutomobilesNewModel::find()->where(['VIN' => $postBigDats['AutomobilesNewModel']['VIN']])->all();
				
				$auto_id = $modelAutosss[0]->id_auto;
				
				$model->auto_id = $auto_id;

				$owner_id = $modelAutosss[0]->owner_id;
				
				$model->owner_id = $owner_id;
			} 
			
			
			if ($postBigDats['Contracts']['newcreatedriver'] == 1) {
				
				$modelDriver->load($postBigDats);
				
				
				$fio = trim($modelDriver->last_name).' '.trim($modelDriver->first_name).' '.trim($modelDriver->middle_name);
				
				$modelDriver->fio = $fio;
				
				$modelDriver->save(false);
				
				$modelDriverssss = DriversModel::find()->where(['fio' => $fio])->all();
				
				$driver_id = $modelDriverssss[0]->id_driver;
				
				$model->driver_id = $driver_id;

				
			} 
			
			
			
           
		    //Обновим статус авто
			if ($model->status_itog == 3) {
			

				$auto = AutomobilesNewModel::find()->where(['id_auto' => $model->auto_id])->all();

				if (!empty($auto[0])) {
			
			
					//print_r ($auto[0]);
			
					$auto[0]->status = 5;
					$auto[0]->save();
				}
			
			} elseif ($model->status_itog == 4) { 
			
				$auto = AutomobilesNewModel::find()->where(['id_auto' => $model->auto_id])->all();

				if (!empty($auto[0])) {
			
			
					//print_r ($auto[0]);
			
					$auto[0]->status = 6;
					$auto[0]->save();
				}
			
			
			
			} elseif ($model->status_itog == 7) {

				$auto = AutomobilesNewModel::find()->where(['id_auto' => $model->auto_id])->all();

				if (!empty($auto[0])) {
			
			
					//print_r ($auto[0]);
			
					$auto[0]->status = 9;
					$auto[0]->save();
				}

			} elseif ($model->status_itog == 1) {
				
				
				$auto = AutomobilesNewModel::find()->where(['id_auto' => $model->auto_id])->all();

				if (!empty($auto[0])) {
			
			
					//print_r ($auto[0]);
			
					$auto[0]->status = 4;
					$auto[0]->save();
				}
				
				
			} elseif ($model->status_itog == 8) {
			    
		    	$auto = AutomobilesNewModel::find()->where(['id_auto' => $model->auto_id])->all();

				if (!empty($auto[0])) {
			
			
					//print_r ($auto[0]);
			
					$auto[0]->status = 10;
					$auto[0]->save();
				}
			    
			    
			} else {
			    $auto = AutomobilesNewModel::find()->where(['id_auto' => $model->auto_id])->all();

				if (!empty($auto[0])) {
			
			
					//print_r ($auto[0]);
			
					$auto[0]->status = 1;
					$auto[0]->save();
				}
			}
			
			
				if (empty($model->arenda_type)) {
			    $model->arenda_type = 1;
			}
			
			
			if (empty($model->arenda_items)) {
			    $model->arenda_items = 365;
			}
			
			
			$new_date_end = 0;
							
							//$arenda_items
							
							//$arenda_type
							
						
							
			if ($arenda_type == 1) {
				
				$koeff  = 1;
				
			} elseif ($arenda_type == 2) {
				
				$koeff = 7; 
				
			} elseif ( $arenda_type == 3) {
				$koeff = 30; 
			} else {
				$koeff = 1; 
			}
			
			$dddays =  $model->arenda_items * $koeff;
				
			if (empty($model->date_pay_arenda)) {
			    $model->date_pay_arenda = date('Y-m-d');
			}	
				
				
			$new_date_end = date('Y-m-d',strtotime($model->date_pay_arenda. ' + '.$dddays.' days'));

			
			//Дата окончания новая
			$model->date_end = $new_date_end;
			
			if ($model->status_itog == 8 or $model->status_itog == 7) {
				$model->date_end = '2099-12-31';
			}
			
			
			
			
			$model->save();
			
			
			$modelLogitems = new Logitems ();
			
			$user_id = Yii::$app->user->identity->id;
			
			//Yii::$app->authManager->getRolesByUser($user_id);
			$type_user = current(Yii::$app->authManager->getRolesByUser( Yii::$app->user->identity->id))->name;
			$modelLogitems->type_user = $type_user;
			$modelLogitems->user_id = $user_id;
			$modelLogitems->type_page = 'contract';
			$modelLogitems->item_id = $model->id_contract;
			$modelLogitems->date_create = date('Y-m-d');
						
			$modelLogitems->save();
			
			
			
			
            return $this->redirect(['view', 'id' => $model->id_contract]);
		   
		   
        }
		
		
		//Владелец
		$model_owner = Owners::find()->orderBy('first_name ASC')->all();
		
		foreach ($model_owner as $value) {
			$arrOwners[$value->id_owner] = $value->last_name.' '.$value->first_name.' '.$value->middle_name;
		}
		
		//Водитель
		$model_driver = DriversModel::find()->where(['not', ['status' => 5]])->orderBy('first_name ASC')->all();
		

		foreach ($model_driver as $value) {
			$arrDrivers[$value->id_driver] = $value->last_name.' '.$value->first_name.' '.$value->middle_name;
		}
		
		
		//Авто
		$model_auto = AutomobilesNewModel::find()->where(['status' => 1])->orderBy('mark, model ASC')->all();
		
		foreach ($model_auto as $value) {
			$arrAutomobiles[$value->id_auto] = $value->mark.' '.$value->model.' '.$value->gosnomer;
		}
		
		
		
		//Тип аренды 
		$model_arendatypes = Arendatypes::find()->orderBy('a_d_type ASC')->all();
		
		foreach ($model_arendatypes as $value) {
			$arrArendatypes[$value->a_d_type] = $value->name_a_type;
		}
		
		
		
		
			
		$model_contr_statuses = Contractsstatus::find()->orderBy('c_status_id ASC')->all();
		
		
		foreach ($model_contr_statuses as $value) {
			$arrCStatuses[$value->c_status_id] = $value->c_status_name;
		}
		
		
		
		$model_texts = Templatecontractstext::find()->all();
		
		
	
		$arrTexts = $model_texts[0];
	
		if (!$model->text1) {
			$model->text1 = $arrTexts->text1;
		}
		
		if (!$model->text2) {
			$model->text2 = $arrTexts->text2;
		}
		
		if (!$model->text3) {
			$model->text3 = $arrTexts->text3;
		}
		
		if (!$model->text4) {
			$model->text4 = $arrTexts->text4;
		}
		
		if (!$model->text5) {
			$model->text5 = $arrTexts->text5;
		}
	
		
		

        return $this->render('newcreate', [
            'model' => $model,
            'modelAuto' => $modelAuto,
			'modelDriver' => $modelDriver,
			'arrOwners' => $arrOwners,
			'arrDrivers' => $arrDrivers,
			'arrAutomobiles' => $arrAutomobiles,
			'arrArendatypes' => $arrArendatypes,
			'arrCStatuses' => $arrCStatuses,
			'arrTexts' => $arrTexts,
			
			'arrMarks' => $arrMarks,
			'arrIns' => $arrIns,
			'arrModels' => [],
			'arrStatus' => $arrStatus,	
			'arrOwners' => $arrOwners,
			'arrLicen' => $arrLicen,
			'arrRazresh' => $arrRazresh,
			
			
			'arrCat' => $arrCat,
			'arrStatus' => $arrStatus,
			'arrStatusDr' => $arrStatusDr,
			'arrPenaltytype' => $arrPenaltytype,
			
			
			
        ]);
    }

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

    /**
     * Updates an existing Contracts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
			
			
			
			
			
			//Обновим статус авто
			if ($model->status_itog == 3) {
			

				$auto = AutomobilesNewModel::find()->where(['id_auto' => $model->auto_id])->all();

				if (!empty($auto[0])) {
			
			
					//print_r ($auto[0]);
			
					$auto[0]->status = 5;
					$auto[0]->save();
				}
			
			} elseif ($model->status_itog == 4) { 
			
				$auto = AutomobilesNewModel::find()->where(['id_auto' => $model->auto_id])->all();

				if (!empty($auto[0])) {
			
			
					//print_r ($auto[0]);
			
					$auto[0]->status = 6;
					$auto[0]->save();
				}
			
			
			
			} elseif ($model->status_itog == 7) {

				$auto = AutomobilesNewModel::find()->where(['id_auto' => $model->auto_id])->all();

				if (!empty($auto[0])) {
			
			
					//print_r ($auto[0]);
			
					$auto[0]->status = 9;
					$auto[0]->save();
				}

			} elseif ($model->status_itog == 1) {
				
				
				$auto = AutomobilesNewModel::find()->where(['id_auto' => $model->auto_id])->all();

				if (!empty($auto[0])) {
			
			
					//print_r ($auto[0]);
			
					$auto[0]->status = 4;
					$auto[0]->save();
				}
				
				
			} elseif ($model->status_itog == 8) {
			    
		    	$auto = AutomobilesNewModel::find()->where(['id_auto' => $model->auto_id])->all();

				if (!empty($auto[0])) {
			
			
					//print_r ($auto[0]);
			
					$auto[0]->status = 10;
					$auto[0]->save();
				}
			    
			    
			} else {
			    $auto = AutomobilesNewModel::find()->where(['id_auto' => $model->auto_id])->all();

				if (!empty($auto[0])) {
			
			
					//print_r ($auto[0]);
			
					$auto[0]->status = 1;
					$auto[0]->save();
				}
			}
			
			
			if (empty($model->arenda_type)) {
			    $model->arenda_type = 1;
			}
			
			
			if (empty($model->arenda_items)) {
			    $model->arenda_items = 365;
			}
			
			
			$new_date_end = 0;
							
							//$arenda_items
							
							//$arenda_type
							
						
							
			if ($arenda_type == 1) {
				
				$koeff  = 1;
				
			} elseif ($arenda_type == 2) {
				
				$koeff = 7; 
				
			} elseif ( $arenda_type == 3) {
				$koeff = 30; 
			} else {
				$koeff = 1; 
			}
			
			$dddays =  $model->arenda_items * $koeff;
				
			if (empty($model->date_pay_arenda)) {
			    $model->date_pay_arenda = date('Y-m-d');
			}	
				
				
			$new_date_end = date('Y-m-d',strtotime($model->date_pay_arenda. ' + '.$dddays.' days'));

			
			//Дата окончания новая
			$model->date_end = $new_date_end;
			
			if ($model->status_itog == 8 or $model->status_itog == 7) {
				$model->date_end = '2099-12-31';
			}
			
			
			$model->save();
			
			
			
			
            return $this->redirect(['view', 'id' => $model->id_contract]);
        }
		
		
		
		//$model['owner_id']
		//$model['driver_id']
		//$model['auto_id']
		
		//Владелец
		$model_owner = Owners::find()->orderBy('first_name ASC')->all();
		
		foreach ($model_owner as $value) {
			$arrOwners[$value->id_owner] = $value->last_name.' '.$value->first_name.' '.$value->middle_name;
		}
		
		//Водитель
		$model_driver = DriversModel::find()->where(['not', ['status' => 5]])->orderBy('first_name ASC')->all();
		
		foreach ($model_driver as $value) {
			$arrDrivers[$value->id_driver] = $value->last_name.' '.$value->first_name.' '.$value->middle_name;
		}
		
		
		
		
		//Авто
		$model_auto = AutomobilesNewModel::find()->where(['status' => 1])->orderBy('mark, model ASC')->all();
		
		foreach ($model_auto as $value) {
			$arrAutomobiles[$value->id_auto] = $value->mark.' '.$value->model.' '.$value->gosnomer;
		}
		
		
		
		
		//
		$owner = Owners::find()->where(['id_owner' => $model['owner_id']])->orderBy ('first_name ASC')->all();
		
		
		//
		$owner = $owner[0];
		
		$res_owner = 'ФИО '.$owner->last_name.' '.$owner->first_name. ' '. $owner->middle_name.'<br>';
		
		$res_owner.= 'Паспорт '.$owner->p_series.' '.$owner->p_number. '<br>';
		
		$res_owner.= 'Лицензия '.$owner->licence_company;
		
			
		//
		
		$driver = DriversModel::find()->where(['id_driver' => $model['driver_id']])->orderBy ('first_name ASC')->all();
		
		$driver = $driver[0];
				
		$res_driver = 'ФИО '.$driver->last_name.' '.$driver->first_name. ' '. $driver->middle_name.'<br>';
		
		$res_driver.= 'Паспорт '.$driver->p_series.' '.$driver->p_number. '<br>';
		
		$res_driver.= 'Дата рождения '.$driver->date_birth.'<br>';
		
		
		$res_driver.= 'Адрес фактического проживания '.$driver->address_fact.'<br>';
		$res_driver.= 'Телефон '.$driver->phone_1.'<br>';
		$res_driver.= 'Вод.удостоверение '.$driver->lic_series.' '.$driver->lic_number. '<br>';
		
		//
		$auto = AutomobilesNewModel::find()->where(['id_auto' => $model['auto_id']])->orderBy ('mark ASC')->all();
		
		$auto = $auto[0];
		
		
		$arrAutomobiles[$auto->id_auto] = $auto->mark.' '.$auto->model.' '.$auto->gosnomer;
			
		$res_auto = 'Марка Модель '.$auto->mark.' '.$auto->model. ' '. $auto->gosnomer.'<br>';
	
	
		//Тип аренды 
		$model_arendatypes = Arendatypes::find()->orderBy('a_d_type ASC')->all();
		
		foreach ($model_arendatypes as $value) {
			$arrArendatypes[$value->a_d_type] = $value->name_a_type;
		}
	
	
		$model_contr_statuses = Contractsstatus::find()->orderBy('c_status_id ASC')->all();
		
		
		foreach ($model_contr_statuses as $value) {
			$arrCStatuses[$value->c_status_id] = $value->c_status_name;
		}
	
	
			
		$model_texts = Templatecontractstext::find()->all();
		
		
	
		$arrTexts = $model_texts[0];
	
		if (!$model->text1) {
			$model->text1 = $arrTexts->text1;
		}
		
		if (!$model->text2) {
			$model->text2 = $arrTexts->text2;
		}
		
		if (!$model->text3) {
			$model->text3 = $arrTexts->text3;
		}
		
		if (!$model->text4) {
			$model->text4 = $arrTexts->text4;
		}
		
		if (!$model->text5) {
			$model->text5 = $arrTexts->text5;
		}
	

		
        return $this->render('update', [
            'model' => $model,
			'arrOwners' => $arrOwners,
			'arrDrivers' => $arrDrivers,
			'arrAutomobiles' => $arrAutomobiles,
			'res_owner' => $res_owner,
			'res_driver' => $res_driver,
			'res_auto' => $res_auto,
			'arrArendatypes' => $arrArendatypes,
			'arrCStatuses' => $arrCStatuses,
			'arrTexts' => 'arrTexts',
        ]);
    }

    /**
     * Deletes an existing Contracts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
	
	
	
	
	//Пробуем создать подсказку владельца по $id для договора
	 
	public  function actionOwner ($id) {
		
		$owner = Owners::find()->where(['id_owner' => $id])->orderBy ('first_name ASC')->all();
		
		
		//
		$owner = $owner[0];
		
		$res = 'ФИО '.$owner->last_name.' '.$owner->first_name. ' '. $owner->middle_name.'<br>';
		
		$res.= 'Паспорт '.$owner->p_series.' '.$owner->p_number. '<br>';
		
		//$res.= 'Лицензия '.$owner->licence_company;

		
		
		return $res;
		
	} 
	
	
	
	
	
	 
	public  function actionOwnernew ($id) {
		
		$auto = AutomobilesNewModel::findOne($id); 
		
		$owner_id = $auto->owner_id;
		
		
		$model_owner = Owners::findOne($owner_id);
		
		$res = '<option value="'.$owner_id.'">'.$model_owner->last_name.' '.$model_owner->first_name.' '.$model_owner->middle_name.'</option>';
			
		
		return $res;
		
	} 
	
	
	
	
	
	
	//Пробуем создать подсказку Водителя по $id для договора
	 
	public  function actionDriver ($id) {
		
		$driver = DriversModel::find()->where(['id_driver' => $id])->orderBy ('first_name ASC')->all();
		
		
		
		
		
		
		//
		$driver = $driver[0];
		
		//var_dump ($driver);
		
		//exit;
		
		$res = 'ФИО '.$driver->last_name.' '.$driver->first_name. ' '. $driver->middle_name.'<br>';
		
		$res.= 'Паспорт '.$driver->p_series.' '.$driver->p_number. '<br>';
		
		$res.= 'Дата рождения '.$driver->date_birth.'<br>';
		
		
		$res.= 'Адрес фактического проживания '.$driver->address_fact.'<br>';
		$res.= 'Телефон '.$driver->phone_1.'<br>';
		$res.= 'Вод.удостоверение '.$driver->lic_series.' '.$driver->lic_number. '<br>';
		//$res.= 'Тип '.$driver->driver_cat.'<br>';
		//$res.= 'Статус '.$driver->status.'<br>';
		
		
		//$res.= 'Лицензия '.$owner->licence_company;

		
		
		return $res;
		
	} 
	
	
	
	
	//Пробуем создать подсказку Автомобиля по $id для договора
	 
	public  function actionAuto ($id) {
		
		$auto = AutomobilesNewModel::find()->where(['id_auto' => $id])->orderBy ('mark ASC')->all();
		
		
		
		
		//
		$auto = $auto[0];
		
		//var_dump ($driver);
		
		//exit;
		
		$res = 'Марка Модель '.$auto->mark.' '.$auto->model. ' '. $auto->gosnomer.'<br>';
		
		//$res.= 'Паспорт '.$auto->p_series.' '.$auto->p_number. '<br>';
		
		//$res.= 'Дата рождения '.$auto->date_birth.'<br>';
		
		
	//	$res.= 'Адрес фактического проживания '.$auto->address_fact.'<br>';
		//$res.= 'Телефон '.$auto->phone_1.'<br>';
		//$res.= 'Вод.удостоверение '.$auto->lic_series.' '.$auto->lic_number. '<br>';
		//$res.= 'Тип '.$driver->driver_cat.'<br>';
		//$res.= 'Статус '.$driver->status.'<br>';
		
		
		//$res.= 'Лицензия '.$owner->licence_company;

		
		
		return $res;
		
	} 
	
	
	
	public  function actionAutos ($id = '') {
		
		$model_autos = AutomobilesNewModel::find()->where(['owner_id' => $id, 'status' => 1])->all();
		//$model_autos = AutomobilesNewModel::find()->where(['owner_id' => $id])->all();
		
		
		
		echo "<option>Выбрать</option>";
		
		foreach ($model_autos as $value) {
			
			echo '<option value="'.$value->id_auto.'">'.$value->mark.' '.$value->model.' '.$value->gosnomer.'</option>';
			
			
		}
		
		
	}
	
	
	
	
	
	
	
	
	
	//Пробуем создать два метода для договоров на утверждении
	// и архив
	
	public function actionContractsarchive ($id) 
	{
		
		
		$request = Yii::$app->request->queryParams;
		
		$request['ContractsSearchModel']['status_itog'] = $id;
		
		
		
		
		$searchModel = new ContractsSearchModel();
        $dataProvider = $searchModel->search($request);
			
					
		//Категории
		$model_owners = Owners::find()->orderBy('first_name ASC')->all();
		
		foreach ($model_owners as $value) {
			$arrOwners[$value->id_owner] = $value->last_name.' '.$value->first_name.' '.$value->middle_name;
		}
		
		
		
		$model_contr_statuses = Contractsstatus::find()->orderBy('c_status_id ASC')->all();
		
		
		foreach ($model_contr_statuses as $value) {
			$arrCStatuses[$value->c_status_id] = $value->c_status_name;
		}
		
		
		
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrOwners' => $arrOwners,
			'arrCStatuses' => $arrCStatuses,
        ]);
		
		
	}
	
	
	public function actionContractsprepared ($id) 
	{
	
		$request = Yii::$app->request->queryParams;
		
		$request['ContractsSearchModel']['status_itog'] = $id;
		
	
			
		$searchModel = new ContractsSearchModel();
        $dataProvider = $searchModel->search($request);
			
					
		//Категории
		$model_owners = Owners::find()->orderBy('first_name ASC')->all();
		
		foreach ($model_owners as $value) {
			$arrOwners[$value->id_owner] = $value->last_name.' '.$value->first_name.' '.$value->middle_name;
		}
		
		
		
		$model_contr_statuses = Contractsstatus::find()->orderBy('c_status_id ASC')->all();
		
		
		foreach ($model_contr_statuses as $value) {
			$arrCStatuses[$value->c_status_id] = $value->c_status_name;
		}
		
		
		
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrOwners' => $arrOwners,
			'arrCStatuses' => $arrCStatuses,
        ]);
		
		
		
	}
	
	
	
	//Пробуем создать обновлялку для договоров
	public function actionContractsupdate () {
		
		
		
		$contracts = Contracts::find()->all();
		
		
	
		
		if (!empty($contracts)) {
			
			foreach ($contracts as $item) {
				
				if ($item->date_pay_arenda < date("Y-m-d") && $item->status_itog == 1) {
					
					
					$item->status_itog = 2; 
					$item->save();
					
					
				}
				
				
			}
			
		}
		
		
		return $this->redirect(['index']);
	}
	
	
	
	
	
	
	
	
	//Пробуем создать сущность для PDF договора
	
	  /**
     * Displays a single Contracts model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPdfcard($id)
    {
		
		
		$model = $this->findModel($id);
		  
		 
		 //print_r ($model);
		$searchModel = new PaymentsSearchModel();
		$par['PaymentsSearchModel']['contract_id'] = $id;

		$ttt = $searchModel->searchAll($par);
		 
		 
		 
		//$model->date_pay_arenda_new = $model->date_pay_arenda;
		
		
		
		$arrAllPays = $ttt;
		
		//Копипаста пока
				
		$full_summa = 0;

		//Стоимость аренды в день
		$arenda_item_summ = $model->arenda_stoim;
		//Количество дней, оплаченных по аренде

		$arenda_summa = 0;

		$dog_summa = 0;

		if (!empty($arrAllPays)) {
			
			//global $prostoy_days;
			
			$summa_week = 0;
			$summa_month = 0;
			
			$prostoy_main = 0;
			
			$prostoy_days = 0;
			
			foreach ($arrAllPays as $item) {
						
				$full_summa = $full_summa + $item['summa'];
				$arenda_summa = $arenda_summa + $item['arenda'];
				$dog_summa = $dog_summa + $item['pay_dogovor'];
				
				//Пробуем просуммировать за неделю и за месяц
				
				
				$date_raznost = date_diff(new DateTime($item['date_payment']), new DateTime())->days;
				
				if ($date_raznost < 7) {
					
					$summa_week = $summa_week + $item['arenda'];
				}
				
				if ($date_raznost < 30) {
					
					$summa_month = $summa_month + $item['arenda'];
				}
				
				//$item['date_payment'];
				
				
				//Добавляем простой
				
				$prostoy_days = $prostoy_days + $item['prostoy_days'];
				
				$prostoy_main = $prostoy_main + $item['prostoy_summa'] * $item['prostoy_days'];
				
			
			}
			
		}


		//echo $prostoy_days;
		
		
		$model->date_pay_arenda_new = date('d.m.Y', strtotime($model->date_pay_arenda. ' + '.$prostoy_days.' days'));
		
		 

		
		//Владелец
		$owners = Owners::find()->all();	
		$owner_id = $this->findModel($id)->owner_id;
		
		foreach ($owners as $item) {
			
			if ($item->id_owner == $owner_id) {
					
				$cur_owner = $item;
				break;
			}
			
		}
		
		
		//Водитель
		$drivers = DriversModel::find()->all();	
		$driver_id = $this->findModel($id)->driver_id;
		
		foreach ($drivers as $item) {
			
			if ($item->id_driver == $driver_id) {
					
				$cur_driver = $item;
				break;
			}
			
		}
		
		
		$driver = $cur_driver;
		
		
		//Машина
		$autos = AutomobilesNewModel::find()->all();	
		$auto_id = $this->findModel($id)->auto_id;
		
		foreach ($autos as $item) {
			
			if ($item->id_auto == $auto_id) {
					
				$cur_auto = $item;
				break;
			}
			
		}

		
		$auto = $cur_auto;
		
		
		$licen = Licen::findOne($auto->licen_id);
		
		
		
		$discount_type = '';
		
		if ($model->discount_type == 1) {
			
			$discount_type = 'В течение 20 дней со скидкой 50%';
		} else {
			$discount_type = 'В полном размере без скидки';
		}
		
		
		if ($model->penalty_comission == 1) {
			$discount_type.= ' + 50р комиссия';
		} else {
			$discount_type.= '';
		}
		
		
		
		
		
		$ar_types = Arendatypes::find()->where(['a_d_type' => $this->findModel($id)->arenda_type])->all();
		
		
		
		$arType = $ar_types[0]->name_a_type;
		
		
		
		$arTypeShort = $ar_types[0]->short_name;
		
		
		
		
		
		
		//Добавляем редактируемые тексты
		
		
		$text1 = $model->text1;
		$text2 = $model->text2;
		$text3 = $model->text3;
		$text4 = $model->text4;
		$text5 = $model->text5;
		
		
		$srok_box = '';
		
		
		$vykup_box = '';
		
		
		
		if ($model->status_itog == 7 or $model->status_itog == 8) {
			$contract_header = 'ДОГОВОР АРЕНДЫ.';
		} else {
			$contract_header = 'ДОГОВОР АРЕНДЫ АВТОМОБИЛЯ С ПРАВОМ ВЫКУПА.';
			
			$srok_box = '<tr>
					<td>Срок аренды с выкупом</td>
					<td style="width: 50%">
					'.$model->arenda_items.' '.$arTypeShort.'
					
					</td>
				</tr>';
				
			$vykup_box = '<tr>
					<td>Выкупной платеж</td>
					<td style="width: 50%">'
					
					.$model->vykup.
					
					' рублей</td>
				</tr>';	
				
		}
		
		
		
		//Для ЗП
		if ($model->status_itog == 7) {
			$arType = 'день';
			$model->arenda_stoim = 1000;
		}
		
		
		$html = '
		
		
		
		<style>
		
		table {
			 border-spacing: 0px;
		}
		
		
		td {
			
			border-left: 1px solid #000;
			border-top: 1px solid #000;
			border-bottom: 1px solid #000;
			border-right: 1px solid #000;
		}
		
		
		
		
		tr {
			margin: 0px;
		}
		
		</style>
		
		
		
	<table class="table table-bordered detail-view">
	
	<tbody>
	
	<tr>
	
		<td>Телефон</td>
		<td>'. $driver->phone_1 .'</td>
		
		<td rowspan="4">Организация</td>
		
		<td rowspan="4">
			
			 '.$licen->name_lic.' <br> ОГРН/ОГРНИП '.$licen->ogrn.' <br >ИНН '.$licen->inn.' 
			 <br> ОКПО  '.$licen->okpo.'	
		
		</td>
		
	</tr>
	
	
	<tr>
	
		<td>Марка</td>
		<td>
		
		'.$auto->mark.' '.$auto->model.'
		
		</td>
		
	</tr>
	
	
	<tr>
	
		<td>ГРЗ</td>
		<td>'. $auto->gosnomer.'</td>
		
	</tr>
	
	<tr>
	
		<td>Водитель</td>
		<td>'.
		
		$driver->last_name.' '.$driver->first_name.' '.$driver->middle_name.'
		
		</td>
		
	</tr>
	
	<tr>
	
		<td colspan=1>Удостоверение</td>
		<td colspan=3>
		
		'.$driver->lic_series.' '.$driver->lic_number. ' '.$driver->lic_type
			
		.'</td>
		
	</tr>


	<tr>
	
		<td colspan=1>Разрешение</td>
		<td colspan=3>
		
		
		'.$auto->razresh_text.'</td>
		
	</tr>
	
	
	<tr>
	
		<td colspan="3">
	
<p>	
Контроль за осуществлением перевозок пассажиров и багажа легковым такси осуществляет московская администрация дорожного инспекция МАДИ
</p>

<p>
Адрес :129090, г Москва, ул. Каланчевская , д 49  Единый многоканальный телефон 
</p>
<p>
+7495540 76 56,  7 929 960 54 35 
</p>
<p>

Телефон (3522) 42-80-01 (доб. 609)
</p>
<p>
День 07:00 – 19:00 150 руб – 5 мин.
</p>
<p>
За МКАД – 15  руб/км
</p>
<p>
Ночь/выходной /праздники
 190 руб. 5 мин
Далее 12 руб /мин
За МКАД 15/мин
	</p>					
	
		</td>
		
		<td>
		
		
		<img width="185" src="/admin/uploads/'. $driver->photo.'">
		
		</td>
		
		
		
	</tr>
	
	
	</tbody>
	
	
	
	</table>
	
	
		
		
		
		
		
		
		
		
		
		
		
		
	
	
	';
		
		
		$mpdf = new Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
        exit;
		
		
	}
	
	
	
	
	
	
	
	
	
	
	//Пробуем создать сущность для PDF договора
	
	  /**
     * Displays a single Contracts model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPdfcontract($id)
    {
		
		
		$model = $this->findModel($id);
		  
		 
		 //print_r ($model);
		$searchModel = new PaymentsSearchModel();
		$par['PaymentsSearchModel']['contract_id'] = $id;

		$ttt = $searchModel->searchAll($par);
		 
		 
		 
		//$model->date_pay_arenda_new = $model->date_pay_arenda;
		
		
		
		$arrAllPays = $ttt;
		
		//Копипаста пока
				
		$full_summa = 0;

		//Стоимость аренды в день
		$arenda_item_summ = $model->arenda_stoim;
		//Количество дней, оплаченных по аренде

		$arenda_summa = 0;

		$dog_summa = 0;

		if (!empty($arrAllPays)) {
			
			//global $prostoy_days;
			
			$summa_week = 0;
			$summa_month = 0;
			
			$prostoy_main = 0;
			
			$prostoy_days = 0;
			
			foreach ($arrAllPays as $item) {
						
				$full_summa = $full_summa + $item['summa'];
				$arenda_summa = $arenda_summa + $item['arenda'];
				$dog_summa = $dog_summa + $item['pay_dogovor'];
				
				//Пробуем просуммировать за неделю и за месяц
				
				
				$date_raznost = date_diff(new DateTime($item['date_payment']), new DateTime())->days;
				
				if ($date_raznost < 7) {
					
					$summa_week = $summa_week + $item['arenda'];
				}
				
				if ($date_raznost < 30) {
					
					$summa_month = $summa_month + $item['arenda'];
				}
				
				//$item['date_payment'];
				
				
				//Добавляем простой
				
				$prostoy_days = $prostoy_days + $item['prostoy_days'];
				
				$prostoy_main = $prostoy_main + $item['prostoy_summa'] * $item['prostoy_days'];
				
			
			}
			
		}


		//echo $prostoy_days;
		
		
		$model->date_pay_arenda_new = date('d.m.Y', strtotime($model->date_pay_arenda. ' + '.$prostoy_days.' days'));
		
		 

		
		//Владелец
		$owners = Owners::find()->all();	
		$owner_id = $this->findModel($id)->owner_id;
		
		foreach ($owners as $item) {
			
			if ($item->id_owner == $owner_id) {
					
				$cur_owner = $item;
				break;
			}
			
		}
		
		
		//Водитель
		$drivers = DriversModel::find()->all();	
		$driver_id = $this->findModel($id)->driver_id;
		
		foreach ($drivers as $item) {
			
			if ($item->id_driver == $driver_id) {
					
				$cur_driver = $item;
				break;
			}
			
		}
		
		
		$driver = $cur_driver;
		
		
		//Машина
		$autos = AutomobilesNewModel::find()->all();	
		$auto_id = $this->findModel($id)->auto_id;
		
		foreach ($autos as $item) {
			
			if ($item->id_auto == $auto_id) {
					
				$cur_auto = $item;
				break;
			}
			
		}

		
		$auto = $cur_auto;
		
		
		$licen = Licen::findOne($auto->licen_id);
		
		
		
		$discount_type = '';
		
		if ($model->discount_type == 1) {
			
			$discount_type = 'В течение 20 дней со скидкой 50%';
		} else {
			$discount_type = 'В полном размере без скидки';
		}
		
		
		if ($model->penalty_comission == 1) {
			$discount_type.= ' + 50р комиссия';
		} else {
			$discount_type.= '';
		}
		
		
		
		
		
		$ar_types = Arendatypes::find()->where(['a_d_type' => $this->findModel($id)->arenda_type])->all();
		
		
		
		$arType = $ar_types[0]->name_a_type;
		
		
		
		$arTypeShort = $ar_types[0]->short_name;
		
		
		
		
		
		
		//Добавляем редактируемые тексты
		
		
		$text1 = $model->text1;
		$text2 = $model->text2;
		$text3 = $model->text3;
		$text4 = $model->text4;
		$text5 = $model->text5;
		
		
		$srok_box = '';
		
		
		$vykup_box = '';
		
		
		
		if ($model->status_itog == 7 or $model->status_itog == 8) {
			$contract_header = 'ДОГОВОР АРЕНДЫ.';
		} else {
			$contract_header = 'ДОГОВОР АРЕНДЫ АВТОМОБИЛЯ С ПРАВОМ ВЫКУПА.';
			
			$srok_box = '<tr>
					<td>Срок аренды с выкупом</td>
					<td style="width: 50%">
					'.$model->arenda_items.' '.$arTypeShort.'
					
					</td>
				</tr>';
				
			$vykup_box = '<tr>
					<td>Выкупной платеж</td>
					<td style="width: 50%">'
					
					.$model->vykup.
					
					' рублей</td>
				</tr>';	
				
		}
		
		
		
		//Для ЗП
		if ($model->status_itog == 7) {
			$arType = 'день';
			$model->arenda_stoim = 1000;
		}
		
		
		$html = '
		
		
		
		<style>
		
		table {
			 border-spacing: 0px;
		}
		
		
		td {
			
			border-left: 1px solid #000;
			border-top: 1px solid #000;
			border-bottom: 1px solid #000;
			border-right: 1px solid #000;
		}
		
		
		
		
		tr {
			margin: 0px;
		}
		
		</style>
		
		
		
		<div style="float: left; width: 15%;">
			г. Москва
		</div>

		<div style="float: right; width: 15%;">
			 '.date("d.m.Y", strtotime($model->date_start)).'
		</div>
		
		<br><br>
		
		<div>
		'.$contract_header.'
		</div>
		<br>
		
		<table border="" width="100%">
			<tbody>
				<tr style="width: 100%">
					<td style="width: 50%">ФИО</td>
					 
					<td width="50%">'.$cur_owner->last_name.' '. $cur_owner->first_name.' '.$cur_owner->middle_name.'</td>
					
				</tr>
				
				<tr>
					<td>Паспорт</td>
					<td style="width: 50%">
					'.$cur_owner->p_series.' '.$cur_owner->p_number.'<br>
					
					
					
					'.$cur_owner->p_who.'
					
					
					</td>
				</tr>
				
				
				<tr>
					<td>Адрес регистрации</td>
					<td style="width: 50%">
					
					'.$cur_owner->address_reg.'
					
					</td>
				</tr>
				
				
				<tr>
					<td>Телефон 1</td>
					<td style="width: 50%">
					'.$cur_owner->phone_1.'
					
					</td>
				</tr>
				
				
				<tr>
					<td>Телефон 2</td>
					<td style="width: 50%">'.$cur_owner->phone_2.'</td>
				</tr>
				
				
				<tr>
					<td>Телефон 3</td>
					<td style="width: 50%">'.$cur_owner->phone_3.'</td>
				</tr>
				
				
				<tr>
					<td>Именуемый в дальнейшем</td>
					<td style="width: 50%">Арендодатель</td>
				</tr>
			
			
			<tbody>
		</table>
			
			<br>
		
		и
		
			<br><br>
			
			
		<table border="1" width="100%">
			<tbody>
				<tr style="width: 100%">
					<td style="width: 50%">
					ФИО
					</td>
					
					<td width="50%">'.$cur_driver->last_name.' '. $cur_driver->first_name.' '.$cur_driver->middle_name.'</td>
					
					
				</tr>
				
				<tr>
					<td>Паспорт</td>
					<td style="width: 50%">
					
					'.$cur_driver->p_series.' '.$cur_driver->p_number.'<br>
					   
					Дата рождения: '.date("d.m.Y", strtotime($cur_driver->date_birth)).' <br>
					'.$cur_driver->p_who.'
					
					</td>
				</tr>
				
				
				<tr>
					<td>Адрес регистрации</td>
					<td style="width: 50%">'.$cur_driver->address_reg.'</td>
				</tr>
				
				<tr>
					<td>Адрес фактического проживания</td>
					<td style="width: 50%">'.$cur_driver->address_fact.'</td>
				</tr>
				
				
				
				<tr>
					<td>Водительское удостоверение</td>
					<td style="width: 50%">
					
					'.$cur_driver->lic_series.' '.$cur_driver->lic_number.'<br>
					
					
					Категории '.$cur_driver->lic_type.' <br>
					Срок действия '.$cur_driver->lic_dates.'<br>
					
					
					
					</td>
				</tr>
				
				
				<tr>
					<td>Телефон 1</td>
					<td style="width: 50%">'.$cur_driver->phone_1.'</td>
				</tr>
				
				
				<tr>
					<td>Телефон 2</td>
					<td style="width: 50%">'.$cur_driver->phone_2.'</td>
				</tr>
				
				
				<tr>
					<td>Именуемый в дальнейшем</td>
					<td style="width: 50%">Арендатор</td>
				</tr>
			
			
			<tbody>
		</table>
				
		<br>

		<div>
		заключили настоящий договор о нижеследующем: <br>
		
		1. Предмет договора
		<br>
		Арендодатель передает во временное владение и  пользование следующее транспортное средство:
		
		
		
		</div>
		
			<br><br>
		
		<table border="1" width="100%">
			<tbody>
				<tr style="width: 100%">
					<td style="width: 50%; ">Марка Модель</td>
					
					<td style="width: 50%; text-transform: uppercase ">'.$cur_auto->mark.' '. $cur_auto->model.'</td>
					
				</tr>
				
				<tr>
					<td>Идентификационный номер (VIN)</td>
					<td style="width: 50%; text-transform: uppercase">
					'.$cur_auto->VIN.'
					
					</td>
				</tr>
				
				
				<tr>
					<td>Гос. номер</td>
					<td style="width: 50%; text-transform: uppercase">
					
					
					'.$cur_auto->gosnomer.'
					</td>
				</tr>
				
				
				<tr>
					<td>Год выпуска</td>
					<td style="width: 50%">
					
					'.$cur_auto->year.'
					
					</td>
				</tr>
	
			<tbody>
		</table>
				
		<br>
		
		<div>
		а Арендатор обязуется выплачивать Арендодателю арендную плату. <br>
		Автомобиль передается на следующих условиях:
		
		</div>
	
		<br><br>
		
		
		<table border="1" width="100%">
			<tbody>
				<tr style="width: 100%">
					<td style="width: 50%">Первоначальный взнос (не возвращается при сдаче ТС)</td>
					
					<td width="50%">
					
					'.$model->p_vznos.' рублей
					</td>
					
				</tr>
				
				<tr>
					<td>Стоимость аренды в '.$arType.'</td>
					<td style="width: 50%">'
					
					.$model->arenda_stoim. ' рублей </td>
				</tr>
				
				
				
				
				'.$srok_box.'
				
				
				'.$vykup_box.'
				
				
				
				
				<tr>
					<td>Условие оплаты штрафов за нарушение ПДД</td>
					<td style="width: 50%">
					
						
					
						'.$discount_type.'
					
					</td>
				</tr>
				
				
				
				
				<tr>
					<td>Штраф за просрочку платежа</td>
					<td style="width: 50%">'
					
					.$model->prosrochka.
					' рублей
					</td>
				</tr>
				
				<tr>
					<td>Штраф за передачу руля другому лицу</td>
					<td style="width: 50%">'
					
					.$model->peredacha_drugomu.
					' 
					рублей</td>
				</tr>
				
				<tr>
					<td>Штраф за выезд без согласия за 200 км от Москвы</td>
					<td style="width: 50%">'
					
					.$model->vyezd_bez_sogl_200.
					
					' рублей</td>
				</tr>
				
				<!--
				<tr>
					<td>Штраф за уплату штрафа ПДД в срок более 60 дней</td>
					<td style="width: 50%">'
					
					.$model->shtraf_pdd.
					
					
					
					' рублей</td>
				</tr>
				-->
				
				
				<tr>
					<td>Штраф за уплату штрафа ПДД в срок более 60 дней</td>
					<td style="width: 50%">
						Двойная сумма неоплаченного штрафа
					</td>
				</tr>
				
				
				
				
				
				<tr>
					<td>Штраф за снятие с автомобиля атрибутики такси</td>
					<td style="width: 50%">'
					
					
					.$model->snyat_atrib.
					
					' рублей</td>
				</tr>
				
				<tr>
					<td>Штраф за движение без путевого листа</td>
					<td style="width: 50%">'
					
					.$model->no_put_list.
					
					' рублей</td>
				</tr>
				
				
				<tr>
					<td>Штраф за движение без ОСАГО</td>
					<td style="width: 50%">'
					
					
					.$model->no_osago.
					
					' рублей</td>
				</tr>
				
				<tr>
					<td>Штраф за утерю документа на автомобиль (полис ОСАГО, лицензия на такси, СТС)</td>
					<td style="width: 50%">'
					
					.$model->poterya_docs.
					
					
					' рублей</td>
				</tr>
				
				<tr>
					<td>Штраф за утерю ключа на автомобиль</td>
					<td style="width: 50%">'
					
					.$model->poterya_keys.
					
					' рублей</td>
				</tr>
				
				
				<tr>
					<td>Штраф за непродленную диагностическую карту</td>
					<td style="width: 50%">'
					
					.$model->no_prodl_diag.
					
					' рублей</td>
				</tr>
				
				
				<tr>
					<td>Дата оплаты аренды за ТС</td>
					<td style="width: 50%">'
					
					
					
					
					.date("d.m.Y", strtotime($model->date_pay_arenda )).
					
					'</td>
				</tr>
				
				<tr>
					<td>Дата окончания страховки</td>
					<td style="width: 50%">'
					
					.date("d.m.Y", strtotime($cur_auto->osago)).
					'
					</td>
				</tr>
				
				<tr>
					<td>Дата окончания диагностической карты</td>
					<td style="width: 50%">'
					
					
					.date("d.m.Y", strtotime($cur_auto->diagnostic_card )).
					
					'
					</td>
				</tr>
				
				
				<tr>
					<td>Выезд сотрудника и возврат ТС и ГСМ</td>
					<td style="width: 50%">'
					
					.$model->vyezd_sotrudnikov.
					
					' рублей</td>
				</tr>
				
				
				<tr>
					<td colspan="2"  style="text-align: center;">
						'.$text1.'
					</td>
					
				</tr>
				
				
				<tr>
					<td colspan="2"  style="text-align: center;">
						'.$text2.'
					</td>
					
				</tr>
				
				
				<tr>
					<td height="400" style="text-align: center;">
						'.$text3.'
					</td>
					
					
					<td height="400" style="text-align: center;">
						'.$text4.'
					</td>
					
				</tr>
				
				
	
			<tbody>
		</table>
		
		
		<br>
	
	
	'.$text5.'
<br>
<br>


	<div style="float: left; width: 30%;">
			Телефоны сторон 
			(автомобиль передал)
			<br><br>
			Арендодатель __________
		</div>

		<div style="float: right; width: 30%;">
			 (автомобиль принял)
			 <br><br><br>
			 Арендатор_________________ 
		</div>



		
		
		
	
	
		
		
		
		
		
		
		
		
		
		
		
		
	
	
	';
		
		
		$mpdf = new Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
        exit;
		
		
	}
	
	



	
	  /**
     * Displays a single Contracts model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionPutevoy($id)
    {
	
		
		
		$today = date("d.m.Y");  
		
		
		$year = date("Y");
		
		
		$model = $this->findModel($id);
		
		
		
		
		$cntr_arr  = Contracts::find()->joinWith(['owners'])->where(['id_contract' => $id])->all();
		
		$lic_id  = $cntr_arr[0]['owners'][0]['licence_company'];
		
		
		//Владелец
		$owner_id = $cntr_arr[0]['owners'][0]['id_owner'];
		
		
		
		//Машина
		$auto = AutomobilesNewModel::find()->where(['id_auto' => $model->auto_id])->all();
		
		
		$mark_model = $auto[0]->mark.' '.$auto[0]->model;
		
		
		$gosnomer = $auto[0]->gosnomer;
		
		
		$lic_id  = $auto[0]['licen_id'];
		
		$razresh_id = $auto[0]['razresh_id'];
		
		$lic_arr = Licen::findOne($lic_id);
		
	
		
		$name_lic = $lic_arr['name_lic'];
		$ogrn = $lic_arr['ogrn'];
		$inn = $lic_arr['inn'];
		$okpo = $lic_arr['okpo'];
		
		
		$mech = $lic_arr['mechanic'];
		$medic = $lic_arr['medic'];
		
		
		//Водитель
		$driver = DriversModel::find()->where(['id_driver' => $model->driver_id])->all();	
		
		$fio_driver = $driver[0]->last_name.' '.$driver[0]->first_name.' '.$driver[0]->middle_name;
		//print_r ($driver[0]);
		
		
		
		$razresh_id = $auto[0]->razresh_id;
		
		
		
		$razresh_arr = Razresheniya::findOne($razresh_id);
		// r_n
		
		$razresh = $razresh_arr->r_n. ' Серия '. $razresh_arr->series. ' №' . $razresh_arr->number; 
		
		$udostov = $driver[0]->lic_series.' '.$driver[0]->lic_number.' '.$driver[0]->lic_type;
		
				
				
		$html = '
		
		
		<!DOCTYPE html >
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8" />
</head>

<body style="margin: 0;">

<div id="p1" style="overflow: hidden; position: relative; background-color: white; width: 935px; height: 1210px;">

<!-- Begin shared CSS values -->
<style class="shared-css" type="text/css" >
.t {
	-webkit-transform-origin: bottom left;
	-ms-transform-origin: bottom left;
	transform-origin: bottom left;
	-webkit-transform: scale(0.25);
	-ms-transform: scale(0.25);
	transform: scale(0.25);
	z-index: 2;
	position: absolute;
	white-space: pre;
	overflow: visible;
	line-height: 1.5;
}
</style>
<!-- End shared CSS values -->


<!-- Begin inline CSS -->
<style type="text/css" >

#t1_1{left:71px;bottom:1189px;}
#t2_1{left:542px;bottom:1189px;}
#t3_1{left:597px;bottom:1189px;}
#t4_1{left:691px;bottom:1189px;}
#t5_1{left:506px;bottom:1177px;}
#t6_1{left:651px;bottom:1177px;letter-spacing:0.1px;}
#t7_1{left:383px;bottom:1141px;letter-spacing:0.2px;}
#t8_1{left:597px;bottom:1119px;}
#t9_1{left:71px;bottom:1098px;}
#ta_1{left:71px;bottom:1057px;letter-spacing:0.2px;word-spacing:-0.2px;}
#tb_1{left:362px;bottom:1057px;letter-spacing:0.1px;}
#tc_1{left:71px;bottom:1036px;letter-spacing:0.1px;word-spacing:-0.2px;}
#td_1{left:651px;bottom:1037px;word-spacing:-0.2px;}
#te_1{left:71px;bottom:1015px;letter-spacing:0.1px;}
#tf_1{left:651px;bottom:1015px;word-spacing:-0.2px;}
#tg_1{left:72px;bottom:957px;letter-spacing:0.1px;}
#th_1{left:72px;bottom:936px;letter-spacing:0.1px;}
#ti_1{left:72px;bottom:916px;letter-spacing:0.1px;}
#tj_1{left:71px;bottom:897px;letter-spacing:0.1px;}
#tk_1{left:285px;bottom:897px;letter-spacing:-0.2px;}
#tl_1{left:358px;bottom:897px;letter-spacing:0.1px;}
#tm_1{left:443px;bottom:897px;letter-spacing:-0.1px;}
#tn_1{left:71px;bottom:875px;letter-spacing:0.1px;word-spacing:-0.1px;}
#to_1{left:225px;bottom:875px;}
#tp_1{left:358px;bottom:875px;letter-spacing:0.2px;word-spacing:-0.5px;}
#tq_1{left:71px;bottom:834px;letter-spacing:0.2px;word-spacing:-0.3px;}
#tr_1{left:71px;bottom:813px;letter-spacing:0.1px;}
#ts_1{left:285px;bottom:813px;letter-spacing:0.1px;}
#tt_1{left:297px;bottom:813px;letter-spacing:0.1px;word-spacing:-0.3px;}
#tu_1{left:71px;bottom:791px;letter-spacing:0.2px;}
#tv_1{left:151px;bottom:791px;letter-spacing:-0.1px;word-spacing:-0.1px;}
#tw_1{left:285px;bottom:791px;letter-spacing:-0.1px;}
#tx_1{left:71px;bottom:770px;}
#ty_1{left:71px;bottom:749px;letter-spacing:0.1px;}
#tz_1{left:432px;bottom:749px;letter-spacing:0.2px;word-spacing:-0.1px;}
#t10_1{left:522px;bottom:747px;}
#t11_1{left:597px;bottom:749px;letter-spacing:-0.2px;}
#t12_1{left:705px;bottom:749px;letter-spacing:-0.1px;word-spacing:-0.1px;}
#t13_1{left:746px;bottom:749px;}
#t14_1{left:71px;bottom:727px;letter-spacing:0.1px;}
#t15_1{left:506px;bottom:727px;letter-spacing:0.2px;}
#t16_1{left:651px;bottom:727px;letter-spacing:0.1px;}
#t17_1{left:285px;bottom:713px;letter-spacing:0.1px;}
#t18_1{left:432px;bottom:713px;}
#t19_1{left:506px;bottom:690px;letter-spacing:0.1px;}
#t1a_1{left:746px;bottom:692px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t1b_1{left:651px;bottom:672px;word-spacing:-0.2px;}
#t1c_1{left:71px;bottom:630px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t1d_1{left:506px;bottom:630px;}
#t1e_1{left:71px;bottom:609px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1f_1{left:319px;bottom:609px;letter-spacing:0.1px;}
#t1g_1{left:597px;bottom:609px;}
#t1h_1{left:746px;bottom:609px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t1i_1{left:71px;bottom:590px;letter-spacing:0.1px;word-spacing:-0.3px;}
#t1j_1{left:597px;bottom:590px;word-spacing:-0.2px;}
#t1k_1{left:506px;bottom:568px;letter-spacing:0.1px;}
#t1l_1{left:71px;bottom:546px;letter-spacing:0.1px;word-spacing:-0.2px;}
#t1m_1{left:597px;bottom:546px;}
#t1n_1{left:686px;bottom:547px;word-spacing:-0.2px;}
#t1o_1{left:506px;bottom:525px;letter-spacing:0.2px;}
#t1p_1{left:682px;bottom:525px;letter-spacing:0.2px;}
#t1q_1{left:796px;bottom:525px;letter-spacing:0.2px;}
#t1r_1{left:71px;bottom:484px;letter-spacing:0.2px;}
#t1s_1{left:331px;bottom:484px;letter-spacing:-0.2px;}
#t1t_1{left:358px;bottom:484px;letter-spacing:-0.2px;}
#t1u_1{left:597px;bottom:484px;letter-spacing:0.1px;word-spacing:-0.3px;}
#t1v_1{left:746px;bottom:463px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t1w_1{left:71px;bottom:443px;letter-spacing:0.2px;}
#t1x_1{left:112px;bottom:443px;}
#t1y_1{left:225px;bottom:443px;}
#t1z_1{left:71px;bottom:422px;letter-spacing:0.1px;word-spacing:-0.3px;}
#t20_1{left:506px;bottom:422px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t21_1{left:71px;bottom:401px;}
#t22_1{left:358px;bottom:401px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t23_1{left:506px;bottom:401px;}
#t24_1{left:300px;bottom:380px;}
#t25_1{left:358px;bottom:380px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t26_1{left:506px;bottom:380px;letter-spacing:0.2px;}
#t27_1{left:579px;bottom:380px;letter-spacing:0.1px;}
#t28_1{left:71px;bottom:360px;}
#t29_1{left:151px;bottom:339px;word-spacing:-0.1px;}
#t2a_1{left:506px;bottom:339px;letter-spacing:0.1px;}
#t2b_1{left:651px;bottom:339px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t2c_1{left:71px;bottom:318px;letter-spacing:0.1px;}
#t2d_1{left:506px;bottom:318px;letter-spacing:0.1px;word-spacing:-0.2px;}
#t2e_1{left:71px;bottom:298px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t2f_1{left:71px;bottom:277px;letter-spacing:0.1px;}
#t2g_1{left:506px;bottom:277px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t2h_1{left:225px;bottom:257px;}
#t2i_1{left:339px;bottom:257px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t2j_1{left:506px;bottom:257px;letter-spacing:0.1px;word-spacing:-0.3px;}
#t2k_1{left:506px;bottom:236px;letter-spacing:0.3px;}
#t2l_1{left:746px;bottom:236px;letter-spacing:0.3px;word-spacing:-0.6px;}
#t2m_1{left:506px;bottom:194px;}
#t2n_1{left:597px;bottom:174px;}
#t2o_1{left:686px;bottom:174px;word-spacing:-0.2px;}
#t2p_1{left:71px;bottom:29px;}
#t2q_1{left:542px;bottom:29px;}
#t2r_1{left:597px;bottom:29px;}
#t2s_1{left:691px;bottom:29px;}
#t2t_1{left:430px;bottom:1154px;}
#t2u_1{left:735px;bottom:1129px;letter-spacing:-0.2px;}
#t2v_1{left:225px;bottom:1108px;letter-spacing:0.1px;word-spacing:-0.2px;}
#t2w_1{left:225px;bottom:1093px;}
#t2x_1{left:225px;bottom:1077px;letter-spacing:-0.1px;word-spacing:-0.1px;}
#t2y_1{left:229px;bottom:897px;letter-spacing:-0.1px;}
#t2z_1{left:229px;bottom:813px;letter-spacing:-0.1px;}
#t30_1{left:513px;bottom:842px;}
#t31_1{left:512px;bottom:827px;}
#t32_1{left:524px;bottom:811px;letter-spacing:0.2px;}
#t33_1{left:608px;bottom:803px;word-spacing:-0.1px;}
#t34_1{left:647px;bottom:787px;word-spacing:-0.3px;}
#t35_1{left:690px;bottom:772px;word-spacing:0.1px;}
#t36_1{left:652px;bottom:566px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t37_1{left:432px;bottom:1057px;letter-spacing:-0.1px;word-spacing:-0.1px;}
#t38_1{left:433px;bottom:1034px;letter-spacing:-0.1px;}
#t39_1{left:226px;bottom:1013px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t3a_1{left:343px;bottom:1003px;word-spacing:-0.1px;}
#t3b_1{left:71px;bottom:981px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t3c_1{left:285px;bottom:979px;}
#t3d_1{left:226px;bottom:1055px;letter-spacing:-0.1px;word-spacing:-0.3px;}

.s1{
	FONT-SIZE: 45.4px;
	FONT-FAMILY: CIDFont-F1_b;
	color: rgb(0,0,0);
}

.s2{
	FONT-SIZE: 45.4px;
	FONT-FAMILY: CIDFont-F2_j;
	color: rgb(0,0,0);
}

.s3{
	FONT-SIZE: 39.5px;
	FONT-FAMILY: CIDFont-F2_j;
	color: rgb(0,0,0);
}

.s4{
	FONT-SIZE: 62.2px;
	FONT-FAMILY: CIDFont-F1_b;
	color: rgb(0,0,0);
}

.s5{
	FONT-SIZE: 62.2px;
	FONT-FAMILY: CIDFont-F2_j;
	color: rgb(0,0,0);
}

</style>
<!-- End inline CSS -->

<!-- Begin embedded font definitions -->
<style id="fonts1" type="text/css" >

@font-face {
	font-family: CIDFont-F1_b;
	src: url("/admin/css/putevoy/fonts/CIDFont-F1_b.woff") format("woff");
}

@font-face {
	font-family: CIDFont-F2_j;
	src: url("/admin/css/putevoy/fonts/CIDFont-F2_j.woff") format("woff");
}

</style>
<!-- End embedded font definitions -->

<!-- Begin page background -->
<div id="pg1Overlay" style="width:100%; height:100%; position:absolute; z-index:1; background-color:rgba(0,0,0,0); -webkit-user-select: none;"></div>
<div id="pg1" style="-webkit-user-select: none;"><object width="935" height="1210" data="/admin/css/putevoy/1/1.svg" type="image/svg+xml" id="pdf1" style="width:935px; height:1210px; -moz-transform:scale(1); z-index: 0;"></object></div>
<!-- End page background -->


<!-- Begin text definitions (Positioned/styled in CSS) -->
<div id="t1_1" class="t s1">ПУТЕВОЙ ЛИСТ ЛЕГКОВОГО АВТОМОБИЛЯ</div>
<div id="t2_1" class="t s2">Д-1</div>
<div id="t3_1" class="t s2">№</div>
<div id="t4_1" class="t s2">85</div>
<div id="t5_1" class="t s2">серия</div>
<div id="t6_1" class="t s2">номер</div>
<div id="t7_1" class="t s2">дата</div>
<div id="t8_1" class="t s2">по ОКПО</div>
<div id="t9_1" class="t s2">Организация</div>
<div id="ta_1" class="t s2">Марка автомобиля</div>
<div id="tb_1" class="t s2">Разрешение </div>
<div id="tc_1" class="t s2">Государственный регистрационный знак</div>
<div id="td_1" class="t s3">Номер парковки</div>
<div id="te_1" class="t s2">Водитель</div>
<div id="tf_1" class="t s3">Табельный номер</div>
<div id="tg_1" class="t s4">Предрейсовый</div>
<div id="th_1" class="t s4">Медосмотр</div>
<div id="ti_1" class="t s4">Пройден</div>
<div id="tj_1" class="t s1">дата</div>
<div id="tk_1" class="t s2">г.</div>
<div id="tl_1" class="t s2">Время</div>
<div id="tm_1" class="t s2">00 ч. 10 мин</div>
<div id="tn_1" class="t s1">должность: Врач</div>
<div id="to_1" class="t s2">подпись</div>
<div id="tp_1" class="t s2">'.$medic.'</div>
<div id="tq_1" class="t s2">Выезд с парковки</div>
<div id="tr_1" class="t s2">Дата</div>
<div id="ts_1" class="t s2">г. </div>
<div id="tt_1" class="t s2">Механик/ Диспечер ____'.$mech.'</div>
<div id="tu_1" class="t s2">Время</div>
<div id="tv_1" class="t s2">00 ч. 20</div>
<div id="tw_1" class="t s2">мин</div>
<div id="tx_1" class="t s2">Послерейсовый </div>
<div id="ty_1" class="t s2">медосмотр</div>
<div id="tz_1" class="t s2">дата, время</div>
<div id="t10_1" class="t s5">'.$today.'</div>
<div id="t11_1" class="t s2">г.</div>
<div id="t12_1" class="t s2">00 ч. 15</div>
<div id="t13_1" class="t s2">мин</div>
<div id="t14_1" class="t s2">пройден</div>
<div id="t15_1" class="t s2">дата</div>
<div id="t16_1" class="t s2">время</div>
<div id="t17_1" class="t s2">должность</div>
<div id="t18_1" class="t s2">подпись</div>
<div id="t19_1" class="t s5">Механик</div>
<div id="t1a_1" class="t s2">'.$mech.'</div>
<div id="t1b_1" class="t s3">подпись и расшифровка подписи</div>
<div id="t1c_1" class="t s2">Задание водителю</div>
<div id="t1d_1" class="t s1">Показание одометра, км</div>
<div id="t1e_1" class="t s2">В распоряжение</div>
<div id="t1f_1" class="t s2">Яндекс</div>
<div id="t1g_1" class="t s2">Механик</div>
<div id="t1h_1" class="t s2">'.$mech.'</div>
<div id="t1i_1" class="t s2">Адрес первой подачи</div>
<div id="t1j_1" class="t s2">Автомобиль в исправном состоянии принял</div>
<div id="t1k_1" class="t s2">Водитель</div>
<div id="t1l_1" class="t s2">По указанию Яндекс г. Москва и Московская область</div>
<div id="t1m_1" class="t s2">подпись</div>
<div id="t1n_1" class="t s3">расшифровка подписи</div>
<div id="t1o_1" class="t s2">Горючее</div>
<div id="t1p_1" class="t s2">марка</div>
<div id="t1q_1" class="t s2">код</div>
<div id="t1r_1" class="t s2">дата</div>
<div id="t1s_1" class="t s2">'.$year.'</div>
<div id="t1t_1" class="t s2">г.</div>
<div id="t1u_1" class="t s2">Движение горючего</div>
<div id="t1v_1" class="t s2">количество, л</div>
<div id="t1w_1" class="t s2">Время </div>
<div id="t1x_1" class="t s2">ч.</div>
<div id="t1y_1" class="t s2">мин</div>
<div id="t1z_1" class="t s2">при заезде на парковку</div>
<div id="t20_1" class="t s2">Выдано: по заправочному</div>
<div id="t21_1" class="t s2">Диспетчер/механик</div>
<div id="t22_1" class="t s2">'.$mech.'</div>
<div id="t23_1" class="t s2">листу №</div>
<div id="t24_1" class="t s2">подпись</div>
<div id="t25_1" class="t s2">расшифровка подписи</div>
<div id="t26_1" class="t s2">остаток: </div>
<div id="t27_1" class="t s2">при выезде</div>
<div id="t28_1" class="t s2">Ожидание :</div>
<div id="t29_1" class="t s2">Опоздания, ожидания, простои в пути, заезды в гараж </div>
<div id="t2a_1" class="t s2">расход</div>
<div id="t2b_1" class="t s2">по норме</div>
<div id="t2c_1" class="t s2">и прочие отметки</div>
<div id="t2d_1" class="t s2">Послерейсовый контроль технического состояния</div>
<div id="t2e_1" class="t s2">Автомобиль сдал</div>
<div id="t2f_1" class="t s2">водитель</div>
<div id="t2g_1" class="t s2">Показания одометра при</div>
<div id="t2h_1" class="t s2">подпись</div>
<div id="t2i_1" class="t s2">расшифровка подписи</div>
<div id="t2j_1" class="t s2">возвращении на парковку,</div>
<div id="t2k_1" class="t s2">км</div>
<div id="t2l_1" class="t s2">дата ; время</div>
<div id="t2m_1" class="t s2">Механик</div>
<div id="t2n_1" class="t s2">подпись</div>
<div id="t2o_1" class="t s3">расшифровка подписи</div>
<div id="t2p_1" class="t s1">ПУТЕВОЙ ЛИСТ ЛЕГКОВОГО АВТОМОБИЛЯ</div>
<div id="t2q_1" class="t s2">Д-1</div>
<div id="t2r_1" class="t s2">№</div>
<div id="t2s_1" class="t s2">85</div>
<div id="t2t_1" class="t s5">'.$today.'</div>
<div id="t2u_1" class="t s2">'.$okpo.'</div>
<div id="t2v_1" class="t s2">'.$name_lic.' </div>
<div id="t2w_1" class="t s2">ОГРН '.$ogrn.' </div>
<div id="t2x_1" class="t s2">ИНН '.$inn.'</div>
<div id="t2y_1" class="t s2">'.$today.'</div>
<div id="t2z_1" class="t s2">'.$today.'</div>
<div id="t30_1" class="t s2">Предрейсовый </div>
<div id="t31_1" class="t s2">(предсменный)</div>
<div id="t32_1" class="t s2">(контроль)</div>
<div id="t33_1" class="t s1">Предрейсовый контроль технического состояния</div>
<div id="t34_1" class="t s1">транспортного средства пройден,</div>
<div id="t35_1" class="t s1">выезд разрешен</div>
<div id="t36_1" class="t s5">'.$fio_driver.'</div>
<div id="t37_1" class="t s2">'.$razresh.'</div>
<div id="t38_1" class="t s5">'.$gosnomer.'</div>
<div id="t39_1" class="t s5">'.$fio_driver.'</div>
<div id="t3a_1" class="t s3">фамилия, имя, отчество</div>
<div id="t3b_1" class="t s2">Удостоверение №</div>
<div id="t3c_1" class="t s5">'.$udostov.'</div>
<div id="t3d_1" class="t s5">'.$mark_model.'</div>

<!-- End text definitions -->


</div>






<div id="p1" style="overflow: hidden; position: relative; background-color: white; width: 935px; height: 1210px;">

<!-- Begin shared CSS values -->
<style class="shared-css" type="text/css" >
.t {
	-webkit-transform-origin: bottom left;
	-ms-transform-origin: bottom left;
	transform-origin: bottom left;
	-webkit-transform: scale(0.25);
	-ms-transform: scale(0.25);
	transform: scale(0.25);
	z-index: 2;
	position: absolute;
	white-space: pre;
	overflow: visible;
	line-height: 1.5;
}
</style>
<!-- End shared CSS values -->


<!-- Begin inline CSS -->
<style type="text/css" >

#t1_1{left:71px;bottom:1189px;}
#t2_1{left:542px;bottom:1189px;}
#t3_1{left:597px;bottom:1189px;}
#t4_1{left:691px;bottom:1189px;}
#t5_1{left:506px;bottom:1177px;}
#t6_1{left:651px;bottom:1177px;letter-spacing:0.1px;}
#t7_1{left:383px;bottom:1141px;letter-spacing:0.2px;}
#t8_1{left:597px;bottom:1119px;}
#t9_1{left:71px;bottom:1098px;}
#ta_1{left:71px;bottom:1057px;letter-spacing:0.2px;word-spacing:-0.2px;}
#tb_1{left:362px;bottom:1057px;letter-spacing:0.1px;}
#tc_1{left:71px;bottom:1036px;letter-spacing:0.1px;word-spacing:-0.2px;}
#td_1{left:651px;bottom:1037px;word-spacing:-0.2px;}
#te_1{left:71px;bottom:1015px;letter-spacing:0.1px;}
#tf_1{left:651px;bottom:1015px;word-spacing:-0.2px;}
#tg_1{left:72px;bottom:957px;letter-spacing:0.1px;}
#th_1{left:72px;bottom:936px;letter-spacing:0.1px;}
#ti_1{left:72px;bottom:916px;letter-spacing:0.1px;}
#tj_1{left:71px;bottom:897px;letter-spacing:0.1px;}
#tk_1{left:285px;bottom:897px;letter-spacing:-0.2px;}
#tl_1{left:358px;bottom:897px;letter-spacing:0.1px;}
#tm_1{left:443px;bottom:897px;letter-spacing:-0.1px;}
#tn_1{left:71px;bottom:875px;letter-spacing:0.1px;word-spacing:-0.1px;}
#to_1{left:225px;bottom:875px;}
#tp_1{left:358px;bottom:875px;letter-spacing:0.2px;word-spacing:-0.5px;}
#tq_1{left:71px;bottom:834px;letter-spacing:0.2px;word-spacing:-0.3px;}
#tr_1{left:71px;bottom:813px;letter-spacing:0.1px;}
#ts_1{left:285px;bottom:813px;letter-spacing:0.1px;}
#tt_1{left:297px;bottom:813px;letter-spacing:0.1px;word-spacing:-0.3px;}
#tu_1{left:71px;bottom:791px;letter-spacing:0.2px;}
#tv_1{left:151px;bottom:791px;letter-spacing:-0.1px;word-spacing:-0.1px;}
#tw_1{left:285px;bottom:791px;letter-spacing:-0.1px;}
#tx_1{left:71px;bottom:770px;}
#ty_1{left:71px;bottom:749px;letter-spacing:0.1px;}
#tz_1{left:432px;bottom:749px;letter-spacing:0.2px;word-spacing:-0.1px;}
#t10_1{left:522px;bottom:747px;}
#t11_1{left:597px;bottom:749px;letter-spacing:-0.2px;}
#t12_1{left:705px;bottom:749px;letter-spacing:-0.1px;word-spacing:-0.1px;}
#t13_1{left:746px;bottom:749px;}
#t14_1{left:71px;bottom:727px;letter-spacing:0.1px;}
#t15_1{left:506px;bottom:727px;letter-spacing:0.2px;}
#t16_1{left:651px;bottom:727px;letter-spacing:0.1px;}
#t17_1{left:285px;bottom:713px;letter-spacing:0.1px;}
#t18_1{left:432px;bottom:713px;}
#t19_1{left:506px;bottom:690px;letter-spacing:0.1px;}
#t1a_1{left:746px;bottom:692px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t1b_1{left:651px;bottom:672px;word-spacing:-0.2px;}
#t1c_1{left:71px;bottom:630px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t1d_1{left:506px;bottom:630px;}
#t1e_1{left:71px;bottom:609px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1f_1{left:319px;bottom:609px;letter-spacing:0.1px;}
#t1g_1{left:597px;bottom:609px;}
#t1h_1{left:746px;bottom:609px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t1i_1{left:71px;bottom:590px;letter-spacing:0.1px;word-spacing:-0.3px;}
#t1j_1{left:597px;bottom:590px;word-spacing:-0.2px;}
#t1k_1{left:506px;bottom:568px;letter-spacing:0.1px;}
#t1l_1{left:71px;bottom:546px;letter-spacing:0.1px;word-spacing:-0.2px;}
#t1m_1{left:597px;bottom:546px;}
#t1n_1{left:686px;bottom:547px;word-spacing:-0.2px;}
#t1o_1{left:506px;bottom:525px;letter-spacing:0.2px;}
#t1p_1{left:682px;bottom:525px;letter-spacing:0.2px;}
#t1q_1{left:796px;bottom:525px;letter-spacing:0.2px;}
#t1r_1{left:71px;bottom:484px;letter-spacing:0.2px;}
#t1s_1{left:331px;bottom:484px;letter-spacing:-0.2px;}
#t1t_1{left:358px;bottom:484px;letter-spacing:-0.2px;}
#t1u_1{left:597px;bottom:484px;letter-spacing:0.1px;word-spacing:-0.3px;}
#t1v_1{left:746px;bottom:463px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t1w_1{left:71px;bottom:443px;letter-spacing:0.2px;}
#t1x_1{left:112px;bottom:443px;}
#t1y_1{left:225px;bottom:443px;}
#t1z_1{left:71px;bottom:422px;letter-spacing:0.1px;word-spacing:-0.3px;}
#t20_1{left:506px;bottom:422px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t21_1{left:71px;bottom:401px;}
#t22_1{left:358px;bottom:401px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t23_1{left:506px;bottom:401px;}
#t24_1{left:300px;bottom:380px;}
#t25_1{left:358px;bottom:380px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t26_1{left:506px;bottom:380px;letter-spacing:0.2px;}
#t27_1{left:579px;bottom:380px;letter-spacing:0.1px;}
#t28_1{left:71px;bottom:360px;}
#t29_1{left:151px;bottom:339px;word-spacing:-0.1px;}
#t2a_1{left:506px;bottom:339px;letter-spacing:0.1px;}
#t2b_1{left:651px;bottom:339px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t2c_1{left:71px;bottom:318px;letter-spacing:0.1px;}
#t2d_1{left:506px;bottom:318px;letter-spacing:0.1px;word-spacing:-0.2px;}
#t2e_1{left:71px;bottom:298px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t2f_1{left:71px;bottom:277px;letter-spacing:0.1px;}
#t2g_1{left:506px;bottom:277px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t2h_1{left:225px;bottom:257px;}
#t2i_1{left:339px;bottom:257px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t2j_1{left:506px;bottom:257px;letter-spacing:0.1px;word-spacing:-0.3px;}
#t2k_1{left:506px;bottom:236px;letter-spacing:0.3px;}
#t2l_1{left:746px;bottom:236px;letter-spacing:0.3px;word-spacing:-0.6px;}
#t2m_1{left:506px;bottom:194px;}
#t2n_1{left:597px;bottom:174px;}
#t2o_1{left:686px;bottom:174px;word-spacing:-0.2px;}
#t2p_1{left:71px;bottom:29px;}
#t2q_1{left:542px;bottom:29px;}
#t2r_1{left:597px;bottom:29px;}
#t2s_1{left:691px;bottom:29px;}
#t2t_1{left:430px;bottom:1154px;}
#t2u_1{left:735px;bottom:1129px;letter-spacing:-0.2px;}
#t2v_1{left:225px;bottom:1108px;letter-spacing:0.1px;word-spacing:-0.2px;}
#t2w_1{left:225px;bottom:1093px;}
#t2x_1{left:225px;bottom:1077px;letter-spacing:-0.1px;word-spacing:-0.1px;}
#t2y_1{left:229px;bottom:897px;letter-spacing:-0.1px;}
#t2z_1{left:229px;bottom:813px;letter-spacing:-0.1px;}
#t30_1{left:513px;bottom:842px;}
#t31_1{left:512px;bottom:827px;}
#t32_1{left:524px;bottom:811px;letter-spacing:0.2px;}
#t33_1{left:608px;bottom:803px;word-spacing:-0.1px;}
#t34_1{left:647px;bottom:787px;word-spacing:-0.3px;}
#t35_1{left:690px;bottom:772px;word-spacing:0.1px;}
#t36_1{left:652px;bottom:566px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t37_1{left:432px;bottom:1057px;letter-spacing:-0.1px;word-spacing:-0.1px;}
#t38_1{left:433px;bottom:1034px;letter-spacing:-0.1px;}
#t39_1{left:226px;bottom:1013px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t3a_1{left:343px;bottom:1003px;word-spacing:-0.1px;}
#t3b_1{left:71px;bottom:981px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t3c_1{left:285px;bottom:979px;}
#t3d_1{left:226px;bottom:1055px;letter-spacing:-0.1px;word-spacing:-0.3px;}

.s1{
	FONT-SIZE: 45.4px;
	FONT-FAMILY: CIDFont-F1_b;
	color: rgb(0,0,0);
}

.s2{
	FONT-SIZE: 45.4px;
	FONT-FAMILY: CIDFont-F2_j;
	color: rgb(0,0,0);
}

.s3{
	FONT-SIZE: 39.5px;
	FONT-FAMILY: CIDFont-F2_j;
	color: rgb(0,0,0);
}

.s4{
	FONT-SIZE: 62.2px;
	FONT-FAMILY: CIDFont-F1_b;
	color: rgb(0,0,0);
}

.s5{
	FONT-SIZE: 62.2px;
	FONT-FAMILY: CIDFont-F2_j;
	color: rgb(0,0,0);
}

</style>
<!-- End inline CSS -->

<!-- Begin embedded font definitions -->
<style id="fonts1" type="text/css" >

@font-face {
	font-family: CIDFont-F1_b;
	src: url("/admin/css/putevoy/fonts/CIDFont-F1_b.woff") format("woff");
}

@font-face {
	font-family: CIDFont-F2_j;
	src: url("/admin/css/putevoy/fonts/CIDFont-F2_j.woff") format("woff");
}

</style>
<!-- End embedded font definitions -->

<!-- Begin page background -->
<div id="pg1Overlay" style="width:100%; height:100%; position:absolute; z-index:1; background-color:rgba(0,0,0,0); -webkit-user-select: none;"></div>
<div id="pg1" style="-webkit-user-select: none;"><object width="935" height="1210" data="/admin/css/putevoy/1/1.svg" type="image/svg+xml" id="pdf1" style="width:935px; height:1210px; -moz-transform:scale(1); z-index: 0;"></object></div>
<!-- End page background -->


<!-- Begin text definitions (Positioned/styled in CSS) -->
<div id="t1_1" class="t s1">ПУТЕВОЙ ЛИСТ ЛЕГКОВОГО АВТОМОБИЛЯ</div>
<div id="t2_1" class="t s2">Д-1</div>
<div id="t3_1" class="t s2">№</div>
<div id="t4_1" class="t s2">85</div>
<div id="t5_1" class="t s2">серия</div>
<div id="t6_1" class="t s2">номер</div>
<div id="t7_1" class="t s2">дата</div>
<div id="t8_1" class="t s2">по ОКПО</div>
<div id="t9_1" class="t s2">Организация</div>
<div id="ta_1" class="t s2">Марка автомобиля</div>
<div id="tb_1" class="t s2">Разрешение </div>
<div id="tc_1" class="t s2">Государственный регистрационный знак</div>
<div id="td_1" class="t s3">Номер парковки</div>
<div id="te_1" class="t s2">Водитель</div>
<div id="tf_1" class="t s3">Табельный номер</div>
<div id="tg_1" class="t s4">Предрейсовый</div>
<div id="th_1" class="t s4">Медосмотр</div>
<div id="ti_1" class="t s4">Пройден</div>
<div id="tj_1" class="t s1">дата</div>
<div id="tk_1" class="t s2">г.</div>
<div id="tl_1" class="t s2">Время</div>
<div id="tm_1" class="t s2">08 ч. 10 мин</div>
<div id="tn_1" class="t s1">должность: Врач</div>
<div id="to_1" class="t s2">подпись</div>
<div id="tp_1" class="t s2">'.$medic.'</div>
<div id="tq_1" class="t s2">Выезд с парковки</div>
<div id="tr_1" class="t s2">Дата</div>
<div id="ts_1" class="t s2">г. </div>
<div id="tt_1" class="t s2">Механик/ Диспечер ____'.$mech.'</div>
<div id="tu_1" class="t s2">Время</div>
<div id="tv_1" class="t s2">08 ч. 20</div>
<div id="tw_1" class="t s2">мин</div>
<div id="tx_1" class="t s2">Послерейсовый </div>
<div id="ty_1" class="t s2">медосмотр</div>
<div id="tz_1" class="t s2">дата, время</div>
<div id="t10_1" class="t s5">'.$today.'</div>
<div id="t11_1" class="t s2">г.</div>
<div id="t12_1" class="t s2">08 ч. 15</div>
<div id="t13_1" class="t s2">мин</div>
<div id="t14_1" class="t s2">пройден</div>
<div id="t15_1" class="t s2">дата</div>
<div id="t16_1" class="t s2">время</div>
<div id="t17_1" class="t s2">должность</div>
<div id="t18_1" class="t s2">подпись</div>
<div id="t19_1" class="t s5">Механик</div>
<div id="t1a_1" class="t s2">'.$mech.'</div>
<div id="t1b_1" class="t s3">подпись и расшифровка подписи</div>
<div id="t1c_1" class="t s2">Задание водителю</div>
<div id="t1d_1" class="t s1">Показание одометра, км</div>
<div id="t1e_1" class="t s2">В распоряжение</div>
<div id="t1f_1" class="t s2">Яндекс</div>
<div id="t1g_1" class="t s2">Механик</div>
<div id="t1h_1" class="t s2">'.$mech.'</div>
<div id="t1i_1" class="t s2">Адрес первой подачи</div>
<div id="t1j_1" class="t s2">Автомобиль в исправном состоянии принял</div>
<div id="t1k_1" class="t s2">Водитель</div>
<div id="t1l_1" class="t s2">По указанию Яндекс г. Москва и Московская область</div>
<div id="t1m_1" class="t s2">подпись</div>
<div id="t1n_1" class="t s3">расшифровка подписи</div>
<div id="t1o_1" class="t s2">Горючее</div>
<div id="t1p_1" class="t s2">марка</div>
<div id="t1q_1" class="t s2">код</div>
<div id="t1r_1" class="t s2">дата</div>
<div id="t1s_1" class="t s2">'.$year.'</div>
<div id="t1t_1" class="t s2">г.</div>
<div id="t1u_1" class="t s2">Движение горючего</div>
<div id="t1v_1" class="t s2">количество, л</div>
<div id="t1w_1" class="t s2">Время </div>
<div id="t1x_1" class="t s2">ч.</div>
<div id="t1y_1" class="t s2">мин</div>
<div id="t1z_1" class="t s2">при заезде на парковку</div>
<div id="t20_1" class="t s2">Выдано: по заправочному</div>
<div id="t21_1" class="t s2">Диспетчер/механик</div>
<div id="t22_1" class="t s2">'.$mech.'</div>
<div id="t23_1" class="t s2">листу №</div>
<div id="t24_1" class="t s2">подпись</div>
<div id="t25_1" class="t s2">расшифровка подписи</div>
<div id="t26_1" class="t s2">остаток: </div>
<div id="t27_1" class="t s2">при выезде</div>
<div id="t28_1" class="t s2">Ожидание :</div>
<div id="t29_1" class="t s2">Опоздания, ожидания, простои в пути, заезды в гараж </div>
<div id="t2a_1" class="t s2">расход</div>
<div id="t2b_1" class="t s2">по норме</div>
<div id="t2c_1" class="t s2">и прочие отметки</div>
<div id="t2d_1" class="t s2">Послерейсовый контроль технического состояния</div>
<div id="t2e_1" class="t s2">Автомобиль сдал</div>
<div id="t2f_1" class="t s2">водитель</div>
<div id="t2g_1" class="t s2">Показания одометра при</div>
<div id="t2h_1" class="t s2">подпись</div>
<div id="t2i_1" class="t s2">расшифровка подписи</div>
<div id="t2j_1" class="t s2">возвращении на парковку,</div>
<div id="t2k_1" class="t s2">км</div>
<div id="t2l_1" class="t s2">дата ; время</div>
<div id="t2m_1" class="t s2">Механик</div>
<div id="t2n_1" class="t s2">подпись</div>
<div id="t2o_1" class="t s3">расшифровка подписи</div>
<div id="t2p_1" class="t s1">ПУТЕВОЙ ЛИСТ ЛЕГКОВОГО АВТОМОБИЛЯ</div>
<div id="t2q_1" class="t s2">Д-1</div>
<div id="t2r_1" class="t s2">№</div>
<div id="t2s_1" class="t s2">85</div>
<div id="t2t_1" class="t s5">'.$today.'</div>
<div id="t2u_1" class="t s2">'.$okpo.'</div>
<div id="t2v_1" class="t s2">'.$name_lic.' </div>
<div id="t2w_1" class="t s2">ОГРН '.$ogrn.' </div>
<div id="t2x_1" class="t s2">ИНН '.$inn.'</div>
<div id="t2y_1" class="t s2">'.$today.'</div>
<div id="t2z_1" class="t s2">'.$today.'</div>
<div id="t30_1" class="t s2">Предрейсовый </div>
<div id="t31_1" class="t s2">(предсменный)</div>
<div id="t32_1" class="t s2">(контроль)</div>
<div id="t33_1" class="t s1">Предрейсовый контроль технического состояния</div>
<div id="t34_1" class="t s1">транспортного средства пройден,</div>
<div id="t35_1" class="t s1">выезд разрешен</div>
<div id="t36_1" class="t s5">'.$fio_driver.'</div>
<div id="t37_1" class="t s2">'.$razresh.'</div>
<div id="t38_1" class="t s5">'.$gosnomer.'</div>
<div id="t39_1" class="t s5">'.$fio_driver.'</div>
<div id="t3a_1" class="t s3">фамилия, имя, отчество</div>
<div id="t3b_1" class="t s2">Удостоверение №</div>
<div id="t3c_1" class="t s5">'.$udostov.'</div>
<div id="t3d_1" class="t s5">'.$mark_model.'</div>

<!-- End text definitions -->


</div>






<div id="p1" style="overflow: hidden; position: relative; background-color: white; width: 935px; height: 1210px;">

<!-- Begin shared CSS values -->
<style class="shared-css" type="text/css" >
.t {
	-webkit-transform-origin: bottom left;
	-ms-transform-origin: bottom left;
	transform-origin: bottom left;
	-webkit-transform: scale(0.25);
	-ms-transform: scale(0.25);
	transform: scale(0.25);
	z-index: 2;
	position: absolute;
	white-space: pre;
	overflow: visible;
	line-height: 1.5;
}
</style>
<!-- End shared CSS values -->


<!-- Begin inline CSS -->
<style type="text/css" >

#t1_1{left:71px;bottom:1189px;}
#t2_1{left:542px;bottom:1189px;}
#t3_1{left:597px;bottom:1189px;}
#t4_1{left:691px;bottom:1189px;}
#t5_1{left:506px;bottom:1177px;}
#t6_1{left:651px;bottom:1177px;letter-spacing:0.1px;}
#t7_1{left:383px;bottom:1141px;letter-spacing:0.2px;}
#t8_1{left:597px;bottom:1119px;}
#t9_1{left:71px;bottom:1098px;}
#ta_1{left:71px;bottom:1057px;letter-spacing:0.2px;word-spacing:-0.2px;}
#tb_1{left:362px;bottom:1057px;letter-spacing:0.1px;}
#tc_1{left:71px;bottom:1036px;letter-spacing:0.1px;word-spacing:-0.2px;}
#td_1{left:651px;bottom:1037px;word-spacing:-0.2px;}
#te_1{left:71px;bottom:1015px;letter-spacing:0.1px;}
#tf_1{left:651px;bottom:1015px;word-spacing:-0.2px;}
#tg_1{left:72px;bottom:957px;letter-spacing:0.1px;}
#th_1{left:72px;bottom:936px;letter-spacing:0.1px;}
#ti_1{left:72px;bottom:916px;letter-spacing:0.1px;}
#tj_1{left:71px;bottom:897px;letter-spacing:0.1px;}
#tk_1{left:285px;bottom:897px;letter-spacing:-0.2px;}
#tl_1{left:358px;bottom:897px;letter-spacing:0.1px;}
#tm_1{left:443px;bottom:897px;letter-spacing:-0.1px;}
#tn_1{left:71px;bottom:875px;letter-spacing:0.1px;word-spacing:-0.1px;}
#to_1{left:225px;bottom:875px;}
#tp_1{left:358px;bottom:875px;letter-spacing:0.2px;word-spacing:-0.5px;}
#tq_1{left:71px;bottom:834px;letter-spacing:0.2px;word-spacing:-0.3px;}
#tr_1{left:71px;bottom:813px;letter-spacing:0.1px;}
#ts_1{left:285px;bottom:813px;letter-spacing:0.1px;}
#tt_1{left:297px;bottom:813px;letter-spacing:0.1px;word-spacing:-0.3px;}
#tu_1{left:71px;bottom:791px;letter-spacing:0.2px;}
#tv_1{left:151px;bottom:791px;letter-spacing:-0.1px;word-spacing:-0.1px;}
#tw_1{left:285px;bottom:791px;letter-spacing:-0.1px;}
#tx_1{left:71px;bottom:770px;}
#ty_1{left:71px;bottom:749px;letter-spacing:0.1px;}
#tz_1{left:432px;bottom:749px;letter-spacing:0.2px;word-spacing:-0.1px;}
#t10_1{left:522px;bottom:747px;}
#t11_1{left:597px;bottom:749px;letter-spacing:-0.2px;}
#t12_1{left:705px;bottom:749px;letter-spacing:-0.1px;word-spacing:-0.1px;}
#t13_1{left:746px;bottom:749px;}
#t14_1{left:71px;bottom:727px;letter-spacing:0.1px;}
#t15_1{left:506px;bottom:727px;letter-spacing:0.2px;}
#t16_1{left:651px;bottom:727px;letter-spacing:0.1px;}
#t17_1{left:285px;bottom:713px;letter-spacing:0.1px;}
#t18_1{left:432px;bottom:713px;}
#t19_1{left:506px;bottom:690px;letter-spacing:0.1px;}
#t1a_1{left:746px;bottom:692px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t1b_1{left:651px;bottom:672px;word-spacing:-0.2px;}
#t1c_1{left:71px;bottom:630px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t1d_1{left:506px;bottom:630px;}
#t1e_1{left:71px;bottom:609px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1f_1{left:319px;bottom:609px;letter-spacing:0.1px;}
#t1g_1{left:597px;bottom:609px;}
#t1h_1{left:746px;bottom:609px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t1i_1{left:71px;bottom:590px;letter-spacing:0.1px;word-spacing:-0.3px;}
#t1j_1{left:597px;bottom:590px;word-spacing:-0.2px;}
#t1k_1{left:506px;bottom:568px;letter-spacing:0.1px;}
#t1l_1{left:71px;bottom:546px;letter-spacing:0.1px;word-spacing:-0.2px;}
#t1m_1{left:597px;bottom:546px;}
#t1n_1{left:686px;bottom:547px;word-spacing:-0.2px;}
#t1o_1{left:506px;bottom:525px;letter-spacing:0.2px;}
#t1p_1{left:682px;bottom:525px;letter-spacing:0.2px;}
#t1q_1{left:796px;bottom:525px;letter-spacing:0.2px;}
#t1r_1{left:71px;bottom:484px;letter-spacing:0.2px;}
#t1s_1{left:331px;bottom:484px;letter-spacing:-0.2px;}
#t1t_1{left:358px;bottom:484px;letter-spacing:-0.2px;}
#t1u_1{left:597px;bottom:484px;letter-spacing:0.1px;word-spacing:-0.3px;}
#t1v_1{left:746px;bottom:463px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t1w_1{left:71px;bottom:443px;letter-spacing:0.2px;}
#t1x_1{left:112px;bottom:443px;}
#t1y_1{left:225px;bottom:443px;}
#t1z_1{left:71px;bottom:422px;letter-spacing:0.1px;word-spacing:-0.3px;}
#t20_1{left:506px;bottom:422px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t21_1{left:71px;bottom:401px;}
#t22_1{left:358px;bottom:401px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t23_1{left:506px;bottom:401px;}
#t24_1{left:300px;bottom:380px;}
#t25_1{left:358px;bottom:380px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t26_1{left:506px;bottom:380px;letter-spacing:0.2px;}
#t27_1{left:579px;bottom:380px;letter-spacing:0.1px;}
#t28_1{left:71px;bottom:360px;}
#t29_1{left:151px;bottom:339px;word-spacing:-0.1px;}
#t2a_1{left:506px;bottom:339px;letter-spacing:0.1px;}
#t2b_1{left:651px;bottom:339px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t2c_1{left:71px;bottom:318px;letter-spacing:0.1px;}
#t2d_1{left:506px;bottom:318px;letter-spacing:0.1px;word-spacing:-0.2px;}
#t2e_1{left:71px;bottom:298px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t2f_1{left:71px;bottom:277px;letter-spacing:0.1px;}
#t2g_1{left:506px;bottom:277px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t2h_1{left:225px;bottom:257px;}
#t2i_1{left:339px;bottom:257px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t2j_1{left:506px;bottom:257px;letter-spacing:0.1px;word-spacing:-0.3px;}
#t2k_1{left:506px;bottom:236px;letter-spacing:0.3px;}
#t2l_1{left:746px;bottom:236px;letter-spacing:0.3px;word-spacing:-0.6px;}
#t2m_1{left:506px;bottom:194px;}
#t2n_1{left:597px;bottom:174px;}
#t2o_1{left:686px;bottom:174px;word-spacing:-0.2px;}
#t2p_1{left:71px;bottom:29px;}
#t2q_1{left:542px;bottom:29px;}
#t2r_1{left:597px;bottom:29px;}
#t2s_1{left:691px;bottom:29px;}
#t2t_1{left:430px;bottom:1154px;}
#t2u_1{left:735px;bottom:1129px;letter-spacing:-0.2px;}
#t2v_1{left:225px;bottom:1108px;letter-spacing:0.1px;word-spacing:-0.2px;}
#t2w_1{left:225px;bottom:1093px;}
#t2x_1{left:225px;bottom:1077px;letter-spacing:-0.1px;word-spacing:-0.1px;}
#t2y_1{left:229px;bottom:897px;letter-spacing:-0.1px;}
#t2z_1{left:229px;bottom:813px;letter-spacing:-0.1px;}
#t30_1{left:513px;bottom:842px;}
#t31_1{left:512px;bottom:827px;}
#t32_1{left:524px;bottom:811px;letter-spacing:0.2px;}
#t33_1{left:608px;bottom:803px;word-spacing:-0.1px;}
#t34_1{left:647px;bottom:787px;word-spacing:-0.3px;}
#t35_1{left:690px;bottom:772px;word-spacing:0.1px;}
#t36_1{left:652px;bottom:566px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t37_1{left:432px;bottom:1057px;letter-spacing:-0.1px;word-spacing:-0.1px;}
#t38_1{left:433px;bottom:1034px;letter-spacing:-0.1px;}
#t39_1{left:226px;bottom:1013px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t3a_1{left:343px;bottom:1003px;word-spacing:-0.1px;}
#t3b_1{left:71px;bottom:981px;letter-spacing:0.1px;word-spacing:-0.4px;}
#t3c_1{left:285px;bottom:979px;}
#t3d_1{left:226px;bottom:1055px;letter-spacing:-0.1px;word-spacing:-0.3px;}

.s1{
	FONT-SIZE: 45.4px;
	FONT-FAMILY: CIDFont-F1_b;
	color: rgb(0,0,0);
}

.s2{
	FONT-SIZE: 45.4px;
	FONT-FAMILY: CIDFont-F2_j;
	color: rgb(0,0,0);
}

.s3{
	FONT-SIZE: 39.5px;
	FONT-FAMILY: CIDFont-F2_j;
	color: rgb(0,0,0);
}

.s4{
	FONT-SIZE: 62.2px;
	FONT-FAMILY: CIDFont-F1_b;
	color: rgb(0,0,0);
}

.s5{
	FONT-SIZE: 62.2px;
	FONT-FAMILY: CIDFont-F2_j;
	color: rgb(0,0,0);
}

</style>
<!-- End inline CSS -->

<!-- Begin embedded font definitions -->
<style id="fonts1" type="text/css" >

@font-face {
	font-family: CIDFont-F1_b;
	src: url("/admin/css/putevoy/fonts/CIDFont-F1_b.woff") format("woff");
}

@font-face {
	font-family: CIDFont-F2_j;
	src: url("/admin/css/putevoy/fonts/CIDFont-F2_j.woff") format("woff");
}

</style>
<!-- End embedded font definitions -->

<!-- Begin page background -->
<div id="pg1Overlay" style="width:100%; height:100%; position:absolute; z-index:1; background-color:rgba(0,0,0,0); -webkit-user-select: none;"></div>
<div id="pg1" style="-webkit-user-select: none;"><object width="935" height="1210" data="/admin/css/putevoy/1/1.svg" type="image/svg+xml" id="pdf1" style="width:935px; height:1210px; -moz-transform:scale(1); z-index: 0;"></object></div>
<!-- End page background -->


<!-- Begin text definitions (Positioned/styled in CSS) -->
<div id="t1_1" class="t s1">ПУТЕВОЙ ЛИСТ ЛЕГКОВОГО АВТОМОБИЛЯ</div>
<div id="t2_1" class="t s2">Д-1</div>
<div id="t3_1" class="t s2">№</div>
<div id="t4_1" class="t s2">85</div>
<div id="t5_1" class="t s2">серия</div>
<div id="t6_1" class="t s2">номер</div>
<div id="t7_1" class="t s2">дата</div>
<div id="t8_1" class="t s2">по ОКПО</div>
<div id="t9_1" class="t s2">Организация</div>
<div id="ta_1" class="t s2">Марка автомобиля</div>
<div id="tb_1" class="t s2">Разрешение </div>
<div id="tc_1" class="t s2">Государственный регистрационный знак</div>
<div id="td_1" class="t s3">Номер парковки</div>
<div id="te_1" class="t s2">Водитель</div>
<div id="tf_1" class="t s3">Табельный номер</div>
<div id="tg_1" class="t s4">Предрейсовый</div>
<div id="th_1" class="t s4">Медосмотр</div>
<div id="ti_1" class="t s4">Пройден</div>
<div id="tj_1" class="t s1">дата</div>
<div id="tk_1" class="t s2">г.</div>
<div id="tl_1" class="t s2">Время</div>
<div id="tm_1" class="t s2">16 ч. 10 мин</div>
<div id="tn_1" class="t s1">должность: Врач</div>
<div id="to_1" class="t s2">подпись</div>
<div id="tp_1" class="t s2">'.$medic.'</div>
<div id="tq_1" class="t s2">Выезд с парковки</div>
<div id="tr_1" class="t s2">Дата</div>
<div id="ts_1" class="t s2">г. </div>
<div id="tt_1" class="t s2">Механик/ Диспечер ____'.$mech.'</div>
<div id="tu_1" class="t s2">Время</div>
<div id="tv_1" class="t s2">16 ч. 20</div>
<div id="tw_1" class="t s2">мин</div>
<div id="tx_1" class="t s2">Послерейсовый </div>
<div id="ty_1" class="t s2">медосмотр</div>
<div id="tz_1" class="t s2">дата, время</div>
<div id="t10_1" class="t s5">'.$today.'</div>
<div id="t11_1" class="t s2">г.</div>
<div id="t12_1" class="t s2">16 ч. 15</div>
<div id="t13_1" class="t s2">мин</div>
<div id="t14_1" class="t s2">пройден</div>
<div id="t15_1" class="t s2">дата</div>
<div id="t16_1" class="t s2">время</div>
<div id="t17_1" class="t s2">должность</div>
<div id="t18_1" class="t s2">подпись</div>
<div id="t19_1" class="t s5">Механик</div>
<div id="t1a_1" class="t s2">'.$mech.'</div>
<div id="t1b_1" class="t s3">подпись и расшифровка подписи</div>
<div id="t1c_1" class="t s2">Задание водителю</div>
<div id="t1d_1" class="t s1">Показание одометра, км</div>
<div id="t1e_1" class="t s2">В распоряжение</div>
<div id="t1f_1" class="t s2">Яндекс</div>
<div id="t1g_1" class="t s2">Механик</div>
<div id="t1h_1" class="t s2">'.$mech.'</div>
<div id="t1i_1" class="t s2">Адрес первой подачи</div>
<div id="t1j_1" class="t s2">Автомобиль в исправном состоянии принял</div>
<div id="t1k_1" class="t s2">Водитель</div>
<div id="t1l_1" class="t s2">По указанию Яндекс г. Москва и Московская область</div>
<div id="t1m_1" class="t s2">подпись</div>
<div id="t1n_1" class="t s3">расшифровка подписи</div>
<div id="t1o_1" class="t s2">Горючее</div>
<div id="t1p_1" class="t s2">марка</div>
<div id="t1q_1" class="t s2">код</div>
<div id="t1r_1" class="t s2">дата</div>
<div id="t1s_1" class="t s2">'.$year.'</div>
<div id="t1t_1" class="t s2">г.</div>
<div id="t1u_1" class="t s2">Движение горючего</div>
<div id="t1v_1" class="t s2">количество, л</div>
<div id="t1w_1" class="t s2">Время </div>
<div id="t1x_1" class="t s2">ч.</div>
<div id="t1y_1" class="t s2">мин</div>
<div id="t1z_1" class="t s2">при заезде на парковку</div>
<div id="t20_1" class="t s2">Выдано: по заправочному</div>
<div id="t21_1" class="t s2">Диспетчер/механик</div>
<div id="t22_1" class="t s2">'.$mech.'</div>
<div id="t23_1" class="t s2">листу №</div>
<div id="t24_1" class="t s2">подпись</div>
<div id="t25_1" class="t s2">расшифровка подписи</div>
<div id="t26_1" class="t s2">остаток: </div>
<div id="t27_1" class="t s2">при выезде</div>
<div id="t28_1" class="t s2">Ожидание :</div>
<div id="t29_1" class="t s2">Опоздания, ожидания, простои в пути, заезды в гараж </div>
<div id="t2a_1" class="t s2">расход</div>
<div id="t2b_1" class="t s2">по норме</div>
<div id="t2c_1" class="t s2">и прочие отметки</div>
<div id="t2d_1" class="t s2">Послерейсовый контроль технического состояния</div>
<div id="t2e_1" class="t s2">Автомобиль сдал</div>
<div id="t2f_1" class="t s2">водитель</div>
<div id="t2g_1" class="t s2">Показания одометра при</div>
<div id="t2h_1" class="t s2">подпись</div>
<div id="t2i_1" class="t s2">расшифровка подписи</div>
<div id="t2j_1" class="t s2">возвращении на парковку,</div>
<div id="t2k_1" class="t s2">км</div>
<div id="t2l_1" class="t s2">дата ; время</div>
<div id="t2m_1" class="t s2">Механик</div>
<div id="t2n_1" class="t s2">подпись</div>
<div id="t2o_1" class="t s3">расшифровка подписи</div>
<div id="t2p_1" class="t s1">ПУТЕВОЙ ЛИСТ ЛЕГКОВОГО АВТОМОБИЛЯ</div>
<div id="t2q_1" class="t s2">Д-1</div>
<div id="t2r_1" class="t s2">№</div>
<div id="t2s_1" class="t s2">85</div>
<div id="t2t_1" class="t s5">'.$today.'</div>
<div id="t2u_1" class="t s2">'.$okpo.'</div>
<div id="t2v_1" class="t s2">'.$name_lic.' </div>
<div id="t2w_1" class="t s2">ОГРН '.$ogrn.' </div>
<div id="t2x_1" class="t s2">ИНН '.$inn.'</div>
<div id="t2y_1" class="t s2">'.$today.'</div>
<div id="t2z_1" class="t s2">'.$today.'</div>
<div id="t30_1" class="t s2">Предрейсовый </div>
<div id="t31_1" class="t s2">(предсменный)</div>
<div id="t32_1" class="t s2">(контроль)</div>
<div id="t33_1" class="t s1">Предрейсовый контроль технического состояния</div>
<div id="t34_1" class="t s1">транспортного средства пройден,</div>
<div id="t35_1" class="t s1">выезд разрешен</div>
<div id="t36_1" class="t s5">'.$fio_driver.'</div>
<div id="t37_1" class="t s2">'.$razresh.'</div>
<div id="t38_1" class="t s5">'.$gosnomer.'</div>
<div id="t39_1" class="t s5">'.$fio_driver.'</div>
<div id="t3a_1" class="t s3">фамилия, имя, отчество</div>
<div id="t3b_1" class="t s2">Удостоверение №</div>
<div id="t3c_1" class="t s5">'.$udostov.'</div>
<div id="t3d_1" class="t s5">'.$mark_model.'</div>

<!-- End text definitions -->


</div>













</body>
</html>
		
		';			
				
				
				
				
		
	
	//	$mpdf = new Mpdf();
    //    $mpdf->WriteHTML($html);
    //    $mpdf->Output();
    //    exit;
	return $html;
	
	}
	
	
	
	
	public function actionPutevie ($id) {
		
		
		$model = $this->findModel($id);
		
		$auto_id = $model->auto_id;
		
		$driver_id = $model->driver_id;
		
		
		$request = Yii::$app->request->queryParams;
		
		
		$request['PutevielistiSearchModel']['driver_id'] = $driver_id;
		$request['PutevielistiSearchModel']['auto_id'] = $auto_id;
		
		
		$searchModel = new PutevielistiSearchModel();
        $dataProvider = $searchModel->search($request);

		
		
		$arrTypes = [ 1 => 'С проставлением дат', 2 => 'Без дат)', ];
		
		
        return $this->render('putevie', [
			'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrTypes' => $arrTypes,
        ]);
		
	}
	
	
	//Импорт договоров
	
	public function actionImport()
    {
		
		$model = new Contracts();
		
		
		$text_result = '';
		
		

		if ($model->load(Yii::$app->request->post())) { 
		
			$text_result = 'Результаты импорта <br>';
			$t_m = 	$model;	
					
					
			$t_m->file_f = UploadedFile::getInstance($t_m,'file_f');
			
			$fileName = $t_m->file_f->name;
			
			
			//$post = Yii::$app->request->post();
			
			//$post['Driversfiles']['file_f'] = $t_m->file_f->name;
			
			$model->file_f->saveAs('uploads/'.$t_m->file_f->baseName.'.'.$t_m->file_f->extension);
			
			
			
			$file_path = 'uploads/'.$t_m->file_f->baseName.'.'.$t_m->file_f->extension;
			
			
			
			$filename = $file_path;

			// The nested array to hold all the arrays
			$the_big_array = []; 

			// Open the file for reading
			if (($h = fopen("{$filename}", "r")) !== FALSE) 
			{
			  // Each line in the file is converted into an individual array that we call $data
			  // The items of the array are comma separated
			  while (($data = fgetcsv($h, 1000, ";")) !== FALSE) 
			  {
				// Each individual array is being pushed into the nested array
				$the_big_array[] = $data;		
			  }

			  // Close the file
			  fclose($h);
			}

			
			unset ($the_big_array[0]);
		
		
			//Массивы из справочников
			
			$status_arr = [
							
								'Выкупает' => '1',
								'Арендует' => '8',
								'ЗП' => '7',
								'Выкуплена' => '4',
								
							
							];
		
			$type_arr = [
								'День' => '1',
								'Неделя' => '2',
								'Месяц' => '3',
						];
		
		
		
		
			foreach ($the_big_array as $key => $value) {
			    		
				

				$item = $value;
						
						
						
				$status = $status_arr[$item[0]];
				
				
				
				
				
				$gosnomer = $item[1];
				
				$fio = trim($item[2]);
					
				
				//$price = (float) ($item[3]);
				
				$price = floatval ($item[3]);
				
				$date_start = date('Y-m-d',strtotime($item[4]));
				
				$date_end = date('Y-m-d',strtotime($item[5]));
					
				$gosnomer = trim($gosnomer);
				
				$models_auto = AutomobilesNewModel::find()->where(['gosnomer' => $gosnomer])->all();
				
				
				// Фио
				
				if (!empty($fio)) {
				
					$fio_arr = explode(' ', $fio);
					
					
					if ($fio_arr[0]) {
						$last_name = $fio_arr[0];
					} 
					
					$first_name = '';
					
					if ($fio_arr[1]) {
						$first_name = $fio_arr[1];
					}
					
					$middle_name = '';
					
					if ($fio_arr[2]) {
						$middle_name = $fio_arr[2];
					}
						
				}
				
				
				if (!empty($item[7])) {
					$arenda_type = $type_arr[$item[7]];
				} else {
					$arenda_type = 1;
				}
				
				$arenda_items = (int) $item[6];
				
			
				
				if (empty($item[6])) {
					$arenda_items = 0;
				}
				
				
				
				
				$pvznos = floatval ($item[8]);
				
				$vykup = floatval ($item[9]);
				
				
				//ДАлее идет блок для настроек договора по умолчанию
				
				
				
				//Ищем водителя
				
				$models_drivers = DriversModel::find()->where(
										[
											//'first_name' => $first_name, 
											//'middle_name'=> $middle_name,
											'fio' => $fio,
											
										])->all();
															
				
				$model = new Contracts();
				
				
				//print_r ($models_auto);
				
				
				
				if (!empty($fio)) {
						
				
					if ( !empty($models_auto) && !empty($models_drivers)) {
															
									
						$id_auto = $models_auto[0]->id_auto;					
									
						$model_contracts = Contracts::find()->where(['auto_id' => $id_auto])->all();			

						if (empty($model_contracts)) {
									
							$model = new Contracts();		
									
							$id_auto = $models_auto[0]->id_auto;			
									
									
							$model->auto_id = $models_auto[0]->id_auto;
							$model->driver_id = $models_drivers[0]->id_driver;

							$model->owner_id = $models_auto[0]->owner_id;
							
							$model->status_itog = $status;
							
							$model->arenda_stoim = $price;
							$model->p_vznos = $pvznos;
							
							$model->date_start = $date_start;
							$model->date_pay_arenda = $date_end;
							
							$model->arenda_type = $arenda_type;
							
							$model->arenda_items = $arenda_items;
							
							
							$new_date_end = 0;
							
							//$arenda_items
							
							//$arenda_type
							
							if (empty($arenda_items)) {
								$arenda_items = 0;
							}
							
							if ($arenda_type == 1) {
								
								$koeff  = 1;
								
							} elseif ($arenda_type == 2) {
								
								$koeff = 7; 
								
							} elseif ( $arenda_type == 3) {
								$koeff = 30; 
							} else {
								$koeff = 1; 
							}
							
							$dddays = $arenda_items * $koeff;
							
							
								
							$new_date_end = date('Y-m-d',strtotime($date_end. ' + '.$dddays.' days'));
							
							
							
							
							
							
							//Дата окончания новая
							$model->date_end = $new_date_end;
							
							if ($status == 8 or $status == 7) {
								$model->date_end = '2099-12-31';
							}
							
							
							$model->prosrochka = 5000;
							$model->peredacha_drugomu = 100000;
							$model->vyezd_bez_sogl_200 = 100000;
							$model->no_put_list = 100000;
							$model->shtraf_pdd = 2;
							$model->snyat_atrib = 100000;
							$model->no_osago = 50000;
							$model->poterya_docs = 50000;
							$model->poterya_keys = 10000;
							$model->no_prodl_diag  = 50000;
							$model->vyezd_sotrudnikov = 10000;
							
							$model->penalty_comission = 1;
							$model->discount_type = 1;
							
							if (!empty($item[10])) {	
								
								$model->comment = $item[10];
								
							}
							
							$model->save(false);
											
							$md_auto = AutomobilesNewModel::findOne($id_auto);
									
							$status_auto = 1;
							
							if ($status == 1) {
								
								$status_auto = 4;
								
							} elseif ($status == 8) {
								
								$status_auto = 10;
								
							} elseif ($status == 7) {
								$status_auto = 9;
							} elseif ($status == 4) { 
								$status_auto = 5;
							}
							
							
							$md_auto->status = $status_auto;
							
							$md_auto->save(false);
							
						
						} else {
							
							$model = $this->findModel($model_contracts[0]->id_contract);
								
							$id_auto = $models_auto[0]->id_auto;		
			
							$model->auto_id = $id_auto;
											
							$model->driver_id = $models_drivers[0]->id_driver;

							$model->owner_id = $models_auto[0]->owner_id;
							
							$model->status_itog = $status;
							
							$model->arenda_stoim = $price;
							$model->p_vznos = $pvznos;
							
							$model->date_start = $date_start;
							$model->date_pay_arenda = $date_end;
							
							$model->arenda_type = $arenda_type;
							
							$model->arenda_items = $arenda_items;
							
							
							
							
							
							$model_prostoys = Prostoys::find()->where(['contract_id' => $model->id_contract])->all();
							
							$prostoy_days = 0;
							
							if (!empty($model_prostoys)) {
								
								foreach ($model_prostoys as $item) {
									
									$prostoy_days = $prostoy_days + $item->days_prostoy;
									
								}
								
								
							}
							
							
							$new_date_end = 0;
							
							//$arenda_items
							
							//$arenda_type
							
						
							
							if ($arenda_type == 1) {
								
								$koeff  = 1;
								
							} elseif ($arenda_type == 2) {
								
								$koeff = 7; 
								
							} elseif ( $arenda_type == 3) {
								$koeff = 30; 
							} else {
								$koeff = 1; 
							}
							
							$dddays = $arenda_items * $koeff + $prostoy_days;
							
							
							
							
							
							$new_date_end = date('Y-m-d',strtotime($date_end. ' + '.$dddays.' days'));
				
							
							//Дата окончания новая
							$model->date_end = $new_date_end;
							
							if ($status == 8 or $status == 7) {
								$model->date_end = '2099-12-31';
							}
							
				
							
							$model->prosrochka = 5000;
							$model->peredacha_drugomu = 100000;
							$model->vyezd_bez_sogl_200 = 100000;
							$model->no_put_list = 100000;
							$model->shtraf_pdd = 2;
							$model->snyat_atrib = 100000;
							$model->no_osago = 50000;
							$model->poterya_docs = 50000;
							$model->poterya_keys = 10000;
							$model->no_prodl_diag  = 50000;
							$model->vyezd_sotrudnikov = 10000;
							$model->penalty_comission = 1;
							$model->discount_type = 1;
								
							if (!empty($item[10])) {	
								
								$model->comment = $item[10];
								
							}
							$model->save();

							
							
							
							$md_auto = AutomobilesNewModel::findOne($id_auto);
							
							
							//$md_auto->id_auto = $id_auto;
							
							
							
							$status_auto = 1;
							
							if ($status == 1) {
								
								$status_auto = 4;
								
							} elseif ($status == 8) {
								
								$status_auto = 10;
								
							} elseif ($status == 7) {
								$status_auto = 9;
							} elseif ($status == 4) { 
								$status_auto = 5;
							}
							
							
							$md_auto->status = $status_auto;
							
							$md_auto->save(false);
										
						}
						
					} else {
						$text_result.= 'НЕт машины или нет водителя '.$fio.' '.$item[1].' в строчке - проверить на опечатки или лишние пробелы'.$key.' <br>';
					}
				
				
				} else {
				
					$text_result.= 'ПУстое фио в строчке '.$key.' <br>';
				
				}
					
			}
			
			
			
			//return $this->redirect(['index']);
		
		}
	
	
	
	
		
        return $this->render('import', [
            'model' => $model,
			'text_result' => $text_result,
			
        ]);
	
	
	}
	
	
	
	
	public function actionMultipledelete()
	{
		$pk = Yii::$app->request->post('row_id');
		
		

		foreach ($pk as $key => $value) {
			$sql = "DELETE FROM contracts WHERE id_contract = $value";
			$query = Yii::$app->db->createCommand($sql)->execute();
		}

		return $this->redirect(['index']);

	} 
	
	
	public function actionRemoveall()
	{
		//$pk = Yii::$app->request->post('row_id');
		
		

		//foreach ($pk as $key => $value) {
		$sql = "DELETE FROM contracts;";
		$query = Yii::$app->db->createCommand($sql)->execute();
		//}

		return $this->redirect(['index']);

	} 
	
	
	
	public function actionCardnaletu () {
		
		
		$model = new Contracts();
		
		
		if ($model->load(Yii::$app->request->post())) { 
		
			
			$post = Yii::$app->request->post();
		
		
			$driver_id = $post['Contracts']['driver_id'];
			$auto_id = $post['Contracts']['auto_id'];
		
			$auto = new AutomobilesNewModel();
		
			$auto = $auto->findOne($auto_id);
		
			
		
			$driver = new DriversModel();
		
			$driver = $driver->findOne($driver_id);
		
		
			$licen_id = $auto->licen_id;
		
		
			$licen = new Licen();
		
			$licen = $licen->findOne($licen_id);
		
			return $this->render('cardprint', [
				'model' => $model,
				'auto' => $auto,
				'driver' => $driver,
				'licen' => $licen,
			]);
		
		
		
		}
		
		
		
		
		
		
		
		
		
		
		
		$model = new Contracts();
		
		
		$model_drivers = DriversModel::find()->orderBy('fio ASC')->all();
		
		$arrDrivers = [];
		
		foreach ($model_drivers as $item) {
			$arrDrivers[$item->id_driver] = $item->fio;
			
		}
		
		
		$model_autos = AutomobilesNewModel::find()->orderBy('mark ASC')->all();
		
		$arrAutos = [];
		
		foreach ($model_autos as $item) {
			$arrAutos[$item->id_auto] = $item->mark.' '.$item->model.' '.$item->gosnomer; 
		}
		
		
		return $this->render('cardnaletu', [
            'model' => $model,
			'arrDrivers' => $arrDrivers,
			'arrAutos' => $arrAutos,
        ]);
	
		
	}
	
	
	
	public function actionFinish ($id) {
		
		
		$model = $this->findModel($id);
		
		$model->status_itog = 4;
		
		$model->save();
		
		
		$auto_id = $model->auto_id;

		$auto = AutomobilesNewModel::findOne($auto_id);
		$auto->status = 5;
		$auto->save();
		
		
		
		return $this->redirect(['view', 'id' => $id]);
		
	}
	
	
	public function actionTerminate ($id) {
		
		
		$model = $this->findModel($id);
		
		$model->status_itog = 6;
		$model->date_end = date('Y-m-d');
		
		$model->save();


		$auto_id = $model->auto_id;

		$auto = AutomobilesNewModel::findOne($auto_id);
		$auto->status = 1;
		$auto->save();
		
		return $this->redirect(['view', 'id' => $id]);
		
	}
	
	
	
	
	public function actionProdlenie ($id) {
		
		
		$model = $this->findModel($id);
		
		
		if (Yii::$app->request->post()) { 
		
			
			$post = Yii::$app->request->post();
			
			
			$days = $post['days'];
		
			$model->date_end = date('Y-m-d', strtotime( $model->date_end. ' + '.$days.' days'));
			
			$model->save();
			return $this->redirect(['view', 'id' => $id]);
		}
		
		
		
		
		return $this->render('prodlenie', [
            'model' => $model,
			
        ]);
	
		
		
	}
	
	
	
	public function actionPeresadka ($id) {
		
		/*
		
		При пересаживании получается по взносу, 
		выкупному и всем арендным платежам переносим? 
		А пдд остается на старом договоре со статусом Пересажен, 
		так?

		Машину освобождаем и общий долг пересчитываем? 
		А в карточке водителя выводим суммарный долг?
				
		*/
			

		$model = $this->findModel($id);
		
		$modelContract = $model;
		
		$modelContract->status_itog = 5;
			
		$modelContract->date_end = date('Y-m-d');
		$modelContract->save();
		
		
		
		
		$model = new Contracts();
		$modelContract->id_contract = null;
		$model->attributes = $modelContract->attributes;
		//$model->id_contract = null; 
		
		$modelContract->status_itog = 1;
		$model->save();
		
		
		$auto_id = $model->auto_id;
	
		$auto = AutomobilesNewModel::findOne($auto_id);
	
		$auto->status = 1;
		
		$auto->save();
		
		$payments = Payments::find()->where(['contract_id' => $id ])
									->andWhere(['in', 'pmnt_variant', '1,2'])
									->all();
		
		if (!empty($payments)) {
			
			foreach ($payments as $item) {
				
				
				$item->contract_id = $model->id_contract;
				$item->save();
			} 
			
			
		}
		
		
		
		
		return $this->redirect(['update', 'id' => $model->id_contract]);
		
	}
	
	
	

    /**
     * Finds the Contracts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contracts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contracts::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
