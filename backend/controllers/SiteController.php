<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\MaintableModelSearch;

use common\models\Autostatus;

use common\models\AutomobilesNewModel;
use app\models\Contracts;
use common\models\User;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'validateregister'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'validateregister'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
					[
								   'actions' => [
												'index', 
												'view',
												],
								   'allow' => true,
								   'roles' => ['user'],
						   ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
		
		//Выведем все авто со статусами
		
		$model_autos_statuses = [];
		
		
		$model_contracts = Contracts::find()->all();
		
		
		
		//print_r (count($model_contracts));
		
		
		
		$model_autos_statuses = AutomobilesNewModel::find()
						->select(['COUNT(*) AS aa', 'automobiles.*'])
						->joinWith(['auto_status'])
						->groupBy(['automobiles.status'])->all();
		

		
		$model_auto_statsss = Autostatus::find()->all();
		
		
		$arrStatuses = [];
		
		foreach ($model_auto_statsss as $item) {
			
			$arrStatuses[$item->status_name] = $item->status_name;
		} 
		
		//print_r ($model_autos_statuses);
		
		//exit;
		
		
		$searchModel = new MaintableModelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'model_autos_statuses' => $model_autos_statuses,
			'arrStatuses' => $arrStatuses,
        ]);
		
		
		
        
    }
	
	
	
	public function actionValidateregister ($auth_key) {
		
		
		if (!empty($auth_key)) {
			
			
			$user = User::findByToken($auth_key);
			
			
			if (!empty($user)) {
				
				$user->status = 10;
				
				$user->save();
				
				
				return $this->goHome();
				
			}
			
		}
		
		
		
		
	}
	
	
	
	
	/**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
	
	
	
	    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionSotrudniki()
    {
		
		//Выведем все авто со статусами
		
		$model_autos_statuses = [];
		
		
		$model_contracts = Contracts::find()->all();
		
		
		
		//print_r (count($model_contracts));
		
		
		
		$model_autos_statuses = AutomobilesNewModel::find()
						->select(['COUNT(*) AS aa', 'automobiles.*'])
						->joinWith(['auto_status'])
						->groupBy(['automobiles.status'])->all();
		

		
		

		
		//print_r ($model_autos_statuses);
		
		//exit;
		
		
		$searchModel = new MaintableModelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('sotrudniki', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'model_autos_statuses' => $model_autos_statuses,
        ]);
		
		
		
        
    }
	
	
	
	
	
	
	
	
	
	

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
