<?php

namespace backend\controllers;

use Yii;
use app\models\Prostoys;
use backend\models\ProstoysSearchModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


use app\models\Contracts;
use app\models\Prostoystatus;
use app\models\Logitems;
use app\models\Payments;
use yii\filters\AccessControl;
/**
 * ProstoysController implements the CRUD actions for Prostoys model.
 */
class ProstoysController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
		
			[
				   'class' => AccessControl::className(),
				   'only' => ['index','create', 'view', 'update', 'delete', 'newcreate',],
				   'rules' => [
						   [
								   'actions' => ['index','create', 'view','update', 'delete', 'newcreate',],
								   'allow' => true,
								   'roles' => ['admin'],
						   ],
						   [
								   'actions' => [
												'index', 
												'view',
												'create',
												'update',
												'newcreate',
												],
								   'allow' => true,
								   'roles' => ['manager', 'payer'],
						   ],
						   
						   [
								   'actions' => [
												'index', 
												'view',
												
												],
								   'allow' => true,
								   'roles' => ['user'],
						   ],
				   ],

		   ],
			
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'create', 'view', 'update', 'delete', 'newcreate',],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
		
		
		
		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Prostoys models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProstoysSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		
		
		$model_prostoy_statuses = Prostoystatus::find()->all();
		
		
		
		
		
		foreach ($model_prostoy_statuses as $value) {
			
			$arrStatus[$value->id_prostoy_status] = $value->prostoy_status_name;
			
		}
		
		
		

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrStatus' => $arrStatus,
        ]);
    }

    /**
     * Displays a single Prostoys model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Prostoys model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Prostoys();
		
			
		$model_prostoy_statuses = Prostoystatus::find()->all();
		
		
		foreach ($model_prostoy_statuses as $value) {
			
			$arrStatus[$value->id_prostoy_status] = $value->prostoy_status_name;
			
		}

		
		$model_contracts = Contracts::find()->joinWith(['drivers','automobiles'])->where(['status_itog' => '1'])->all();
		
		
		foreach ($model_contracts as $value) {
			
			
			$drivers = $value['drivers'];
			$automobiles = $value['automobiles'];
			
			
			$arrContracts[$value->id_contract] = 'Договор '.$value->id_contract.' '
												.$drivers[0]->first_name.' '
												.$drivers[0]->last_name.' '
												.$automobiles[0]->mark.' '
												.$automobiles[0]->model.' '
												.$automobiles[0]->gosnomer;
			
			
		}
		
		
		
		
		

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			
			
			
			$model_prostoys = Prostoys::find()->where(['contract_id' => $model->contract_id])->all();
			
			$days_prrr = 0;
			
			
			if (!empty($model_prostoys)) {
				
				foreach ($model_prostoys as $item) {
						
					$days_prrr = $days_prrr + $item->days_prostoy;
					
					
				}
			
			}
			
			
			$contract_model = Contracts::findOne($model->contract_id);
			
			
			$new_date_end = 0;
							
							//$arenda_items
							
							//$arenda_type
							
			$arenda_type = $contract_model->arenda_type;			
							
			if ($arenda_type == 1) {
				
				$koeff  = 1;
				
			} elseif ($arenda_type == 2) {
				
				$koeff = 7; 
				
			} elseif ( $arenda_type == 3) {
				$koeff = 30; 
			} else {
				$koeff = 1; 
			}
			
			$dddays =  $contract_model->arenda_items * $koeff;
				
			if (empty($contract_model->date_pay_arenda)) {
			    $contract_model->date_pay_arenda = date('Y-m-d');
			}	
			
			$dddays = $dddays + $days_prrr;
				
			$new_date_end = date('Y-m-d',strtotime($contract_model->date_pay_arenda. ' + '.$dddays.' days'));

			
			//Дата окончания новая
			$contract_model->date_end = $new_date_end;
			
			if ($contract_model->status_itog == 8 or $contract_model->status_itog == 7) {
				$contract_model->date_end = '2099-12-31';
			}
			
			
			$contract_model->save(false);
			
			$modelLogitems = new Logitems ();
			
			$user_id = Yii::$app->user->identity->id;
			
			//Yii::$app->authManager->getRolesByUser($user_id);
			$type_user = current(Yii::$app->authManager->getRolesByUser( Yii::$app->user->identity->id))->name;
			$modelLogitems->type_user = $type_user;
			$modelLogitems->user_id = $user_id;
			$modelLogitems->type_page = 'prostoy';
			$modelLogitems->item_id = $model->id_prostoy;
			$modelLogitems->date_create = date('Y-m-d');
						
			$modelLogitems->save();
			
			
			
            return $this->redirect(['view', 'id' => $model->id_prostoy]);
        }

        return $this->render('create', [
            'model' => $model,
			'arrContracts' => $arrContracts,
			'arrStatus' => $arrStatus,
        ]);
    }
	
	
	
	 public function actionNewcreate()
    {
        $model = new Prostoys();
		
		
		$modelPayments = new Payments();
		
		$model_prostoy_statuses = Prostoystatus::find()->all();
		
		
		foreach ($model_prostoy_statuses as $value) {
			
			$arrStatus[$value->id_prostoy_status] = $value->prostoy_status_name;
			
		}
		
		
		
		
		$model_contracts = Contracts::find()->joinWith(['drivers','automobiles'])->where(['status_itog' => '1'])->all();
		
		
		foreach ($model_contracts as $value) {
			
			
			$drivers = $value['drivers'];
			$automobiles = $value['automobiles'];
			
			
			$arrContracts[$value->id_contract] = 'Договор '.$value->id_contract.' '
												.$drivers[0]->first_name.' '
												.$drivers[0]->last_name.' '
												.$automobiles[0]->mark.' '
												.$automobiles[0]->model.' '
												.$automobiles[0]->gosnomer;
			
			
		}
			

        if ($model->load(Yii::$app->request->post())) {
			
			
			$postBigDats = Yii::$app->request->post();
		
			
			$model->save();
					
	
			$id_prostoy = $model->id_prostoy;
			
			
			$modelPayments = new Payments();
			
			$modelPayments->load($postBigDats);
			
			
			$modelPayments->contract_id = $model->contract_id;
			
			
			$modelPayments->p_id_prostoy = $id_prostoy;

			$modelPayments->save(false);
						
		
			$modelLogitems = new Logitems ();
			
			$user_id = Yii::$app->user->identity->id;
			
			
			
			
			
			
			
			$model_prostoys = Prostoys::find()->where(['contract_id' => $model->contract_id])->all();
			
			$days_prrr = 0;
			
			
			if (!empty($model_prostoys)) {
				
				foreach ($model_prostoys as $item) {
						
					$days_prrr = $days_prrr + $item->days_prostoy;
					
					
				}
			
			}
			
			
			$contract_model = Contracts::findOne($model->contract_id);
			
			
			$new_date_end = 0;
							
							//$arenda_items
							
							//$arenda_type
							
			$arenda_type = $contract_model->arenda_type;			
							
			if ($arenda_type == 1) {
				
				$koeff  = 1;
				
			} elseif ($arenda_type == 2) {
				
				$koeff = 7; 
				
			} elseif ( $arenda_type == 3) {
				$koeff = 30; 
			} else {
				$koeff = 1; 
			}
			
			$dddays =  $contract_model->arenda_items * $koeff;
				
			if (empty($contract_model->date_pay_arenda)) {
			    $contract_model->date_pay_arenda = date('Y-m-d');
			}	
			
			$dddays = $dddays + $days_prrr;
				
			$new_date_end = date('Y-m-d',strtotime($contract_model->date_pay_arenda. ' + '.$dddays.' days'));

			
			//Дата окончания новая
			$contract_model->date_end = $new_date_end;
			
			if ($contract_model->status_itog == 8 or $contract_model->status_itog == 7) {
				$contract_model->date_end = '2099-12-31';
			}
			
			
			$contract_model->save(false);
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			//Yii::$app->authManager->getRolesByUser($user_id);
			$type_user = current(Yii::$app->authManager->getRolesByUser( Yii::$app->user->identity->id))->name;
			$modelLogitems->type_user = $type_user;
			$modelLogitems->user_id = $user_id;
			$modelLogitems->type_page = 'prostoy';
			$modelLogitems->item_id = $model->id_prostoy;
			$modelLogitems->date_create = date('Y-m-d');
						
			$modelLogitems->save();
			
			
			
            return $this->redirect(['view', 'id' => $model->id_prostoy]);
        }

        return $this->render('newcreate', [
            'model' => $model,
            'modelPayments' => $modelPayments,
			'arrContracts' => $arrContracts,
			'arrStatus' => $arrStatus,
        ]);
    }
	
	

    /**
     * Updates an existing Prostoys model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			
			
			
			
			
			
					$model_prostoys = Prostoys::find()->where(['contract_id' => $model->contract_id])->all();
			
			$days_prrr = 0;
			
			
			if (!empty($model_prostoys)) {
				
				foreach ($model_prostoys as $item) {
						
					$days_prrr = $days_prrr + $item->days_prostoy;
					
					
				}
			
			}
			
			
			$contract_model = Contracts::findOne($model->contract_id);
			
			
			$new_date_end = 0;
							
							//$arenda_items
							
							//$arenda_type
							
			$arenda_type = $contract_model->arenda_type;			
							
			if ($arenda_type == 1) {
				
				$koeff  = 1;
				
			} elseif ($arenda_type == 2) {
				
				$koeff = 7; 
				
			} elseif ( $arenda_type == 3) {
				$koeff = 30; 
			} else {
				$koeff = 1; 
			}
			
			$dddays =  $contract_model->arenda_items * $koeff;
				
			if (empty($contract_model->date_pay_arenda)) {
			    $contract_model->date_pay_arenda = date('Y-m-d');
			}	
			
			$dddays = $dddays + $days_prrr;
				
			$new_date_end = date('Y-m-d',strtotime($contract_model->date_pay_arenda. ' + '.$dddays.' days'));

			
			//Дата окончания новая
			$contract_model->date_end = $new_date_end;
			
			if ($contract_model->status_itog == 8 or $contract_model->status_itog == 7) {
				$contract_model->date_end = '2099-12-31';
			}
			
			
			$contract_model->save(false);
			
			
			
			
			
			
			
			
			
			
			
            return $this->redirect(['view', 'id' => $model->id_prostoy]);
        }
		
		
		$model_prostoy_statuses = Prostoystatus::find()->all();

		
		foreach ($model_prostoy_statuses as $value) {
			
			$arrStatus[$value->id_prostoy_status] = $value->prostoy_status_name;
			
		}
		
			
		$model_contracts = Contracts::find()->joinWith(['drivers','automobiles'])->where(['status_itog' => '1'])->all();
		
		
		foreach ($model_contracts as $value) {
			
			
			$drivers = $value['drivers'];
			$automobiles = $value['automobiles'];
			
			
			$arrContracts[$value->id_contract] = 'Договор '.$value->id_contract.' '
												.$drivers[0]->first_name
												.$drivers[0]->last_name
												.$automobiles[0]->mark
												.$automobiles[0]->model
												.$automobiles[0]->gosnomer;
				
		}
			

        return $this->render('update', [
            'model' => $model,
			'arrContracts' => $arrContracts,
			'arrStatus' => $arrStatus,
        ]);
    }

    /**
     * Deletes an existing Prostoys model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
			$model = $this->findModel($id);
		
		
			$model_prostoys = Prostoys::find()->where(['contract_id' => $model->contract_id])->all();
			
			$days_prrr = 0;
			
			
			if (!empty($model_prostoys)) {
				
				foreach ($model_prostoys as $item) {
						
					$days_prrr = $days_prrr + $item->days_prostoy;
					
					
				}
			
			}
			
			
			$contract_model = Contracts::findOne($model->contract_id);
			
			
			$new_date_end = 0;
							
							//$arenda_items
							
							//$arenda_type
							
			$arenda_type = $contract_model->arenda_type;			
							
			if ($arenda_type == 1) {
				
				$koeff  = 1;
				
			} elseif ($arenda_type == 2) {
				
				$koeff = 7; 
				
			} elseif ( $arenda_type == 3) {
				$koeff = 30; 
			} else {
				$koeff = 1; 
			}
			
			$dddays =  $contract_model->arenda_items * $koeff;
				
			if (empty($contract_model->date_pay_arenda)) {
			    $contract_model->date_pay_arenda = date('Y-m-d');
			}	
			
			$dddays = $dddays + $days_prrr;
				
			$new_date_end = date('Y-m-d',strtotime($contract_model->date_pay_arenda. ' + '.$dddays.' days'));

			
			//Дата окончания новая
			$contract_model->date_end = $new_date_end;
			
			if ($contract_model->status_itog == 8 or $contract_model->status_itog == 7) {
				$contract_model->date_end = '2099-12-31';
			}
			
			
			$contract_model->save(false);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
	
	
	
	
	
	
		//Пробуем создать метод для получения списка штрафов по договору аяксом
	public function actionProstoysbycontract ($id) {
			
			
			
		$countProstoys = Prostoys::find()->where(
		
				[
					'contract_id' => $id,
					'type_opl' => [1,3],
				]
			)->count();
		
		
		$prostoys = Prostoys::find()->where(
				[
					'contract_id' => $id,
					'type_opl' => [1,3],
				]
				
		)->orderBy ('contract_id ASC')->all();
		
		
		
		$model_contract = Contracts::find()->where(['id_contract' => $id])->all();
		
		$contract_obj = $model_contract[0];
		//print_r ($contract_obj);
		
		
		//exit;
		
		
		
		if ($countProstoys > 0) {
			
			$res = "<option value=''></option>";
			foreach ($prostoys as $item ) {
				
				
				//Сюда нужно закодить формульную логику к оплате с учетом
				// комиссионных галок и срока давности
				
				
				$summa = $item->summa_prostoy * $item->days_prostoy;
					
				$res.= '<option data-sum="'.$summa.'" value="'.$item->id_prostoy.'">
								
								Договор '.$item->contract_id.' '.$summa.' за '.$item->days_prostoy.' дней								
				
								</option>';
				
			}
			
			
		} else {
			$res.= "<option value=''></option>";			
		}
		return $res;
	}
	 
	
	
	
	
	
	
	
	
	
	
	
	public function actionMultipledelete()
	{
		$pk = Yii::$app->request->post('row_id');
		
		

		foreach ($pk as $key => $value) {
			$sql = "DELETE FROM prostoys WHERE id_prostoy = $value";
			$query = Yii::$app->db->createCommand($sql)->execute();
		}

		return $this->redirect(['index']);

	} 
	
	
	public function actionRemoveall()
	{
		//$pk = Yii::$app->request->post('row_id');
		
		

		//foreach ($pk as $key => $value) {
		$sql = "DELETE FROM prostoys;";
		$query = Yii::$app->db->createCommand($sql)->execute();
		//}

		return $this->redirect(['index']);

	} 
	
	
	
	
	public function actionProstoysupdate () {
		
		$prostoys = Prostoys::find()->all();
		
		
	
		
		if (!empty($prostoys)) {
			
			foreach ($prostoys as $item) {
				
				
				$summa = $item->summa_prostoy * days_prostoy;
				$id_prostoy = $item->id_prostoy;
				
				$payments = Payments::find()->where(['p_id_prostoy' => $id_prostoy])->all();
				
				$res_summa = 0;
				
				foreach ($payments as $pmnt) {
					
					$res_summa = $res_summa + $pmnt->prostoy_summa;
				
				}
				
				if ($res_summa == 0) {
					
					$item->type_opl = 1;
					$item->save();
					
				} elseif ($res_summa >= $summa) {
					$item->type_opl = 2;
					$item->save();
				} else {
					$item->type_opl = 3;
					$item->save();
				}
				
			}
			
		}
		
		
		return $this->redirect(['index']);
		
		
	}
	
	
	

    /**
     * Finds the Prostoys model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Prostoys the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Prostoys::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
