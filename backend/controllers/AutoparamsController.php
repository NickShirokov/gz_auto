<?php

namespace backend\controllers;

use Yii;
use app\models\Autoparams;
use backend\models\AutoparamsSearchModel;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\filters\AccessControl;
use common\models\AutomobilesNewModel;

/**
 * AutoparamsController implements the CRUD actions for Autoparams model.
 */
class AutoparamsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
		
		
		
		
        return [
		
			//Доступ только для админа 
			[
				   'class' => AccessControl::className(),
				   'only' => ['index', 
										'create', 
										'view', 
										'update', 
										'delete', 
										'multipledelete',
										'removeall'],
				   'rules' => [
						   [
								   'actions' => ['index', 
													'create', 
													'view', 
													'update', 
													'delete', 
													'multipledelete',
													'removeall'
													],
								   'allow' => true,
								   'roles' => ['admin'],
						   ],
						   [
								   'actions' => [
												'index', 
												'view',
												'create',
												],
								   'allow' => true,
								   'roles' => ['manager', 'payer'],
						   ],
						   [
								   'actions' => [
												'index', 
												'view',
												
												],
								   'allow' => true,
								   'roles' => ['user'],
						   ],
				   ],

		   ],
			
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 
										'index', 
										'create', 
										'view', 
										'update', 
										'delete', 
										'multipledelete',
										'removeall',
									],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
		
		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Autoparams models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AutoparamsSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		
		
		
		
		
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Autoparams model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Autoparams model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Autoparams();
		
		
		
	
		
		

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			
				//print_r (Yii::$app->request->post());
			
			
            return $this->redirect(['view', 'id' => $model->id_param]);
        }
		
		
		//Авто
		$model_auto = AutomobilesNewModel::find()->orderBy('mark, model ASC')->all();
		
		foreach ($model_auto as $value) {
			$arrAutomobiles[$value->id_auto] = $value->mark.' '.$value->model.' '.$value->VIN;
		}
		
		

        return $this->render('create', [
            'model' => $model,
			'arrAutomobiles' => $arrAutomobiles, 
        ]);
    }

    /**
     * Updates an existing Autoparams model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_param]);
        }
		
		
			//Авто
		$model_auto = AutomobilesNewModel::find()->orderBy('mark, model ASC')->all();
		
		foreach ($model_auto as $value) {
			$arrAutomobiles[$value->id_auto] = $value->mark.' '.$value->model.' '.$value->VIN;
		}
		
		

        return $this->render('update', [
            'model' => $model,
			'arrAutomobiles' => $arrAutomobiles,
        ]);
    }

    /**
     * Deletes an existing Autoparams model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

	
	
	
	public function actionMultipledelete()
	{
		$pk = Yii::$app->request->post('row_id');
		
		

		foreach ($pk as $key => $value) {
			$sql = "DELETE FROM autoparams WHERE id_param = $value";
			$query = Yii::$app->db->createCommand($sql)->execute();
		}

		return $this->redirect(['index']);

	} 
	
	
	
	
	public function actionRemoveall()
	{
		//$pk = Yii::$app->request->post('row_id');
		
		

		//foreach ($pk as $key => $value) {
		$sql = "DELETE FROM autoparams;";
		$query = Yii::$app->db->createCommand($sql)->execute();
		//}

		return $this->redirect(['index']);

	} 
	
	
	
	
	
	
	
    /**
     * Finds the Autoparams model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Autoparams the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
	 
    protected function findModel($id)
    {
        if (($model = Autoparams::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
