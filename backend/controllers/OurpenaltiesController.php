<?php

namespace backend\controllers;

use Yii;
use app\models\Ourpenalties;

use app\models\Contracts;

use app\models\Payments;

use app\models\Ourpenaltiesstatus;
use app\models\Logitems;



use backend\models\OurpenaltiesSearchModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\filters\AccessControl;

/**
 * OurpenaltiesController implements the CRUD actions for Ourpenalties model.
 */
class OurpenaltiesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
		
		[
				   'class' => AccessControl::className(),
				   'only' => ['index', 
										'create', 
										'view', 
										'update', 
										'delete',
										'shtrafsbycontract',
										'multipledelete',
										'ourpenaltiesupdate',
										'removeall',
										'newcreate',],
				   'rules' => [
						   [
								   'actions' => ['index', 
										'create', 
										'view', 
										'update', 
										'delete',
										'shtrafsbycontract',
										'multipledelete',
										'ourpenaltiesupdate',
										'removeall',
										'newcreate',],
								   'allow' => true,
								   'roles' => ['admin'],
						   ],
						   [
								   'actions' => [
												'index', 
												'view',
												],
								   'allow' => true,
								   'roles' => ['user', 'manager', 'payer'],
						   ],
				   ],

		   ],
			
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
										'logout', 
										'index', 
										'create', 
										'view', 
										'update', 
										'delete',
										'shtrafsbycontract',
										'multipledelete',
										'ourpenaltiesupdate',
										'removeall',
										'newcreate',
										
										
									],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
		
		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ourpenalties models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OurpenaltiesSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		$model_opentls = Ourpenaltiesstatus::find()->orderBy('status_o_name ASC')->all();
		
		foreach ($model_opentls as $value) {
			$arrOurpenstatus[$value->status_o_id] = $value->status_o_name;
		}
		
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrOurpenstatus' => $arrOurpenstatus,
        ]);
    }

    /**
     * Displays a single Ourpenalties model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ourpenalties model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($contract_id = '')
    {
		
		
        $model = new Ourpenalties();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			
			
			
			
			$modelLogitems = new Logitems ();
			
			$user_id = Yii::$app->user->identity->id;
			
			//Yii::$app->authManager->getRolesByUser($user_id);
			$type_user = current(Yii::$app->authManager->getRolesByUser( Yii::$app->user->identity->id))->name;
			$modelLogitems->type_user = $type_user;
			$modelLogitems->user_id = $user_id;
			$modelLogitems->type_page = 'ourpenalty';
			$modelLogitems->item_id = $model->id_penalty;
			$modelLogitems->date_create = date('Y-m-d');
						
			$modelLogitems->save();
			
			
			
			
            return $this->redirect(['view', 'id' => $model->id_penalty]);
        }

		
		$model_opentls = Ourpenaltiesstatus::find()->orderBy('status_o_name ASC')->all();
		
		foreach ($model_opentls as $value) {
			$arrOurpenstatus[$value->status_o_id] = $value->status_o_name;
		}
		

		
		
		$model_cntrcts = Contracts::find()
					->joinWith('drivers')
					->joinWith('automobiles')
					->orderBy('id_contract ASC')->all();
		
		foreach ($model_cntrcts as $value) {
			
			$drvr = $value->drivers[0];
			$auto = $value->automobiles[0];
			
			
			$arrContracts[$value->id_contract] = 'Договор № '.$value->id_contract
				. ' '.
			$drvr->last_name.' '. $drvr->first_name.' '.$drvr->middle_name
			.' '.$auto->mark.' '.$auto->model.' '.$auto->gosnomer;
			
			
		}

		
		
		
        return $this->render('create', [
            'model' => $model,
			'arrOurpenstatus' => $arrOurpenstatus,
			'arrContracts' => $arrContracts,
        ]);
    }
	
	
	public function actionNewcreate($contract_id = '')
    {
		
		
        $model = new Ourpenalties();
		
		$modelPayments = new Payments();

        if ($model->load(Yii::$app->request->post())) {
			
			
			$postBigDats = Yii::$app->request->post();
		
			
			$model->save();
					
	
			$id_penalty = $model->id_penalty;
			
			
			$modelPayments = new Payments();
			
			$modelPayments->load($postBigDats);
			
			
			$modelPayments->contract_id = $model->p_contract_id;
			
			
			$modelPayments->id_ourpen = $id_penalty;

			$modelPayments->save(false);
						
			
		
						
			$modelLogitems = new Logitems ();
			
			$user_id = Yii::$app->user->identity->id;
			
			//Yii::$app->authManager->getRolesByUser($user_id);
			$type_user = current(Yii::$app->authManager->getRolesByUser( Yii::$app->user->identity->id))->name;
			$modelLogitems->type_user = $type_user;
			$modelLogitems->user_id = $user_id;
			$modelLogitems->type_page = 'ourpenalty';
			$modelLogitems->item_id = $model->id_penalty;
			$modelLogitems->date_create = date('Y-m-d');
						
			$modelLogitems->save();
			
			
			
			
            return $this->redirect(['view', 'id' => $model->id_penalty]);
        }

		
		$model_opentls = Ourpenaltiesstatus::find()->orderBy('status_o_name ASC')->all();
		
		foreach ($model_opentls as $value) {
			$arrOurpenstatus[$value->status_o_id] = $value->status_o_name;
		}
		

		
		
		$model_cntrcts = Contracts::find()
					->joinWith('drivers')
					->joinWith('automobiles')
					->orderBy('id_contract ASC')->all();
		
		foreach ($model_cntrcts as $value) {
			
			$drvr = $value->drivers[0];
			$auto = $value->automobiles[0];
			
			
			$arrContracts[$value->id_contract] = 'Договор № '.$value->id_contract
				. ' '.
			$drvr->last_name.' '. $drvr->first_name.' '.$drvr->middle_name
			.' '.$auto->mark.' '.$auto->model.' '.$auto->gosnomer;
			
			
		}

		
		
		
        return $this->render('newcreate', [
            'model' => $model,
			'modelPayments' => $modelPayments,
			'arrOurpenstatus' => $arrOurpenstatus,
			'arrContracts' => $arrContracts,
        ]);
    }
	
	

    /**
     * Updates an existing Ourpenalties model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		//print_r ($model);
		
		

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_penalty]);
        }
		
		$model_opentls = Ourpenaltiesstatus::find()->orderBy('status_o_name ASC')->all();
		
		foreach ($model_opentls as $value) {
			$arrOurpenstatus[$value->status_o_id] = $value->status_o_name;
		}
		
		
		$model_cntrcts = Contracts::find()
				->joinWith('drivers')
				->joinWith('automobiles')
				->orderBy('id_contract ASC')->all();
		
		foreach ($model_cntrcts as $value) {
			
			$drvr = $value->drivers[0];
			$auto = $value->automobiles[0];
			
			
			$arrContracts[$value->id_contract] = 'Договор № '.$value->id_contract
				. ' '.
			$drvr->last_name.' '.$drvr->first_name.' '.$drvr->middle_name
			.' '.$auto->mark.' '.$auto->model.' '.$auto->gosnomer;
			
			
		}
		
		
		

        return $this->render('update', [
            'model' => $model,
			'arrOurpenstatus' => $arrOurpenstatus,
			'arrContracts' => $arrContracts,
        ]);
    }

    /**
     * Deletes an existing Ourpenalties model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
	
	
	//Пробуем создать метод для получения списка штрафов по договору аяксом
	public function actionShtrafsbycontract ($id) {
		
		
		$countShtrafs = Ourpenalties::find()->where(
		
				[
					'p_contract_id' => $id,
					'status_o_pen' => [1,3,4],
				]
			)->count();
		
		
		$shtrafs = Ourpenalties::find()->where(
				[
					'p_contract_id' => $id,
					'status_o_pen' => [1, 3, 4],
				]
				
		)->orderBy ('p_contract_id ASC')->all();
		
		
		
		
		if ($countShtrafs > 0) {
			
			$res = "";
			foreach ($shtrafs as $item ) {
				
				//Пробуем подставить данные так, чтобы была возможность посчитать
				//Частичные взносы
				
				$status_op = Ourpenaltiesstatus::findOne($item->status_o_pen);
				
				
				$ostatok = 0;
				
				$summs = 0;
				
				
				$pmnts_by_shtraf = Payments::find()->where(['id_ourpen' => $item->id_penalty])->all();
				
				if (!empty($pmnts_by_shtraf)) {
					
					foreach ($pmnts_by_shtraf as $pmnt) {
						
						$summs = $summs + $pmnt->pay_ourpen;
						
						
					}
					
					
					
				}
				
				
				$ostatok = $item->pen_summa - $summs;
				
				
				$res.= '<option value="'.$item->id_penalty.'">'.
				
				'№'.$item->id_penalty
				.' '.$item->pen_summa.' рублей'
				.' '.$item->date_pen
				.' '.$item->pen_comment
				.' Статус '.$status_op->status_o_name
				.' Осталось доплатить '.$ostatok.' рублей
				
				
				</option>';
				
			}
			
			
		} else {
			$res.= "<option value=''></option>";
			
		}
		return $res;
	}
	
	
	
	public function actionMultipledelete()
	{
		$pk = Yii::$app->request->post('row_id');
		
		

		foreach ($pk as $key => $value) {
			$sql = "DELETE FROM our_penalties WHERE id_penalty = $value";
			$query = Yii::$app->db->createCommand($sql)->execute();
		}

		return $this->redirect(['index']);

	} 
	
		//Пробуем создать обновлялку для договоров
	public function actionOurpenaltiesupdate () {
		
		
		
		$pentls = Ourpenalties::find()->all();
		
		//Нужно проверить, есть ли у нас платежи, выставить правильные статусы
		//Если вдруг что-то не так проставилось
	
		
		if (!empty($pentls)) {
			
			foreach ($pentls as $item) {
				
				
				$summa = $item->pen_summa;
				$id_penalty = $item->id_penalty;
				
				
				$payments = Payments::find()->where(['id_ourpen' => $id_penalty])->all();
				
				$res_summa = 0;
				
				foreach ($payments as $pmnt) {
					
					$res_summa = $res_summa + $pmnt->pay_ourpen;
				}
				
				if ($res_summa == 0) {
					
					$item->status_o_pen = 1;
					$item->save();
					
				} elseif ($res_summa >= $summa) {
					$item->status_o_pen = 2;
					$item->save();
				} else {
					$item->status_o_pen = 4;
					$item->save();
				}
				
				
				
			}
			
		}
		
		
		return $this->redirect(['index']);
	}
	
	
	
	public function actionRemoveall()
	{
		//$pk = Yii::$app->request->post('row_id');
		
		

		//foreach ($pk as $key => $value) {
		$sql = "DELETE FROM our_penalties;";
		$query = Yii::$app->db->createCommand($sql)->execute();
		//}

		return $this->redirect(['index']);

	} 
	
	
	

    /**
     * Finds the Ourpenalties model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ourpenalties the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ourpenalties::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
