<?php

namespace backend\controllers;

use Yii;
use app\models\Driverscomments;
use common\models\DriversModel;
use backend\models\DriverscommentsSearchModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * DriverscommentsController implements the CRUD actions for Driverscomments model.
 */
class DriverscommentsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
		
			[
				   'class' => AccessControl::className(),
				   'only' => ['index','create', 'view', 'update', 'delete'],
				   'rules' => [
						   [
								   'actions' => ['index','create', 'view','update', 'delete'],
								   'allow' => true,
								   'roles' => ['admin'],
						   ],
						   [
								   'actions' => [
												'index', 
												'view',
												'create',
												],
								   'allow' => true,
								   'roles' => ['manager', 'payer'],
						   ],
						   
						   [
								   'actions' => [
												'index', 
												'view',
												
												],
								   'allow' => true,
								   'roles' => ['user'],
						   ],
				   ],

		   ],
			
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'create', 'view', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
		
		
		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Driverscomments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DriverscommentsSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		
		//Категории
		$model_drivers = DriversModel::find()->orderBy('first_name ASC')->all();
		
		foreach ($model_drivers as $value) {
			$arrDrivers[$value->id_driver] = $value->first_name.' '.$value->middle_name.' '.$value->last_name;
		}
			

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrDrivers' => $arrDrivers,
        ]);
    }

    /**
     * Displays a single Driverscomments model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Driverscomments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Driverscomments();

		
		//Категории
		$model_drivers = DriversModel::find()->orderBy('first_name ASC')->all();
		
		foreach ($model_drivers as $value) {
			$arrDrivers[$value->id_driver] = $value->first_name.' '.$value->middle_name.' '.$value->last_name;
		}
		
		
		
		/*
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			
			$dr_id = Yii::$app->request->post()['Driverscomments']['driver_id'];
			
            return $this->redirect(['drivers/view', 'id' => $dr_id]);
        }
		*/
		
		
		
		if ($model->load(Yii::$app->request->post())) {
            		
					
			$t_m = 	$model;	
					
					
			$t_m->file_f = UploadedFile::getInstance($t_m,'file_f');
			
			$fileName = $t_m->file_f->name;
			
			$post = Yii::$app->request->post();
			
			$post['Driversfiles']['file_f'] = $t_m->file_f->name;
			
			
			if ($t_m->file_f->baseName) {
			
				$model->file_f->saveAs('uploads/'.$t_m->file_f->baseName.'.'.$t_m->file_f->extension);
				$model->comment_file = $t_m->file_f->name;
			}
			
			$model->save(false);
			
			return $this->redirect(['drivers/view', 'id' => $model->driver_id]);
		}
		

        return $this->render('create', [
            'model' => $model,
			'arrDrivers' => $arrDrivers,
        ]);
    }

    /**
     * Updates an existing Driverscomments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		
		//Категории
		$model_drivers = DriversModel::find()->orderBy('first_name ASC')->all();
		
		foreach ($model_drivers as $value) {
			$arrDrivers[$value->id_driver] = $value->first_name.' '.$value->middle_name.' '.$value->last_name;
		}
		
		
		if ($model->load(Yii::$app->request->post())) {
            		
					
			$t_m = 	$model;	
					
					
			$t_m->file_f = UploadedFile::getInstance($t_m,'file_f');
			
			$fileName = $t_m->file_f->name;
			
			$post = Yii::$app->request->post();
			
			$post['Driversfiles']['file_f'] = $t_m->file_f->name;
			
			$model->file_f->saveAs('uploads/'.$t_m->file_f->baseName.'.'.$t_m->file_f->extension);
			
			
			$model->comment_file = $t_m->file_f->name;
			
			
			$model->save(false);
			
			return $this->redirect(['drivers/view', 'id' => $model->driver_id]);
		}

        return $this->render('update', [
            'model' => $model,
			'arrDrivers' => $arrDrivers,
        ]);
    }

    /**
     * Deletes an existing Driverscomments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

	
	
	
	public function actionMultipledelete()
	{
		$pk = Yii::$app->request->post('row_id');
		
		

		foreach ($pk as $key => $value) {
			$sql = "DELETE FROM drivers_comments WHERE id_comment = $value";
			$query = Yii::$app->db->createCommand($sql)->execute();
		}

		return $this->redirect(['index']);

	} 
	
	
	
	
	
	
	
	
    /**
     * Finds the Driverscomments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Driverscomments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Driverscomments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
