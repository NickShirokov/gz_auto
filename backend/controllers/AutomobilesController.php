<?php

namespace backend\controllers;

use Yii;
use common\models\AutomobilesNewModel;
use backend\models\AutomobilesNewSearchModel;

use common\models\Marks;
use common\models\Models;
use app\models\Owners;

use common\models\Insurancecompany;
use common\models\Autostatus;

use app\models\Automobilefiles;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\AutoparamsSearchModel;


use app\models\Autoparams;
use app\models\Licen;
use app\models\Razresheniya;
use app\models\Contracts;
use yii\filters\AccessControl;

use DateTime;
use app\models\Logitems;

use yii\web\UploadedFile;

/**
 * AutomobilesController implements the CRUD actions for AutomobilesNewModel model.
 */
class AutomobilesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
		
			[
				   'class' => AccessControl::className(),
				   'only' => ['index', 
										'create', 
										'view', 
										'update', 
										'delete',
										'removeall',
										'import',
										'multipledelete',
										'lists',
										'razresh',
										'licens'],
				   'rules' => [
						   [
								   'actions' => ['index', 
										'create', 
										'view', 
										'update', 
										'delete',
										'removeall',
										'import',
										'multipledelete',
										'lists',
										'razresh',
										'licens'],
								   'allow' => true,
								   'roles' => ['admin'],
						   ],
						   [
								   'actions' => [
												'index', 
												'view',
												'create',
												'update',
												'delete',
												'lists',
												],
								   'allow' => true,
								   'roles' => ['manager', 'payer'],
						   ],
						   [
								   'actions' => [
												'index', 
												'view',
												
												],
								   'allow' => true,
								   'roles' => ['user'],
						   ],
						   
						   
				   ],

		   ],
			
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
										'logout', 
										'index', 
										'create', 
										'view', 
										'update', 
										'delete',
										'removeall',
										'import',
										'multipledelete',
										'lists',
										'razresh',
										'licens'
										
									
									
									],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
		
		
		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AutomobilesNewModel models.
     * @return mixed
     */
    public function actionIndex()
    {
	
		
	
        $searchModel = new AutomobilesNewSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		
		 //$dataProvider->pagination->pageSize=100;
		 //$dataProvider->pagination->pageSize=10;
		
		//Марки
		$model_marks = Marks::find()->orderBy('name_mark ASC')->all();
		
		
		foreach ($model_marks as $value) {
			$arrMarks[$value->name_mark] = $value->name_mark;
		}
		
		
		//Модели
		$model_models = Models::find()->orderBy('name ASC')->all();
		
		
		foreach ($model_models as $value) {
			$arrModels[$value->name] = $value->name;
		}
		
		//Статусы
		$model_ststs = Autostatus::find()->orderBy('status_name ASC')->all();
		
		
		foreach ($model_ststs as $value) {
			$arrStsts[$value->id_status] = $value->status_name;
		}
		
		
		
		
		
		
			
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrMarks' => $arrMarks,
			'arrModels' => $arrModels,
			'arrStsts' => $arrStsts,
        ]);
    }

    /**
     * Displays a single AutomobilesNewModel model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
		
		$model_status = Autostatus::find()->where(['id_status'=> $this->findModel($id)->status])->all();
		
		
		//Подгружаем во вьюху картинки
		$files = Automobilefiles::find()->where(['id_auto' => $id])->orderBy ('id_auto ASC')->all();
		
		foreach ($files as $item) {
			$arrFiles[$item->id_file] = $item['file'];  	
		}
		
		
		
		
		
		//Делаем блок с Grid для пар СТС-Госномер
		$request = Yii::$app->request->queryParams;
		
		
		
		
		
		$request['AutoparamsSearchModel']['auto_id'] = $id;
		
		
		$searchModel = new AutoparamsSearchModel();
        $dataProvider = $searchModel->search($request);
		
		
		
		
			
		$modelAutoparams = new Autoparams();  	
		
		
		if ($modelAutoparams->load(Yii::$app->request->post())) {
            		
			
			
			$modelAutoparams->save(false);
			
			return $this->redirect(['automobiles/view', 'id' => $modelAutoparams->auto_id]);
		}
		
		
		
		$model = $this->findModel($id);
		
		$arrAutomobiles[$model->id_auto] = $model->mark.' '.$model->model.' '.$model->VIN;
			
		$model_contracts = Contracts::find()->where(['auto_id' => $id] )->all();	
			
			
        return $this->render('view', [
            'model' => $model,
			'arrFiles' => $arrFiles,
			'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrAutomobiles' => $arrAutomobiles,
			'modelAutoparams' => $modelAutoparams,
			'status' => $model_status[0]->status_name,
			'model_contracts' => $model_contracts,
        ]);
    }

    /**
     * Creates a new AutomobilesNewModel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AutomobilesNewModel();

		$model_marks = Marks::find()->orderBy('name_mark ASC')->all();
		
		
		foreach ($model_marks as $value) {
			$arrMarks[$value->name_mark] = $value->name_mark;
		}
		
		
		
		//Модели
		$model_models = Models::find()->orderBy('name ASC')->all();
		
		
		foreach ($model_models as $value) {
			$arrModels[$value->name] = $value->name;
		}
		
		//Страховые
		$model_insurance = Insurancecompany::find()->orderBy('name ASC')->all();
		
		
		foreach ($model_insurance as $value) {
			$arrIns[$value->id_company] = $value->name;
		}
			
		
		//Статусы
		$model_status = Autostatus::find()->orderBy('status_name ASC')->all();
					
		foreach ($model_status as $value) {
			$arrStatus[$value->id_status] = $value->status_name;
		}
		
		
		// Владельцы
		$arrOwners = [];
		
		
		$model_owners = Owners::find()->orderBy('id_owner ASC')->all();
					
		foreach ($model_owners as $value) {
			$arrOwners[$value->id_owner] = $value->first_name.' '.$value->middle_name.' '.$value->last_name;
		}
		
		
		$arrLicen = [];
		
		$model_licens = Licen::find()->all();
		
		
		foreach ($model_licens as $value) {
			
			
			
			$arrLicen[$value->id_lic] = $value->name_lic;
			
		}
		
		
		
		$arrRazresh = [];
		
		$model_razresh = Razresheniya::find()->all();
		
		
		
		foreach ($model_razresh as $value) {
			
			
			
			
			$arrRazresh[$value->id_razresh] = 'Лицензия №'.$value->licen_id.' '.$value->r_n.' Серия '.$value->series.' № ' .$value->number; 
			
		}
		
		
		
	
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			
			
			
			
			$modelAp = new Autoparams();
			
			$ap_param['Autoparams']['param_sts'] = $model->sts;
			$ap_param['Autoparams']['param_gosnomer'] = $model->gosnomer;
			$ap_param['Autoparams']['auto_id'] = $model->id_auto;
			$ap_param['Autoparams']['date_param'] = date('Y-m-d');
			
			$modelAp->load($ap_param);
			$modelAp->save();
						
						
						
			$modelLogitems = new Logitems ();
			
			$user_id = Yii::$app->user->identity->id;
			
			//Yii::$app->authManager->getRolesByUser($user_id);
			$type_user = current(Yii::$app->authManager->getRolesByUser( Yii::$app->user->identity->id))->name;
			$modelLogitems->type_user = $type_user;
			$modelLogitems->user_id = $user_id;
			$modelLogitems->type_page = 'auto';
			$modelLogitems->item_id = $model->id_auto;
			$modelLogitems->date_create = date('Y-m-d');
						
			$modelLogitems->save();
			
            return $this->redirect(['view', 'id' => $model->id_auto]);
        } else {
			 return $this->render('create', [
				'model' => $model,
				'arrMarks' => $arrMarks,
				'arrIns' => $arrIns,
				'arrModels' => [],
				'arrStatus' => $arrStatus,	
				'arrOwners' => $arrOwners,
				'arrLicen' => $arrLicen,
				'arrRazresh' => $arrRazresh,
			]);
			
		}
		
		

       
    }

    /**
     * Updates an existing AutomobilesNewModel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

      
		$model_marks = Marks::find()->orderBy('name_mark ASC')->all();
		
		
		foreach ($model_marks as $value) {
			$arrMarks[$value->name_mark] = $value->name_mark;
		}
		
		//Модели
		$model_models = Models::find()->where(['mark' => $model->mark])->orderBy('name ASC')->all();
		
		
		foreach ($model_models as $value) {
			$arrModels[$value->name] = $value->name;
		}
		
		
		//Страховые
		$model_insurance = Insurancecompany::find()->orderBy('name ASC')->all();
		
		
		foreach ($model_insurance as $value) {
			$arrIns[$value->id_company] = $value->name;
		}
		
		
		//Статусы
		$model_status = Autostatus::find()->orderBy('status_name ASC')->all();
	
		foreach ($model_status as $value) {
			$arrStatus[$value->id_status] = $value->status_name;
		}
		
		// Владельцы
		$arrOwners = [];
		
		$model_owners = Owners::find()->orderBy('id_owner ASC')->all();
					
		foreach ($model_owners as $value) {
			$arrOwners[$value->id_owner] = $value->first_name.' '.$value->middle_name.' '.$value->last_name;
		}
		
		
		$old_model = $model;
		
		//echo '<pre>';
			//print_r ($old_model);
			//print_r ($post);
			
			
			//echo '</pre>';
			//exit;
		
		$old_sts = $old_model->sts;
		$old_gosnomer = $old_model->gosnomer;
		
		
		
		
		$arrLicen = [];
		
		$model_licens = Licen::find()->all();
		
		
		foreach ($model_licens as $value) {
			
			$arrLicen[$value->id_lic] = 'Лицензия №'.$value->id_lic.' '.$value->name_lic;
			
		}
		
		
		
		$arrRazresh = [];
		
		$model_razresh = Razresheniya::find()->all();
		
		
		
		foreach ($model_razresh as $value) {
			
			$arrRazresh[$value->id_razresh] = 'Лицензия №'.$value->licen_id.' '.$value->r_n.' Серия '.$value->series.' № ' .$value->number; 
			
		}
		
		
		
		
		
			
	    if ($model->load(Yii::$app->request->post()) ) {
			
			
			$post = Yii::$app->request->post();
			
			
			/*
			echo '<pre>';
			print_r ($old_sts);
			print_r ($old_gosnomer);
			print_r ($post);
			
			
			echo '</pre>';
			exit;
			*/
			
			/*
			$modelAp = new Autoparams();
			
			$ap_param['Autoparams']['param_sts'] = $old_sts;
			$ap_param['Autoparams']['param_gosnomer'] = $old_gosnomer;
			$ap_param['Autoparams']['auto_id'] = $id;
			$ap_param['Autoparams']['date_param'] = date('Y-m-d');
			
			$modelAp->load($ap_param);
			$modelAp->save();
			*/
			
			$model->save();
			
			
			//НАдо избавиться от дублей
			
			
			$prom_res = Autoparams::find()->where(['param_sts' => $model->sts, 'param_gosnomer' => $model->gosnomer])->all();
			
			
			if (empty($prom_res)) {
			
				$ap_param['Autoparams']['param_sts'] = $model->sts;
				$ap_param['Autoparams']['param_gosnomer'] = $model->gosnomer;
				$ap_param['Autoparams']['auto_id'] = $model->id_auto;
				$ap_param['Autoparams']['date_param'] = date('Y-m-d');
				
				$modelAp->load($ap_param);
				$modelAp->save();
			
			}
			
			
			
			
            return $this->redirect(['view', 'id' => $model->id_auto]);
        
		
		} else {
			
			 return $this->render('update', [
				'model' => $model,
				'arrMarks' => $arrMarks,
				'arrModels' => $arrModels,
				'arrIns' => $arrIns,
				'arrStatus' => $arrStatus,
				'arrOwners' => $arrOwners,
				'arrLicen' => $arrLicen,
				'arrRazresh' => $arrRazresh,
			]);
		}
		

    }
	
	
	
	//Грузим csv файлик
	
	
	public function actionImport()
    {
		
        $model = new AutomobilesNewModel();
		
        if ($model->load(Yii::$app->request->post())) {
            
			
			$t_m = 	$model;	
					
					
			$t_m->file_f = UploadedFile::getInstance($t_m,'file_f');
			
			$fileName = $t_m->file_f->name;
			
			
			//$post = Yii::$app->request->post();
			
			//$post['Driversfiles']['file_f'] = $t_m->file_f->name;
			
			$model->file_f->saveAs('uploads/'.$t_m->file_f->baseName.'.'.$t_m->file_f->extension);
				
			
			$file_path = 'uploads/'.$t_m->file_f->baseName.'.'.$t_m->file_f->extension;
			
			$filename = $file_path;

			// The nested array to hold all the arrays
			$the_big_array = []; 

			// Open the file for reading
			if (($h = fopen("{$filename}", "r")) !== FALSE) 
			{
			  // Each line in the file is converted into an individual array that we call $data
			  // The items of the array are comma separated
			  while (($data = fgetcsv($h, 1000, ";")) !== FALSE) 
			  {
				// Each individual array is being pushed into the nested array
				$the_big_array[] = $data;		
			  }

			  // Close the file
			  fclose($h);
			}

			
			unset ($the_big_array[0]);
				
			/*
			
			1. Первым делом нужно пройтись по марке-модели, проверить есть ли они в наличии, 
			если нет, то добавить - не забыть преобразовать формат в БОЛЬШИЕ БУКВЫ
			2. Коробка передач - заменить Механика-Автомат на Ручная-Автомат
			3. Проверить наличие страховой в справочнике, если отсутствует, то добавить
			4. Ограничения - Без ограничений - заменить на  0 - 1
			5. Лицензии - проверить наличие, если нет, то добавить в справочник
			6. Статусы поставить в соответствие как в базе админки
			
			Арендует => 10
			Выкупает => 4
			Свободна => 1
			Любой другой => Непонятный статус индекс 7,

			потом Ануар сам проставит
			вручную во избежание косяков от опечаток

			7. Привести данные авто VIN, Госномер в БОЛЬШИЕ буквы - БЕЗ ПРОБЕЛОВ
			8. Преобразовать даты в удобоваримый формат 
			9. Вставка по уникальному полю VIN, если совпадает, то обновить данные 
			
			*/
			
			
			$status_array = [
								'Арендует' => 10, 
								'Выкупает' => 4, 
								'Свободна' => 1, 
								'Любой другой' => 7, 					
							];
			
			
			$kpp_array = [
							'Механика' => 'Ручная',
							'Автомат' => 'Автомат',
			
						];
						
			$ogran_arr = [
							'Без ограничений' => 0,
							'Ограниченная' => 1,
			
							];			
						
						
			
			foreach ($the_big_array as $item) {
			
			
				$new_mark = mb_strtoupper($item[1]);
				$new_model = mb_strtoupper($item[2]);
				
				$gosnomer = mb_strtoupper($item[3]); 
				
				$VIN = mb_strtoupper($item[5]);
				
				$new_kpp = $item[6];
				
				
				$marks_model = Marks::find()->where(['name_mark' => $new_mark])->all();
				
				//Пункт 1 
				
				if (empty($marks_model)) {
					
					$mrk_model = new Marks();
					$mrk_model->name_mark = $new_mark;
					
					$mrk_model->save(false);
					
				}
				
				
				
				$mod_models = Models::find()->where(['name' => $new_model])->all();
				
				//Пункт 1 
				
				if (empty($mod_models)) {
					
					$mod_model = new Models();
					$mod_model->mark = $new_mark;
					$mod_model->name = $new_model;
					
					$mod_model->save(false);
					
				}
				
				//Пункт 2
				
				$kpp = $kpp_array[$new_kpp];
			
				//Пункт 3
				
				$new_ins = $item[8];
				
				$ins_models = Insurancecompany::find()->where(['name' => $new_ins])->all();
				
				
				$ins_model = new Insurancecompany();
				
				
				if (!empty($new_ins)) {
				
					if (empty($ins_models)) {
							
						$ins_model->name = $new_ins;
						
						$ins_model->save(false);
						
						$ins_id = $ins_model->id_company;
					} else {
						$ins_id = $ins_models[0]->id_company;
					}
					
				} else {	
					$ins_id = '';
				}

				//Пункт 4
					
				$new_ogr = $item[10];
				
				$ogranich = $ogran_arr[$new_ogr];
				
				//Пункт 5
				
				$new_lic = $item[13];
				
				$lic_models = Licen::find()->where(['name_lic' => $new_lic])->all();
				
				
				$lic_model = new Licen;
				
				if (!empty($new_lic)) {
				
					if (empty($lic_models)) {
							
						$lic_model->name_lic = $new_lic;
						
						$lic_model->save(false);
						
						$lic_id = $lic_model->id_lic;
					} else {
						$lic_id = $lic_models[0]->id_lic;
					}
				
				} else {
					$lic_id = '';
				}
				
				//Пункт 6
				
				$new_status = $item[0];
				
				$auto_status = $status_array[$new_status];
				
				//Пункт 7 - сделал в начале, даты сразу в коне
				
				
				//Нужно добавить если есть владелец в последней колонке
				
				$new_owner = $item[15];
				
				$new_owner_arr = explode(' ', $new_owner);
		
				
				$owners_models = Owners::find()->where(['last_name' => $new_owner_arr[0]])->all();
				
				$id_owner = 0;
				
				if (!empty($owners_models)) {
					
					$id_owner = $owners_models[0]->id_owner;
				}
				
				
				//Пункn 8 
				
				$auto_models = AutomobilesNewModel::find()->where(['VIN' => $VIN])->all();
				
				
				
				
			
				
				$model = new AutomobilesNewModel();
			
				$model->owner_id = $id_owner;
				$model->mark = $new_mark;
				$model->model = $new_model;
				$model->VIN = $VIN;
				$model->sts = $item[12];
				$model->gosnomer = $gosnomer;
				$model->year = $item[7];
				$model->transmission = $kpp;
				$model->status = $auto_status;
				$model->osago = date('Y-m-d',strtotime($item[9]));
				$model->ogranichenia = $ogranich;
				$model->ins_company = $ins_id;
				$model->diagnostic_card = date('Y-m-d',strtotime($item[11]));
				$model->licen_id = $lic_id;
				$model->razresh_text = $item[14];
				$model->comment = $item[16];
				
				if (empty($auto_models[0])) {
				
				
					
				
					$model->save(false);
					
					$modelAp = new Autoparams();
			
					$ap_param['Autoparams']['param_sts'] = $model->sts;
					$ap_param['Autoparams']['param_gosnomer'] = $model->gosnomer;
					$ap_param['Autoparams']['auto_id'] = $model->id_auto;
					$ap_param['Autoparams']['date_param'] = date('Y-m-d');
					
					
					$models_params = Autoparams::find()->where(['param_sts' => $item[12], 'param_gosnomer' => $gosnomer])->all();
					
					
					//if (empty($models_params)) {
					
					$modelAp->load($ap_param);
					$modelAp->save();
					
					
						
				} else {
					
					
					
					$model = $this->findModel($auto_models[0]->id_auto);
					$model->owner_id = $id_owner;
					$model->mark = $new_mark;
					$model->model = $new_model;
					$model->VIN = $VIN;
					$model->sts = $item[12];
					$model->gosnomer = $gosnomer;
					$model->year = $item[7];
					$model->transmission = $kpp;
					$model->status = $auto_status;
					$model->osago = date('Y-m-d',strtotime($item[9]));
					$model->ogranichenia = $ogranich;
					$model->ins_company = $ins_id;
					$model->diagnostic_card = date('Y-m-d',strtotime($item[11]));
					$model->licen_id = $lic_id;
					$model->razresh_text = $item[14];
					$model->comment = $item[16];
					
					
					$model->save(false);
					
					
					
					$modelAp = new Autoparams();
			
					$ap_param['Autoparams']['param_sts'] = $model->sts;
					$ap_param['Autoparams']['param_gosnomer'] = $model->gosnomer;
					$ap_param['Autoparams']['auto_id'] = $auto_models[0]->id_auto;
					$ap_param['Autoparams']['date_param'] = date('Y-m-d');
					
					
					$models_params = Autoparams::find()->where(['param_sts' => $item[12], 'param_gosnomer' => $gosnomer])->all();
					
					
					if (empty($models_params)) {
					
					    $modelAp->load($ap_param);
					    $modelAp->save();
					
					}
					
					
					
				}
				
				
				
			}
			
			
			
		
			
			return $this->redirect(['index']);
        }
		
		
	
		

        return $this->render('import', [
            'model' => $model,
			
        ]);
    }
	
	
	
	
	
	
	 
	public function actionMultipledelete()
	{
		$pk = Yii::$app->request->post('row_id');
		



		foreach ($pk as $key => $value) {
			$sql = "DELETE FROM automobiles WHERE id_auto = $value";
			$query = Yii::$app->db->createCommand($sql)->execute();
		}

		return $this->redirect(['index']);

	} 
	
	
	
	
	
	
	
	
	
	

    /**
     * Deletes an existing AutomobilesNewModel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AutomobilesNewModel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AutomobilesNewModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
	 
	 
	public  function actionLists ($mark) {
		
		$countModels = Models::find()->where(['mark' => $mark])->count();
		
		
		$models = Models::find()->where(['mark' => $mark])->orderBy ('name ASC')->all();
			
		if ($countModels > 0) {
			
			echo "<option>Выбрать</option>";
			foreach ($models as $item ) {
				
				
				echo '<option value="'.$item->name.'">'.$item->name.'</option>';
				
			}
			
			
		} else {
			echo "<option> - </option>";
			
		}
		
		
		
	} 
	
	
	
	
	//Лицензии по владельцу
	
	public function actionLicens ($id) {
		
		
		$model_licens = Licen::find()->all();
		
		
		
		echo "<option>Выбрать</option>";
		
		foreach ($model_licens as $item ) {
			
			if ($item->owner_id == 0 or $item->owner_id == $id) {
			
				echo '<option value="'.$item->id_lic.'">Лицензия №'.$item->id_lic.' '.$item->name_lic.'</option>';
			}
		}
		
		
	}
	
	
	//Разрешение по лицензии 
	
	public function actionRazresh ($id) {
		
		
		$model_licens = Razresheniya::find()->where(['licen_id' => $id])->all();
		
		
		
		echo "<option>Выбрать</option>";
		
		foreach ($model_licens as $item ) {
			
			
			echo '<option value="'.$item->id_razresh.'">Лицензия №'.$item->licen_id.' '
												.$item->r_n.' Серия '.$item->series.' Номер'.$item->number.'</option>';
			
		}
		
		
	}
	
	
	
	public function actionRemoveall()
	{
		//$pk = Yii::$app->request->post('row_id');
		
		

		//foreach ($pk as $key => $value) {
		$sql = "DELETE FROM automobiles;";
		$query = Yii::$app->db->createCommand($sql)->execute();
		
		
		$sql = "DELETE FROM autoparams;";
		$query = Yii::$app->db->createCommand($sql)->execute();
		
		//}

		return $this->redirect(['index']);

	} 
	
	
	
	
	
	 
    protected function findModel($id)
    {
        if (($model = AutomobilesNewModel::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
