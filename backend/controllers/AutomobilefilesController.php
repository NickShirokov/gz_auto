<?php

namespace backend\controllers;

use Yii;
use app\models\Automobilefiles;
use common\models\AutomobilefilesModelSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\filters\AccessControl;

use common\models\AutomobilesNewModel;

use yii\web\UploadedFile;

/**
 * AutomobilefilesController implements the CRUD actions for Automobilefiles model.
 */
class AutomobilefilesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
		
		
		
		
        return [
		
			[
				   'class' => AccessControl::className(),
				   'only' => ['index','create', 'view', 'update', 'delete', 'multipledelete'],
				   'rules' => [
						   [
								   'actions' => ['index','create', 'view', 'update', 'delete', 'multipledelete'],
								   'allow' => true,
								   'roles' => ['admin'],
						   ],
						   [
								   'actions' => [
												'index', 
												'view',
												'create',
												],
								   'allow' => true,
								   'roles' => ['manager', 'payer'],
						   ],
						   
						   [
								   'actions' => [
												'index', 
												'view',
												
												],
								   'allow' => true,
								   'roles' => ['user'],
						   ],
				   ],

		   ],
			
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'create', 'view', 'update', 'delete', 'multipledelete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
		
		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Automobilefiles models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AutomobilefilesModelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		
		
		//Авто
		$model_auto = AutomobilesNewModel::find()->orderBy('mark ASC')->all();
		
		foreach ($model_auto as $value) {
			$arrAutomobiles[$value->id_auto] = $value->mark.' '.$value->model.' '.$value->gosnomer;
		}
		
		
		

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrAutomobiles' => $arrAutomobiles,
        ]);
    }

    /**
     * Displays a single Automobilefiles model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
		
		
        return $this->render('view', [
            'model' => $this->findModel($id),
			
        ]);
    }

    /**
     * Creates a new Automobilefiles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		
		//Авто
		$model_auto = AutomobilesNewModel::find()->orderBy('mark ASC')->all();
		
		foreach ($model_auto as $value) {
			$arrAutomobiles[$value->id_auto] = $value->mark.' '.$value->model.' '.$value->gosnomer;
		}
		
		
        $model = new Automobilefiles();
		
		
		
		if ($model->load(Yii::$app->request->post())) {
            		
					
			$t_m = 	$model;	
					
					
			$t_m->file_f = UploadedFile::getInstance($t_m,'file_f');
			
			$fileName = $t_m->file_f->name;
			
			$post = Yii::$app->request->post();
			
			$post['Driversfiles']['file_f'] = $t_m->file_f->name;
			
			$model->file_f->saveAs('uploads/'.$t_m->file_f->baseName.'.'.$t_m->file_f->extension);
			
			
			$model->file = $t_m->file_f->name;
			
			
			$model->save(false);
			
			return $this->redirect(['automobiles/view', 'id' => $model->id_auto]);
		}
		
		
		/*
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_file]);
        }
		*/
		
        return $this->render('create', [
            'model' => $model,
			'arrAutomobiles' => $arrAutomobiles,
        ]);
    }

    /**
     * Updates an existing Automobilefiles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		
		//Авто
		$model_auto = AutomobilesNewModel::find()->orderBy('mark ASC')->all();
		
		foreach ($model_auto as $value) {
			$arrAutomobiles[$value->id_auto] = $value->mark.' '.$value->model.' '.$value->gosnomer;
		}
		
		
		

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_file]);
        }

        return $this->render('update', [
            'model' => $model,
			'arrAutomobiles' => $arrAutomobiles,
        ]);
    }

    /**
     * Deletes an existing Automobilefiles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

	
	
	public function actionMultipledelete()
	{
		$pk = Yii::$app->request->post('row_id');
		
		

		foreach ($pk as $key => $value) {
			$sql = "DELETE FROM automobile_files WHERE id_file = $value";
			$query = Yii::$app->db->createCommand($sql)->execute();
		}

		return $this->redirect(['index']);

	} 
	
	
	
	
	
	
	
	
	
    /**
     * Finds the Automobilefiles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Automobilefiles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Automobilefiles::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
