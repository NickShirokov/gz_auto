<?php

namespace backend\controllers;

use Yii;
use app\models\Pddpenalties;

use app\models\Contracts;
use app\models\Pddpenstatus;
use app\models\Pddpenstatusapi;

use backend\models\PddpenaltiesSearchModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\UploadedFile;

use app\models\Autoparams;

use common\models\AutomobilesNewModel;
use common\models\DriversModel;
use app\models\Payments;

use app\models\Pddpenaltiesapi;
use app\models\Logitems;
use yii\filters\AccessControl;

use DateTime;
/**
 * PddpenaltiesController implements the CRUD actions for Pddpenalties model.
 */
class PddpenaltiesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
			
			[
				   'class' => AccessControl::className(),
				   'only' => ['index', 
										'create', 
										'view', 
										'update', 
										'delete',
										'import',
										'shtrafsbycontract',
										'params',
										'contracts',
										'drivers',
										'autos',
										'multipledelete',
										'removeall',
										'newcreate',],
				   'rules' => [
						   [
								   'actions' => ['index', 
										'create', 
										'view', 
										'update', 
										'delete',
										'import',
										'shtrafsbycontract',
										'params',
										'contracts',
										'drivers',
										'autos',
										'multipledelete',
										'removeall',
										'newcreate',],
								   'allow' => true,
								   'roles' => ['admin'],
						   ],
						   [
								   'actions' => [
												'index', 
												'view',
												'create',
												],
								   'allow' => true,
								   'roles' => ['manager', 'payer'],
						   ],
						   
						   [
								   'actions' => [
												'index', 
												'view',
												
												],
								   'allow' => true,
								   'roles' => ['user'],
						   ],
				   ],

		   ],
			
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
										'logout', 
										'index', 
										'create', 
										'view', 
										'update', 
										'delete',
										'import',
										'shtrafsbycontract',
										'params',
										'contracts',
										'drivers',
										'autos',
										'multipledelete',
										'removeall',
										'newcreate',
										
										
										
										],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
		
		
		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pddpenalties models.
     * @return mixed
     */
    public function actionIndex()
    {
		
		
		
		
		
		
		
		
		
		
		
        $searchModel = new PddpenaltiesSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		
		//print_r ($searchModel);
		
		
		$model_pddpentls = Pddpenstatus::find()->all();
		

		foreach ($model_pddpentls as $value) {
			$arrPddpenstatus[$value->p_status_id] = $value->p_status_name;
		}
		
		
		$model_pddpentlsapi = Pddpenstatusapi::find()->all();
		
	

		foreach ($model_pddpentlsapi as $value) {
			$arrPddpenstatusapi[$value->p_status_id] = $value->p_status_name_api;
		}
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrPddpenstatus' => $arrPddpenstatus,
			'arrPddpenstatusapi' => $arrPddpenstatusapi,
        ]);
    }

    /**
     * Displays a single Pddpenalties model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
		
		
		
		
		
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pddpenalties model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pddpenalties();
		
		
		
		//Пробуем сохранить файл чека
		
		if ($model->load(Yii::$app->request->post())) { 
		
		
			$t_m = 	$model;	
					
					
			$t_m->file_f = UploadedFile::getInstance($t_m,'file_f');
			
			$fileName = $t_m->file_f->name;
			
			$post = Yii::$app->request->post();
			
			$post['Pddpenalties']['file_f'] = $t_m->file_f->name;
			
			
			//Проверяем, пустой ли файл и сохраняем
			
			if (!empty($model->file_f)) {
				$model->file_f->saveAs('uploads/'.$t_m->file_f->baseName.'.'.$t_m->file_f->extension);
			
			}
			
			
			$model->image_doc = $t_m->file_f->name;
			
				
			$model->image_doc = $t_m->file_f->name;
			
			if (!empty($model->image_doc)) {
				if ($model->type_opl_api == 1) {
					$model->type_opl_api = 2;
				}
			}
			
			
			$model->save(false);
			
			
			
			$modelLogitems = new Logitems ();
			
			$user_id = Yii::$app->user->identity->id;
			
			//Yii::$app->authManager->getRolesByUser($user_id);
			$type_user = current(Yii::$app->authManager->getRolesByUser( Yii::$app->user->identity->id))->name;
			$modelLogitems->type_user = $type_user;
			$modelLogitems->user_id = $user_id;
			$modelLogitems->type_page = 'pdd';
			$modelLogitems->item_id = $model->id_shtraf;
			$modelLogitems->date_create = date('Y-m-d');
						
			$modelLogitems->save();
			
			
			
			
		
			return $this->redirect(['view', 'id' => $model->id_shtraf]);
		}
		
		
		
		//print_r (Yii::$app->request->get()['contract_id']);
		
		
		//exit;
		
		//Добавим машину
		$model_autos = AutomobilesNewModel::find()->all();
		
		
		foreach ($model_autos as $value) {
			
			$arrAutos[$value->id_auto] = 'Машина №'.$value->id_auto.' '
											. $value->mark .' '
											. $value->model. ' Текущий госномер '
											. $value->gosnomer. ' VIN '
											
											. $value->VIN; 
			
		}
		
		
	
		
		$model_contracts = Contracts::find()->joinWith(['owners', 'drivers', 'automobiles'])->all();

		
		$arrContracts = [];
		
		
		foreach ($model_contracts as $value) {
			
			$f_name = $value->drivers[0]->first_name;
			
			$m_name = $value->drivers[0]->middle_name;
			
			$l_name = $value->drivers[0]->last_name;
			
			
			$mark = $value->automobiles[0]->mark;
			
			$mdl = $value->automobiles[0]->model;
			
			$gosnomer = $value->automobiles[0]->gosnomer;
			
			
			
			$arrContracts[$value->id_contract] = 'Договор № '.$value->id_contract
							.' '.$l_name
							.' '.$f_name
							.' '.$m_name
							
							.' '.$mark
							.' '.$mdl
							.' '.$gosnomer;
		
			
		
		}
		
		
		$model_pddpentls = Pddpenstatus::find()->all();
		
		
		
		
		
		

		foreach ($model_pddpentls as $value) {
			$arrPddpenstatus[$value->p_status_id] = $value->p_status_name;
		}
		
		
		$model_pddpentlsapi = Pddpenstatusapi::find()->all();
		
	

		foreach ($model_pddpentlsapi as $value) {
			$arrPddpenstatusapi[$value->p_status_id] = $value->p_status_name_api;
		}
		
		
		//Водители
		
		$model_drivers = DriversModel::find()->all();
		
		
		foreach ($model_drivers as $value) {
			
			$arrDrivers[$value->id_driver] = $value->last_name.' '.$value->first_name;
		}
		
		
		
		
		$model_params = Autoparams::find()->orderBy('id_param DESC')->all();
		
		foreach ($model_params as $value) {
			
			$arrParams[$value->id_param] = 'СТС '.$value->param_sts.' Госномер '.$value->param_gosnomer;
			
			
		}
		
		
		
		
        return $this->render('create', [
            'model' => $model,
			'arrContracts' => $arrContracts,
			'arrPddpenstatus' => $arrPddpenstatus,
			'arrPddpenstatusapi' => $arrPddpenstatusapi,
			'arrAutos' => $arrAutos,
			'arrDrivers' => $arrDrivers,
			'arrParams' => $arrParams,
        ]);
    }
	
	
	
	
	
	
	
	/**
     * Creates a new Pddpenalties model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionNewcreate()
    {
        $model = new Pddpenalties();
		
		$modelPayments = new Payments();
		
		//Пробуем сохранить файл чека
		
		if ($model->load(Yii::$app->request->post())) { 
		
		
			$t_m = 	$model;	
					
					
			$t_m->file_f = UploadedFile::getInstance($t_m,'file_f');
			
			$fileName = $t_m->file_f->name;
			
			$post = Yii::$app->request->post();
			
			$post['Pddpenalties']['file_f'] = $t_m->file_f->name;
			
			
			//Проверяем, пустой ли файл и сохраняем
			
			if (!empty($model->file_f)) {
				$model->file_f->saveAs('uploads/'.$t_m->file_f->baseName.'.'.$t_m->file_f->extension);
			
			}
			
			
			$model->image_doc = $t_m->file_f->name;
			
				
			$model->image_doc = $t_m->file_f->name;
			
			if (!empty($model->image_doc)) {
				if ($model->type_opl_api == 1) {
					$model->type_opl_api = 2;
				}
			}
		

			$postBigDats = Yii::$app->request->post();
		
			
			$model->save();
					
		
			
			$id_shtraf = $model->id_shtraf;
			
			
			$modelPayments->load($postBigDats);
			
			
			$modelPayments->contract_id = $model->contract_id;
			
			
			$modelPayments->id_pdd_shtraf = $id_shtraf;
			

			$modelPayments->save();
			
			
			$modelLogitems = new Logitems ();
			
			$user_id = Yii::$app->user->identity->id;
			
			//Yii::$app->authManager->getRolesByUser($user_id);
			$type_user = current(Yii::$app->authManager->getRolesByUser( Yii::$app->user->identity->id))->name;
			$modelLogitems->type_user = $type_user;
			$modelLogitems->user_id = $user_id;
			$modelLogitems->type_page = 'pdd';
			$modelLogitems->item_id = $model->id_shtraf;
			$modelLogitems->date_create = date('Y-m-d');
						
			$modelLogitems->save();
			
			
			
			
		
			return $this->redirect(['view', 'id' => $model->id_shtraf]);
		}
		
		
		
		//print_r (Yii::$app->request->get()['contract_id']);
		
		
		//exit;
		
		//Добавим машину
		$model_autos = AutomobilesNewModel::find()->all();
		
		
		foreach ($model_autos as $value) {
			
			$arrAutos[$value->id_auto] = 'Машина №'.$value->id_auto.' '
											. $value->mark .' '
											. $value->model. ' Текущий госномер '
											. $value->gosnomer. ' VIN '
											
											. $value->VIN; 
			
		}
		
		
	
		
		$model_contracts = Contracts::find()->joinWith(['owners', 'drivers', 'automobiles'])->all();

		
		$arrContracts = [];
		
		
		foreach ($model_contracts as $value) {
			
			$f_name = $value->drivers[0]->first_name;
			
			$m_name = $value->drivers[0]->middle_name;
			
			$l_name = $value->drivers[0]->last_name;
			
			
			$mark = $value->automobiles[0]->mark;
			
			$mdl = $value->automobiles[0]->model;
			
			$gosnomer = $value->automobiles[0]->gosnomer;
			
			
			
			$arrContracts[$value->id_contract] = 'Договор № '.$value->id_contract
							.' '.$l_name
							.' '.$f_name
							.' '.$m_name
							
							.' '.$mark
							.' '.$mdl
							.' '.$gosnomer;
		
			
		
		}
		
		
		$model_pddpentls = Pddpenstatus::find()->all();
		
		
		
		
		
		

		foreach ($model_pddpentls as $value) {
			$arrPddpenstatus[$value->p_status_id] = $value->p_status_name;
		}
		
		
		$model_pddpentlsapi = Pddpenstatusapi::find()->all();
		
	

		foreach ($model_pddpentlsapi as $value) {
			$arrPddpenstatusapi[$value->p_status_id] = $value->p_status_name_api;
		}
		
		
		//Водители
		
		$model_drivers = DriversModel::find()->all();
		
		
		foreach ($model_drivers as $value) {
			
			$arrDrivers[$value->id_driver] = $value->last_name.' '.$value->first_name;
		}
		
		
		
		
		$model_params = Autoparams::find()->orderBy('id_param DESC')->all();
		
		foreach ($model_params as $value) {
			
			$arrParams[$value->id_param] = 'СТС '.$value->param_sts.' Госномер '.$value->param_gosnomer;
			
			
		}
		
		
		
		
        return $this->render('newcreate', [
            'model' => $model,
            'modelPayments' => $modelPayments,
			'arrContracts' => $arrContracts,
			'arrPddpenstatus' => $arrPddpenstatus,
			'arrPddpenstatusapi' => $arrPddpenstatusapi,
			'arrAutos' => $arrAutos,
			'arrDrivers' => $arrDrivers,
			'arrParams' => $arrParams,
        ]);
    }
	
	
	
	
	
	
	
	
	
	

    /**
     * Updates an existing Pddpenalties model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

       //Пробуем сохранить файл чека
		
		if ($model->load(Yii::$app->request->post())) { 
		
		
			$t_m = 	$model;	
					
					
			$t_m->file_f = UploadedFile::getInstance($t_m,'file_f');
			
			$fileName = $t_m->file_f->name;
			
			$post = Yii::$app->request->post();
			
			$post['Pddpenalties']['file_f'] = $t_m->file_f->name;
			
			//Проверяем, пустой ли файл и сохраняем
			
			if (!empty($model->file_f)) {
				$model->file_f->saveAs('uploads/'.$t_m->file_f->baseName.'.'.$t_m->file_f->extension);
			
			}
			
			$model->image_doc = $t_m->file_f->name;
			
			if (!empty($model->image_doc)) {
				if ($model->type_opl_api == 1) {
					$model->type_opl_api = 2;
				}
			}
			
			
			$model->save(false);
		
			return $this->redirect(['view', 'id' => $model->id_shtraf]);
		}
			
			
		//Добавим машину
		$model_autos = AutomobilesNewModel::find()->all();
		
		
		foreach ($model_autos as $value) {
			
		
			
			$arrAutos[$value->id_auto] = 'Машина №'.$value->id_auto.' '
											. $value->mark .' '
											. $value->model. ' Текущий госномер '
											. $value->gosnomer. ' VIN '
											
											. $value->VIN; 
		
		
		}	
			
			
		$model_params = Autoparams::find()->where(['auto_id' => $model->auto_id])->orderBy('id_param DESC')->all();
		
		foreach ($model_params as $value) {
			
			$arrParams[$value->id_param] = 'СТС '.$value->param_sts.' Госномер '.$value->param_gosnomer;
			
			
		}
			
			
			
			
		$model_contracts = Contracts::find()->joinWith(['owners', 'drivers', 'automobiles'])->all();

		
		foreach ($model_contracts as $value) {
			
			$f_name = $value->drivers[0]->first_name;
			
			$m_name = $value->drivers[0]->middle_name;
			
			$l_name = $value->drivers[0]->last_name;
			
			
			$mark = $value->automobiles[0]->mark;
			
			$mdl = $value->automobiles[0]->model;
			
			$gosnomer = $value->automobiles[0]->gosnomer;
			
			
			
			$arrContracts[$value->id_contract] = 'Договор №'.$value->id_contract
							.' '.$l_name
							.' '.$f_name
							.' '.$m_name
							
							.' '.$mark
							.' '.$mdl
							.' '.$gosnomer;
		
			
		
		}
		
		
		
		$model_pddpentls = Pddpenstatus::find()->all();
		
		foreach ($model_pddpentls as $value) {
			$arrPddpenstatus[$value->p_status_id] = $value->p_status_name;
		}
		
		
		$model_pddpentlsapi = Pddpenstatusapi::find()->all();
		
	

		foreach ($model_pddpentlsapi as $value) {
			$arrPddpenstatusapi[$value->p_status_id] = $value->p_status_name_api;
		}
		
		
		
		
		//Водители
		
		$model_drivers = DriversModel::find()->all();
		
		
		foreach ($model_drivers as $value) {
			
			$arrDrivers[$value->id_driver] = $value->last_name.' '.$value->first_name;
		}
		
		
		
        return $this->render('update', [
            'model' => $model,
			
			'arrContracts' => $arrContracts,
			'arrPddpenstatus' => $arrPddpenstatus,
			'arrPddpenstatusapi' => $arrPddpenstatusapi,
			'arrAutos' => $arrAutos,
			'arrDrivers' => $arrDrivers,
			'arrParams' => $arrParams,
        ]);
    }

    /**
     * Deletes an existing Pddpenalties model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pddpenalties model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pddpenalties the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
	 
	 
		//Пробуем создать метод для получения списка штрафов по договору аяксом
	public function actionShtrafsbycontract ($id) {
			
		$countShtrafs = Pddpenalties::find()->where(
		
				[
					'contract_id' => $id,
					'type_opl' => [1,3, 4],
				]
			)->count();
		
		
		$shtrafs = Pddpenalties::find()->where(
				[
					'contract_id' => $id,
					'type_opl' => [1, 3, 4],
				]
				
		)->orderBy ('contract_id ASC')->all();
		
		
		
		$model_contract = Contracts::find()->where(['id_contract' => $id])->all();
		
		$contract_obj = $model_contract[0];
		//print_r ($contract_obj);
		
		
		//exit;
		
		
		
		if ($countShtrafs > 0) {
			
			$res = "";
			foreach ($shtrafs as $item ) {
				
				
				//Сюда нужно закодить формульную логику к оплате с учетом
				// комиссионных галок и срока давности
				$pereraschet = 0;
				

				$summa = $item->summa_opl;
				
				
				$skidka_status = $item->skidka_status;
				
				
				$pen_com = $contract_obj->penalty_comission;
				
				$dsc_type = $contract_obj->discount_type;


				$date_raznost = date_diff(new DateTime($item->date_post), new DateTime())->days;
			
				
				
				if ( $date_raznost <= 19 ) {
							
					//skidka_status
				
					//Если есть галочка скидка, то ебашим скидку 50%
					if ($skidka_status == 0) {
						if ($dsc_type == 1) {					
							$summa = $summa / 2;
						}
					}
					
				} elseif ($date_raznost >= 59) {
					
					$summa = $summa * 2;
					
				} else {
					//
				}
				
			
				
				
				
				
				//Если есть галочка комиссия, то добавляем 50 рублей
				if ($pen_com == 1) {
					
					$summa = $summa + 50;
				}
				
				$pereraschet = $summa;
				
				
				$res.= '<option data-sum="'.$pereraschet.'" value="'.$item->id_shtraf.'">'.
				
				
				$item->n_post
				.' '.$item->date_post
				//.' '.$item->date_narush
				.' '.$item->summa_opl.' рублей'
				.' '.mb_strimwidth($item->comment, 0, 60, "...")
				.' сумма с перерасчетом '.$pereraschet.'
				
				
				</option>';
				
			}
			
			
		} else {
			$res.= "";
			
		}
		return $res;
	}
	 
	 
	//Добавим функцию для вывода ajax стс и госномеров по auto_id

	public function actionParams ($id) {
		
		
		$model_auto = AutomobilesNewModel::findOne($id);
		
		
		//$sts_gosnomer = 'СТС '.$model_auto->sts.' Госномер '.$model->gosnomer;
		
		
		$model_params = Autoparams::find()->where(['auto_id' => $id])->orderBy('id_param DESC')->all();
		
		//print_r ($model_params);
		
		$res = "";
		
		
		
		
		foreach ($model_params as $item) {
			
			
			$res.= '<option value="'.$item->id_param.'">СТС '.$item->param_sts.' Госномер '.$item->param_gosnomer.'</option>';
			
		}
		
		
		return $res;
		
	}
	
	
	//Добавим функцию для вывода ajax договоров по auto_id

	public function actionContracts ($id) {
		
		
		$model_contracts = Contracts::find()->joinWith(['automobiles', 'drivers'])->where(['auto_id' => $id])->all();
		
		$res = '<option value="0"></option>';
		
		foreach ($model_contracts as $item) {
			
			
			$res.= '<option value="'.$item->id_contract.'"> Договор №'
			
										.$item->id_contract.' '
										.$item['automobiles'][0]->mark.' '
										.$item['automobiles'][0]->model.' '
										.$item['drivers'][0]->last_name.' '
										.$item['drivers'][0]->first_name.'
										
										
										</option>';
			
		}
		
		return $res;	
		
	}
	
	//Добавим функцию подкладывания водителя по договору
	public function actionDrivers ($id) {
		
		
		$model_contract = Contracts::findOne($id);
		
		
		
		
		$driver = DriversModel::findOne($model_contract->driver_id);
		
		
		
		
		$res = '<option value="'.$driver->id_driver.'">'.$driver->last_name.' '.$driver->first_name.'</option>';
		
		
		
		return $res;
	}
	
	 
	 
	//Добавим функцию подкладывания водителя по договору
	public function actionAutos ($id) {
		
		
		$model_contract = Contracts::findOne($id);
		
		
		
		
		$auto = AutomobilesNewModel::findOne($model_contract->auto_id);
		
			
		$res = '<option value="'.$auto->id_auto.'">
		
						Машина №'.$auto->id_auto.' '
											. $auto->mark .' '
											. $auto->model. ' Текущий госномер '
											. $auto->gosnomer. ' VIN '
											
											. $auto->VIN.'
		
		
					</option>';

		return $res;
	}
	
	  
	 
	 
	public function actionMultipledelete()
	{
		$pk = Yii::$app->request->post('row_id');
		
		

		foreach ($pk as $key => $value) {
			$sql = "DELETE FROM pdd_penalties WHERE id_shtraf = $value";
			$query = Yii::$app->db->createCommand($sql)->execute();
		}

		return $this->redirect(['index']);

	} 
	
	
	public function actionRemoveall()
	{
		//$pk = Yii::$app->request->post('row_id');
		
		

		//foreach ($pk as $key => $value) {
		$sql = "DELETE FROM pdd_penalties;";
		$query = Yii::$app->db->createCommand($sql)->execute();
		//}

		return $this->redirect(['index']);

	} 
	
	 
	 
	 
	 
	 
	//Загрузка штрафов ПДД 
	 public function actionImport()
    {
	
		$model = new Pddpenalties();
	
		//Все штрафы до 10 февраля включительно, которые добавились
		//$api_models = Pddpenaltiesapi::find()->where(['<=', 'created_at', '2020-02-10'])->orderBy('created_at DESC')->all();
		
		
		$api_models = Pddpenaltiesapi::find()->orderBy('created_at DESC')->all();
		
	
		foreach ($api_models as $item) {
			
			
			$sts = $item->vehicle_certificate;
			
			
			
			
			
			
			$summa_opl = $item->price;
			
			//$type_opl = $item->violator_paid;
			
			if ($item->violator_paid == 0) {
				
				$type_opl = 1;
				
			} else {
				
				$type_opl = 2;
				
			}
			
			$date_post = $item->ruling_date;
			
			$date_narush = $item->violation_date;
			
			$n_post = $item->ruling_number;
			
			
			if (strtotime($date_narush) > strtotime('31.12.2019')) {
				
				$type_opl = 1;
				
			}
			
			
			
			
			
			$narushitel = $item->violator;
			
			
			
			$type_opl_api = $item->paid;
			
			if ($item->paid == 0) {
				
				$type_opl_api = 1;
				
			} else {
				
				$type_opl_api = 3;
				
			}
			
			
			
			
			
			
			
			$skidka_status = 0;
			
			
			if ($summa_opl > 5000) {
				
				$skidka_status = 1;
			}
			
			if ($summa_opl == 2000) {
				
				$skidka_status = 1;
			}
			
			
			
			
			//ПРобуем найти машину по стс
			$model_autoparams = Autoparams::find()->where(['param_sts' => $sts])->all();
			
	
			
			$model_pens = Pddpenalties::find()->where(['n_post' => $n_post])->all();
			
			$id_shtraf = $model_pens[0]->id_shtraf;
			
			//Проверка наличия штрафы
			
			
			
			
			if (!empty($id_shtraf)) {
				
				
				$auto_id = $model_autoparams[0]->auto_id;
					
					
				$id_param = $model_autoparams[0]->id_param;
				
				//
				$model = $this->findModel($id_shtraf);
				
				if ($summa_opl > 5000) {
				
					$skidka_status = 1;
				}
				
				if ($summa_opl == 2000) {
					
					$skidka_status = 1;
				}
				
				$model_contracts = Contracts::find()->where(['auto_id' => $auto_id])->all();
			
				$contract_id = 0;
				
				$driver_id = 0;
				
				if (!empty($model_contracts)) {
					
					
					foreach ($model_contracts as $cntrct) {
					    
					    
					    $cntrct_date_start = $cntrct->date_pay_arenda;
					    $cntrct_date_end = $cntrct->date_end;


                        if (strtotime($date_narush) >= strtotime($cntrct_date_start) 
                            && strtotime($date_narush) <= strtotime($cntrct_date_end)) 
                        {
                            $driver_id = $cntrct->driver_id;
					        $contract_id = $cntrct->id_contract;
					        break;
                        }						    
					    
					}
					
				}
			
			
			
				$model->auto_id = $auto_id;
				$model->param_id = $id_param;
				$model->contract_id = $contract_id;
				$model->driver_id = $driver_id;
				
				
				$model->n_post = $n_post;
				$model->date_post = $date_post;
				$model->date_narush = $date_narush;
				$model->type_opl = $type_opl;
				$model->type_opl_api = $type_opl_api;
				$model->summa_opl = $summa_opl;
				$model->skidka_status = $skidka_status;
				$model->narushitel = $narushitel;
				
				$model->save(false);
				
				
				
				
			} else {
				
				
				//Пробуем по машине найти договор и водителя
				
				
				if (!empty($model_autoparams)) {
					
					
					$auto_id = $model_autoparams[0]->auto_id;
					
					
					$id_param = $model_autoparams[0]->id_param;
					//Ищем договор, чтобы определиться с водителем и вычислениями
					$model_contracts = Contracts::find()->where(['auto_id' => $auto_id])->all();
					
					
					
			
					$contract_id = 0;
					
					$driver_id = 0;
					
					if (!empty($model_contracts)) {
						
						
						foreach ($model_contracts as $cntrct) {
						    
						    
						    $cntrct_date_start = $cntrct->date_pay_arenda;
						    $cntrct_date_end = $cntrct->date_end;


                            if (strtotime($date_narush) >= strtotime($cntrct_date_start) 
                                && strtotime($date_narush) <= strtotime($cntrct_date_end)) 
                            {
                                $driver_id = $cntrct->driver_id;
						        $contract_id = $cntrct->id_contract;
						        break;
                            }						    
						    
						}
						
					}
					
					$model = new Pddpenalties();
					
					
					if ($summa_opl > 5000) {
				
						$skidka_status = 1;
					}
					
					if ($summa_opl == 2000) {
						
						$skidka_status = 1;
					}
					
					
					$model->auto_id = $auto_id;
					$model->param_id = $id_param;
					$model->contract_id = $contract_id;
					$model->driver_id = $driver_id;
					$model->n_post = $n_post;
					$model->date_post = $date_post;
					$model->date_narush = $date_narush;
					$model->type_opl = $type_opl;
					$model->type_opl_api = $type_opl_api;
					$model->summa_opl = $summa_opl;
					$model->skidka_status = $skidka_status;
					$model->narushitel = $narushitel;
					
					
					$model->save(false);
									
					
				} 
			
			}
			
				
		}
		
		
		return $this->render('import', [
            'model' => $model,
			
        ]);
		
		
	}
	 
	 
	 
	 
	 
    protected function findModel($id)
    {
        if (($model = Pddpenalties::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
