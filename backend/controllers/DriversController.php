<?php

namespace backend\controllers;

use Yii;
use common\models\DriversModel;

use common\models\Driverscategory;
use common\models\Driversstatus;
use common\models\Penaltytypes;
use common\models\AutomobilesNewModel;

use app\models\Driversfiles;


use common\models\DriversModelSearch;

use backend\models\DriverscommentsSearchModel;
use app\models\Driverscomments;
use app\models\Contracts;
use app\models\Logitems;

use backend\models\PaymentsSearchModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use DateTime;

use common\models\MaintableModelSearch;

use yii\web\UploadedFile;
use backend\models\PddpenaltiesSearchModel;
use app\models\Maintable;
/**
 * DriversController implements the CRUD actions for DriversModel model.
 */
class DriversController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
		
			[
				   'class' => AccessControl::className(),
				   'only' => ['index', 
										'create', 
										'view', 
										'update', 
										'delete',
										'import',
										'multipledelete',
										'removeall',
										'svodka',
										'pdd',
										'payments',],
				   'rules' => [
						   [
								   'actions' => ['index', 
										'create', 
										'view', 
										'update', 
										'delete',
										'import',
										'multipledelete',
										'removeall',
										'card',
										'svodka',
										'pdd',
										'payments',
										],
								   'allow' => true,
								   'roles' => ['admin'],
						   ],
						   [
								   'actions' => [
												'index', 
												'create', 
												'view', 
												'update', 
												'delete',
												'card',
												'svodka',
												'pdd',
										'payments',
												],
								   'allow' => true,
								   'roles' => ['manager', 'payer'],
						   ],
						   
						   [
								   'actions' => [
												'index', 
												
												'view', 
												'card',
												'svodka',
												'pdd',
										'payments',
												],
								   'allow' => true,
								   'roles' => ['user'],
						   ],
				   ],

		   ],
			
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
										'logout', 
										'index', 
										'create', 
										'view', 
										'update', 
										'delete',
										'import',
										'multipledelete',
										'removeall',
										'card',
										'svodka',
										'pdd',
										'payments',
										
										],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all DriversModel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DriversModelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		
		$dataProvider->pagination->pageSize=200;
		
		//Категории
		$model_drvrcat = Driverscategory::find()->orderBy('id_cat ASC')->all();
		
		foreach ($model_drvrcat as $value) {
			$arrCat[$value->id_cat] = $value->category_name;
		}
		
		//Статусы
		$model_drvrstatus = Driversstatus::find()->orderBy('id_status ASC')->all();
		
		foreach ($model_drvrstatus as $value) {
			$arrStatus[$value->id_status] = $value->status_name;
		}
		
		//Типы штрафов
		$model_penaltytype = Penaltytypes::find()->orderBy('id_type ASC')->all();
		
		foreach ($model_penaltytype as $value) {
			$arrPenaltytype[$value->id_type] = $value->name_type;
		}
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrCat' => $arrCat,
			'arrStatus' => $arrStatus,
			'arrPenaltytype' => $arrPenaltytype,

        ]);
    }
	
	
	
	

	
	
	

    /**
     * Displays a single DriversModel model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
		//Пробуем модельку
		
		$modelComments = new Driverscomments();
		
		
		//Пробуем добавить ссылки на договора, если они у нас есть
		
		
		$model_contracts = Contracts::find()->where(['driver_id' => $id] )->all();
			
		$request = Yii::$app->request->queryParams;
		
		$request['DriverscommentsSearchModel']['driver_id'] = $id;
		
		$searchModel = new DriverscommentsSearchModel();
        $dataProvider = $searchModel->search($request);
		
		$par['DriverscommentsSearchModel']['driver_id'] = $id;
		
		
		//$ttt = $searchModel->searchAll($par);
		
		
		$drvr_cat_id = $this->findModel($id)->driver_cat;
		//$drvr_status_id = $this->findModel($id)->driver_status;
		
		$model_drvrcat = Driverscategory::find()->where(['id_cat' => $drvr_cat_id] )->all();
		
		$drvr_status_id = $this->findModel($id)->status;
		//$drvr_status_id = $this->findModel($id)->driver_status;
		
		$model_drvrstat = Driversstatus::find()->where(['id_status' => $drvr_status_id] )->all();
		
		
		
		
		//Подгружаем во вьюху картинки
		$files = Driversfiles::find()->where(['id_driver' => $id])->orderBy ('id_driver ASC')->all();
		
		foreach ($files as $item) {
			$arrFiles[$item->id_file] = $item['file'];  	
		}
		
		
		//Типы штрафов статус - где платит ПДД 
		$model_penaltytype = Penaltytypes::find()->where(['id_type' => $this->findModel($id)->penalty_type] )->all();;
		
		
		$pdd_st_name  = $model_penaltytype[0]->name_type;
		
		

		
		$model = $this->findModel($id);
		
		
		
		/*
	    if ($modelComments->load(Yii::$app->request->post()) && $modelComments->save()) {
			
			$dr_id = Yii::$app->request->post()['Driverscomments']['driver_id'];
			
            return $this->redirect(['drivers/view', 'id' => $dr_id]);
        }
		*/
		
		
		if ($modelComments->load(Yii::$app->request->post())) {
            		
					
			$t_m = 	$modelComments;	
					
					
			$t_m->file_f = UploadedFile::getInstance($t_m,'file_f');
			
			$fileName = $t_m->file_f->name;
			
			$post = Yii::$app->request->post();
			
			$post['Driversfiles']['file_f'] = $t_m->file_f->name;
			
			
			if ($t_m->file_f->baseName) {
				$modelComments->file_f->saveAs('uploads/'.$t_m->file_f->baseName.'.'.$t_m->file_f->extension);
			
				$modelComments->comment_file = $t_m->file_f->name;
			
			}
			
			$modelComments->save(false);
			
			return $this->redirect(['drivers/view', 'id' => $modelComments->driver_id]);
		}
		
		
		
		
		
		
				
		$arrDrivers[$model->id_driver] = $model->last_name.' '.$model->first_name.' '.$model->middle_name;	
				
        return $this->render('view', [
            'model' => $this->findModel($id),
			'arrFiles' => $arrFiles,
			'searchModel_1' => $searchModel,
            'dataProvider_1' => $dataProvider,
			'driver_categ' => $model_drvrcat[0]->category_name,
			'driver_stat' => $model_drvrstat[0]->status_name,
			'pdd_st_name' => $pdd_st_name,
			'model_contracts' => $model_contracts,
			
			'modelComments' => $modelComments,
			'arrDrivers' => $arrDrivers,
        ]);
    }

    /**
     * Creates a new DriversModel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DriversModel();

        if ($model->load(Yii::$app->request->post())) {
			
			
			
			$t_m = 	$model;	
					
					
			$t_m->photo_f = UploadedFile::getInstance($t_m,'photo_f');
			
			$fileName = $t_m->photo_f->name;
			
			$post = Yii::$app->request->post();
			
			$post['Driversfiles']['photo_f'] = $t_m->photo_f->name;
				
			if ($t_m->photo_f->baseName) {
				$model->photo_f->saveAs('uploads/'.$t_m->photo_f->baseName.'.'.$t_m->photo_f->extension);
			
				$model->photo = $t_m->photo_f->name;
			
			}
					
			
			
			$model->fio = trim($model->last_name).' '.trim($model->first_name).' '.trim($model->middle_name);
			
			$model->save(false);
			
			$modelLogitems = new Logitems ();
			
			$user_id = Yii::$app->user->identity->id;
			
			//Yii::$app->authManager->getRolesByUser($user_id);
			$type_user = current(Yii::$app->authManager->getRolesByUser( Yii::$app->user->identity->id))->name;
			$modelLogitems->type_user = $type_user;
			$modelLogitems->user_id = $user_id;
			$modelLogitems->type_page = 'driver';
			$modelLogitems->item_id = $model->id_driver;
			$modelLogitems->date_create = date('Y-m-d');
						
			$modelLogitems->save();
			
			
			
			
			
			
			
			
            return $this->redirect(['view', 'id' => $model->id_driver]);
			
			
			
        }
			
		$model_drvrcat = Driverscategory::find()->orderBy('id_cat ASC')->all();
		
		foreach ($model_drvrcat as $value) {
			$arrCat[$value->id_cat] = $value->category_name;
		}
		
		$model_drvrstatus = Driversstatus::find()->orderBy('id_status ASC')->all();
		
		foreach ($model_drvrstatus as $value) {
			$arrStatus[$value->id_status] = $value->status_name;
		}
				
		//Типы штрафов
		$model_penaltytype = Penaltytypes::find()->orderBy('id_type ASC')->all();
		
		foreach ($model_penaltytype as $value) {
			$arrPenaltytype[$value->id_type] = $value->name_type;
		}
		
				

        return $this->render('create', [
            'model' => $model,
			'arrCat' => $arrCat,
			'arrStatus' => $arrStatus,
			'arrPenaltytype' => $arrPenaltytype,
        ]);
    }

    /**
     * Updates an existing DriversModel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
		
		//Пробуем добавить ссылки на договора, если они у нас есть
		
		
		$model_contracts = Contracts::find()->where(['driver_id' => $id] )->all();
		
		
		
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
			
			
			
			
			$t_m = 	$model;	
					
					
			$t_m->photo_f = UploadedFile::getInstance($t_m,'photo_f');
			
			$fileName = $t_m->photo_f->name;
			
			$post = Yii::$app->request->post();
			
			$post['Driversfiles']['photo_f'] = $t_m->photo_f->name;
				
			if ($t_m->photo_f->baseName) {
				$model->photo_f->saveAs('uploads/'.$t_m->photo_f->baseName.'.'.$t_m->photo_f->extension);
			
				$model->photo = $t_m->photo_f->name;
			
			}
			
			
			
			
			$model->fio = trim($model->last_name).' '.trim($model->first_name).' '.trim($model->middle_name);
			
			$model->save(false);
			
            return $this->redirect(['view', 'id' => $model->id_driver]);
        
		
		}

		$model_drvrcat = Driverscategory::find()->orderBy('id_cat ASC')->all();
		
		foreach ($model_drvrcat as $value) {
			$arrCat[$value->id_cat] = $value->category_name;
		}
				
		$model_drvrstatus = Driversstatus::find()->orderBy('id_status ASC')->all();
		
		foreach ($model_drvrstatus as $value) {
			$arrStatus[$value->id_status] = $value->status_name;
		}		
				
		//Типы штрафов
		$model_penaltytype = Penaltytypes::find()->orderBy('id_type ASC')->all();
		
		foreach ($model_penaltytype as $value) {
			$arrPenaltytype[$value->id_type] = $value->name_type;
		}
		
		
        return $this->render('update', [
            'model' => $model,
			'arrCat' => $arrCat,
			'arrStatus' => $arrStatus,
			'arrPenaltytype' => $arrPenaltytype,
			'model_contracts' => $model_contracts,
        ]);
    }

	
	
	
	
	
	
	//Грузим csv файлик
	
	
	public function actionImport()
    {
		
		$model = new DriversModel();
		

		if ($model->load(Yii::$app->request->post())) { 
		
		
			$t_m = 	$model;	
					
					
			$t_m->file_f = UploadedFile::getInstance($t_m,'file_f');
			
			$fileName = $t_m->file_f->name;
			
			
			//$post = Yii::$app->request->post();
			
			//$post['Driversfiles']['file_f'] = $t_m->file_f->name;
			
			$model->file_f->saveAs('uploads/'.$t_m->file_f->baseName.'.'.$t_m->file_f->extension);
			
			
			
			$file_path = 'uploads/'.$t_m->file_f->baseName.'.'.$t_m->file_f->extension;
			
			
			
			$filename = $file_path;

			// The nested array to hold all the arrays
			$the_big_array = []; 

			// Open the file for reading
			if (($h = fopen("{$filename}", "r")) !== FALSE) 
			{
			  // Each line in the file is converted into an individual array that we call $data
			  // The items of the array are comma separated
			  while (($data = fgetcsv($h, 1000, ";")) !== FALSE) 
			  {
				// Each individual array is being pushed into the nested array
				$the_big_array[] = $data;		
			  }

			  // Close the file
			  fclose($h);
			}

			
			unset ($the_big_array[0]);
		
		
		
			/*
			1. Взять паспорт серия-номер, проверить, есть ли в базе
			2. В случае если есть - обновить, иначе просто вставить
			
			*/
			
			
			
			
			//Массивы из справочников
			
			$status_arr = [
							
								'Работает' => '1',
								'Не работает' => '2',
								'Уволен' => '3',
								'Сотрудник' => '4',
								'Заблокирован' => '5',
							
							];
			
			$cat_arr = [
							'Новый' => '1',
							'Старый' => '2',
							'Проблемный' => '3',
							'Тестовый' => '4',
			
						];
						
						
			$penalty_arr = [
								'У нас' => 1,
								'В ГИБДД' => 2,
			
			
							];			
			
			
			
			
			foreach ($the_big_array as $item) {
				
				$first_name = trim($item[1]);
				$middle_name = trim($item[2]);
				$last_name = trim($item[0]);
				
				
				$fio = $last_name.' '.$first_name.' '.$middle_name;
				$fio = trim($fio);
				
				
				if (!empty($item[3])) {
					
					$date_birth = date('Y-m-d',strtotime($item[3]));
				} else {
					$date_birth = '';
				}
					
	
				$p_series = trim($item[4]);
				$p_number = trim($item[5]);
				
				$p_who = $item[6];
				
	
		
				$passport = $p_series.$p_number;
				
				$phone_1 = $item[9];
				$phone_2 = $item[10];
				
				$address_reg = $item[7];
				$address_fact = $item[8];
				
				
				$lic_series = $item[11];
				$lic_number = $item[12];
				$lic_type = $item[13];
				
				
				$lic_date = $item[14];
				
				
				$driver_cat = 1;
				
				if (!empty($item[15])) {
				//Статусы пока формально ... щас обработаю
				    $driver_cat = $cat_arr[$item[15]];		
				}
				
				$driver_status = 1;
				
				if (!empty($item[16])) {
				
				    $driver_status = $status_arr[$item[16]];
				
				}
				
				
				$penalty_type = 1;
				
				if (!empty($item[17])) {
					$penalty_type = $penalty_arr[$item[17]];	
				}
				
			
				//Проверяем, есть ли такой водитель в базе по паспорту
				
				$drivers_models = [];
				
				if (!empty($passport)) {
				
					$drivers_models = DriversModel::find()->where(['p_series' => $p_series, 'p_number' => $p_number])->all();
				
				}
				
				
				
				
				
				

				if (!empty($drivers_models)) {
					
					$model = $this->findModel($drivers_models[0]->id_driver);
					
					$model->first_name = $first_name;
					$model->last_name = $last_name;
					$model->middle_name = $middle_name;
					
					$model->fio = trim($fio);
					
					$model->date_birth = $date_birth;
					
					$model->p_series = $p_series;
					$model->p_number = $p_number;
					$model->p_who = $p_who;
					
					$model->phone_1 = $phone_1; 
					$model->phone_2 = $phone_2; 
					$model->address_reg = $address_reg;
					$model->address_fact = $address_fact;
					$model->lic_series = $lic_series;
					$model->lic_number = $lic_number;
					$model->lic_type = $lic_type;
					$model->lic_dates = $lic_date;
					$model->status = $driver_status;
					$model->driver_cat = $driver_cat;
					$model->penalty_type = $penalty_type;
					
					$model->save(false);
					
				} else {
					

					//Сделаем костыльчик если водитель пока без паспорта в ячеках
					//Чтобы не дублировались по фио
					
					$drivers_models_by_fio = DriversModel::find()
											->where([
													'fio' => $fio,
													]
											)->all();
					
					if (!empty($drivers_models_by_fio)) {
	
						$model = $this->findModel($drivers_models_by_fio[0]->id_driver);
			
						$model->first_name = $first_name;
						$model->last_name = $last_name;
						$model->middle_name = $middle_name;
						
						$model->fio = trim($fio);
						
						$model->date_birth = $date_birth;
						
						$model->p_series = $p_series;
						$model->p_number = $p_number;
						$model->p_who = $p_who;
						
						$model->phone_1 = $phone_1; 
						$model->phone_2 = $phone_2; 
						$model->address_reg = $address_reg;
						$model->address_fact = $address_fact;
						$model->lic_series = $lic_series;
						$model->lic_number = $lic_number;
						$model->lic_type = $lic_type;
						$model->lic_dates = $lic_date;
						$model->status = $driver_status;
						$model->driver_cat = $driver_cat;
						$model->penalty_type = $penalty_type;
						
						$model->save(false);
					
					} else {
					    
	    
					    $drivers_models_by_vu = DriversModel::find()
											->where(['lic_series' => $lic_series, 
													'lic_number' => $lic_number,
													
													]
											)->all();
					    
						
						
					    
					    
					    if (!empty($drivers_models_by_vu) && !empty($lic_series) && !empty($lic_number)) {
				        	$model = $this->findModel($drivers_models_by_vu[0]->id_driver);
					    } 
					    
					    
						
						$model->first_name = $first_name;
						$model->last_name = $last_name;
						$model->middle_name = $middle_name;
						
						
						$model->fio = trim($fio);
						
						$model->date_birth = $date_birth;
						
						$model->p_series = $p_series;
						$model->p_number = $p_number;
						$model->p_who = $p_who;
						
						$model->phone_1 = $phone_1; 
						$model->phone_2 = $phone_2; 
						$model->address_reg = $address_reg;
						$model->address_fact = $address_fact;
						$model->lic_series = $lic_series;
						$model->lic_number = $lic_number;
						$model->lic_type = $lic_type;
						$model->lic_dates = $lic_date;
						$model->status = $driver_status;
						$model->driver_cat = $driver_cat;
						$model->penalty_type = $penalty_type;
						$model->save(false);
						
						
					}
					
					
					
				
					
				}
				
	
				
				
			}
			
		
			return $this->redirect(['index']);
		
		
		}
			
	
	
		

        return $this->render('import', [
            'model' => $model,
			
        ]);
    }
	
	
	//ПРобуем прогрузить водителей только по фио на будущее
	
	public function actionImportfio1()
    {
	
		$model = new DriversModel();
		

		if ($model->load(Yii::$app->request->post())) { 
		
		
			$t_m = 	$model;	
					
					
			$t_m->file_f = UploadedFile::getInstance($t_m,'file_f');
			
			$fileName = $t_m->file_f->name;
			
			
			//$post = Yii::$app->request->post();
			
			//$post['Driversfiles']['file_f'] = $t_m->file_f->name;
			
			$model->file_f->saveAs('uploads/'.$t_m->file_f->baseName.'.'.$t_m->file_f->extension);
			
			
			
			$file_path = 'uploads/'.$t_m->file_f->baseName.'.'.$t_m->file_f->extension;
			
			
			
			$filename = $file_path;

			// The nested array to hold all the arrays
			$the_big_array = []; 

			// Open the file for reading
			if (($h = fopen("{$filename}", "r")) !== FALSE) 
			{
			  // Each line in the file is converted into an individual array that we call $data
			  // The items of the array are comma separated
			  while (($data = fgetcsv($h, 1000, ";")) !== FALSE) 
			  {
				// Each individual array is being pushed into the nested array
				$the_big_array[] = $data;		
			  }

			  // Close the file
			  fclose($h);
			}

			
			unset ($the_big_array[0]);
		
	
			
			foreach ($the_big_array as $item) {
						
				$fio = $item[0];
				
				if (!empty($fio)) {
				
					$fio_arr = explode(' ', $fio);
					
					
					if ($fio_arr[0]) {
						$last_name = $fio_arr[0];
					} 
					
					$first_name = '';
					
					if ($fio_arr[1]) {
						$first_name = $fio_arr[1];
					}
					
					$middle_name = '';
					
					if ($fio_arr[2]) {
						$middle_name = $fio_arr[2];
					}
					
					
					
				
				}
				
			}
	
			exit;
		}
	
		
	
	
		return $this->render('importfio', [
            'model' => $model,
			
        ]);
	
	}
	
	
	
    /**
     * Deletes an existing DriversModel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
	
	
	public function actionMultipledelete()
	{
	
		
		$pk = Yii::$app->request->get('row_id');
		

		

		foreach ($pk as $key => $value) {
			
			
			
			$sql = "DELETE FROM drivers WHERE id_driver = $value";
		
			
			
			$query = Yii::$app->db->createCommand($sql)->execute();
		}

		return $this->redirect(['index']);

	} 
	
	
	
	public function actionRemoveall()
	{
		//$pk = Yii::$app->request->post('row_id');
		
		

		//foreach ($pk as $key => $value) {
		$sql = "DELETE FROM drivers;";
		$query = Yii::$app->db->createCommand($sql)->execute();
		//}

		return $this->redirect(['index']);

	} 
	
	
	
	
	
	public function actionSvodka ($id) {
		
		
		
		$model = $this->findModel($id);
		
		$models_main = Maintable::find()->where(['id_driver' => $id])->all();
		
		
		if (!empty($models_main)) {
			
			$arrMainDolg = 0;
			$arrOtherDolg = 0;
			$arrArendaDolg = 0;
			$arrPddDolg = 0;
			
			
			
			
			foreach ($models_main as $item) {
				
				$arrMainDolg+= $item->MainDolg;
				$arrOtherDolg+= $item->DolgPoDogovoru;
				$arrArendaDolg+= $item->DolgPoArende;
				$arrPddDolg+=  $item->DolgPddSummaReal;
				
			}
			
			
		}
		
		

		$req = Yii::$app->request->queryParams;
		
		$req['MaintableModelSearch']['id_driver'] = $id;
		
		
		$searchModel = new MaintableModelSearch();
        $dataProvider = $searchModel->search($req);
		
		
		
		
		return $this->render('svodka', [
            'model' => $this->findModel($id),
			'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrMainDolg' =>	$arrMainDolg,
			'arrOtherDolg' =>	$arrOtherDolg,
			'arrArendaDolg' =>	$arrArendaDolg,
			'arrPddDolg'	=> $arrPddDolg,
			
			
        ]);
		
		
	}
	
	
	
	
	public function actionPdd ($id) {
		
		
		
		$model = $this->findModel($id);
		
	
		$req = Yii::$app->request->queryParams;
		
		$req['PddpenaltiesSearchModel']['driver_id'] = $id;
		
		
		$searchModel = new PddpenaltiesSearchModel();
        $dataProvider = $searchModel->search($req);
		
		
		
		
		return $this->render('pdd', [
            'model' => $this->findModel($id),
			'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			
			
			
        ]);
		
		
	}
	
	
	public function actionPayments ($id) {
		
		
		
		$model = $this->findModel($id);
		
	
		$req = Yii::$app->request->queryParams;
		
		$req['PaymentsSearchModel']['driver_id'] = $id;
		
		
		$searchModel = new PaymentsSearchModel();
        $dataProvider = $searchModel->search($req);
		
		
		
		return $this->render('payments', [
            'model' => $this->findModel($id),
			'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			
			
			
        ]);
		
		
	}
	
	
	
	
	
	
	
	

    /**
     * Finds the DriversModel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DriversModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DriversModel::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
