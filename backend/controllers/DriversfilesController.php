<?php

namespace backend\controllers;

use Yii;
use app\models\Driversfiles;

use common\models\DriversModel;

use backend\models\DriversfilesSearchModel;
use yii\web\Controller;

use yii\web\UploadedFile;

use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * DriversfilesController implements the CRUD actions for Driversfiles model.
 */
class DriversfilesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
		
			[
				   'class' => AccessControl::className(),
				   'only' => ['index','create', 'view', 'update', 'delete'],
				   'rules' => [
						   [
								   'actions' => ['index','create', 'update', 'delete'],
								   'allow' => true,
								   'roles' => ['admin'],
						   ],
						   [
								   'actions' => [
												'index', 
												'view',
												'create',
												],
								   'allow' => true,
								   'roles' => ['manager', 'payer'],
						   ],
						   
						   [
								   'actions' => [
												'index', 
												'view',
												],
								   'allow' => true,
								   'roles' => ['user'],
						   ],
				   ],

		   ],
			
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'create', 'view', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
		
		
		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Driversfiles models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DriversfilesSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Driversfiles model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
		
		//Водитель
		$model_driver = DriversModel::find()->orderBy('first_name ASC')->all();
		
		foreach ($model_driver as $value) {
			$arrDrivers[$value->id_driver] = $value->first_name.' '.$value->middle_name.' '.$value->last_name;
		}
		
		
        return $this->render('view', [
            'model' => $this->findModel($id),
			'arrDrivers' => $arrDrivers,
        ]);
    }

    /**
     * Creates a new Driversfiles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id = '')
    {
		
		//Водитель
		$model_driver = DriversModel::find()->orderBy('first_name ASC')->all();
		
		foreach ($model_driver as $value) {
			$arrDrivers[$value->id_driver] = $value->first_name.' '.$value->middle_name.' '.$value->last_name;
		}
		
		
		
		
		
        $model = new Driversfiles();

		
		
		if ($model->load(Yii::$app->request->post())) {
            		
					
			$t_m = 	$model;	
					
					
			$t_m->file_f = UploadedFile::getInstance($t_m,'file_f');
			
			$fileName = $t_m->file_f->name;
			
			$post = Yii::$app->request->post();
			
			$post['Driversfiles']['file_f'] = $t_m->file_f->name;
			
			$model->file_f->saveAs('uploads/'.$t_m->file_f->baseName.'.'.$t_m->file_f->extension);
			
			
			$model->file = $t_m->file_f->name;
			
			
			$model->save(false);
			
			return $this->redirect(['drivers/view', 'id' => $model->id_driver]);
		}
		
		
        return $this->render('create', [
            'model' => $model,
			'arrDrivers' => $arrDrivers,
        ]);
    }

    /**
     * Updates an existing Driversfiles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		
		
		//Водитель
		$model_driver = DriversModel::find()->orderBy('first_name ASC')->all();
		
		foreach ($model_driver as $value) {
			$arrDrivers[$value->id_driver] = $value->first_name.' '.$value->middle_name.' '.$value->last_name;
		}
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_file]);
        }

        return $this->render('update', [
            'model' => $model,
			'arrDrivers' => $arrDrivers,
        ]);
    }

    /**
     * Deletes an existing Driversfiles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

	
	public function actionMultipledelete()
	{
		$pk = Yii::$app->request->post('row_id');
		
		

		foreach ($pk as $key => $value) {
			$sql = "DELETE FROM drivers_files WHERE id_file = $value";
			$query = Yii::$app->db->createCommand($sql)->execute();
		}

		return $this->redirect(['index']);

	} 
	
	
	
    /**
     * Finds the Driversfiles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Driversfiles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Driversfiles::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
