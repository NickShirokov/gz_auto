<?php

namespace backend\controllers;

use Yii;
use app\models\Payments;
use backend\models\PaymentsSearchModel;


use app\models\Contracts;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


use app\models\Ourpenalties;
use app\models\Pddpenalties;
use app\models\Prostoys;
use app\models\Logitems;

use common\models\DriversModel;
use common\models\AutomobilesNewModel;


use DateTime;

use yii\web\UploadedFile;

use yii\filters\AccessControl;

/**
 * PaymentsController implements the CRUD actions for Payments model.
 */
class PaymentsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
		
			
			[
				   'class' => AccessControl::className(),
				   'only' => ['index', 
										'create', 
										'view', 
										'update', 
										'delete',
										'import',
										'multipledelete',
										'removeall',],
				   'rules' => [
						   [
								   'actions' => [
												'index', 
												'create', 
												'view', 
												'update', 
												'delete',
												'import',
												'multipledelete',
												'removeall',
												],
								   'allow' => true,
								   'roles' => ['admin'],
						   ],
						   
						   
						   [
								   'actions' => [
												'index', 
												'view',
												],
								   'allow' => true,
								   'roles' => ['user', 'manager'],
						   ],
						   
						   [
								   'actions' => [
												'index', 
												'view',
												'create', 
												'update',
												'delete',
												],
								   'allow' => true,
								   'roles' => ['payer'],
						   ],
						   
						   
				   ],

		   ],
		   
		   
			
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
										'logout', 
										'index', 
										'create', 
										'view', 
										'update', 
										'delete',
										'import',
										'multipledelete',
										'removeall',
									
									],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
		
		
		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Payments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentsSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Payments model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
		$searchModel = new PaymentsSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        return $this->render('view', [
            'model' => $this->findModel($id),
			'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Payments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($contract_id = '')
    {
        $model = new Payments();
		
		//Водитель
		$model_contracts = Contracts::find()
							->joinWith(['owners', 'drivers', 'automobiles'])
							
							->orderBy('drivers.last_name ASC')
							->all();

		
		foreach ($model_contracts as $value) {
			
			$f_name = $value->drivers[0]->first_name;
			
			$m_name = $value->drivers[0]->middle_name;
			
			$l_name = $value->drivers[0]->last_name;
			
			
			$mark = $value->automobiles[0]->mark;
			
			$mdl = $value->automobiles[0]->model;
			
			$gosnomer = $value->automobiles[0]->gosnomer;
			
			
			
			$arrContracts[$value->id_contract] = 'Договор №'.$value->id_contract
							.' '.$l_name
							.' '.$f_name
							.' '.$m_name	
							.' '.$mark
							.' '.$mdl
							.' '.$gosnomer;
			
		}
		
		
		//print_r (Yii::$app->request->post());
		
		
	//	exit;
		
		
		
		
		
	
        if ($model->load(Yii::$app->request->post()) ) {
			
	
			$post = Yii::$app->request->post();
			
			
			
			
			if ($post['Payments']['pmnt_variant'] == 1) {
				
				$model->arenda = $post['Payments']['summa'];
			}
			
			
			if ($post['Payments']['pmnt_variant'] == 2) {
				
				$model->pay_dogovor = $post['Payments']['summa'];
			}
			
			
			if ($post['Payments']['pmnt_variant'] == 3) {
				
				$model->pay_ourpen = $post['Payments']['summa'];
			}
			
			
			if ($post['Payments']['pmnt_variant'] == 5) {
				
				$model->prostoy_summa = $post['Payments']['summa'];
			}
			
			
			
			//ID  штрафа ПДД
			$id_pddshtraf = $post['Payments']['id_pdd_shtraf'];
				
	
			$modelLogitems = new Logitems ();
			
			$user_id = Yii::$app->user->identity->id;
			
			//Yii::$app->authManager->getRolesByUser($user_id);
			$type_user = current(Yii::$app->authManager->getRolesByUser( Yii::$app->user->identity->id))->name;
			$modelLogitems->type_user = $type_user;
			$modelLogitems->user_id = $user_id;
			$modelLogitems->type_page = 'payment';
			$modelLogitems->item_id = $model->id_payment;
			$modelLogitems->date_create = date('Y-m-d');
						
			$modelLogitems->save();
			

			if (!empty($id_pddshtraf)) {
				
				//$summa = 0;
				
				$model_pdd_op = Pddpenalties::findOne($id_pddshtraf);
				
				//$model_pdd_op->type_opl = 2;
				$summa = $model_pdd_op['summa_opl'];	
				
				$model_cntrct = Contracts::findOne($model->contract_id);
	
				$pen_com = $model_cntrct['penalty_comission']; 
				$dsc_type = $model_cntrct['discount_type']; 
				
				
				$date_raznost = date_diff(new DateTime($model_pdd_op->date_post), new DateTime())->days;
				
				
				if ( $date_raznost <= 19 ) {
				
					//skidka_status
				
					//Если есть галочка скидка, то ебашим скидку 50%
					if ($model_pdd_op->skidka_status == 0) {
						if ($dsc_type == 1) {					
							$summa = $summa / 2;
						}
					}
					
				} elseif ($date_raznost >= 59) {
					
					$summa = $summa * 2;
					
				} else {
					//
				}
				
				//Если есть галочка комиссия, то добавляем 50 рублей
				if ($pen_com == 1) {
					
					$summa = $summa + 50;
				}
				
				
				
				
				$pmnts_op = Payments::find()->where(['id_pdd_shtraf' => $id_pddshtraf])->all(); 
			
				$vnes_summa = 0;
				
				
				if (!empty($pmnts_op)) {
					
					foreach ($pmnts_op as $item) {
						$vnes_summa = $vnes_summa + $item->pay_pdd;
					}
		
				}
				
				
				
				$ostatok = $summa - $vnes_summa;
				
				

			
				if ($vnes_summa == 0) {
					$model_pdd_op->type_opl = 1;
	
					$model_pdd_op->save();
			
				} elseif ($vnes_summa > 0 && $vnes_summa < $summa) {
					
					$model_pdd_op->type_opl = 4;
	
					$model_pdd_op->save();
							
				} else {
					
					$model_pdd_op->type_opl = 2;
	
					$model_pdd_op->save();
					
				}
				
	
	
				
			}
			
	
	
	
	
	
			//echo '<pre>';
				
			
			if (!empty($id_prostoy)) { 
			
				$model_prsts = Prostoys::findOne($id_prostoy);
			
				$ful_summa = $model_prsts->summa_prostoy * $model_prsts->days_prostoy;
				
				$ostatok = 0;
				
				
				$pmnts_op = Payments::find()->where(['p_id_prostoy' => $id_prostoy])->all(); 
			
				$vnes_summa = 0;
				
				
				if (!empty($pmnts_op)) {
					
					foreach ($pmnts_op as $item) {
						$vnes_summa = $vnes_summa + $item->prostoy_summa;
					}
		
				}
					
				
				
				$ostatok = $ful_summa - $vnes_summa;
				
			
				if ($vnes_summa == 0 && $ful_summa > 0) {
					$model_prsts->type_opl = 1;
	
					$model_prsts->save();
			
				} elseif ($vnes_summa >= 0 && $vnes_summa < $ful_summa) {
					
					$model_prsts->type_opl = 3;
	
					$model_prsts->save();
							
				} elseif ($vnes_summa == 0 && $ful_summa == 0) {
					
					$model_prsts->type_opl = 2;
	
					$model_prsts->save();
					
				} else {
					$model_prsts->type_opl = 2;
	
					$model_prsts->save();
				}
					
			}
			
			
			
			
			//ID нашего штрафа
			$id_shtraf = $post['Payments']['id_ourpen'];
			
			
			
			//ID простоя
			//$id_prostoy = $post['Payments']['p_id_prostoy'];
		
			if (!empty($id_shtraf) && is_int($id_shtraf)) { 
		
				$model->pay_ourpen =  $post['Payments']['summa'];
	
			}
	
			$model->save();
			
			
		
		
			
			
			
			if (!empty($id_shtraf)) {
				
				$model_op = Ourpenalties::findOne($id_shtraf);
				
				$ful_summa = $model_op->pen_summa;
				
				$ostatok = 0;
	
				$pmnts_op = Payments::find()->where(['id_ourpen' => $id_shtraf])->all(); 
			
				$vnes_summa = 0;
			
				if (!empty($pmnts_op)) {
					
					foreach ($pmnts_op as $item) {
						$vnes_summa = $vnes_summa + $item->pay_ourpen;
					}
		
				}
			
				$ostatok = $ful_summa - $vnes_summa;
			
				if ($vnes_summa == 0) {
					$model_op->status_o_pen = 1;
	
					
			
				} elseif ($vnes_summa > 0 && $vnes_summa < $ful_summa) {
					
					$model_op->status_o_pen = 4;
	
					
							
				} else {
					
					$model_op->status_o_pen = 2;
	
				}
				
			
				
				
				$model_op->save();
				
				
			}

	
			
			
			
			
			
			
			
			
			
            return $this->redirect(['view', 'id' => $model->id_payment]);
        }

        return $this->render('create', [
            'model' => $model,
			'arrContracts' => $arrContracts,
			'arrShtrafs' => [],
			'arrPddShtrafs' => [],
			'arrProstoys' => [],
        ]);
    }

    /**
     * Updates an existing Payments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		
		
        if (Yii::$app->request->post())  {
			
			
			$model->arenda = '';
			$model->pay_dogovor = '';
			$model->id_pdd_shtraf = '';
			$model->pay_pdd = '';
			$model->id_ourpen = '';
			$model->id_ourpen = '';
			$model->pay_ourpen = '';
			$model->prostoy_summa = '';
			$model->prostoy_days = '';
			
			
			$model->load(Yii::$app->request->post());
			
			$post = Yii::$app->request->post();
		
			$model->prostoy_summa =  $post['Payments']['summa'];
			
			if ($post['Payments']['pmnt_variant'] == 1) {
				
				$model->arenda = $post['Payments']['summa'];
			}
			
			
			if ($post['Payments']['pmnt_variant'] == 2) {
				
				$model->pay_dogovor = $post['Payments']['summa'];
			}
			
			
			if ($post['Payments']['pmnt_variant'] == 3) {
				
				$model->pay_ourpen = $post['Payments']['summa'];
			}
			
			
			if ($post['Payments']['pmnt_variant'] == 5) {
				
				$model->prostoy_summa = $post['Payments']['summa'];
			}
			
			
			
			
	
			//$post = Yii::$app->request->post();
			
			//ID нашего штрафа
			$id_shtraf = $post['Payments']['id_ourpen'];
			
			
			
			if (!empty($id_shtraf)) {
				
				$model_op = Ourpenalties::findOne($id_shtraf);
				
				$model_op->status_o_pen = 2;
	
				$model_op->save();
			}

	
			
			//ID  штрафа ПДД
			$id_pddshtraf = $post['Payments']['id_pdd_shtraf'];
				
			$model->id_pdd_shtraf = $id_pddshtraf;	
			
				
		
			if (!empty($id_pddshtraf)) {
				
				$model->id_pdd_shtraf = $id_pddshtraf;	
				
				
				$model->pay_pdd = $post['Payments']['summa'];
				
				$model_pdd_op = Pddpenalties::findOne($id_pddshtraf);
				
				$model_pdd_op->type_opl = 2;
	
				$model_pdd_op->save();
			}
			
			
			
			
			//ID  простоя
			$id_prostoy = $post['Payments']['p_id_prostoy'];
				
		
				
			if (!empty($id_prostoy)) { 
			
				$model_prsts = Prostoys::findOne($id_prostoy);
			
				$ful_summa = $model_prsts->summa_prostoy * $model_prsts->days_prostoy;
				
				$ostatok = 0;
				
				
				$pmnts_op = Payments::find()->where(['p_id_prostoy' => $id_prostoy])->all(); 
			
				$vnes_summa = 0;
				
				
				if (!empty($pmnts_op)) {
					
					foreach ($pmnts_op as $item) {
						$vnes_summa = $vnes_summa + $item->prostoy_summa;
					}
		
				}
					
				
				
				$ostatok = $ful_summa - $vnes_summa;
				
				

			
				if ($vnes_summa == 0 && $ful_summa > 0) {
					$model_prsts->type_opl = 1;
	
					$model_prsts->save();
			
				} elseif ($vnes_summa >= 0 && $vnes_summa < $ful_summa) {
					
					$model_prsts->type_opl = 3;
	
					$model_prsts->save();
							
				} elseif ($vnes_summa == 0 && $ful_summa == 0) {
					
					$model_prsts->type_opl = 2;
	
					$model_prsts->save();
					
				} else {
					$model_prsts->type_opl = 2;
	
					$model_prsts->save();
				}
					
			}
			
			$model->save();
			
			
			
            return $this->redirect(['view', 'id' => $model->id_payment]);
        }
		
		
		//ПРостои
		$prostoys = Prostoys::find()->where(['contract_id' => $model->contract_id, 'type_opl' => 1])->all();  
		
		
		$arrProstoys = [];
		
		foreach ($prostoys as $value) {
			
			$summa = $value->summa_prostoy * $value->days_prostoy;
			$arrProstoys[$value->id_prostoy] = 'Договор '.$value->contract_id.' '.$summa.' за '.$value->days_prostoy.' дней';
			
		}
		
		
		
		
		
		//Наши штрафы этого договора
		$shtrafs = Ourpenalties::find()->where(
				[
					'p_contract_id' => $model->contract_id,
					'status_o_pen' => 1,
				
				])->all();
		

		$arrShtrafs = [];
		
		foreach ($shtrafs as $value) {
			
			$arrShtrafs[$value->id_penalty] = '№'.$value->id_penalty
				.' '.$value->pen_summa.' рублей'
				.' '.$value->date_pen
				.' '.$value->pen_comment;
		}
		
		
		//Проверочка на оплаченный штраф
		$id_ourpen = $model['id_ourpen'];
		
		
		
		//Наши штрафы этого договора
		$shtraf_oplachen = Ourpenalties::find()->where(
				[
					'id_penalty' => $id_ourpen,
					'status_o_pen' => 2,
				
				])->all();
		
		if (!empty($shtraf_oplachen)) {
			
			$arrShtrafs = [];
			
			
			$arrShtrafs[$shtraf_oplachen[0]->id_penalty] = '№'.$shtraf_oplachen[0]->id_penalty
				.' '.$shtraf_oplachen[0]->pen_summa.' рублей'
				.' '.$shtraf_oplachen[0]->date_pen
				.' '.$shtraf_oplachen[0]->pen_comment. ' ОПЛАЧЕН!';
			
			
			
		}
		
		
		
		
		
		//ПДД штрафы этого договора
		$pddshtrafs = Pddpenalties::find()->where(
				[
					'contract_id' => $model->contract_id,
					'type_opl' => 1,
				
				])->all();
				
				
		$arrPddShtrafs = [];	
				
		foreach ($pddshtrafs as $value) {
			
			$arrPddShtrafs[$value->id_shtraf] = '№'.$value->id_shtraf
				.' '.$value->n_post
				.' '.$value->date_post
			
				.' '.$value->summa_opl.' рублей'
				.' '.$value->comment
				;
		}
		
		
		
		
		
		
		//Водитель
		$model_contracts = Contracts::find()->joinWith(['owners', 'drivers', 'automobiles'])->all();

		
		
		
		
		foreach ($model_contracts as $value) {
			
			$f_name = $value->drivers[0]->first_name;
			
			$m_name = $value->drivers[0]->middle_name;
			
			$l_name = $value->drivers[0]->last_name;
			
			
			$mark = $value->automobiles[0]->mark;
			
			$mdl = $value->automobiles[0]->model;
			
			$gosnomer = $value->automobiles[0]->gosnomer;
			
			
			
			$arrContracts[$value->id_contract] = 'Договор №'.$value->id_contract
							.' '.$f_name
							.' '.$m_name
							.' '.$l_name
							.' '.$mark
							.' '.$mdl
							.' '.$gosnomer;
		}
		
		
		

        return $this->render('update', [
            'model' => $model,
			'arrContracts' => $arrContracts,
			'arrShtrafs' => $arrShtrafs,
			'arrPddShtrafs' => $arrPddShtrafs,
			'arrProstoys' => $arrProstoys,
        ]);
    }

    /**
     * Deletes an existing Payments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

	
	
	public function actionMultipledelete()
	{
		$pk = Yii::$app->request->post('row_id');
		
		

		foreach ($pk as $key => $value) {
			$sql = "DELETE FROM payments WHERE id_payment = $value";
			$query = Yii::$app->db->createCommand($sql)->execute();
		}

		return $this->redirect(['index']);

	} 
	
	
	
	public function actionRemoveall()
	{
		//$pk = Yii::$app->request->post('row_id');
		
		

		//foreach ($pk as $key => $value) {
		$sql = "DELETE FROM payments;";
		$query = Yii::$app->db->createCommand($sql)->execute();
		//}

		return $this->redirect(['index']);

	} 
	
	
	
	
	
	
	
	public function actionImport()
    {
	
		$model = new Payments();
		

		if ($model->load(Yii::$app->request->post())) { 
		
		
			$t_m = 	$model;	
					
					
			$t_m->file_f = UploadedFile::getInstance($t_m,'file_f');
			
			$fileName = $t_m->file_f->name;
			
			
			//$post = Yii::$app->request->post();
			
			//$post['Driversfiles']['file_f'] = $t_m->file_f->name;
			
			$model->file_f->saveAs('uploads/'.$t_m->file_f->baseName.'.'.$t_m->file_f->extension);
			
			
			
			$file_path = 'uploads/'.$t_m->file_f->baseName.'.'.$t_m->file_f->extension;
			
			
			
			$filename = $file_path;

			// The nested array to hold all the arrays
			$the_big_array = []; 

			// Open the file for reading
			if (($h = fopen("{$filename}", "r")) !== FALSE) 
			{
			  // Each line in the file is converted into an individual array that we call $data
			  // The items of the array are comma separated
			  while (($data = fgetcsv($h, 1000, ";")) !== FALSE) 
			  {
				// Each individual array is being pushed into the nested array
				$the_big_array[] = $data;		
			  }

			  // Close the file
			  fclose($h);
			}

			
			unset ($the_big_array[0]);
	
			
			$status_arr = [
							
								'Наличные' => 'Наличные',
								'На карту' => 'На карту',
								'Списано с диспетчерской' => 'Списано с диспетчерской',
								'Бонус/Простили' => 'Бонус/Простили',
								
							
							];
							
			$type_arr = [
							'Арендный платеж' => '1',
							'Платеж по договору' => '2',				
							'Наши штрафы по договору' => '3',
							'Штраф ПДД (строго)' => '4',
							'Простой' => '5',							
						];				
							
							
			
		
			
			foreach ($the_big_array as $item) {
				
				
				$id_payment = $item[0];
				
				$date = date('Y-m-d',strtotime($item[1]));
				
				$fio = trim($item[2]);
				
				$gosnomer = trim($item[3]);
				
				$type_opl = trim($status_arr[$item[4]]);
				
				
				
				
				$summa = floatval ($item[6]);
				
				$pmnt_type = trim($type_arr[$item[5]]);
				
				
				
				
				$comment = trim($item[7]);
				
				
				// Фио
				
				if (!empty($fio)) {
				
					$fio_arr = explode(' ', $fio);
					
					
					if ($fio_arr[0]) {
						$last_name = $fio_arr[0];
					} 
					
					$first_name = '';
					
					if ($fio_arr[1]) {
						$first_name = $fio_arr[1];
					}
					
					$middle_name = '';
					
					if ($fio_arr[2]) {
						$middle_name = $fio_arr[2];
					}
						
				}
				
				//Ищем водителя
				
				$models_drivers = DriversModel::find()->where(
										[
											'fio' => $fio, 
											
											
										])->all();
				
				$models_auto = AutomobilesNewModel::find()->where(['gosnomer' => $gosnomer])->all();
				
				
				
				if (!empty($fio) && !empty($models_auto) && !empty($models_drivers)) {
					
					
					
					
					
					$id_auto = $models_auto[0]->id_auto;	
					
					$model_contracts = Contracts::find()->where(['auto_id' => $id_auto])->all();	

					
					//Только если есть договор
					if (!empty($model_contracts)) {
						
						$id_contract = $model_contracts[0]->id_contract;
						
						$model_payments = Payments::find()->where(['id_payment' => $id_payment])->all();
						
						
						
						
						if (empty($model_payments)) {
						
							$model = new Payments;
													
							$model->id_payment = $id_payment;
						
							$model->contract_id = $id_contract;
							$model->date_payment = $date;
							$model->pmnt_variant = $pmnt_type;
							
							$n_post = '';
							
							//ПРобуем добавить создание штрафов
							
							if ($pmnt_type == 3) {
								
								//$model_ourpens = new Ourpenalties();
								
								$model->pay_ourpen = $summa;
								//$model_ourpens->
								
								
							} elseif ($pmnt_type == 4) {
								
								
								//Пробуем найти штраф ПДД и выставить его
								$n_post = trim(str_replace("№", '', $comment)); 
								if (!empty($n_post)) {
								
									$pdd_pens = Pddpenalties::find()->where(['n_post' => $n_post])->all();
									
									if (!empty($pdd_pens[0]->id_shtraf)) {
										
										$model->id_pdd_shtraf = $pdd_pens[0]->id_shtraf;
										
										$model->pay_pdd = $summa;
										$pdd_pens[0]->type_opl = 2;
										
										$pdd_pens[0]->save();
									}
									
								} 
								
							} 
							
							
							
							
							$model->type = $type_opl;
							$model->summa = $summa;
							
							if ($pmnt_type == 1) {
								$model->arenda = $summa;
							} elseif ($pmnt_type == 2) {
								$model->pay_dogovor = $summa;
							} 
							
							
							
							
							$model->comment = $comment;
							
									
							$model->save(false);
						
						} else {
							
							
							
							
							$model = $this->findModel($id_payment);
							
							$model->id_payment = $id_payment;
							
							$model->contract_id = $id_contract;
							$model->date_payment = $date;
							$model->pmnt_variant = $pmnt_type;
							
							$model->type = $type_opl;
							$model->summa = $summa;
							
							
							if ($pmnt_type == 3) {
								
								//$model_ourpens = new Ourpenalties();
								
								
								//$model_ourpens->
								
								$model->pay_ourpen = $summa;
								
								
							} elseif ($pmnt_type == 4) {
								
								
								//Пробуем найти штраф ПДД и выставить его
								$n_post = trim(str_replace("№", '', $comment)); 
								if (!empty($n_post)) {
								
									$pdd_pens = Pddpenalties::find()->where(['n_post' => $n_post])->all();
									
									if (!empty($pdd_pens[0]->id_shtraf)) {
										
										$model->id_pdd_shtraf = $pdd_pens[0]->id_shtraf;
										$model->pay_pdd = $summa;
										
										$pdd_pens[0]->type_opl = 2;
										
										$pdd_pens[0]->save();
									}
									
								} 
								
							}
							
							
							
							
							
							
							
							if ($pmnt_type == 1) {
								$model->arenda = $summa;
							} elseif ($pmnt_type == 2) {
								$model->pay_dogovor = $summa;
							}
							
							
							$model->comment = $comment;
						
							$model->save(false);
						}
						
					}
					
					
					
				}	
				
				
				
			}
			
			return $this->redirect(['index']);
	
		}
	
		return $this->render('import', [
            'model' => $model,
			
        ]);
	
	
	}
	
	
    /**
     * Finds the Payments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Payments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Payments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
