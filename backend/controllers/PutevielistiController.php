<?php

namespace backend\controllers;

use Yii;
use app\models\Putevielisti;
use app\models\Contracts;


use common\models\DriversModel;
use common\models\AutomobilesNewModel;


use backend\models\PutevielistiSearchModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


use app\models\Licen;

use DateTime;
use app\models\Razresheniya;
use yii\filters\AccessControl;
/**
 * PutevielistiController implements the CRUD actions for Putevielisti model.
 */
class PutevielistiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
		
			//Доступ только для админа 
			[
				'class' => AccessControl::className(),
				'only' => ['index', 
										'create',
										'view',
										'update',
										'delete',
										'week', 
										'month', 
										'putik',
										'put',
										'putevoy',],
				'rules' => [
					[
						'actions' => ['index', 
										'create',
										'view',
										'update',
										'delete',
										'week', 
										'month', 
										'putik',
										'put',
										'putevoy',],
						'allow' => true,
						'roles' => ['admin'],
					],
					[
					   'actions' => [
									'index', 
									'view',
									],
					   'allow' => true,
					   'roles' => ['user'],
				   ],
				   
				   [
						'actions' => ['index', 
										'create',
										'view',
										'update',
										'delete',
										
										'week', 
										'month', 
										'putik',
										'put',
										'putevoy',],
						'allow' => true,
						'roles' => ['manager', 'payer'],
					],
				   
				   
				],
				
			],
			
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
										'logout', 
										'index', 
										'create',
										'view',
										'update',
										'delete',
										'week', 
										'month', 
										'putik',
										'put',
										'putevoy',
										],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
		
		
		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Putevielisti models.
     * @return mixed
     */
	 
	 
	 
	 
	 public function actionPutik ($contract_id, $put_type) {
		

				
				
				$date_raznost = 7;
				
				
				//Взять тип
				if (!empty($item['type'])) {
					$put_type = 2;
					$date_raznost = 30;
				}

				$model_contract = Contracts::findOne($contract_id);
				
				
				//Машина
				$auto = AutomobilesNewModel::find()->where(['id_auto' => $model_contract->auto_id])->all();
				
				
				$mark_model = $auto[0]->mark.' '.$auto[0]->model;
				
				
				$gosnomer = $auto[0]->gosnomer;
				
				
				$lic_id  = $auto[0]['licen_id'];
				
				//$razresh_id = $auto[0]['razresh_id'];
				
				$lic_arr = Licen::findOne($lic_id);
				
			
				
				$name_lic = $lic_arr['name_lic'];
				$ogrn = $lic_arr['ogrn'];
				$inn = $lic_arr['inn'];
				$okpo = $lic_arr['okpo'];
				
				
				$mech = $lic_arr['mechanic'];
				$medic = $lic_arr['medic'];
				
				
				//Водитель
				$driver = DriversModel::find()->where(['id_driver' => $model_contract->driver_id])->all();	
				
				$fio_driver = $driver[0]->last_name.' '.$driver[0]->first_name.' '.$driver[0]->middle_name;
				//print_r ($driver[0]);
				

				$razresh = $auto[0]->razresh_text; 
				
				$udostov = $driver[0]->lic_series.' '.$driver[0]->lic_number.' '.$driver[0]->lic_type;
				
				
				
				
				
				
				
				
		
		/*
		if ($put_type == 2) { 
			$date_raznost = 30;
		} 
		*/
		
		for ($i = 0; $i<= $date_raznost; $i++ ) {
		
		
		
		//$date = new DateTime('2006-12-12');
		//$date->modify('+1 day');
		
		if ($put_type == 1) {
		
			$date = new DateTime();
			$date->modify('+'.$i.' day');
			$today = $date->format("d.m.Y");
			
			$year = $date->format("Y");
			
			
			$time1 = '00 ч. 10 мин';
			$time2 = '00 ч. 20 мин';
			$time3 = '00 ч. 15 мин';
			$time4 = '08 ч. 10 мин';
			$time5 = '08 ч. 20 мин';
			$time6 = '08 ч. 15 мин';
			$time7 = '16 ч. 10 мин';
			$time8 = '16 ч. 20 мин';
			$time9 = '16 ч. 15 мин';
			
			
			$html.= '
		
		<!DOCTYPE html >
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8" />
</head>

<body style="margin: 0;">

<div id="p1" style="overflow: hidden; position: relative; background-color: white; width: 909px; height: 1286px;">

<!-- Begin shared CSS values -->
<style class="shared-css" type="text/css" >
.t {
	-webkit-transform-origin: bottom left;
	-ms-transform-origin: bottom left;
	transform-origin: bottom left;
	-webkit-transform: scale(0.25);
	-ms-transform: scale(0.25);
	transform: scale(0.25);
	z-index: 2;
	position: absolute;
	white-space: pre;
	overflow: visible;
	line-height: 1.5;
}
</style>
<!-- End shared CSS values -->


<!-- Begin inline CSS -->
<style type="text/css" >

#t1_1{left:29px;bottom:1263px;letter-spacing:0.3px;word-spacing:1.7px;}
#t2_1{left:531px;bottom:1263px;}
#t3_1{left:591px;bottom:1263px;}
#t4_1{left:691px;bottom:1263px;}
#t5_1{left:493px;bottom:1251px;}
#t6_1{left:648px;bottom:1251px;letter-spacing:0.8px;}
#t7_1{left:361px;bottom:1211px;letter-spacing:0.4px;}
#t8_1{left:591px;bottom:1188px;letter-spacing:-0.3px;word-spacing:1.5px;}
#t9_1{left:29px;bottom:1165px;letter-spacing:0.3px;}
#ta_1{left:29px;bottom:1120px;letter-spacing:0.7px;word-spacing:0.8px;}
#tb_1{left:339px;bottom:1120px;letter-spacing:0.3px;}
#tc_1{left:29px;bottom:1097px;letter-spacing:0.4px;word-spacing:0.4px;}
#td_1{left:648px;bottom:1097px;letter-spacing:-0.2px;word-spacing:0.2px;}
#te_1{left:29px;bottom:1073px;letter-spacing:0.2px;}
#tf_1{left:648px;bottom:1074px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tg_1{left:29px;bottom:1010px;letter-spacing:0.1px;}
#th_1{left:29px;bottom:987px;letter-spacing:-0.2px;}
#ti_1{left:29px;bottom:965px;letter-spacing:0.3px;}
#tj_1{left:29px;bottom:944px;letter-spacing:0.5px;}
#tk_1{left:257px;bottom:944px;}
#tl_1{left:336px;bottom:944px;letter-spacing:0.7px;}
#tm_1{left:424px;bottom:944px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tn_1{left:29px;bottom:921px;letter-spacing:0.5px;word-spacing:-0.5px;}
#to_1{left:193px;bottom:921px;letter-spacing:-0.2px;}
#tp_1{left:336px;bottom:921px;letter-spacing:0.7px;word-spacing:-0.7px;}
#tq_1{left:29px;bottom:876px;letter-spacing:0.1px;word-spacing:-0.1px;}
#tr_1{left:29px;bottom:853px;letter-spacing:0.4px;}
#ts_1{left:257px;bottom:853px;}
#tt_1{left:270px;bottom:853px;letter-spacing:0.2px;word-spacing:1.5px;}
#tu_1{left:29px;bottom:829px;letter-spacing:0.7px;}
#tv_1{left:114px;bottom:829px;}
#tw_1{left:257px;bottom:829px;letter-spacing:0.8px;}
#tx_1{left:29px;bottom:806px;letter-spacing:0.1px;}
#ty_1{left:29px;bottom:783px;letter-spacing:0.4px;}
#tz_1{left:414px;bottom:783px;letter-spacing:0.5px;word-spacing:1.7px;}
#t10_1{left:508px;bottom:781px;letter-spacing:-0.4px;}
#t11_1{left:591px;bottom:783px;}
#t12_1{left:705px;bottom:783px;}
#t13_1{left:749px;bottom:783px;letter-spacing:0.8px;}
#t14_1{left:29px;bottom:759px;letter-spacing:0.2px;}
#t15_1{left:493px;bottom:759px;letter-spacing:0.4px;}
#t16_1{left:648px;bottom:759px;letter-spacing:0.9px;}
#t17_1{left:257px;bottom:744px;letter-spacing:0.2px;}
#t18_1{left:414px;bottom:744px;letter-spacing:-0.2px;}
#t19_1{left:494px;bottom:719px;letter-spacing:-0.2px;}
#t1a_1{left:749px;bottom:721px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1b_1{left:648px;bottom:699px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1c_1{left:29px;bottom:653px;letter-spacing:0.3px;word-spacing:1px;}
#t1d_1{left:493px;bottom:653px;letter-spacing:0.3px;word-spacing:0.4px;}
#t1e_1{left:29px;bottom:631px;letter-spacing:0.2px;word-spacing:1.3px;}
#t1f_1{left:294px;bottom:631px;}
#t1g_1{left:591px;bottom:631px;letter-spacing:0.3px;}
#t1h_1{left:749px;bottom:631px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1i_1{left:29px;bottom:609px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1j_1{left:591px;bottom:609px;letter-spacing:0.5px;word-spacing:-0.1px;}
#t1k_1{left:493px;bottom:586px;letter-spacing:0.2px;}
#t1l_1{left:29px;bottom:562px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t1m_1{left:591px;bottom:562px;letter-spacing:-0.2px;}
#t1n_1{left:688px;bottom:562px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1o_1{left:493px;bottom:539px;letter-spacing:0.3px;}
#t1p_1{left:680px;bottom:539px;letter-spacing:0.5px;}
#t1q_1{left:802px;bottom:539px;}
#t1r_1{left:29px;bottom:494px;letter-spacing:0.4px;}
#t1s_1{left:306px;bottom:494px;word-spacing:5.9px;}
#t1t_1{left:591px;bottom:494px;letter-spacing:0.3px;word-spacing:1.4px;}
#t1u_1{left:749px;bottom:471px;word-spacing:2.3px;}
#t1v_1{left:29px;bottom:449px;letter-spacing:0.6px;}
#t1w_1{left:74px;bottom:449px;}
#t1x_1{left:193px;bottom:449px;letter-spacing:0.8px;}
#t1y_1{left:29px;bottom:426px;letter-spacing:0.2px;word-spacing:0.8px;}
#t1z_1{left:493px;bottom:426px;letter-spacing:0.3px;word-spacing:0.5px;}
#t20_1{left:29px;bottom:404px;letter-spacing:0.2px;}
#t21_1{left:336px;bottom:404px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t22_1{left:493px;bottom:404px;}
#t23_1{left:273px;bottom:381px;letter-spacing:-0.2px;}
#t24_1{left:336px;bottom:381px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t25_1{left:493px;bottom:381px;letter-spacing:0.2px;}
#t26_1{left:575px;bottom:381px;letter-spacing:0.2px;word-spacing:1.4px;}
#t27_1{left:29px;bottom:358px;letter-spacing:0.2px;word-spacing:1.5px;}
#t28_1{left:114px;bottom:336px;letter-spacing:0.3px;word-spacing:0.3px;}
#t29_1{left:493px;bottom:336px;letter-spacing:0.4px;}
#t2a_1{left:648px;bottom:336px;letter-spacing:0.7px;word-spacing:0.6px;}
#t2b_1{left:29px;bottom:313px;letter-spacing:0.4px;word-spacing:0.4px;}
#t2c_1{left:493px;bottom:313px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t2d_1{left:29px;bottom:290px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2e_1{left:29px;bottom:268px;letter-spacing:0.3px;}
#t2f_1{left:493px;bottom:268px;letter-spacing:0.3px;word-spacing:0.8px;}
#t2g_1{left:193px;bottom:246px;letter-spacing:-0.2px;}
#t2h_1{left:314px;bottom:246px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t2i_1{left:493px;bottom:246px;letter-spacing:0.5px;word-spacing:0.3px;}
#t2j_1{left:493px;bottom:223px;}
#t2k_1{left:749px;bottom:223px;letter-spacing:0.4px;word-spacing:0.7px;}
#t2l_1{left:493px;bottom:178px;letter-spacing:0.3px;}
#t2m_1{left:591px;bottom:155px;letter-spacing:-0.2px;}
#t2n_1{left:688px;bottom:156px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t2o_1{left:411px;bottom:1225px;letter-spacing:-0.4px;}
#t2p_1{left:736px;bottom:1197px;letter-spacing:-0.2px;}
#t2q_1{left:193px;bottom:1143px;letter-spacing:-0.1px;word-spacing:0.1px;}
#t2r_1{left:197px;bottom:944px;letter-spacing:-0.2px;}
#t2s_1{left:197px;bottom:853px;letter-spacing:-0.2px;}
#t2t_1{left:501px;bottom:884px;letter-spacing:0.1px;}
#t2u_1{left:499px;bottom:869px;letter-spacing:0.1px;}
#t2v_1{left:512px;bottom:853px;letter-spacing:0.3px;}
#t2w_1{left:600px;bottom:840px;letter-spacing:0.2px;word-spacing:0.9px;}
#t2x_1{left:641px;bottom:825px;letter-spacing:0.2px;word-spacing:1.5px;}
#t2y_1{left:688px;bottom:809px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2z_1{left:649px;bottom:583px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t30_1{left:789px;bottom:583px;letter-spacing:0.3px;}
#t31_1{left:414px;bottom:1120px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t32_1{left:415px;bottom:1094px;}
#t33_1{left:194px;bottom:1071px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t34_1{left:333px;bottom:1071px;letter-spacing:0.3px;}
#t35_1{left:317px;bottom:1060px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t36_1{left:29px;bottom:1036px;letter-spacing:0.5px;word-spacing:0.8px;}
#t37_1{left:257px;bottom:1034px;letter-spacing:-0.5px;word-spacing:0.5px;}
#t38_1{left:194px;bottom:1118px;letter-spacing:-0.1px;word-spacing:0.1px;}

.s1{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s2{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s3{
	FONT-SIZE: 44px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s4{
	FONT-SIZE: 68.4px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s5{
	FONT-SIZE: 58.4px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

</style>
<!-- End inline CSS -->

<!-- Begin embedded font definitions -->
<style id="fonts1" type="text/css" >

@font-face {
	font-family: Calibri-Bold_d;
	src: url("/admin/css/put/fonts/Calibri-Bold_d.woff") format("woff");
}

@font-face {
	font-family: Calibri_h;
	src: url("/admin/css/put/fonts/Calibri_h.woff") format("woff");
}

</style>
<!-- End embedded font definitions -->

<!-- Begin page background -->
<div id="pg1Overlay" style="width:100%; height:100%; position:absolute; z-index:1; background-color:rgba(0,0,0,0); -webkit-user-select: none;"></div>
<div id="pg1" style="-webkit-user-select: none;"><object width="909" height="1286" data="/admin/css/put/1/1.svg" type="image/svg+xml" id="pdf1" style="width:909px; height:1286px; -moz-transform:scale(1); z-index: 0;"></object></div>
<!-- End page background -->


<!-- Begin text definitions (Positioned/styled in CSS) -->
<div id="t1_1" class="t s1">ПУТЕВОЙ ЛИСТ ЛЕГКОВОГО АВТОМОБИЛЯ</div>
<div id="t2_1" class="t s2">Д-1</div>
<div id="t3_1" class="t s2">№</div>
<div id="t4_1" class="t s2">85</div>
<div id="t5_1" class="t s2">серия</div>
<div id="t6_1" class="t s2">номер</div>
<div id="t7_1" class="t s2">дата</div>
<div id="t8_1" class="t s2">по ОКПО</div>
<div id="t9_1" class="t s2">Организация</div>
<div id="ta_1" class="t s2">Марка автомобиля</div>
<div id="tb_1" class="t s2">Разрешение </div>
<div id="tc_1" class="t s2">Государственный регистрационный знак</div>
<div id="td_1" class="t s3">Номер парковки</div>
<div id="te_1" class="t s2">Водитель</div>
<div id="tf_1" class="t s3">Табельный номер</div>
<div id="tg_1" class="t s4">Предрейсовый</div>
<div id="th_1" class="t s4">Медосмотр</div>
<div id="ti_1" class="t s4">Пройден</div>
<div id="tj_1" class="t s1">дата</div>
<div id="tk_1" class="t s2">г.</div>
<div id="tl_1" class="t s2">Время</div>
<div id="tm_1" class="t s2">'.$time1.'</div>
<div id="tn_1" class="t s1">должность: Врач</div>
<div id="to_1" class="t s2">подпись</div>
<div id="tp_1" class="t s2">'.$medic.'</div>
<div id="tq_1" class="t s2">Выезд с парковки</div>
<div id="tr_1" class="t s2">Дата</div>
<div id="ts_1" class="t s2">г. </div>
<div id="tt_1" class="t s2">Механик/ Диспечер ____'.$mech.'</div>
<div id="tu_1" class="t s2">Время</div>
<div id="tv_1" class="t s2">'.$time2.'</div>
<div id="tw_1" class="t s2">мин</div>
<div id="tx_1" class="t s2">Послерейсовый </div>
<div id="ty_1" class="t s2">медосмотр</div>
<div id="tz_1" class="t s2">дата, время</div>
<div id="t10_1" class="t s5">'.$today.'</div>
<div id="t11_1" class="t s2">г.</div>
<div id="t12_1" class="t s2">'.$time3.'</div>
<div id="t13_1" class="t s2">мин</div>
<div id="t14_1" class="t s2">пройден</div>
<div id="t15_1" class="t s2">дата</div>
<div id="t16_1" class="t s2">время</div>
<div id="t17_1" class="t s2">должность</div>
<div id="t18_1" class="t s2">подпись</div>
<div id="t19_1" class="t s5">Механик</div>
<div id="t1a_1" class="t s2">'.$mech.'</div>
<div id="t1b_1" class="t s3">подпись и расшифровка подписи</div>
<div id="t1c_1" class="t s2">Задание водителю</div>
<div id="t1d_1" class="t s1">Показание одометра, км</div>
<div id="t1e_1" class="t s2">В распоряжение</div>
<div id="t1f_1" class="t s2">Яндекс</div>
<div id="t1g_1" class="t s2">Механик</div>
<div id="t1h_1" class="t s2">'.$mech.'</div>
<div id="t1i_1" class="t s2">Адрес первой подачи</div>
<div id="t1j_1" class="t s2">Автомобиль в исправном состоянии принял</div>
<div id="t1k_1" class="t s2">Водитель</div>
<div id="t1l_1" class="t s2">По указанию Яндекс г. Москва и Московская область</div>
<div id="t1m_1" class="t s2">подпись</div>
<div id="t1n_1" class="t s3">расшифровка подписи</div>
<div id="t1o_1" class="t s2">Горючее</div>
<div id="t1p_1" class="t s2">марка</div>
<div id="t1q_1" class="t s2">код</div>
<div id="t1r_1" class="t s2">дата</div>
<div id="t1s_1" class="t s2">'.$year.' г.</div>
<div id="t1t_1" class="t s2">Движение горючего</div>
<div id="t1u_1" class="t s2">количество, л</div>
<div id="t1v_1" class="t s2">Время </div>
<div id="t1w_1" class="t s2">ч.</div>
<div id="t1x_1" class="t s2">мин</div>
<div id="t1y_1" class="t s2">при заезде на парковку</div>
<div id="t1z_1" class="t s2">Выдано: по заправочному</div>
<div id="t20_1" class="t s2">Диспетчер/механик</div>
<div id="t21_1" class="t s2">'.$mech.'</div>
<div id="t22_1" class="t s2">листу №</div>
<div id="t23_1" class="t s2">подпись</div>
<div id="t24_1" class="t s2">расшифровка подписи</div>
<div id="t25_1" class="t s2">остаток: </div>
<div id="t26_1" class="t s2">при выезде</div>
<div id="t27_1" class="t s2">Ожидание :</div>
<div id="t28_1" class="t s2">Опоздания, ожидания, простои в пути, заезды в гараж </div>
<div id="t29_1" class="t s2">расход</div>
<div id="t2a_1" class="t s2">по норме</div>
<div id="t2b_1" class="t s2">и прочие отметки</div>
<div id="t2c_1" class="t s2">Послерейсовый контроль технического состояния</div>
<div id="t2d_1" class="t s2">Автомобиль сдал</div>
<div id="t2e_1" class="t s2">водитель</div>
<div id="t2f_1" class="t s2">Показания одометра при</div>
<div id="t2g_1" class="t s2">подпись</div>
<div id="t2h_1" class="t s2">расшифровка подписи</div>
<div id="t2i_1" class="t s2">возвращении на парковку,</div>
<div id="t2j_1" class="t s2">км</div>
<div id="t2k_1" class="t s2">дата ; время</div>
<div id="t2l_1" class="t s2">Механик</div>
<div id="t2m_1" class="t s2">подпись</div>
<div id="t2n_1" class="t s3">расшифровка подписи</div>
<div id="t2o_1" class="t s5">'.$today.'</div>
<div id="t2p_1" class="t s2">'.$okpo.'</div>
<div id="t2q_1" class="t s2">'.$name_lic.'<br> ОГРН / ОГРНИП '.$ogrn.' ИНН '.$inn.'</div>
<div id="t2r_1" class="t s2">'.$today.'</div>
<div id="t2s_1" class="t s2">'.$today.'</div>
<div id="t2t_1" class="t s2">Предрейсовый </div>
<div id="t2u_1" class="t s2">(предсменный)</div>
<div id="t2v_1" class="t s2">(контроль)</div>
<div id="t2w_1" class="t s1">Предрейсовый контроль технического состояния</div>
<div id="t2x_1" class="t s1">транспортного средства пройден,</div>
<div id="t2y_1" class="t s1">выезд разрешен</div>
<div id="t2z_1" class="t s5">'.$fio_driver.'</div>
<div id="t30_1" class="t s5"></div>
<div id="t31_1" class="t s2">'.$razresh.'</div>
<div id="t32_1" class="t s5">'.$gosnomer.'</div>
<div id="t33_1" class="t s5">'.$fio_driver.' </div>
<div id="t34_1" class="t s5"></div>
<div id="t35_1" class="t s3">фамилия, имя, отчество</div>
<div id="t36_1" class="t s2">Удостоверение №</div>
<div id="t37_1" class="t s5">'.$udostov.'</div>
<div id="t38_1" class="t s5">'.$mark_model.'</div>

<!-- End text definitions -->


</div>







<div id="p1" style="overflow: hidden; position: relative; background-color: white; width: 909px; height: 1286px;">

<!-- Begin shared CSS values -->
<style class="shared-css" type="text/css" >
.t {
	-webkit-transform-origin: bottom left;
	-ms-transform-origin: bottom left;
	transform-origin: bottom left;
	-webkit-transform: scale(0.25);
	-ms-transform: scale(0.25);
	transform: scale(0.25);
	z-index: 2;
	position: absolute;
	white-space: pre;
	overflow: visible;
	line-height: 1.5;
}
</style>
<!-- End shared CSS values -->


<!-- Begin inline CSS -->
<style type="text/css" >

#t1_1{left:29px;bottom:1263px;letter-spacing:0.3px;word-spacing:1.7px;}
#t2_1{left:531px;bottom:1263px;}
#t3_1{left:591px;bottom:1263px;}
#t4_1{left:691px;bottom:1263px;}
#t5_1{left:493px;bottom:1251px;}
#t6_1{left:648px;bottom:1251px;letter-spacing:0.8px;}
#t7_1{left:361px;bottom:1211px;letter-spacing:0.4px;}
#t8_1{left:591px;bottom:1188px;letter-spacing:-0.3px;word-spacing:1.5px;}
#t9_1{left:29px;bottom:1165px;letter-spacing:0.3px;}
#ta_1{left:29px;bottom:1120px;letter-spacing:0.7px;word-spacing:0.8px;}
#tb_1{left:339px;bottom:1120px;letter-spacing:0.3px;}
#tc_1{left:29px;bottom:1097px;letter-spacing:0.4px;word-spacing:0.4px;}
#td_1{left:648px;bottom:1097px;letter-spacing:-0.2px;word-spacing:0.2px;}
#te_1{left:29px;bottom:1073px;letter-spacing:0.2px;}
#tf_1{left:648px;bottom:1074px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tg_1{left:29px;bottom:1010px;letter-spacing:0.1px;}
#th_1{left:29px;bottom:987px;letter-spacing:-0.2px;}
#ti_1{left:29px;bottom:965px;letter-spacing:0.3px;}
#tj_1{left:29px;bottom:944px;letter-spacing:0.5px;}
#tk_1{left:257px;bottom:944px;}
#tl_1{left:336px;bottom:944px;letter-spacing:0.7px;}
#tm_1{left:424px;bottom:944px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tn_1{left:29px;bottom:921px;letter-spacing:0.5px;word-spacing:-0.5px;}
#to_1{left:193px;bottom:921px;letter-spacing:-0.2px;}
#tp_1{left:336px;bottom:921px;letter-spacing:0.7px;word-spacing:-0.7px;}
#tq_1{left:29px;bottom:876px;letter-spacing:0.1px;word-spacing:-0.1px;}
#tr_1{left:29px;bottom:853px;letter-spacing:0.4px;}
#ts_1{left:257px;bottom:853px;}
#tt_1{left:270px;bottom:853px;letter-spacing:0.2px;word-spacing:1.5px;}
#tu_1{left:29px;bottom:829px;letter-spacing:0.7px;}
#tv_1{left:114px;bottom:829px;}
#tw_1{left:257px;bottom:829px;letter-spacing:0.8px;}
#tx_1{left:29px;bottom:806px;letter-spacing:0.1px;}
#ty_1{left:29px;bottom:783px;letter-spacing:0.4px;}
#tz_1{left:414px;bottom:783px;letter-spacing:0.5px;word-spacing:1.7px;}
#t10_1{left:508px;bottom:781px;letter-spacing:-0.4px;}
#t11_1{left:591px;bottom:783px;}
#t12_1{left:705px;bottom:783px;}
#t13_1{left:749px;bottom:783px;letter-spacing:0.8px;}
#t14_1{left:29px;bottom:759px;letter-spacing:0.2px;}
#t15_1{left:493px;bottom:759px;letter-spacing:0.4px;}
#t16_1{left:648px;bottom:759px;letter-spacing:0.9px;}
#t17_1{left:257px;bottom:744px;letter-spacing:0.2px;}
#t18_1{left:414px;bottom:744px;letter-spacing:-0.2px;}
#t19_1{left:494px;bottom:719px;letter-spacing:-0.2px;}
#t1a_1{left:749px;bottom:721px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1b_1{left:648px;bottom:699px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1c_1{left:29px;bottom:653px;letter-spacing:0.3px;word-spacing:1px;}
#t1d_1{left:493px;bottom:653px;letter-spacing:0.3px;word-spacing:0.4px;}
#t1e_1{left:29px;bottom:631px;letter-spacing:0.2px;word-spacing:1.3px;}
#t1f_1{left:294px;bottom:631px;}
#t1g_1{left:591px;bottom:631px;letter-spacing:0.3px;}
#t1h_1{left:749px;bottom:631px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1i_1{left:29px;bottom:609px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1j_1{left:591px;bottom:609px;letter-spacing:0.5px;word-spacing:-0.1px;}
#t1k_1{left:493px;bottom:586px;letter-spacing:0.2px;}
#t1l_1{left:29px;bottom:562px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t1m_1{left:591px;bottom:562px;letter-spacing:-0.2px;}
#t1n_1{left:688px;bottom:562px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1o_1{left:493px;bottom:539px;letter-spacing:0.3px;}
#t1p_1{left:680px;bottom:539px;letter-spacing:0.5px;}
#t1q_1{left:802px;bottom:539px;}
#t1r_1{left:29px;bottom:494px;letter-spacing:0.4px;}
#t1s_1{left:306px;bottom:494px;word-spacing:5.9px;}
#t1t_1{left:591px;bottom:494px;letter-spacing:0.3px;word-spacing:1.4px;}
#t1u_1{left:749px;bottom:471px;word-spacing:2.3px;}
#t1v_1{left:29px;bottom:449px;letter-spacing:0.6px;}
#t1w_1{left:74px;bottom:449px;}
#t1x_1{left:193px;bottom:449px;letter-spacing:0.8px;}
#t1y_1{left:29px;bottom:426px;letter-spacing:0.2px;word-spacing:0.8px;}
#t1z_1{left:493px;bottom:426px;letter-spacing:0.3px;word-spacing:0.5px;}
#t20_1{left:29px;bottom:404px;letter-spacing:0.2px;}
#t21_1{left:336px;bottom:404px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t22_1{left:493px;bottom:404px;}
#t23_1{left:273px;bottom:381px;letter-spacing:-0.2px;}
#t24_1{left:336px;bottom:381px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t25_1{left:493px;bottom:381px;letter-spacing:0.2px;}
#t26_1{left:575px;bottom:381px;letter-spacing:0.2px;word-spacing:1.4px;}
#t27_1{left:29px;bottom:358px;letter-spacing:0.2px;word-spacing:1.5px;}
#t28_1{left:114px;bottom:336px;letter-spacing:0.3px;word-spacing:0.3px;}
#t29_1{left:493px;bottom:336px;letter-spacing:0.4px;}
#t2a_1{left:648px;bottom:336px;letter-spacing:0.7px;word-spacing:0.6px;}
#t2b_1{left:29px;bottom:313px;letter-spacing:0.4px;word-spacing:0.4px;}
#t2c_1{left:493px;bottom:313px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t2d_1{left:29px;bottom:290px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2e_1{left:29px;bottom:268px;letter-spacing:0.3px;}
#t2f_1{left:493px;bottom:268px;letter-spacing:0.3px;word-spacing:0.8px;}
#t2g_1{left:193px;bottom:246px;letter-spacing:-0.2px;}
#t2h_1{left:314px;bottom:246px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t2i_1{left:493px;bottom:246px;letter-spacing:0.5px;word-spacing:0.3px;}
#t2j_1{left:493px;bottom:223px;}
#t2k_1{left:749px;bottom:223px;letter-spacing:0.4px;word-spacing:0.7px;}
#t2l_1{left:493px;bottom:178px;letter-spacing:0.3px;}
#t2m_1{left:591px;bottom:155px;letter-spacing:-0.2px;}
#t2n_1{left:688px;bottom:156px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t2o_1{left:411px;bottom:1225px;letter-spacing:-0.4px;}
#t2p_1{left:736px;bottom:1197px;letter-spacing:-0.2px;}
#t2q_1{left:193px;bottom:1143px;letter-spacing:-0.1px;word-spacing:0.1px;}
#t2r_1{left:197px;bottom:944px;letter-spacing:-0.2px;}
#t2s_1{left:197px;bottom:853px;letter-spacing:-0.2px;}
#t2t_1{left:501px;bottom:884px;letter-spacing:0.1px;}
#t2u_1{left:499px;bottom:869px;letter-spacing:0.1px;}
#t2v_1{left:512px;bottom:853px;letter-spacing:0.3px;}
#t2w_1{left:600px;bottom:840px;letter-spacing:0.2px;word-spacing:0.9px;}
#t2x_1{left:641px;bottom:825px;letter-spacing:0.2px;word-spacing:1.5px;}
#t2y_1{left:688px;bottom:809px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2z_1{left:649px;bottom:583px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t30_1{left:789px;bottom:583px;letter-spacing:0.3px;}
#t31_1{left:414px;bottom:1120px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t32_1{left:415px;bottom:1094px;}
#t33_1{left:194px;bottom:1071px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t34_1{left:333px;bottom:1071px;letter-spacing:0.3px;}
#t35_1{left:317px;bottom:1060px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t36_1{left:29px;bottom:1036px;letter-spacing:0.5px;word-spacing:0.8px;}
#t37_1{left:257px;bottom:1034px;letter-spacing:-0.5px;word-spacing:0.5px;}
#t38_1{left:194px;bottom:1118px;letter-spacing:-0.1px;word-spacing:0.1px;}

.s1{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s2{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s3{
	FONT-SIZE: 44px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s4{
	FONT-SIZE: 68.4px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s5{
	FONT-SIZE: 58.4px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

</style>
<!-- End inline CSS -->

<!-- Begin embedded font definitions -->
<style id="fonts1" type="text/css" >

@font-face {
	font-family: Calibri-Bold_d;
	src: url("/admin/css/put/fonts/Calibri-Bold_d.woff") format("woff");
}

@font-face {
	font-family: Calibri_h;
	src: url("/admin/css/put/fonts/Calibri_h.woff") format("woff");
}

</style>
<!-- End embedded font definitions -->

<!-- Begin page background -->
<div id="pg1Overlay" style="width:100%; height:100%; position:absolute; z-index:1; background-color:rgba(0,0,0,0); -webkit-user-select: none;"></div>
<div id="pg1" style="-webkit-user-select: none;"><object width="909" height="1286" data="/admin/css/put/1/1.svg" type="image/svg+xml" id="pdf1" style="width:909px; height:1286px; -moz-transform:scale(1); z-index: 0;"></object></div>
<!-- End page background -->


<!-- Begin text definitions (Positioned/styled in CSS) -->
<div id="t1_1" class="t s1">ПУТЕВОЙ ЛИСТ ЛЕГКОВОГО АВТОМОБИЛЯ</div>
<div id="t2_1" class="t s2">Д-1</div>
<div id="t3_1" class="t s2">№</div>
<div id="t4_1" class="t s2">85</div>
<div id="t5_1" class="t s2">серия</div>
<div id="t6_1" class="t s2">номер</div>
<div id="t7_1" class="t s2">дата</div>
<div id="t8_1" class="t s2">по ОКПО</div>
<div id="t9_1" class="t s2">Организация</div>
<div id="ta_1" class="t s2">Марка автомобиля</div>
<div id="tb_1" class="t s2">Разрешение </div>
<div id="tc_1" class="t s2">Государственный регистрационный знак</div>
<div id="td_1" class="t s3">Номер парковки</div>
<div id="te_1" class="t s2">Водитель</div>
<div id="tf_1" class="t s3">Табельный номер</div>
<div id="tg_1" class="t s4">Предрейсовый</div>
<div id="th_1" class="t s4">Медосмотр</div>
<div id="ti_1" class="t s4">Пройден</div>
<div id="tj_1" class="t s1">дата</div>
<div id="tk_1" class="t s2">г.</div>
<div id="tl_1" class="t s2">Время</div>
<div id="tm_1" class="t s2">'.$time4.'</div>
<div id="tn_1" class="t s1">должность: Врач</div>
<div id="to_1" class="t s2">подпись</div>
<div id="tp_1" class="t s2">'.$medic.'</div>
<div id="tq_1" class="t s2">Выезд с парковки</div>
<div id="tr_1" class="t s2">Дата</div>
<div id="ts_1" class="t s2">г. </div>
<div id="tt_1" class="t s2">Механик/ Диспечер ____'.$mech.'</div>
<div id="tu_1" class="t s2">Время</div>
<div id="tv_1" class="t s2">'.$time5.'</div>
<div id="tw_1" class="t s2">мин</div>
<div id="tx_1" class="t s2">Послерейсовый </div>
<div id="ty_1" class="t s2">медосмотр</div>
<div id="tz_1" class="t s2">дата, время</div>
<div id="t10_1" class="t s5">'.$today.'</div>
<div id="t11_1" class="t s2">г.</div>
<div id="t12_1" class="t s2">'.$time6.'</div>
<div id="t13_1" class="t s2">мин</div>
<div id="t14_1" class="t s2">пройден</div>
<div id="t15_1" class="t s2">дата</div>
<div id="t16_1" class="t s2">время</div>
<div id="t17_1" class="t s2">должность</div>
<div id="t18_1" class="t s2">подпись</div>
<div id="t19_1" class="t s5">Механик</div>
<div id="t1a_1" class="t s2">'.$mech.'</div>
<div id="t1b_1" class="t s3">подпись и расшифровка подписи</div>
<div id="t1c_1" class="t s2">Задание водителю</div>
<div id="t1d_1" class="t s1">Показание одометра, км</div>
<div id="t1e_1" class="t s2">В распоряжение</div>
<div id="t1f_1" class="t s2">Яндекс</div>
<div id="t1g_1" class="t s2">Механик</div>
<div id="t1h_1" class="t s2">'.$mech.'</div>
<div id="t1i_1" class="t s2">Адрес первой подачи</div>
<div id="t1j_1" class="t s2">Автомобиль в исправном состоянии принял</div>
<div id="t1k_1" class="t s2">Водитель</div>
<div id="t1l_1" class="t s2">По указанию Яндекс г. Москва и Московская область</div>
<div id="t1m_1" class="t s2">подпись</div>
<div id="t1n_1" class="t s3">расшифровка подписи</div>
<div id="t1o_1" class="t s2">Горючее</div>
<div id="t1p_1" class="t s2">марка</div>
<div id="t1q_1" class="t s2">код</div>
<div id="t1r_1" class="t s2">дата</div>
<div id="t1s_1" class="t s2">'.$year.' г.</div>
<div id="t1t_1" class="t s2">Движение горючего</div>
<div id="t1u_1" class="t s2">количество, л</div>
<div id="t1v_1" class="t s2">Время </div>
<div id="t1w_1" class="t s2">ч.</div>
<div id="t1x_1" class="t s2">мин</div>
<div id="t1y_1" class="t s2">при заезде на парковку</div>
<div id="t1z_1" class="t s2">Выдано: по заправочному</div>
<div id="t20_1" class="t s2">Диспетчер/механик</div>
<div id="t21_1" class="t s2">'.$mech.'</div>
<div id="t22_1" class="t s2">листу №</div>
<div id="t23_1" class="t s2">подпись</div>
<div id="t24_1" class="t s2">расшифровка подписи</div>
<div id="t25_1" class="t s2">остаток: </div>
<div id="t26_1" class="t s2">при выезде</div>
<div id="t27_1" class="t s2">Ожидание :</div>
<div id="t28_1" class="t s2">Опоздания, ожидания, простои в пути, заезды в гараж </div>
<div id="t29_1" class="t s2">расход</div>
<div id="t2a_1" class="t s2">по норме</div>
<div id="t2b_1" class="t s2">и прочие отметки</div>
<div id="t2c_1" class="t s2">Послерейсовый контроль технического состояния</div>
<div id="t2d_1" class="t s2">Автомобиль сдал</div>
<div id="t2e_1" class="t s2">водитель</div>
<div id="t2f_1" class="t s2">Показания одометра при</div>
<div id="t2g_1" class="t s2">подпись</div>
<div id="t2h_1" class="t s2">расшифровка подписи</div>
<div id="t2i_1" class="t s2">возвращении на парковку,</div>
<div id="t2j_1" class="t s2">км</div>
<div id="t2k_1" class="t s2">дата ; время</div>
<div id="t2l_1" class="t s2">Механик</div>
<div id="t2m_1" class="t s2">подпись</div>
<div id="t2n_1" class="t s3">расшифровка подписи</div>
<div id="t2o_1" class="t s5">'.$today.'</div>
<div id="t2p_1" class="t s2">'.$okpo.'</div>
<div id="t2q_1" class="t s2">'.$name_lic.'<br> ОГРН / ОГРНИП '.$ogrn.' ИНН '.$inn.'</div>
<div id="t2r_1" class="t s2">'.$today.'</div>
<div id="t2s_1" class="t s2">'.$today.'</div>
<div id="t2t_1" class="t s2">Предрейсовый </div>
<div id="t2u_1" class="t s2">(предсменный)</div>
<div id="t2v_1" class="t s2">(контроль)</div>
<div id="t2w_1" class="t s1">Предрейсовый контроль технического состояния</div>
<div id="t2x_1" class="t s1">транспортного средства пройден,</div>
<div id="t2y_1" class="t s1">выезд разрешен</div>
<div id="t2z_1" class="t s5">'.$fio_driver.' </div>
<div id="t30_1" class="t s5"></div>
<div id="t31_1" class="t s2">'.$razresh.'</div>
<div id="t32_1" class="t s5">'.$gosnomer.'</div>
<div id="t33_1" class="t s5">'.$fio_driver.' </div>
<div id="t34_1" class="t s5"></div>
<div id="t35_1" class="t s3">фамилия, имя, отчество</div>
<div id="t36_1" class="t s2">Удостоверение №</div>
<div id="t37_1" class="t s5">'.$udostov.'</div>
<div id="t38_1" class="t s5">'.$mark_model.'</div>

<!-- End text definitions -->


</div>





<div id="p1" style="overflow: hidden; position: relative; background-color: white; width: 909px; height: 1286px;">

<!-- Begin shared CSS values -->
<style class="shared-css" type="text/css" >
.t {
	-webkit-transform-origin: bottom left;
	-ms-transform-origin: bottom left;
	transform-origin: bottom left;
	-webkit-transform: scale(0.25);
	-ms-transform: scale(0.25);
	transform: scale(0.25);
	z-index: 2;
	position: absolute;
	white-space: pre;
	overflow: visible;
	line-height: 1.5;
}
</style>
<!-- End shared CSS values -->


<!-- Begin inline CSS -->
<style type="text/css" >

#t1_1{left:29px;bottom:1263px;letter-spacing:0.3px;word-spacing:1.7px;}
#t2_1{left:531px;bottom:1263px;}
#t3_1{left:591px;bottom:1263px;}
#t4_1{left:691px;bottom:1263px;}
#t5_1{left:493px;bottom:1251px;}
#t6_1{left:648px;bottom:1251px;letter-spacing:0.8px;}
#t7_1{left:361px;bottom:1211px;letter-spacing:0.4px;}
#t8_1{left:591px;bottom:1188px;letter-spacing:-0.3px;word-spacing:1.5px;}
#t9_1{left:29px;bottom:1165px;letter-spacing:0.3px;}
#ta_1{left:29px;bottom:1120px;letter-spacing:0.7px;word-spacing:0.8px;}
#tb_1{left:339px;bottom:1120px;letter-spacing:0.3px;}
#tc_1{left:29px;bottom:1097px;letter-spacing:0.4px;word-spacing:0.4px;}
#td_1{left:648px;bottom:1097px;letter-spacing:-0.2px;word-spacing:0.2px;}
#te_1{left:29px;bottom:1073px;letter-spacing:0.2px;}
#tf_1{left:648px;bottom:1074px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tg_1{left:29px;bottom:1010px;letter-spacing:0.1px;}
#th_1{left:29px;bottom:987px;letter-spacing:-0.2px;}
#ti_1{left:29px;bottom:965px;letter-spacing:0.3px;}
#tj_1{left:29px;bottom:944px;letter-spacing:0.5px;}
#tk_1{left:257px;bottom:944px;}
#tl_1{left:336px;bottom:944px;letter-spacing:0.7px;}
#tm_1{left:424px;bottom:944px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tn_1{left:29px;bottom:921px;letter-spacing:0.5px;word-spacing:-0.5px;}
#to_1{left:193px;bottom:921px;letter-spacing:-0.2px;}
#tp_1{left:336px;bottom:921px;letter-spacing:0.7px;word-spacing:-0.7px;}
#tq_1{left:29px;bottom:876px;letter-spacing:0.1px;word-spacing:-0.1px;}
#tr_1{left:29px;bottom:853px;letter-spacing:0.4px;}
#ts_1{left:257px;bottom:853px;}
#tt_1{left:270px;bottom:853px;letter-spacing:0.2px;word-spacing:1.5px;}
#tu_1{left:29px;bottom:829px;letter-spacing:0.7px;}
#tv_1{left:114px;bottom:829px;}
#tw_1{left:257px;bottom:829px;letter-spacing:0.8px;}
#tx_1{left:29px;bottom:806px;letter-spacing:0.1px;}
#ty_1{left:29px;bottom:783px;letter-spacing:0.4px;}
#tz_1{left:414px;bottom:783px;letter-spacing:0.5px;word-spacing:1.7px;}
#t10_1{left:508px;bottom:781px;letter-spacing:-0.4px;}
#t11_1{left:591px;bottom:783px;}
#t12_1{left:705px;bottom:783px;}
#t13_1{left:749px;bottom:783px;letter-spacing:0.8px;}
#t14_1{left:29px;bottom:759px;letter-spacing:0.2px;}
#t15_1{left:493px;bottom:759px;letter-spacing:0.4px;}
#t16_1{left:648px;bottom:759px;letter-spacing:0.9px;}
#t17_1{left:257px;bottom:744px;letter-spacing:0.2px;}
#t18_1{left:414px;bottom:744px;letter-spacing:-0.2px;}
#t19_1{left:494px;bottom:719px;letter-spacing:-0.2px;}
#t1a_1{left:749px;bottom:721px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1b_1{left:648px;bottom:699px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1c_1{left:29px;bottom:653px;letter-spacing:0.3px;word-spacing:1px;}
#t1d_1{left:493px;bottom:653px;letter-spacing:0.3px;word-spacing:0.4px;}
#t1e_1{left:29px;bottom:631px;letter-spacing:0.2px;word-spacing:1.3px;}
#t1f_1{left:294px;bottom:631px;}
#t1g_1{left:591px;bottom:631px;letter-spacing:0.3px;}
#t1h_1{left:749px;bottom:631px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1i_1{left:29px;bottom:609px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1j_1{left:591px;bottom:609px;letter-spacing:0.5px;word-spacing:-0.1px;}
#t1k_1{left:493px;bottom:586px;letter-spacing:0.2px;}
#t1l_1{left:29px;bottom:562px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t1m_1{left:591px;bottom:562px;letter-spacing:-0.2px;}
#t1n_1{left:688px;bottom:562px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1o_1{left:493px;bottom:539px;letter-spacing:0.3px;}
#t1p_1{left:680px;bottom:539px;letter-spacing:0.5px;}
#t1q_1{left:802px;bottom:539px;}
#t1r_1{left:29px;bottom:494px;letter-spacing:0.4px;}
#t1s_1{left:306px;bottom:494px;word-spacing:5.9px;}
#t1t_1{left:591px;bottom:494px;letter-spacing:0.3px;word-spacing:1.4px;}
#t1u_1{left:749px;bottom:471px;word-spacing:2.3px;}
#t1v_1{left:29px;bottom:449px;letter-spacing:0.6px;}
#t1w_1{left:74px;bottom:449px;}
#t1x_1{left:193px;bottom:449px;letter-spacing:0.8px;}
#t1y_1{left:29px;bottom:426px;letter-spacing:0.2px;word-spacing:0.8px;}
#t1z_1{left:493px;bottom:426px;letter-spacing:0.3px;word-spacing:0.5px;}
#t20_1{left:29px;bottom:404px;letter-spacing:0.2px;}
#t21_1{left:336px;bottom:404px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t22_1{left:493px;bottom:404px;}
#t23_1{left:273px;bottom:381px;letter-spacing:-0.2px;}
#t24_1{left:336px;bottom:381px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t25_1{left:493px;bottom:381px;letter-spacing:0.2px;}
#t26_1{left:575px;bottom:381px;letter-spacing:0.2px;word-spacing:1.4px;}
#t27_1{left:29px;bottom:358px;letter-spacing:0.2px;word-spacing:1.5px;}
#t28_1{left:114px;bottom:336px;letter-spacing:0.3px;word-spacing:0.3px;}
#t29_1{left:493px;bottom:336px;letter-spacing:0.4px;}
#t2a_1{left:648px;bottom:336px;letter-spacing:0.7px;word-spacing:0.6px;}
#t2b_1{left:29px;bottom:313px;letter-spacing:0.4px;word-spacing:0.4px;}
#t2c_1{left:493px;bottom:313px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t2d_1{left:29px;bottom:290px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2e_1{left:29px;bottom:268px;letter-spacing:0.3px;}
#t2f_1{left:493px;bottom:268px;letter-spacing:0.3px;word-spacing:0.8px;}
#t2g_1{left:193px;bottom:246px;letter-spacing:-0.2px;}
#t2h_1{left:314px;bottom:246px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t2i_1{left:493px;bottom:246px;letter-spacing:0.5px;word-spacing:0.3px;}
#t2j_1{left:493px;bottom:223px;}
#t2k_1{left:749px;bottom:223px;letter-spacing:0.4px;word-spacing:0.7px;}
#t2l_1{left:493px;bottom:178px;letter-spacing:0.3px;}
#t2m_1{left:591px;bottom:155px;letter-spacing:-0.2px;}
#t2n_1{left:688px;bottom:156px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t2o_1{left:411px;bottom:1225px;letter-spacing:-0.4px;}
#t2p_1{left:736px;bottom:1197px;letter-spacing:-0.2px;}
#t2q_1{left:193px;bottom:1143px;letter-spacing:-0.1px;word-spacing:0.1px;}
#t2r_1{left:197px;bottom:944px;letter-spacing:-0.2px;}
#t2s_1{left:197px;bottom:853px;letter-spacing:-0.2px;}
#t2t_1{left:501px;bottom:884px;letter-spacing:0.1px;}
#t2u_1{left:499px;bottom:869px;letter-spacing:0.1px;}
#t2v_1{left:512px;bottom:853px;letter-spacing:0.3px;}
#t2w_1{left:600px;bottom:840px;letter-spacing:0.2px;word-spacing:0.9px;}
#t2x_1{left:641px;bottom:825px;letter-spacing:0.2px;word-spacing:1.5px;}
#t2y_1{left:688px;bottom:809px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2z_1{left:649px;bottom:583px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t30_1{left:789px;bottom:583px;letter-spacing:0.3px;}
#t31_1{left:414px;bottom:1120px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t32_1{left:415px;bottom:1094px;}
#t33_1{left:194px;bottom:1071px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t34_1{left:333px;bottom:1071px;letter-spacing:0.3px;}
#t35_1{left:317px;bottom:1060px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t36_1{left:29px;bottom:1036px;letter-spacing:0.5px;word-spacing:0.8px;}
#t37_1{left:257px;bottom:1034px;letter-spacing:-0.5px;word-spacing:0.5px;}
#t38_1{left:194px;bottom:1118px;letter-spacing:-0.1px;word-spacing:0.1px;}

.s1{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s2{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s3{
	FONT-SIZE: 44px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s4{
	FONT-SIZE: 68.4px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s5{
	FONT-SIZE: 58.4px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

</style>
<!-- End inline CSS -->

<!-- Begin embedded font definitions -->
<style id="fonts1" type="text/css" >

@font-face {
	font-family: Calibri-Bold_d;
	src: url("/admin/css/put/fonts/Calibri-Bold_d.woff") format("woff");
}

@font-face {
	font-family: Calibri_h;
	src: url("/admin/css/put/fonts/Calibri_h.woff") format("woff");
}

</style>
<!-- End embedded font definitions -->

<!-- Begin page background -->
<div id="pg1Overlay" style="width:100%; height:100%; position:absolute; z-index:1; background-color:rgba(0,0,0,0); -webkit-user-select: none;"></div>
<div id="pg1" style="-webkit-user-select: none;"><object width="909" height="1286" data="/admin/css/put/1/1.svg" type="image/svg+xml" id="pdf1" style="width:909px; height:1286px; -moz-transform:scale(1); z-index: 0;"></object></div>
<!-- End page background -->


<!-- Begin text definitions (Positioned/styled in CSS) -->
<div id="t1_1" class="t s1">ПУТЕВОЙ ЛИСТ ЛЕГКОВОГО АВТОМОБИЛЯ</div>
<div id="t2_1" class="t s2">Д-1</div>
<div id="t3_1" class="t s2">№</div>
<div id="t4_1" class="t s2">85</div>
<div id="t5_1" class="t s2">серия</div>
<div id="t6_1" class="t s2">номер</div>
<div id="t7_1" class="t s2">дата</div>
<div id="t8_1" class="t s2">по ОКПО</div>
<div id="t9_1" class="t s2">Организация</div>
<div id="ta_1" class="t s2">Марка автомобиля</div>
<div id="tb_1" class="t s2">Разрешение </div>
<div id="tc_1" class="t s2">Государственный регистрационный знак</div>
<div id="td_1" class="t s3">Номер парковки</div>
<div id="te_1" class="t s2">Водитель</div>
<div id="tf_1" class="t s3">Табельный номер</div>
<div id="tg_1" class="t s4">Предрейсовый</div>
<div id="th_1" class="t s4">Медосмотр</div>
<div id="ti_1" class="t s4">Пройден</div>
<div id="tj_1" class="t s1">дата</div>
<div id="tk_1" class="t s2">г.</div>
<div id="tl_1" class="t s2">Время</div>
<div id="tm_1" class="t s2">'.$time7.'</div>
<div id="tn_1" class="t s1">должность: Врач</div>
<div id="to_1" class="t s2">подпись</div>
<div id="tp_1" class="t s2">'.$medic.'</div>
<div id="tq_1" class="t s2">Выезд с парковки</div>
<div id="tr_1" class="t s2">Дата</div>
<div id="ts_1" class="t s2">г. </div>
<div id="tt_1" class="t s2">Механик/ Диспечер ____'.$mech.'</div>
<div id="tu_1" class="t s2">Время</div>
<div id="tv_1" class="t s2">'.$time8.'</div>
<div id="tw_1" class="t s2">мин</div>
<div id="tx_1" class="t s2">Послерейсовый </div>
<div id="ty_1" class="t s2">медосмотр</div>
<div id="tz_1" class="t s2">дата, время</div>
<div id="t10_1" class="t s5">'.$today.'</div>
<div id="t11_1" class="t s2">г.</div>
<div id="t12_1" class="t s2">'.$time9.'</div>
<div id="t13_1" class="t s2">мин</div>
<div id="t14_1" class="t s2">пройден</div>
<div id="t15_1" class="t s2">дата</div>
<div id="t16_1" class="t s2">время</div>
<div id="t17_1" class="t s2">должность</div>
<div id="t18_1" class="t s2">подпись</div>
<div id="t19_1" class="t s5">Механик</div>
<div id="t1a_1" class="t s2">'.$mech.'</div>
<div id="t1b_1" class="t s3">подпись и расшифровка подписи</div>
<div id="t1c_1" class="t s2">Задание водителю</div>
<div id="t1d_1" class="t s1">Показание одометра, км</div>
<div id="t1e_1" class="t s2">В распоряжение</div>
<div id="t1f_1" class="t s2">Яндекс</div>
<div id="t1g_1" class="t s2">Механик</div>
<div id="t1h_1" class="t s2">'.$mech.'</div>
<div id="t1i_1" class="t s2">Адрес первой подачи</div>
<div id="t1j_1" class="t s2">Автомобиль в исправном состоянии принял</div>
<div id="t1k_1" class="t s2">Водитель</div>
<div id="t1l_1" class="t s2">По указанию Яндекс г. Москва и Московская область</div>
<div id="t1m_1" class="t s2">подпись</div>
<div id="t1n_1" class="t s3">расшифровка подписи</div>
<div id="t1o_1" class="t s2">Горючее</div>
<div id="t1p_1" class="t s2">марка</div>
<div id="t1q_1" class="t s2">код</div>
<div id="t1r_1" class="t s2">дата</div>
<div id="t1s_1" class="t s2">'.$year.' г.</div>
<div id="t1t_1" class="t s2">Движение горючего</div>
<div id="t1u_1" class="t s2">количество, л</div>
<div id="t1v_1" class="t s2">Время </div>
<div id="t1w_1" class="t s2">ч.</div>
<div id="t1x_1" class="t s2">мин</div>
<div id="t1y_1" class="t s2">при заезде на парковку</div>
<div id="t1z_1" class="t s2">Выдано: по заправочному</div>
<div id="t20_1" class="t s2">Диспетчер/механик</div>
<div id="t21_1" class="t s2">'.$mech.'</div>
<div id="t22_1" class="t s2">листу №</div>
<div id="t23_1" class="t s2">подпись</div>
<div id="t24_1" class="t s2">расшифровка подписи</div>
<div id="t25_1" class="t s2">остаток: </div>
<div id="t26_1" class="t s2">при выезде</div>
<div id="t27_1" class="t s2">Ожидание :</div>
<div id="t28_1" class="t s2">Опоздания, ожидания, простои в пути, заезды в гараж </div>
<div id="t29_1" class="t s2">расход</div>
<div id="t2a_1" class="t s2">по норме</div>
<div id="t2b_1" class="t s2">и прочие отметки</div>
<div id="t2c_1" class="t s2">Послерейсовый контроль технического состояния</div>
<div id="t2d_1" class="t s2">Автомобиль сдал</div>
<div id="t2e_1" class="t s2">водитель</div>
<div id="t2f_1" class="t s2">Показания одометра при</div>
<div id="t2g_1" class="t s2">подпись</div>
<div id="t2h_1" class="t s2">расшифровка подписи</div>
<div id="t2i_1" class="t s2">возвращении на парковку,</div>
<div id="t2j_1" class="t s2">км</div>
<div id="t2k_1" class="t s2">дата ; время</div>
<div id="t2l_1" class="t s2">Механик</div>
<div id="t2m_1" class="t s2">подпись</div>
<div id="t2n_1" class="t s3">расшифровка подписи</div>
<div id="t2o_1" class="t s5">'.$today.'</div>
<div id="t2p_1" class="t s2">'.$okpo.'</div>
<div id="t2q_1" class="t s2">'.$name_lic.'<br> ОГРН / ОГРНИП '.$ogrn.' ИНН '.$inn.'</div>
<div id="t2r_1" class="t s2">'.$today.'</div>
<div id="t2s_1" class="t s2">'.$today.'</div>
<div id="t2t_1" class="t s2">Предрейсовый </div>
<div id="t2u_1" class="t s2">(предсменный)</div>
<div id="t2v_1" class="t s2">(контроль)</div>
<div id="t2w_1" class="t s1">Предрейсовый контроль технического состояния</div>
<div id="t2x_1" class="t s1">транспортного средства пройден,</div>
<div id="t2y_1" class="t s1">выезд разрешен</div>
<div id="t2z_1" class="t s5">'.$fio_driver.' </div>
<div id="t30_1" class="t s5"></div>
<div id="t31_1" class="t s2">'.$razresh.'</div>
<div id="t32_1" class="t s5">'.$gosnomer.'</div>
<div id="t33_1" class="t s5">'.$fio_driver.'</div>
<div id="t34_1" class="t s5"></div>
<div id="t35_1" class="t s3">фамилия, имя, отчество</div>
<div id="t36_1" class="t s2">Удостоверение №</div>
<div id="t37_1" class="t s5">'.$udostov.'</div>
<div id="t38_1" class="t s5">'.$mark_model.'</div>

<!-- End text definitions -->


</div>








</body>
</html>

		
		';			
				
			
			
			
		} else {
			$today = '';
			$year = '';
		
		
		
			$time1 = '';
			$time2 = '';
			$time3 = '';
			$time4 = '';
			$time5 = '';
			$time6 = '';
			$time7 = '';
			$time8 = '';
			$time9 = '';
		
		
			$html.= '
		
		<!DOCTYPE html >
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8" />
</head>

<body style="margin: 0;">

<div id="p1" style="overflow: hidden; position: relative; background-color: white; width: 909px; height: 1286px;">

<!-- Begin shared CSS values -->
<style class="shared-css" type="text/css" >
.t {
	-webkit-transform-origin: bottom left;
	-ms-transform-origin: bottom left;
	transform-origin: bottom left;
	-webkit-transform: scale(0.25);
	-ms-transform: scale(0.25);
	transform: scale(0.25);
	z-index: 2;
	position: absolute;
	white-space: pre;
	overflow: visible;
	line-height: 1.5;
}
</style>
<!-- End shared CSS values -->


<!-- Begin inline CSS -->
<style type="text/css" >

#t1_1{left:29px;bottom:1263px;letter-spacing:0.3px;word-spacing:1.7px;}
#t2_1{left:531px;bottom:1263px;}
#t3_1{left:591px;bottom:1263px;}
#t4_1{left:691px;bottom:1263px;}
#t5_1{left:493px;bottom:1251px;}
#t6_1{left:648px;bottom:1251px;letter-spacing:0.8px;}
#t7_1{left:361px;bottom:1211px;letter-spacing:0.4px;}
#t8_1{left:591px;bottom:1188px;letter-spacing:-0.3px;word-spacing:1.5px;}
#t9_1{left:29px;bottom:1165px;letter-spacing:0.3px;}
#ta_1{left:29px;bottom:1120px;letter-spacing:0.7px;word-spacing:0.8px;}
#tb_1{left:339px;bottom:1120px;letter-spacing:0.3px;}
#tc_1{left:29px;bottom:1097px;letter-spacing:0.4px;word-spacing:0.4px;}
#td_1{left:648px;bottom:1097px;letter-spacing:-0.2px;word-spacing:0.2px;}
#te_1{left:29px;bottom:1073px;letter-spacing:0.2px;}
#tf_1{left:648px;bottom:1074px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tg_1{left:29px;bottom:1010px;letter-spacing:0.1px;}
#th_1{left:29px;bottom:987px;letter-spacing:-0.2px;}
#ti_1{left:29px;bottom:965px;letter-spacing:0.3px;}
#tj_1{left:29px;bottom:944px;letter-spacing:0.5px;}
#tk_1{left:257px;bottom:944px;}
#tl_1{left:336px;bottom:944px;letter-spacing:0.7px;}
#tm_1{left:424px;bottom:944px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tn_1{left:29px;bottom:921px;letter-spacing:0.5px;word-spacing:-0.5px;}
#to_1{left:193px;bottom:921px;letter-spacing:-0.2px;}
#tp_1{left:336px;bottom:921px;letter-spacing:0.7px;word-spacing:-0.7px;}
#tq_1{left:29px;bottom:876px;letter-spacing:0.1px;word-spacing:-0.1px;}
#tr_1{left:29px;bottom:853px;letter-spacing:0.4px;}
#ts_1{left:257px;bottom:853px;}
#tt_1{left:270px;bottom:853px;letter-spacing:0.2px;word-spacing:1.5px;}
#tu_1{left:29px;bottom:829px;letter-spacing:0.7px;}
#tv_1{left:114px;bottom:829px;}
#tw_1{left:257px;bottom:829px;letter-spacing:0.8px;}
#tx_1{left:29px;bottom:806px;letter-spacing:0.1px;}
#ty_1{left:29px;bottom:783px;letter-spacing:0.4px;}
#tz_1{left:414px;bottom:783px;letter-spacing:0.5px;word-spacing:1.7px;}
#t10_1{left:508px;bottom:781px;letter-spacing:-0.4px;}
#t11_1{left:591px;bottom:783px;}
#t12_1{left:705px;bottom:783px;}
#t13_1{left:749px;bottom:783px;letter-spacing:0.8px;}
#t14_1{left:29px;bottom:759px;letter-spacing:0.2px;}
#t15_1{left:493px;bottom:759px;letter-spacing:0.4px;}
#t16_1{left:648px;bottom:759px;letter-spacing:0.9px;}
#t17_1{left:257px;bottom:744px;letter-spacing:0.2px;}
#t18_1{left:414px;bottom:744px;letter-spacing:-0.2px;}
#t19_1{left:494px;bottom:719px;letter-spacing:-0.2px;}
#t1a_1{left:749px;bottom:721px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1b_1{left:648px;bottom:699px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1c_1{left:29px;bottom:653px;letter-spacing:0.3px;word-spacing:1px;}
#t1d_1{left:493px;bottom:653px;letter-spacing:0.3px;word-spacing:0.4px;}
#t1e_1{left:29px;bottom:631px;letter-spacing:0.2px;word-spacing:1.3px;}
#t1f_1{left:294px;bottom:631px;}
#t1g_1{left:591px;bottom:631px;letter-spacing:0.3px;}
#t1h_1{left:749px;bottom:631px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1i_1{left:29px;bottom:609px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1j_1{left:591px;bottom:609px;letter-spacing:0.5px;word-spacing:-0.1px;}
#t1k_1{left:493px;bottom:586px;letter-spacing:0.2px;}
#t1l_1{left:29px;bottom:562px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t1m_1{left:591px;bottom:562px;letter-spacing:-0.2px;}
#t1n_1{left:688px;bottom:562px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1o_1{left:493px;bottom:539px;letter-spacing:0.3px;}
#t1p_1{left:680px;bottom:539px;letter-spacing:0.5px;}
#t1q_1{left:802px;bottom:539px;}
#t1r_1{left:29px;bottom:494px;letter-spacing:0.4px;}
#t1s_1{left:306px;bottom:494px;word-spacing:5.9px;}
#t1t_1{left:591px;bottom:494px;letter-spacing:0.3px;word-spacing:1.4px;}
#t1u_1{left:749px;bottom:471px;word-spacing:2.3px;}
#t1v_1{left:29px;bottom:449px;letter-spacing:0.6px;}
#t1w_1{left:74px;bottom:449px;}
#t1x_1{left:193px;bottom:449px;letter-spacing:0.8px;}
#t1y_1{left:29px;bottom:426px;letter-spacing:0.2px;word-spacing:0.8px;}
#t1z_1{left:493px;bottom:426px;letter-spacing:0.3px;word-spacing:0.5px;}
#t20_1{left:29px;bottom:404px;letter-spacing:0.2px;}
#t21_1{left:336px;bottom:404px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t22_1{left:493px;bottom:404px;}
#t23_1{left:273px;bottom:381px;letter-spacing:-0.2px;}
#t24_1{left:336px;bottom:381px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t25_1{left:493px;bottom:381px;letter-spacing:0.2px;}
#t26_1{left:575px;bottom:381px;letter-spacing:0.2px;word-spacing:1.4px;}
#t27_1{left:29px;bottom:358px;letter-spacing:0.2px;word-spacing:1.5px;}
#t28_1{left:114px;bottom:336px;letter-spacing:0.3px;word-spacing:0.3px;}
#t29_1{left:493px;bottom:336px;letter-spacing:0.4px;}
#t2a_1{left:648px;bottom:336px;letter-spacing:0.7px;word-spacing:0.6px;}
#t2b_1{left:29px;bottom:313px;letter-spacing:0.4px;word-spacing:0.4px;}
#t2c_1{left:493px;bottom:313px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t2d_1{left:29px;bottom:290px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2e_1{left:29px;bottom:268px;letter-spacing:0.3px;}
#t2f_1{left:493px;bottom:268px;letter-spacing:0.3px;word-spacing:0.8px;}
#t2g_1{left:193px;bottom:246px;letter-spacing:-0.2px;}
#t2h_1{left:314px;bottom:246px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t2i_1{left:493px;bottom:246px;letter-spacing:0.5px;word-spacing:0.3px;}
#t2j_1{left:493px;bottom:223px;}
#t2k_1{left:749px;bottom:223px;letter-spacing:0.4px;word-spacing:0.7px;}
#t2l_1{left:493px;bottom:178px;letter-spacing:0.3px;}
#t2m_1{left:591px;bottom:155px;letter-spacing:-0.2px;}
#t2n_1{left:688px;bottom:156px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t2o_1{left:411px;bottom:1225px;letter-spacing:-0.4px;}
#t2p_1{left:736px;bottom:1197px;letter-spacing:-0.2px;}
#t2q_1{left:193px;bottom:1143px;letter-spacing:-0.1px;word-spacing:0.1px;}
#t2r_1{left:197px;bottom:944px;letter-spacing:-0.2px;}
#t2s_1{left:197px;bottom:853px;letter-spacing:-0.2px;}
#t2t_1{left:501px;bottom:884px;letter-spacing:0.1px;}
#t2u_1{left:499px;bottom:869px;letter-spacing:0.1px;}
#t2v_1{left:512px;bottom:853px;letter-spacing:0.3px;}
#t2w_1{left:600px;bottom:840px;letter-spacing:0.2px;word-spacing:0.9px;}
#t2x_1{left:641px;bottom:825px;letter-spacing:0.2px;word-spacing:1.5px;}
#t2y_1{left:688px;bottom:809px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2z_1{left:649px;bottom:583px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t30_1{left:789px;bottom:583px;letter-spacing:0.3px;}
#t31_1{left:414px;bottom:1120px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t32_1{left:415px;bottom:1094px;}
#t33_1{left:194px;bottom:1071px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t34_1{left:333px;bottom:1071px;letter-spacing:0.3px;}
#t35_1{left:317px;bottom:1060px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t36_1{left:29px;bottom:1036px;letter-spacing:0.5px;word-spacing:0.8px;}
#t37_1{left:257px;bottom:1034px;letter-spacing:-0.5px;word-spacing:0.5px;}
#t38_1{left:194px;bottom:1118px;letter-spacing:-0.1px;word-spacing:0.1px;}

.s1{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s2{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s3{
	FONT-SIZE: 44px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s4{
	FONT-SIZE: 68.4px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s5{
	FONT-SIZE: 58.4px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

</style>
<!-- End inline CSS -->

<!-- Begin embedded font definitions -->
<style id="fonts1" type="text/css" >

@font-face {
	font-family: Calibri-Bold_d;
	src: url("/admin/css/put/fonts/Calibri-Bold_d.woff") format("woff");
}

@font-face {
	font-family: Calibri_h;
	src: url("/admin/css/put/fonts/Calibri_h.woff") format("woff");
}

</style>
<!-- End embedded font definitions -->

<!-- Begin page background -->
<div id="pg1Overlay" style="width:100%; height:100%; position:absolute; z-index:1; background-color:rgba(0,0,0,0); -webkit-user-select: none;"></div>
<div id="pg1" style="-webkit-user-select: none;"><object width="909" height="1286" data="/admin/css/put/1/1.svg" type="image/svg+xml" id="pdf1" style="width:909px; height:1286px; -moz-transform:scale(1); z-index: 0;"></object></div>
<!-- End page background -->


<!-- Begin text definitions (Positioned/styled in CSS) -->
<div id="t1_1" class="t s1">ПУТЕВОЙ ЛИСТ ЛЕГКОВОГО АВТОМОБИЛЯ</div>
<div id="t2_1" class="t s2">Д-1</div>
<div id="t3_1" class="t s2">№</div>
<div id="t4_1" class="t s2">85</div>
<div id="t5_1" class="t s2">серия</div>
<div id="t6_1" class="t s2">номер</div>
<div id="t7_1" class="t s2">дата</div>
<div id="t8_1" class="t s2">по ОКПО</div>
<div id="t9_1" class="t s2">Организация</div>
<div id="ta_1" class="t s2">Марка автомобиля</div>
<div id="tb_1" class="t s2">Разрешение </div>
<div id="tc_1" class="t s2">Государственный регистрационный знак</div>
<div id="td_1" class="t s3">Номер парковки</div>
<div id="te_1" class="t s2">Водитель</div>
<div id="tf_1" class="t s3">Табельный номер</div>
<div id="tg_1" class="t s4">Предрейсовый</div>
<div id="th_1" class="t s4">Медосмотр</div>
<div id="ti_1" class="t s4">Пройден</div>
<div id="tj_1" class="t s1">дата</div>
<div id="tk_1" class="t s2">г.</div>
<div id="tl_1" class="t s2">Время</div>
<div id="tm_1" class="t s2">'.$time1.'</div>
<div id="tn_1" class="t s1">должность: Врач</div>
<div id="to_1" class="t s2">подпись</div>
<div id="tp_1" class="t s2">'.$medic.'</div>
<div id="tq_1" class="t s2">Выезд с парковки</div>
<div id="tr_1" class="t s2">Дата</div>
<div id="ts_1" class="t s2">г. </div>
<div id="tt_1" class="t s2">Механик/ Диспечер ____'.$mech.'</div>
<div id="tu_1" class="t s2">Время</div>
<div id="tv_1" class="t s2">'.$time2.'</div>
<div id="tw_1" class="t s2">мин</div>
<div id="tx_1" class="t s2">Послерейсовый </div>
<div id="ty_1" class="t s2">медосмотр</div>
<div id="tz_1" class="t s2">дата, время</div>
<div id="t10_1" class="t s5">'.$today.'</div>
<div id="t11_1" class="t s2">г.</div>
<div id="t12_1" class="t s2">'.$time3.'</div>
<div id="t13_1" class="t s2">мин</div>
<div id="t14_1" class="t s2">пройден</div>
<div id="t15_1" class="t s2">дата</div>
<div id="t16_1" class="t s2">время</div>
<div id="t17_1" class="t s2">должность</div>
<div id="t18_1" class="t s2">подпись</div>
<div id="t19_1" class="t s5">Механик</div>
<div id="t1a_1" class="t s2">'.$mech.'</div>
<div id="t1b_1" class="t s3">подпись и расшифровка подписи</div>
<div id="t1c_1" class="t s2">Задание водителю</div>
<div id="t1d_1" class="t s1">Показание одометра, км</div>
<div id="t1e_1" class="t s2">В распоряжение</div>
<div id="t1f_1" class="t s2">Яндекс</div>
<div id="t1g_1" class="t s2">Механик</div>
<div id="t1h_1" class="t s2">'.$mech.'</div>
<div id="t1i_1" class="t s2">Адрес первой подачи</div>
<div id="t1j_1" class="t s2">Автомобиль в исправном состоянии принял</div>
<div id="t1k_1" class="t s2">Водитель</div>
<div id="t1l_1" class="t s2">По указанию Яндекс г. Москва и Московская область</div>
<div id="t1m_1" class="t s2">подпись</div>
<div id="t1n_1" class="t s3">расшифровка подписи</div>
<div id="t1o_1" class="t s2">Горючее</div>
<div id="t1p_1" class="t s2">марка</div>
<div id="t1q_1" class="t s2">код</div>
<div id="t1r_1" class="t s2">дата</div>
<div id="t1s_1" class="t s2">'.$year.' г.</div>
<div id="t1t_1" class="t s2">Движение горючего</div>
<div id="t1u_1" class="t s2">количество, л</div>
<div id="t1v_1" class="t s2">Время </div>
<div id="t1w_1" class="t s2">ч.</div>
<div id="t1x_1" class="t s2">мин</div>
<div id="t1y_1" class="t s2">при заезде на парковку</div>
<div id="t1z_1" class="t s2">Выдано: по заправочному</div>
<div id="t20_1" class="t s2">Диспетчер/механик</div>
<div id="t21_1" class="t s2">'.$mech.'</div>
<div id="t22_1" class="t s2">листу №</div>
<div id="t23_1" class="t s2">подпись</div>
<div id="t24_1" class="t s2">расшифровка подписи</div>
<div id="t25_1" class="t s2">остаток: </div>
<div id="t26_1" class="t s2">при выезде</div>
<div id="t27_1" class="t s2">Ожидание :</div>
<div id="t28_1" class="t s2">Опоздания, ожидания, простои в пути, заезды в гараж </div>
<div id="t29_1" class="t s2">расход</div>
<div id="t2a_1" class="t s2">по норме</div>
<div id="t2b_1" class="t s2">и прочие отметки</div>
<div id="t2c_1" class="t s2">Послерейсовый контроль технического состояния</div>
<div id="t2d_1" class="t s2">Автомобиль сдал</div>
<div id="t2e_1" class="t s2">водитель</div>
<div id="t2f_1" class="t s2">Показания одометра при</div>
<div id="t2g_1" class="t s2">подпись</div>
<div id="t2h_1" class="t s2">расшифровка подписи</div>
<div id="t2i_1" class="t s2">возвращении на парковку,</div>
<div id="t2j_1" class="t s2">км</div>
<div id="t2k_1" class="t s2">дата ; время</div>
<div id="t2l_1" class="t s2">Механик</div>
<div id="t2m_1" class="t s2">подпись</div>
<div id="t2n_1" class="t s3">расшифровка подписи</div>
<div id="t2o_1" class="t s5">'.$today.'</div>
<div id="t2p_1" class="t s2">'.$okpo.'</div>
<div id="t2q_1" class="t s2">'.$name_lic.'<br> ОГРН / ОГРНИП '.$ogrn.' ИНН '.$inn.'</div>
<div id="t2r_1" class="t s2">'.$today.'</div>
<div id="t2s_1" class="t s2">'.$today.'</div>
<div id="t2t_1" class="t s2">Предрейсовый </div>
<div id="t2u_1" class="t s2">(предсменный)</div>
<div id="t2v_1" class="t s2">(контроль)</div>
<div id="t2w_1" class="t s1">Предрейсовый контроль технического состояния</div>
<div id="t2x_1" class="t s1">транспортного средства пройден,</div>
<div id="t2y_1" class="t s1">выезд разрешен</div>
<div id="t2z_1" class="t s5">'.$fio_driver.'</div>
<div id="t30_1" class="t s5"></div>
<div id="t31_1" class="t s2">'.$razresh.'</div>
<div id="t32_1" class="t s5">'.$gosnomer.'</div>
<div id="t33_1" class="t s5">'.$fio_driver.' </div>
<div id="t34_1" class="t s5"></div>
<div id="t35_1" class="t s3">фамилия, имя, отчество</div>
<div id="t36_1" class="t s2">Удостоверение №</div>
<div id="t37_1" class="t s5">'.$udostov.'</div>
<div id="t38_1" class="t s5">'.$mark_model.'</div>

<!-- End text definitions -->


</div>




</body>
</html>

		
		
		
		
		';			
				
			
		
		}
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				

			
			}
			
			
		//}
		
		
		
		
		
		//}
		
		return $html;
	} 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	public function actionPut () {
		
		if (!(Yii::$app->request->post())) {
			
			 return $this->redirect(['index']);
			
		}
		
		
		$post = Yii::$app->request->post();
			
		$html = '';	
			
			
		$data = $post['test'];
		$date_starts = $post['dd']['date_starts'];
		
		
			
		foreach ($data as $item) {
			
			
			if ($item['name']) {
				//Номер договора
				$contract_id = $item['name'];
				
				$put_type = 1;
				
				
				$date_raznost = 7;
				
				
				//Взять тип
				if (!empty($item['type'])) {
					$put_type = 2;
					$date_raznost = 30;
				}
				
				if (!empty($post['dd']['days']) && $post['dd']['days'] > 0) {
					$date_raznost = $post['dd']['days'];
					
					$date_raznost = $date_raznost - 1;
					$put_type = 1;
				}
				
				
				

				$model_contract = Contracts::findOne($contract_id);
				
				
				//Машина
				$auto = AutomobilesNewModel::find()->where(['id_auto' => $model_contract->auto_id])->all();
				
				
				$mark_model = $auto[0]->mark.' '.$auto[0]->model;
				
				
				$gosnomer = $auto[0]->gosnomer;
				
				
				$lic_id  = $auto[0]['licen_id'];
				
				//$razresh_id = $auto[0]['razresh_id'];
				
				$lic_arr = Licen::findOne($lic_id);
				
			
				
				$name_lic = $lic_arr['name_lic'];
				$ogrn = $lic_arr['ogrn'];
				$inn = $lic_arr['inn'];
				$okpo = $lic_arr['okpo'];
				
				
				$mech = $lic_arr['mechanic'];
				$medic = $lic_arr['medic'];
				
				
				//Водитель
				$driver = DriversModel::find()->where(['id_driver' => $model_contract->driver_id])->all();	
				
				$fio_driver = $driver[0]->last_name.' '.$driver[0]->first_name.' '.$driver[0]->middle_name;
				//print_r ($driver[0]);
				

				$razresh = $auto[0]->razresh_text; 
				
				$udostov = $driver[0]->lic_series.' '.$driver[0]->lic_number.' '.$driver[0]->lic_type;
				
				
				
				
				
				
				
				
		
		/*
		if ($put_type == 2) { 
			$date_raznost = 30;
		} 
		*/
		
		for ($i = 0; $i<= $date_raznost; $i++ ) {
		
		
		
		//$date = new DateTime('2006-12-12');
		//$date->modify('+1 day');
		
		if ($put_type == 1) {
		
			if (!empty($date_starts)) {
				
				$date = $date_starts;
			} else {
				$date = new DateTime();
			}
		
		
		
			$date = new DateTime();
		
			$date->modify('+'.$i.' day');
			$today = $date->format("d.m.Y");
			
			$year = $date->format("Y");
			
			
			$time1 = '00 ч. 10 мин';
			$time2 = '00 ч. 20 мин';
			$time3 = '00 ч. 15 мин';
			$time4 = '08 ч. 10 мин';
			$time5 = '08 ч. 20 мин';
			$time6 = '08 ч. 15 мин';
			$time7 = '16 ч. 10 мин';
			$time8 = '16 ч. 20 мин';
			$time9 = '16 ч. 15 мин';
			
			
			$html.= '
		
		<!DOCTYPE html >
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8" />
</head>

<body style="margin: 0;">

<div id="p1" style="overflow: hidden; position: relative; background-color: white; width: 909px; height: 1286px;">

<!-- Begin shared CSS values -->
<style class="shared-css" type="text/css" >
.t {
	-webkit-transform-origin: bottom left;
	-ms-transform-origin: bottom left;
	transform-origin: bottom left;
	-webkit-transform: scale(0.25);
	-ms-transform: scale(0.25);
	transform: scale(0.25);
	z-index: 2;
	position: absolute;
	white-space: pre;
	overflow: visible;
	line-height: 1.5;
}
</style>
<!-- End shared CSS values -->


<!-- Begin inline CSS -->
<style type="text/css" >

#t1_1{left:29px;bottom:1263px;letter-spacing:0.3px;word-spacing:1.7px;}
#t2_1{left:531px;bottom:1263px;}
#t3_1{left:591px;bottom:1263px;}
#t4_1{left:691px;bottom:1263px;}
#t5_1{left:493px;bottom:1251px;}
#t6_1{left:648px;bottom:1251px;letter-spacing:0.8px;}
#t7_1{left:361px;bottom:1211px;letter-spacing:0.4px;}
#t8_1{left:591px;bottom:1188px;letter-spacing:-0.3px;word-spacing:1.5px;}
#t9_1{left:29px;bottom:1165px;letter-spacing:0.3px;}
#ta_1{left:29px;bottom:1120px;letter-spacing:0.7px;word-spacing:0.8px;}
#tb_1{left:339px;bottom:1120px;letter-spacing:0.3px;}
#tc_1{left:29px;bottom:1097px;letter-spacing:0.4px;word-spacing:0.4px;}
#td_1{left:648px;bottom:1097px;letter-spacing:-0.2px;word-spacing:0.2px;}
#te_1{left:29px;bottom:1073px;letter-spacing:0.2px;}
#tf_1{left:648px;bottom:1074px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tg_1{left:29px;bottom:1010px;letter-spacing:0.1px;}
#th_1{left:29px;bottom:987px;letter-spacing:-0.2px;}
#ti_1{left:29px;bottom:965px;letter-spacing:0.3px;}
#tj_1{left:29px;bottom:944px;letter-spacing:0.5px;}
#tk_1{left:257px;bottom:944px;}
#tl_1{left:336px;bottom:944px;letter-spacing:0.7px;}
#tm_1{left:424px;bottom:944px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tn_1{left:29px;bottom:921px;letter-spacing:0.5px;word-spacing:-0.5px;}
#to_1{left:193px;bottom:921px;letter-spacing:-0.2px;}
#tp_1{left:336px;bottom:921px;letter-spacing:0.7px;word-spacing:-0.7px;}
#tq_1{left:29px;bottom:876px;letter-spacing:0.1px;word-spacing:-0.1px;}
#tr_1{left:29px;bottom:853px;letter-spacing:0.4px;}
#ts_1{left:257px;bottom:853px;}
#tt_1{left:270px;bottom:853px;letter-spacing:0.2px;word-spacing:1.5px;}
#tu_1{left:29px;bottom:829px;letter-spacing:0.7px;}
#tv_1{left:114px;bottom:829px;}
#tw_1{left:257px;bottom:829px;letter-spacing:0.8px;}
#tx_1{left:29px;bottom:806px;letter-spacing:0.1px;}
#ty_1{left:29px;bottom:783px;letter-spacing:0.4px;}
#tz_1{left:414px;bottom:783px;letter-spacing:0.5px;word-spacing:1.7px;}
#t10_1{left:508px;bottom:781px;letter-spacing:-0.4px;}
#t11_1{left:591px;bottom:783px;}
#t12_1{left:705px;bottom:783px;}
#t13_1{left:749px;bottom:783px;letter-spacing:0.8px;}
#t14_1{left:29px;bottom:759px;letter-spacing:0.2px;}
#t15_1{left:493px;bottom:759px;letter-spacing:0.4px;}
#t16_1{left:648px;bottom:759px;letter-spacing:0.9px;}
#t17_1{left:257px;bottom:744px;letter-spacing:0.2px;}
#t18_1{left:414px;bottom:744px;letter-spacing:-0.2px;}
#t19_1{left:494px;bottom:719px;letter-spacing:-0.2px;}
#t1a_1{left:749px;bottom:721px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1b_1{left:648px;bottom:699px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1c_1{left:29px;bottom:653px;letter-spacing:0.3px;word-spacing:1px;}
#t1d_1{left:493px;bottom:653px;letter-spacing:0.3px;word-spacing:0.4px;}
#t1e_1{left:29px;bottom:631px;letter-spacing:0.2px;word-spacing:1.3px;}
#t1f_1{left:294px;bottom:631px;}
#t1g_1{left:591px;bottom:631px;letter-spacing:0.3px;}
#t1h_1{left:749px;bottom:631px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1i_1{left:29px;bottom:609px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1j_1{left:591px;bottom:609px;letter-spacing:0.5px;word-spacing:-0.1px;}
#t1k_1{left:493px;bottom:586px;letter-spacing:0.2px;}
#t1l_1{left:29px;bottom:562px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t1m_1{left:591px;bottom:562px;letter-spacing:-0.2px;}
#t1n_1{left:688px;bottom:562px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1o_1{left:493px;bottom:539px;letter-spacing:0.3px;}
#t1p_1{left:680px;bottom:539px;letter-spacing:0.5px;}
#t1q_1{left:802px;bottom:539px;}
#t1r_1{left:29px;bottom:494px;letter-spacing:0.4px;}
#t1s_1{left:306px;bottom:494px;word-spacing:5.9px;}
#t1t_1{left:591px;bottom:494px;letter-spacing:0.3px;word-spacing:1.4px;}
#t1u_1{left:749px;bottom:471px;word-spacing:2.3px;}
#t1v_1{left:29px;bottom:449px;letter-spacing:0.6px;}
#t1w_1{left:74px;bottom:449px;}
#t1x_1{left:193px;bottom:449px;letter-spacing:0.8px;}
#t1y_1{left:29px;bottom:426px;letter-spacing:0.2px;word-spacing:0.8px;}
#t1z_1{left:493px;bottom:426px;letter-spacing:0.3px;word-spacing:0.5px;}
#t20_1{left:29px;bottom:404px;letter-spacing:0.2px;}
#t21_1{left:336px;bottom:404px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t22_1{left:493px;bottom:404px;}
#t23_1{left:273px;bottom:381px;letter-spacing:-0.2px;}
#t24_1{left:336px;bottom:381px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t25_1{left:493px;bottom:381px;letter-spacing:0.2px;}
#t26_1{left:575px;bottom:381px;letter-spacing:0.2px;word-spacing:1.4px;}
#t27_1{left:29px;bottom:358px;letter-spacing:0.2px;word-spacing:1.5px;}
#t28_1{left:114px;bottom:336px;letter-spacing:0.3px;word-spacing:0.3px;}
#t29_1{left:493px;bottom:336px;letter-spacing:0.4px;}
#t2a_1{left:648px;bottom:336px;letter-spacing:0.7px;word-spacing:0.6px;}
#t2b_1{left:29px;bottom:313px;letter-spacing:0.4px;word-spacing:0.4px;}
#t2c_1{left:493px;bottom:313px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t2d_1{left:29px;bottom:290px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2e_1{left:29px;bottom:268px;letter-spacing:0.3px;}
#t2f_1{left:493px;bottom:268px;letter-spacing:0.3px;word-spacing:0.8px;}
#t2g_1{left:193px;bottom:246px;letter-spacing:-0.2px;}
#t2h_1{left:314px;bottom:246px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t2i_1{left:493px;bottom:246px;letter-spacing:0.5px;word-spacing:0.3px;}
#t2j_1{left:493px;bottom:223px;}
#t2k_1{left:749px;bottom:223px;letter-spacing:0.4px;word-spacing:0.7px;}
#t2l_1{left:493px;bottom:178px;letter-spacing:0.3px;}
#t2m_1{left:591px;bottom:155px;letter-spacing:-0.2px;}
#t2n_1{left:688px;bottom:156px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t2o_1{left:411px;bottom:1225px;letter-spacing:-0.4px;}
#t2p_1{left:736px;bottom:1197px;letter-spacing:-0.2px;}
#t2q_1{left:193px;bottom:1143px;letter-spacing:-0.1px;word-spacing:0.1px;}
#t2r_1{left:197px;bottom:944px;letter-spacing:-0.2px;}
#t2s_1{left:197px;bottom:853px;letter-spacing:-0.2px;}
#t2t_1{left:501px;bottom:884px;letter-spacing:0.1px;}
#t2u_1{left:499px;bottom:869px;letter-spacing:0.1px;}
#t2v_1{left:512px;bottom:853px;letter-spacing:0.3px;}
#t2w_1{left:600px;bottom:840px;letter-spacing:0.2px;word-spacing:0.9px;}
#t2x_1{left:641px;bottom:825px;letter-spacing:0.2px;word-spacing:1.5px;}
#t2y_1{left:688px;bottom:809px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2z_1{left:649px;bottom:583px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t30_1{left:789px;bottom:583px;letter-spacing:0.3px;}
#t31_1{left:414px;bottom:1120px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t32_1{left:415px;bottom:1094px;}
#t33_1{left:194px;bottom:1071px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t34_1{left:333px;bottom:1071px;letter-spacing:0.3px;}
#t35_1{left:317px;bottom:1060px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t36_1{left:29px;bottom:1036px;letter-spacing:0.5px;word-spacing:0.8px;}
#t37_1{left:257px;bottom:1034px;letter-spacing:-0.5px;word-spacing:0.5px;}
#t38_1{left:194px;bottom:1118px;letter-spacing:-0.1px;word-spacing:0.1px;}

.s1{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s2{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s3{
	FONT-SIZE: 44px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s4{
	FONT-SIZE: 68.4px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s5{
	FONT-SIZE: 58.4px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

</style>
<!-- End inline CSS -->

<!-- Begin embedded font definitions -->
<style id="fonts1" type="text/css" >

@font-face {
	font-family: Calibri-Bold_d;
	src: url("/admin/css/put/fonts/Calibri-Bold_d.woff") format("woff");
}

@font-face {
	font-family: Calibri_h;
	src: url("/admin/css/put/fonts/Calibri_h.woff") format("woff");
}

</style>
<!-- End embedded font definitions -->

<!-- Begin page background -->
<div id="pg1Overlay" style="width:100%; height:100%; position:absolute; z-index:1; background-color:rgba(0,0,0,0); -webkit-user-select: none;"></div>
<div id="pg1" style="-webkit-user-select: none;"><object width="909" height="1286" data="/admin/css/put/1/1.svg" type="image/svg+xml" id="pdf1" style="width:909px; height:1286px; -moz-transform:scale(1); z-index: 0;"></object></div>
<!-- End page background -->


<!-- Begin text definitions (Positioned/styled in CSS) -->
<div id="t1_1" class="t s1">ПУТЕВОЙ ЛИСТ ЛЕГКОВОГО АВТОМОБИЛЯ</div>
<div id="t2_1" class="t s2">Д-1</div>
<div id="t3_1" class="t s2">№</div>
<div id="t4_1" class="t s2">85</div>
<div id="t5_1" class="t s2">серия</div>
<div id="t6_1" class="t s2">номер</div>
<div id="t7_1" class="t s2">дата</div>
<div id="t8_1" class="t s2">по ОКПО</div>
<div id="t9_1" class="t s2">Организация</div>
<div id="ta_1" class="t s2">Марка автомобиля</div>
<div id="tb_1" class="t s2">Разрешение </div>
<div id="tc_1" class="t s2">Государственный регистрационный знак</div>
<div id="td_1" class="t s3">Номер парковки</div>
<div id="te_1" class="t s2">Водитель</div>
<div id="tf_1" class="t s3">Табельный номер</div>
<div id="tg_1" class="t s4">Предрейсовый</div>
<div id="th_1" class="t s4">Медосмотр</div>
<div id="ti_1" class="t s4">Пройден</div>
<div id="tj_1" class="t s1">дата</div>
<div id="tk_1" class="t s2">г.</div>
<div id="tl_1" class="t s2">Время</div>
<div id="tm_1" class="t s2">'.$time1.'</div>
<div id="tn_1" class="t s1">должность: Врач</div>
<div id="to_1" class="t s2">подпись</div>
<div id="tp_1" class="t s2">'.$medic.'</div>
<div id="tq_1" class="t s2">Выезд с парковки</div>
<div id="tr_1" class="t s2">Дата</div>
<div id="ts_1" class="t s2">г. </div>
<div id="tt_1" class="t s2">Механик/ Диспечер ____'.$mech.'</div>
<div id="tu_1" class="t s2">Время</div>
<div id="tv_1" class="t s2">'.$time2.'</div>
<div id="tw_1" class="t s2">мин</div>
<div id="tx_1" class="t s2">Послерейсовый </div>
<div id="ty_1" class="t s2">медосмотр</div>
<div id="tz_1" class="t s2">дата, время</div>
<div id="t10_1" class="t s5">'.$today.'</div>
<div id="t11_1" class="t s2">г.</div>
<div id="t12_1" class="t s2">'.$time3.'</div>
<div id="t13_1" class="t s2">мин</div>
<div id="t14_1" class="t s2">пройден</div>
<div id="t15_1" class="t s2">дата</div>
<div id="t16_1" class="t s2">время</div>
<div id="t17_1" class="t s2">должность</div>
<div id="t18_1" class="t s2">подпись</div>
<div id="t19_1" class="t s5">Механик</div>
<div id="t1a_1" class="t s2">'.$mech.'</div>
<div id="t1b_1" class="t s3">подпись и расшифровка подписи</div>
<div id="t1c_1" class="t s2">Задание водителю</div>
<div id="t1d_1" class="t s1">Показание одометра, км</div>
<div id="t1e_1" class="t s2">В распоряжение</div>
<div id="t1f_1" class="t s2">Яндекс</div>
<div id="t1g_1" class="t s2">Механик</div>
<div id="t1h_1" class="t s2">'.$mech.'</div>
<div id="t1i_1" class="t s2">Адрес первой подачи</div>
<div id="t1j_1" class="t s2">Автомобиль в исправном состоянии принял</div>
<div id="t1k_1" class="t s2">Водитель</div>
<div id="t1l_1" class="t s2">По указанию Яндекс г. Москва и Московская область</div>
<div id="t1m_1" class="t s2">подпись</div>
<div id="t1n_1" class="t s3">расшифровка подписи</div>
<div id="t1o_1" class="t s2">Горючее</div>
<div id="t1p_1" class="t s2">марка</div>
<div id="t1q_1" class="t s2">код</div>
<div id="t1r_1" class="t s2">дата</div>
<div id="t1s_1" class="t s2">'.$year.' г.</div>
<div id="t1t_1" class="t s2">Движение горючего</div>
<div id="t1u_1" class="t s2">количество, л</div>
<div id="t1v_1" class="t s2">Время </div>
<div id="t1w_1" class="t s2">ч.</div>
<div id="t1x_1" class="t s2">мин</div>
<div id="t1y_1" class="t s2">при заезде на парковку</div>
<div id="t1z_1" class="t s2">Выдано: по заправочному</div>
<div id="t20_1" class="t s2">Диспетчер/механик</div>
<div id="t21_1" class="t s2">'.$mech.'</div>
<div id="t22_1" class="t s2">листу №</div>
<div id="t23_1" class="t s2">подпись</div>
<div id="t24_1" class="t s2">расшифровка подписи</div>
<div id="t25_1" class="t s2">остаток: </div>
<div id="t26_1" class="t s2">при выезде</div>
<div id="t27_1" class="t s2">Ожидание :</div>
<div id="t28_1" class="t s2">Опоздания, ожидания, простои в пути, заезды в гараж </div>
<div id="t29_1" class="t s2">расход</div>
<div id="t2a_1" class="t s2">по норме</div>
<div id="t2b_1" class="t s2">и прочие отметки</div>
<div id="t2c_1" class="t s2">Послерейсовый контроль технического состояния</div>
<div id="t2d_1" class="t s2">Автомобиль сдал</div>
<div id="t2e_1" class="t s2">водитель</div>
<div id="t2f_1" class="t s2">Показания одометра при</div>
<div id="t2g_1" class="t s2">подпись</div>
<div id="t2h_1" class="t s2">расшифровка подписи</div>
<div id="t2i_1" class="t s2">возвращении на парковку,</div>
<div id="t2j_1" class="t s2">км</div>
<div id="t2k_1" class="t s2">дата ; время</div>
<div id="t2l_1" class="t s2">Механик</div>
<div id="t2m_1" class="t s2">подпись</div>
<div id="t2n_1" class="t s3">расшифровка подписи</div>
<div id="t2o_1" class="t s5">'.$today.'</div>
<div id="t2p_1" class="t s2">'.$okpo.'</div>
<div id="t2q_1" class="t s2">'.$name_lic.'<br> ОГРН / ОГРНИП '.$ogrn.' ИНН '.$inn.'</div>
<div id="t2r_1" class="t s2">'.$today.'</div>
<div id="t2s_1" class="t s2">'.$today.'</div>
<div id="t2t_1" class="t s2">Предрейсовый </div>
<div id="t2u_1" class="t s2">(предсменный)</div>
<div id="t2v_1" class="t s2">(контроль)</div>
<div id="t2w_1" class="t s1">Предрейсовый контроль технического состояния</div>
<div id="t2x_1" class="t s1">транспортного средства пройден,</div>
<div id="t2y_1" class="t s1">выезд разрешен</div>
<div id="t2z_1" class="t s5">'.$fio_driver.'</div>
<div id="t30_1" class="t s5"></div>
<div id="t31_1" class="t s2">'.$razresh.'</div>
<div id="t32_1" class="t s5">'.$gosnomer.'</div>
<div id="t33_1" class="t s5">'.$fio_driver.' </div>
<div id="t34_1" class="t s5"></div>
<div id="t35_1" class="t s3">фамилия, имя, отчество</div>
<div id="t36_1" class="t s2">Удостоверение №</div>
<div id="t37_1" class="t s5">'.$udostov.'</div>
<div id="t38_1" class="t s5">'.$mark_model.'</div>

<!-- End text definitions -->


</div>







<div id="p1" style="overflow: hidden; position: relative; background-color: white; width: 909px; height: 1286px;">

<!-- Begin shared CSS values -->
<style class="shared-css" type="text/css" >
.t {
	-webkit-transform-origin: bottom left;
	-ms-transform-origin: bottom left;
	transform-origin: bottom left;
	-webkit-transform: scale(0.25);
	-ms-transform: scale(0.25);
	transform: scale(0.25);
	z-index: 2;
	position: absolute;
	white-space: pre;
	overflow: visible;
	line-height: 1.5;
}
</style>
<!-- End shared CSS values -->


<!-- Begin inline CSS -->
<style type="text/css" >

#t1_1{left:29px;bottom:1263px;letter-spacing:0.3px;word-spacing:1.7px;}
#t2_1{left:531px;bottom:1263px;}
#t3_1{left:591px;bottom:1263px;}
#t4_1{left:691px;bottom:1263px;}
#t5_1{left:493px;bottom:1251px;}
#t6_1{left:648px;bottom:1251px;letter-spacing:0.8px;}
#t7_1{left:361px;bottom:1211px;letter-spacing:0.4px;}
#t8_1{left:591px;bottom:1188px;letter-spacing:-0.3px;word-spacing:1.5px;}
#t9_1{left:29px;bottom:1165px;letter-spacing:0.3px;}
#ta_1{left:29px;bottom:1120px;letter-spacing:0.7px;word-spacing:0.8px;}
#tb_1{left:339px;bottom:1120px;letter-spacing:0.3px;}
#tc_1{left:29px;bottom:1097px;letter-spacing:0.4px;word-spacing:0.4px;}
#td_1{left:648px;bottom:1097px;letter-spacing:-0.2px;word-spacing:0.2px;}
#te_1{left:29px;bottom:1073px;letter-spacing:0.2px;}
#tf_1{left:648px;bottom:1074px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tg_1{left:29px;bottom:1010px;letter-spacing:0.1px;}
#th_1{left:29px;bottom:987px;letter-spacing:-0.2px;}
#ti_1{left:29px;bottom:965px;letter-spacing:0.3px;}
#tj_1{left:29px;bottom:944px;letter-spacing:0.5px;}
#tk_1{left:257px;bottom:944px;}
#tl_1{left:336px;bottom:944px;letter-spacing:0.7px;}
#tm_1{left:424px;bottom:944px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tn_1{left:29px;bottom:921px;letter-spacing:0.5px;word-spacing:-0.5px;}
#to_1{left:193px;bottom:921px;letter-spacing:-0.2px;}
#tp_1{left:336px;bottom:921px;letter-spacing:0.7px;word-spacing:-0.7px;}
#tq_1{left:29px;bottom:876px;letter-spacing:0.1px;word-spacing:-0.1px;}
#tr_1{left:29px;bottom:853px;letter-spacing:0.4px;}
#ts_1{left:257px;bottom:853px;}
#tt_1{left:270px;bottom:853px;letter-spacing:0.2px;word-spacing:1.5px;}
#tu_1{left:29px;bottom:829px;letter-spacing:0.7px;}
#tv_1{left:114px;bottom:829px;}
#tw_1{left:257px;bottom:829px;letter-spacing:0.8px;}
#tx_1{left:29px;bottom:806px;letter-spacing:0.1px;}
#ty_1{left:29px;bottom:783px;letter-spacing:0.4px;}
#tz_1{left:414px;bottom:783px;letter-spacing:0.5px;word-spacing:1.7px;}
#t10_1{left:508px;bottom:781px;letter-spacing:-0.4px;}
#t11_1{left:591px;bottom:783px;}
#t12_1{left:705px;bottom:783px;}
#t13_1{left:749px;bottom:783px;letter-spacing:0.8px;}
#t14_1{left:29px;bottom:759px;letter-spacing:0.2px;}
#t15_1{left:493px;bottom:759px;letter-spacing:0.4px;}
#t16_1{left:648px;bottom:759px;letter-spacing:0.9px;}
#t17_1{left:257px;bottom:744px;letter-spacing:0.2px;}
#t18_1{left:414px;bottom:744px;letter-spacing:-0.2px;}
#t19_1{left:494px;bottom:719px;letter-spacing:-0.2px;}
#t1a_1{left:749px;bottom:721px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1b_1{left:648px;bottom:699px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1c_1{left:29px;bottom:653px;letter-spacing:0.3px;word-spacing:1px;}
#t1d_1{left:493px;bottom:653px;letter-spacing:0.3px;word-spacing:0.4px;}
#t1e_1{left:29px;bottom:631px;letter-spacing:0.2px;word-spacing:1.3px;}
#t1f_1{left:294px;bottom:631px;}
#t1g_1{left:591px;bottom:631px;letter-spacing:0.3px;}
#t1h_1{left:749px;bottom:631px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1i_1{left:29px;bottom:609px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1j_1{left:591px;bottom:609px;letter-spacing:0.5px;word-spacing:-0.1px;}
#t1k_1{left:493px;bottom:586px;letter-spacing:0.2px;}
#t1l_1{left:29px;bottom:562px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t1m_1{left:591px;bottom:562px;letter-spacing:-0.2px;}
#t1n_1{left:688px;bottom:562px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1o_1{left:493px;bottom:539px;letter-spacing:0.3px;}
#t1p_1{left:680px;bottom:539px;letter-spacing:0.5px;}
#t1q_1{left:802px;bottom:539px;}
#t1r_1{left:29px;bottom:494px;letter-spacing:0.4px;}
#t1s_1{left:306px;bottom:494px;word-spacing:5.9px;}
#t1t_1{left:591px;bottom:494px;letter-spacing:0.3px;word-spacing:1.4px;}
#t1u_1{left:749px;bottom:471px;word-spacing:2.3px;}
#t1v_1{left:29px;bottom:449px;letter-spacing:0.6px;}
#t1w_1{left:74px;bottom:449px;}
#t1x_1{left:193px;bottom:449px;letter-spacing:0.8px;}
#t1y_1{left:29px;bottom:426px;letter-spacing:0.2px;word-spacing:0.8px;}
#t1z_1{left:493px;bottom:426px;letter-spacing:0.3px;word-spacing:0.5px;}
#t20_1{left:29px;bottom:404px;letter-spacing:0.2px;}
#t21_1{left:336px;bottom:404px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t22_1{left:493px;bottom:404px;}
#t23_1{left:273px;bottom:381px;letter-spacing:-0.2px;}
#t24_1{left:336px;bottom:381px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t25_1{left:493px;bottom:381px;letter-spacing:0.2px;}
#t26_1{left:575px;bottom:381px;letter-spacing:0.2px;word-spacing:1.4px;}
#t27_1{left:29px;bottom:358px;letter-spacing:0.2px;word-spacing:1.5px;}
#t28_1{left:114px;bottom:336px;letter-spacing:0.3px;word-spacing:0.3px;}
#t29_1{left:493px;bottom:336px;letter-spacing:0.4px;}
#t2a_1{left:648px;bottom:336px;letter-spacing:0.7px;word-spacing:0.6px;}
#t2b_1{left:29px;bottom:313px;letter-spacing:0.4px;word-spacing:0.4px;}
#t2c_1{left:493px;bottom:313px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t2d_1{left:29px;bottom:290px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2e_1{left:29px;bottom:268px;letter-spacing:0.3px;}
#t2f_1{left:493px;bottom:268px;letter-spacing:0.3px;word-spacing:0.8px;}
#t2g_1{left:193px;bottom:246px;letter-spacing:-0.2px;}
#t2h_1{left:314px;bottom:246px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t2i_1{left:493px;bottom:246px;letter-spacing:0.5px;word-spacing:0.3px;}
#t2j_1{left:493px;bottom:223px;}
#t2k_1{left:749px;bottom:223px;letter-spacing:0.4px;word-spacing:0.7px;}
#t2l_1{left:493px;bottom:178px;letter-spacing:0.3px;}
#t2m_1{left:591px;bottom:155px;letter-spacing:-0.2px;}
#t2n_1{left:688px;bottom:156px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t2o_1{left:411px;bottom:1225px;letter-spacing:-0.4px;}
#t2p_1{left:736px;bottom:1197px;letter-spacing:-0.2px;}
#t2q_1{left:193px;bottom:1143px;letter-spacing:-0.1px;word-spacing:0.1px;}
#t2r_1{left:197px;bottom:944px;letter-spacing:-0.2px;}
#t2s_1{left:197px;bottom:853px;letter-spacing:-0.2px;}
#t2t_1{left:501px;bottom:884px;letter-spacing:0.1px;}
#t2u_1{left:499px;bottom:869px;letter-spacing:0.1px;}
#t2v_1{left:512px;bottom:853px;letter-spacing:0.3px;}
#t2w_1{left:600px;bottom:840px;letter-spacing:0.2px;word-spacing:0.9px;}
#t2x_1{left:641px;bottom:825px;letter-spacing:0.2px;word-spacing:1.5px;}
#t2y_1{left:688px;bottom:809px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2z_1{left:649px;bottom:583px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t30_1{left:789px;bottom:583px;letter-spacing:0.3px;}
#t31_1{left:414px;bottom:1120px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t32_1{left:415px;bottom:1094px;}
#t33_1{left:194px;bottom:1071px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t34_1{left:333px;bottom:1071px;letter-spacing:0.3px;}
#t35_1{left:317px;bottom:1060px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t36_1{left:29px;bottom:1036px;letter-spacing:0.5px;word-spacing:0.8px;}
#t37_1{left:257px;bottom:1034px;letter-spacing:-0.5px;word-spacing:0.5px;}
#t38_1{left:194px;bottom:1118px;letter-spacing:-0.1px;word-spacing:0.1px;}

.s1{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s2{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s3{
	FONT-SIZE: 44px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s4{
	FONT-SIZE: 68.4px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s5{
	FONT-SIZE: 58.4px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

</style>
<!-- End inline CSS -->

<!-- Begin embedded font definitions -->
<style id="fonts1" type="text/css" >

@font-face {
	font-family: Calibri-Bold_d;
	src: url("/admin/css/put/fonts/Calibri-Bold_d.woff") format("woff");
}

@font-face {
	font-family: Calibri_h;
	src: url("/admin/css/put/fonts/Calibri_h.woff") format("woff");
}

</style>
<!-- End embedded font definitions -->

<!-- Begin page background -->
<div id="pg1Overlay" style="width:100%; height:100%; position:absolute; z-index:1; background-color:rgba(0,0,0,0); -webkit-user-select: none;"></div>
<div id="pg1" style="-webkit-user-select: none;"><object width="909" height="1286" data="/admin/css/put/1/1.svg" type="image/svg+xml" id="pdf1" style="width:909px; height:1286px; -moz-transform:scale(1); z-index: 0;"></object></div>
<!-- End page background -->


<!-- Begin text definitions (Positioned/styled in CSS) -->
<div id="t1_1" class="t s1">ПУТЕВОЙ ЛИСТ ЛЕГКОВОГО АВТОМОБИЛЯ</div>
<div id="t2_1" class="t s2">Д-1</div>
<div id="t3_1" class="t s2">№</div>
<div id="t4_1" class="t s2">85</div>
<div id="t5_1" class="t s2">серия</div>
<div id="t6_1" class="t s2">номер</div>
<div id="t7_1" class="t s2">дата</div>
<div id="t8_1" class="t s2">по ОКПО</div>
<div id="t9_1" class="t s2">Организация</div>
<div id="ta_1" class="t s2">Марка автомобиля</div>
<div id="tb_1" class="t s2">Разрешение </div>
<div id="tc_1" class="t s2">Государственный регистрационный знак</div>
<div id="td_1" class="t s3">Номер парковки</div>
<div id="te_1" class="t s2">Водитель</div>
<div id="tf_1" class="t s3">Табельный номер</div>
<div id="tg_1" class="t s4">Предрейсовый</div>
<div id="th_1" class="t s4">Медосмотр</div>
<div id="ti_1" class="t s4">Пройден</div>
<div id="tj_1" class="t s1">дата</div>
<div id="tk_1" class="t s2">г.</div>
<div id="tl_1" class="t s2">Время</div>
<div id="tm_1" class="t s2">'.$time4.'</div>
<div id="tn_1" class="t s1">должность: Врач</div>
<div id="to_1" class="t s2">подпись</div>
<div id="tp_1" class="t s2">'.$medic.'</div>
<div id="tq_1" class="t s2">Выезд с парковки</div>
<div id="tr_1" class="t s2">Дата</div>
<div id="ts_1" class="t s2">г. </div>
<div id="tt_1" class="t s2">Механик/ Диспечер ____'.$mech.'</div>
<div id="tu_1" class="t s2">Время</div>
<div id="tv_1" class="t s2">'.$time5.'</div>
<div id="tw_1" class="t s2">мин</div>
<div id="tx_1" class="t s2">Послерейсовый </div>
<div id="ty_1" class="t s2">медосмотр</div>
<div id="tz_1" class="t s2">дата, время</div>
<div id="t10_1" class="t s5">'.$today.'</div>
<div id="t11_1" class="t s2">г.</div>
<div id="t12_1" class="t s2">'.$time6.'</div>
<div id="t13_1" class="t s2">мин</div>
<div id="t14_1" class="t s2">пройден</div>
<div id="t15_1" class="t s2">дата</div>
<div id="t16_1" class="t s2">время</div>
<div id="t17_1" class="t s2">должность</div>
<div id="t18_1" class="t s2">подпись</div>
<div id="t19_1" class="t s5">Механик</div>
<div id="t1a_1" class="t s2">'.$mech.'</div>
<div id="t1b_1" class="t s3">подпись и расшифровка подписи</div>
<div id="t1c_1" class="t s2">Задание водителю</div>
<div id="t1d_1" class="t s1">Показание одометра, км</div>
<div id="t1e_1" class="t s2">В распоряжение</div>
<div id="t1f_1" class="t s2">Яндекс</div>
<div id="t1g_1" class="t s2">Механик</div>
<div id="t1h_1" class="t s2">'.$mech.'</div>
<div id="t1i_1" class="t s2">Адрес первой подачи</div>
<div id="t1j_1" class="t s2">Автомобиль в исправном состоянии принял</div>
<div id="t1k_1" class="t s2">Водитель</div>
<div id="t1l_1" class="t s2">По указанию Яндекс г. Москва и Московская область</div>
<div id="t1m_1" class="t s2">подпись</div>
<div id="t1n_1" class="t s3">расшифровка подписи</div>
<div id="t1o_1" class="t s2">Горючее</div>
<div id="t1p_1" class="t s2">марка</div>
<div id="t1q_1" class="t s2">код</div>
<div id="t1r_1" class="t s2">дата</div>
<div id="t1s_1" class="t s2">'.$year.' г.</div>
<div id="t1t_1" class="t s2">Движение горючего</div>
<div id="t1u_1" class="t s2">количество, л</div>
<div id="t1v_1" class="t s2">Время </div>
<div id="t1w_1" class="t s2">ч.</div>
<div id="t1x_1" class="t s2">мин</div>
<div id="t1y_1" class="t s2">при заезде на парковку</div>
<div id="t1z_1" class="t s2">Выдано: по заправочному</div>
<div id="t20_1" class="t s2">Диспетчер/механик</div>
<div id="t21_1" class="t s2">'.$mech.'</div>
<div id="t22_1" class="t s2">листу №</div>
<div id="t23_1" class="t s2">подпись</div>
<div id="t24_1" class="t s2">расшифровка подписи</div>
<div id="t25_1" class="t s2">остаток: </div>
<div id="t26_1" class="t s2">при выезде</div>
<div id="t27_1" class="t s2">Ожидание :</div>
<div id="t28_1" class="t s2">Опоздания, ожидания, простои в пути, заезды в гараж </div>
<div id="t29_1" class="t s2">расход</div>
<div id="t2a_1" class="t s2">по норме</div>
<div id="t2b_1" class="t s2">и прочие отметки</div>
<div id="t2c_1" class="t s2">Послерейсовый контроль технического состояния</div>
<div id="t2d_1" class="t s2">Автомобиль сдал</div>
<div id="t2e_1" class="t s2">водитель</div>
<div id="t2f_1" class="t s2">Показания одометра при</div>
<div id="t2g_1" class="t s2">подпись</div>
<div id="t2h_1" class="t s2">расшифровка подписи</div>
<div id="t2i_1" class="t s2">возвращении на парковку,</div>
<div id="t2j_1" class="t s2">км</div>
<div id="t2k_1" class="t s2">дата ; время</div>
<div id="t2l_1" class="t s2">Механик</div>
<div id="t2m_1" class="t s2">подпись</div>
<div id="t2n_1" class="t s3">расшифровка подписи</div>
<div id="t2o_1" class="t s5">'.$today.'</div>
<div id="t2p_1" class="t s2">'.$okpo.'</div>
<div id="t2q_1" class="t s2">'.$name_lic.'<br> ОГРН / ОГРНИП '.$ogrn.' ИНН '.$inn.'</div>
<div id="t2r_1" class="t s2">'.$today.'</div>
<div id="t2s_1" class="t s2">'.$today.'</div>
<div id="t2t_1" class="t s2">Предрейсовый </div>
<div id="t2u_1" class="t s2">(предсменный)</div>
<div id="t2v_1" class="t s2">(контроль)</div>
<div id="t2w_1" class="t s1">Предрейсовый контроль технического состояния</div>
<div id="t2x_1" class="t s1">транспортного средства пройден,</div>
<div id="t2y_1" class="t s1">выезд разрешен</div>
<div id="t2z_1" class="t s5">'.$fio_driver.' </div>
<div id="t30_1" class="t s5"></div>
<div id="t31_1" class="t s2">'.$razresh.'</div>
<div id="t32_1" class="t s5">'.$gosnomer.'</div>
<div id="t33_1" class="t s5">'.$fio_driver.' </div>
<div id="t34_1" class="t s5"></div>
<div id="t35_1" class="t s3">фамилия, имя, отчество</div>
<div id="t36_1" class="t s2">Удостоверение №</div>
<div id="t37_1" class="t s5">'.$udostov.'</div>
<div id="t38_1" class="t s5">'.$mark_model.'</div>

<!-- End text definitions -->


</div>





<div id="p1" style="overflow: hidden; position: relative; background-color: white; width: 909px; height: 1286px;">

<!-- Begin shared CSS values -->
<style class="shared-css" type="text/css" >
.t {
	-webkit-transform-origin: bottom left;
	-ms-transform-origin: bottom left;
	transform-origin: bottom left;
	-webkit-transform: scale(0.25);
	-ms-transform: scale(0.25);
	transform: scale(0.25);
	z-index: 2;
	position: absolute;
	white-space: pre;
	overflow: visible;
	line-height: 1.5;
}
</style>
<!-- End shared CSS values -->


<!-- Begin inline CSS -->
<style type="text/css" >

#t1_1{left:29px;bottom:1263px;letter-spacing:0.3px;word-spacing:1.7px;}
#t2_1{left:531px;bottom:1263px;}
#t3_1{left:591px;bottom:1263px;}
#t4_1{left:691px;bottom:1263px;}
#t5_1{left:493px;bottom:1251px;}
#t6_1{left:648px;bottom:1251px;letter-spacing:0.8px;}
#t7_1{left:361px;bottom:1211px;letter-spacing:0.4px;}
#t8_1{left:591px;bottom:1188px;letter-spacing:-0.3px;word-spacing:1.5px;}
#t9_1{left:29px;bottom:1165px;letter-spacing:0.3px;}
#ta_1{left:29px;bottom:1120px;letter-spacing:0.7px;word-spacing:0.8px;}
#tb_1{left:339px;bottom:1120px;letter-spacing:0.3px;}
#tc_1{left:29px;bottom:1097px;letter-spacing:0.4px;word-spacing:0.4px;}
#td_1{left:648px;bottom:1097px;letter-spacing:-0.2px;word-spacing:0.2px;}
#te_1{left:29px;bottom:1073px;letter-spacing:0.2px;}
#tf_1{left:648px;bottom:1074px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tg_1{left:29px;bottom:1010px;letter-spacing:0.1px;}
#th_1{left:29px;bottom:987px;letter-spacing:-0.2px;}
#ti_1{left:29px;bottom:965px;letter-spacing:0.3px;}
#tj_1{left:29px;bottom:944px;letter-spacing:0.5px;}
#tk_1{left:257px;bottom:944px;}
#tl_1{left:336px;bottom:944px;letter-spacing:0.7px;}
#tm_1{left:424px;bottom:944px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tn_1{left:29px;bottom:921px;letter-spacing:0.5px;word-spacing:-0.5px;}
#to_1{left:193px;bottom:921px;letter-spacing:-0.2px;}
#tp_1{left:336px;bottom:921px;letter-spacing:0.7px;word-spacing:-0.7px;}
#tq_1{left:29px;bottom:876px;letter-spacing:0.1px;word-spacing:-0.1px;}
#tr_1{left:29px;bottom:853px;letter-spacing:0.4px;}
#ts_1{left:257px;bottom:853px;}
#tt_1{left:270px;bottom:853px;letter-spacing:0.2px;word-spacing:1.5px;}
#tu_1{left:29px;bottom:829px;letter-spacing:0.7px;}
#tv_1{left:114px;bottom:829px;}
#tw_1{left:257px;bottom:829px;letter-spacing:0.8px;}
#tx_1{left:29px;bottom:806px;letter-spacing:0.1px;}
#ty_1{left:29px;bottom:783px;letter-spacing:0.4px;}
#tz_1{left:414px;bottom:783px;letter-spacing:0.5px;word-spacing:1.7px;}
#t10_1{left:508px;bottom:781px;letter-spacing:-0.4px;}
#t11_1{left:591px;bottom:783px;}
#t12_1{left:705px;bottom:783px;}
#t13_1{left:749px;bottom:783px;letter-spacing:0.8px;}
#t14_1{left:29px;bottom:759px;letter-spacing:0.2px;}
#t15_1{left:493px;bottom:759px;letter-spacing:0.4px;}
#t16_1{left:648px;bottom:759px;letter-spacing:0.9px;}
#t17_1{left:257px;bottom:744px;letter-spacing:0.2px;}
#t18_1{left:414px;bottom:744px;letter-spacing:-0.2px;}
#t19_1{left:494px;bottom:719px;letter-spacing:-0.2px;}
#t1a_1{left:749px;bottom:721px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1b_1{left:648px;bottom:699px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1c_1{left:29px;bottom:653px;letter-spacing:0.3px;word-spacing:1px;}
#t1d_1{left:493px;bottom:653px;letter-spacing:0.3px;word-spacing:0.4px;}
#t1e_1{left:29px;bottom:631px;letter-spacing:0.2px;word-spacing:1.3px;}
#t1f_1{left:294px;bottom:631px;}
#t1g_1{left:591px;bottom:631px;letter-spacing:0.3px;}
#t1h_1{left:749px;bottom:631px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1i_1{left:29px;bottom:609px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1j_1{left:591px;bottom:609px;letter-spacing:0.5px;word-spacing:-0.1px;}
#t1k_1{left:493px;bottom:586px;letter-spacing:0.2px;}
#t1l_1{left:29px;bottom:562px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t1m_1{left:591px;bottom:562px;letter-spacing:-0.2px;}
#t1n_1{left:688px;bottom:562px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1o_1{left:493px;bottom:539px;letter-spacing:0.3px;}
#t1p_1{left:680px;bottom:539px;letter-spacing:0.5px;}
#t1q_1{left:802px;bottom:539px;}
#t1r_1{left:29px;bottom:494px;letter-spacing:0.4px;}
#t1s_1{left:306px;bottom:494px;word-spacing:5.9px;}
#t1t_1{left:591px;bottom:494px;letter-spacing:0.3px;word-spacing:1.4px;}
#t1u_1{left:749px;bottom:471px;word-spacing:2.3px;}
#t1v_1{left:29px;bottom:449px;letter-spacing:0.6px;}
#t1w_1{left:74px;bottom:449px;}
#t1x_1{left:193px;bottom:449px;letter-spacing:0.8px;}
#t1y_1{left:29px;bottom:426px;letter-spacing:0.2px;word-spacing:0.8px;}
#t1z_1{left:493px;bottom:426px;letter-spacing:0.3px;word-spacing:0.5px;}
#t20_1{left:29px;bottom:404px;letter-spacing:0.2px;}
#t21_1{left:336px;bottom:404px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t22_1{left:493px;bottom:404px;}
#t23_1{left:273px;bottom:381px;letter-spacing:-0.2px;}
#t24_1{left:336px;bottom:381px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t25_1{left:493px;bottom:381px;letter-spacing:0.2px;}
#t26_1{left:575px;bottom:381px;letter-spacing:0.2px;word-spacing:1.4px;}
#t27_1{left:29px;bottom:358px;letter-spacing:0.2px;word-spacing:1.5px;}
#t28_1{left:114px;bottom:336px;letter-spacing:0.3px;word-spacing:0.3px;}
#t29_1{left:493px;bottom:336px;letter-spacing:0.4px;}
#t2a_1{left:648px;bottom:336px;letter-spacing:0.7px;word-spacing:0.6px;}
#t2b_1{left:29px;bottom:313px;letter-spacing:0.4px;word-spacing:0.4px;}
#t2c_1{left:493px;bottom:313px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t2d_1{left:29px;bottom:290px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2e_1{left:29px;bottom:268px;letter-spacing:0.3px;}
#t2f_1{left:493px;bottom:268px;letter-spacing:0.3px;word-spacing:0.8px;}
#t2g_1{left:193px;bottom:246px;letter-spacing:-0.2px;}
#t2h_1{left:314px;bottom:246px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t2i_1{left:493px;bottom:246px;letter-spacing:0.5px;word-spacing:0.3px;}
#t2j_1{left:493px;bottom:223px;}
#t2k_1{left:749px;bottom:223px;letter-spacing:0.4px;word-spacing:0.7px;}
#t2l_1{left:493px;bottom:178px;letter-spacing:0.3px;}
#t2m_1{left:591px;bottom:155px;letter-spacing:-0.2px;}
#t2n_1{left:688px;bottom:156px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t2o_1{left:411px;bottom:1225px;letter-spacing:-0.4px;}
#t2p_1{left:736px;bottom:1197px;letter-spacing:-0.2px;}
#t2q_1{left:193px;bottom:1143px;letter-spacing:-0.1px;word-spacing:0.1px;}
#t2r_1{left:197px;bottom:944px;letter-spacing:-0.2px;}
#t2s_1{left:197px;bottom:853px;letter-spacing:-0.2px;}
#t2t_1{left:501px;bottom:884px;letter-spacing:0.1px;}
#t2u_1{left:499px;bottom:869px;letter-spacing:0.1px;}
#t2v_1{left:512px;bottom:853px;letter-spacing:0.3px;}
#t2w_1{left:600px;bottom:840px;letter-spacing:0.2px;word-spacing:0.9px;}
#t2x_1{left:641px;bottom:825px;letter-spacing:0.2px;word-spacing:1.5px;}
#t2y_1{left:688px;bottom:809px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2z_1{left:649px;bottom:583px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t30_1{left:789px;bottom:583px;letter-spacing:0.3px;}
#t31_1{left:414px;bottom:1120px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t32_1{left:415px;bottom:1094px;}
#t33_1{left:194px;bottom:1071px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t34_1{left:333px;bottom:1071px;letter-spacing:0.3px;}
#t35_1{left:317px;bottom:1060px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t36_1{left:29px;bottom:1036px;letter-spacing:0.5px;word-spacing:0.8px;}
#t37_1{left:257px;bottom:1034px;letter-spacing:-0.5px;word-spacing:0.5px;}
#t38_1{left:194px;bottom:1118px;letter-spacing:-0.1px;word-spacing:0.1px;}

.s1{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s2{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s3{
	FONT-SIZE: 44px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s4{
	FONT-SIZE: 68.4px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s5{
	FONT-SIZE: 58.4px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

</style>
<!-- End inline CSS -->

<!-- Begin embedded font definitions -->
<style id="fonts1" type="text/css" >

@font-face {
	font-family: Calibri-Bold_d;
	src: url("/admin/css/put/fonts/Calibri-Bold_d.woff") format("woff");
}

@font-face {
	font-family: Calibri_h;
	src: url("/admin/css/put/fonts/Calibri_h.woff") format("woff");
}

</style>
<!-- End embedded font definitions -->

<!-- Begin page background -->
<div id="pg1Overlay" style="width:100%; height:100%; position:absolute; z-index:1; background-color:rgba(0,0,0,0); -webkit-user-select: none;"></div>
<div id="pg1" style="-webkit-user-select: none;"><object width="909" height="1286" data="/admin/css/put/1/1.svg" type="image/svg+xml" id="pdf1" style="width:909px; height:1286px; -moz-transform:scale(1); z-index: 0;"></object></div>
<!-- End page background -->


<!-- Begin text definitions (Positioned/styled in CSS) -->
<div id="t1_1" class="t s1">ПУТЕВОЙ ЛИСТ ЛЕГКОВОГО АВТОМОБИЛЯ</div>
<div id="t2_1" class="t s2">Д-1</div>
<div id="t3_1" class="t s2">№</div>
<div id="t4_1" class="t s2">85</div>
<div id="t5_1" class="t s2">серия</div>
<div id="t6_1" class="t s2">номер</div>
<div id="t7_1" class="t s2">дата</div>
<div id="t8_1" class="t s2">по ОКПО</div>
<div id="t9_1" class="t s2">Организация</div>
<div id="ta_1" class="t s2">Марка автомобиля</div>
<div id="tb_1" class="t s2">Разрешение </div>
<div id="tc_1" class="t s2">Государственный регистрационный знак</div>
<div id="td_1" class="t s3">Номер парковки</div>
<div id="te_1" class="t s2">Водитель</div>
<div id="tf_1" class="t s3">Табельный номер</div>
<div id="tg_1" class="t s4">Предрейсовый</div>
<div id="th_1" class="t s4">Медосмотр</div>
<div id="ti_1" class="t s4">Пройден</div>
<div id="tj_1" class="t s1">дата</div>
<div id="tk_1" class="t s2">г.</div>
<div id="tl_1" class="t s2">Время</div>
<div id="tm_1" class="t s2">'.$time7.'</div>
<div id="tn_1" class="t s1">должность: Врач</div>
<div id="to_1" class="t s2">подпись</div>
<div id="tp_1" class="t s2">'.$medic.'</div>
<div id="tq_1" class="t s2">Выезд с парковки</div>
<div id="tr_1" class="t s2">Дата</div>
<div id="ts_1" class="t s2">г. </div>
<div id="tt_1" class="t s2">Механик/ Диспечер ____'.$mech.'</div>
<div id="tu_1" class="t s2">Время</div>
<div id="tv_1" class="t s2">'.$time8.'</div>
<div id="tw_1" class="t s2">мин</div>
<div id="tx_1" class="t s2">Послерейсовый </div>
<div id="ty_1" class="t s2">медосмотр</div>
<div id="tz_1" class="t s2">дата, время</div>
<div id="t10_1" class="t s5">'.$today.'</div>
<div id="t11_1" class="t s2">г.</div>
<div id="t12_1" class="t s2">'.$time9.'</div>
<div id="t13_1" class="t s2">мин</div>
<div id="t14_1" class="t s2">пройден</div>
<div id="t15_1" class="t s2">дата</div>
<div id="t16_1" class="t s2">время</div>
<div id="t17_1" class="t s2">должность</div>
<div id="t18_1" class="t s2">подпись</div>
<div id="t19_1" class="t s5">Механик</div>
<div id="t1a_1" class="t s2">'.$mech.'</div>
<div id="t1b_1" class="t s3">подпись и расшифровка подписи</div>
<div id="t1c_1" class="t s2">Задание водителю</div>
<div id="t1d_1" class="t s1">Показание одометра, км</div>
<div id="t1e_1" class="t s2">В распоряжение</div>
<div id="t1f_1" class="t s2">Яндекс</div>
<div id="t1g_1" class="t s2">Механик</div>
<div id="t1h_1" class="t s2">'.$mech.'</div>
<div id="t1i_1" class="t s2">Адрес первой подачи</div>
<div id="t1j_1" class="t s2">Автомобиль в исправном состоянии принял</div>
<div id="t1k_1" class="t s2">Водитель</div>
<div id="t1l_1" class="t s2">По указанию Яндекс г. Москва и Московская область</div>
<div id="t1m_1" class="t s2">подпись</div>
<div id="t1n_1" class="t s3">расшифровка подписи</div>
<div id="t1o_1" class="t s2">Горючее</div>
<div id="t1p_1" class="t s2">марка</div>
<div id="t1q_1" class="t s2">код</div>
<div id="t1r_1" class="t s2">дата</div>
<div id="t1s_1" class="t s2">'.$year.' г.</div>
<div id="t1t_1" class="t s2">Движение горючего</div>
<div id="t1u_1" class="t s2">количество, л</div>
<div id="t1v_1" class="t s2">Время </div>
<div id="t1w_1" class="t s2">ч.</div>
<div id="t1x_1" class="t s2">мин</div>
<div id="t1y_1" class="t s2">при заезде на парковку</div>
<div id="t1z_1" class="t s2">Выдано: по заправочному</div>
<div id="t20_1" class="t s2">Диспетчер/механик</div>
<div id="t21_1" class="t s2">'.$mech.'</div>
<div id="t22_1" class="t s2">листу №</div>
<div id="t23_1" class="t s2">подпись</div>
<div id="t24_1" class="t s2">расшифровка подписи</div>
<div id="t25_1" class="t s2">остаток: </div>
<div id="t26_1" class="t s2">при выезде</div>
<div id="t27_1" class="t s2">Ожидание :</div>
<div id="t28_1" class="t s2">Опоздания, ожидания, простои в пути, заезды в гараж </div>
<div id="t29_1" class="t s2">расход</div>
<div id="t2a_1" class="t s2">по норме</div>
<div id="t2b_1" class="t s2">и прочие отметки</div>
<div id="t2c_1" class="t s2">Послерейсовый контроль технического состояния</div>
<div id="t2d_1" class="t s2">Автомобиль сдал</div>
<div id="t2e_1" class="t s2">водитель</div>
<div id="t2f_1" class="t s2">Показания одометра при</div>
<div id="t2g_1" class="t s2">подпись</div>
<div id="t2h_1" class="t s2">расшифровка подписи</div>
<div id="t2i_1" class="t s2">возвращении на парковку,</div>
<div id="t2j_1" class="t s2">км</div>
<div id="t2k_1" class="t s2">дата ; время</div>
<div id="t2l_1" class="t s2">Механик</div>
<div id="t2m_1" class="t s2">подпись</div>
<div id="t2n_1" class="t s3">расшифровка подписи</div>
<div id="t2o_1" class="t s5">'.$today.'</div>
<div id="t2p_1" class="t s2">'.$okpo.'</div>
<div id="t2q_1" class="t s2">'.$name_lic.'<br> ОГРН / ОГРНИП '.$ogrn.' ИНН '.$inn.'</div>
<div id="t2r_1" class="t s2">'.$today.'</div>
<div id="t2s_1" class="t s2">'.$today.'</div>
<div id="t2t_1" class="t s2">Предрейсовый </div>
<div id="t2u_1" class="t s2">(предсменный)</div>
<div id="t2v_1" class="t s2">(контроль)</div>
<div id="t2w_1" class="t s1">Предрейсовый контроль технического состояния</div>
<div id="t2x_1" class="t s1">транспортного средства пройден,</div>
<div id="t2y_1" class="t s1">выезд разрешен</div>
<div id="t2z_1" class="t s5">'.$fio_driver.' </div>
<div id="t30_1" class="t s5"></div>
<div id="t31_1" class="t s2">'.$razresh.'</div>
<div id="t32_1" class="t s5">'.$gosnomer.'</div>
<div id="t33_1" class="t s5">'.$fio_driver.'</div>
<div id="t34_1" class="t s5"></div>
<div id="t35_1" class="t s3">фамилия, имя, отчество</div>
<div id="t36_1" class="t s2">Удостоверение №</div>
<div id="t37_1" class="t s5">'.$udostov.'</div>
<div id="t38_1" class="t s5">'.$mark_model.'</div>

<!-- End text definitions -->


</div>








</body>
</html>

		
		';			
				
			
			
			
		} else {
			$today = '';
			$year = '';
		
		
		
			$time1 = '';
			$time2 = '';
			$time3 = '';
			$time4 = '';
			$time5 = '';
			$time6 = '';
			$time7 = '';
			$time8 = '';
			$time9 = '';
		
		
			$html.= '
		
		<!DOCTYPE html >
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8" />
</head>

<body style="margin: 0;">

<div id="p1" style="overflow: hidden; position: relative; background-color: white; width: 909px; height: 1286px;">

<!-- Begin shared CSS values -->
<style class="shared-css" type="text/css" >
.t {
	-webkit-transform-origin: bottom left;
	-ms-transform-origin: bottom left;
	transform-origin: bottom left;
	-webkit-transform: scale(0.25);
	-ms-transform: scale(0.25);
	transform: scale(0.25);
	z-index: 2;
	position: absolute;
	white-space: pre;
	overflow: visible;
	line-height: 1.5;
}
</style>
<!-- End shared CSS values -->


<!-- Begin inline CSS -->
<style type="text/css" >

#t1_1{left:29px;bottom:1263px;letter-spacing:0.3px;word-spacing:1.7px;}
#t2_1{left:531px;bottom:1263px;}
#t3_1{left:591px;bottom:1263px;}
#t4_1{left:691px;bottom:1263px;}
#t5_1{left:493px;bottom:1251px;}
#t6_1{left:648px;bottom:1251px;letter-spacing:0.8px;}
#t7_1{left:361px;bottom:1211px;letter-spacing:0.4px;}
#t8_1{left:591px;bottom:1188px;letter-spacing:-0.3px;word-spacing:1.5px;}
#t9_1{left:29px;bottom:1165px;letter-spacing:0.3px;}
#ta_1{left:29px;bottom:1120px;letter-spacing:0.7px;word-spacing:0.8px;}
#tb_1{left:339px;bottom:1120px;letter-spacing:0.3px;}
#tc_1{left:29px;bottom:1097px;letter-spacing:0.4px;word-spacing:0.4px;}
#td_1{left:648px;bottom:1097px;letter-spacing:-0.2px;word-spacing:0.2px;}
#te_1{left:29px;bottom:1073px;letter-spacing:0.2px;}
#tf_1{left:648px;bottom:1074px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tg_1{left:29px;bottom:1010px;letter-spacing:0.1px;}
#th_1{left:29px;bottom:987px;letter-spacing:-0.2px;}
#ti_1{left:29px;bottom:965px;letter-spacing:0.3px;}
#tj_1{left:29px;bottom:944px;letter-spacing:0.5px;}
#tk_1{left:257px;bottom:944px;}
#tl_1{left:336px;bottom:944px;letter-spacing:0.7px;}
#tm_1{left:424px;bottom:944px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tn_1{left:29px;bottom:921px;letter-spacing:0.5px;word-spacing:-0.5px;}
#to_1{left:193px;bottom:921px;letter-spacing:-0.2px;}
#tp_1{left:336px;bottom:921px;letter-spacing:0.7px;word-spacing:-0.7px;}
#tq_1{left:29px;bottom:876px;letter-spacing:0.1px;word-spacing:-0.1px;}
#tr_1{left:29px;bottom:853px;letter-spacing:0.4px;}
#ts_1{left:257px;bottom:853px;}
#tt_1{left:270px;bottom:853px;letter-spacing:0.2px;word-spacing:1.5px;}
#tu_1{left:29px;bottom:829px;letter-spacing:0.7px;}
#tv_1{left:114px;bottom:829px;}
#tw_1{left:257px;bottom:829px;letter-spacing:0.8px;}
#tx_1{left:29px;bottom:806px;letter-spacing:0.1px;}
#ty_1{left:29px;bottom:783px;letter-spacing:0.4px;}
#tz_1{left:414px;bottom:783px;letter-spacing:0.5px;word-spacing:1.7px;}
#t10_1{left:508px;bottom:781px;letter-spacing:-0.4px;}
#t11_1{left:591px;bottom:783px;}
#t12_1{left:705px;bottom:783px;}
#t13_1{left:749px;bottom:783px;letter-spacing:0.8px;}
#t14_1{left:29px;bottom:759px;letter-spacing:0.2px;}
#t15_1{left:493px;bottom:759px;letter-spacing:0.4px;}
#t16_1{left:648px;bottom:759px;letter-spacing:0.9px;}
#t17_1{left:257px;bottom:744px;letter-spacing:0.2px;}
#t18_1{left:414px;bottom:744px;letter-spacing:-0.2px;}
#t19_1{left:494px;bottom:719px;letter-spacing:-0.2px;}
#t1a_1{left:749px;bottom:721px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1b_1{left:648px;bottom:699px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1c_1{left:29px;bottom:653px;letter-spacing:0.3px;word-spacing:1px;}
#t1d_1{left:493px;bottom:653px;letter-spacing:0.3px;word-spacing:0.4px;}
#t1e_1{left:29px;bottom:631px;letter-spacing:0.2px;word-spacing:1.3px;}
#t1f_1{left:294px;bottom:631px;}
#t1g_1{left:591px;bottom:631px;letter-spacing:0.3px;}
#t1h_1{left:749px;bottom:631px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1i_1{left:29px;bottom:609px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1j_1{left:591px;bottom:609px;letter-spacing:0.5px;word-spacing:-0.1px;}
#t1k_1{left:493px;bottom:586px;letter-spacing:0.2px;}
#t1l_1{left:29px;bottom:562px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t1m_1{left:591px;bottom:562px;letter-spacing:-0.2px;}
#t1n_1{left:688px;bottom:562px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1o_1{left:493px;bottom:539px;letter-spacing:0.3px;}
#t1p_1{left:680px;bottom:539px;letter-spacing:0.5px;}
#t1q_1{left:802px;bottom:539px;}
#t1r_1{left:29px;bottom:494px;letter-spacing:0.4px;}
#t1s_1{left:306px;bottom:494px;word-spacing:5.9px;}
#t1t_1{left:591px;bottom:494px;letter-spacing:0.3px;word-spacing:1.4px;}
#t1u_1{left:749px;bottom:471px;word-spacing:2.3px;}
#t1v_1{left:29px;bottom:449px;letter-spacing:0.6px;}
#t1w_1{left:74px;bottom:449px;}
#t1x_1{left:193px;bottom:449px;letter-spacing:0.8px;}
#t1y_1{left:29px;bottom:426px;letter-spacing:0.2px;word-spacing:0.8px;}
#t1z_1{left:493px;bottom:426px;letter-spacing:0.3px;word-spacing:0.5px;}
#t20_1{left:29px;bottom:404px;letter-spacing:0.2px;}
#t21_1{left:336px;bottom:404px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t22_1{left:493px;bottom:404px;}
#t23_1{left:273px;bottom:381px;letter-spacing:-0.2px;}
#t24_1{left:336px;bottom:381px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t25_1{left:493px;bottom:381px;letter-spacing:0.2px;}
#t26_1{left:575px;bottom:381px;letter-spacing:0.2px;word-spacing:1.4px;}
#t27_1{left:29px;bottom:358px;letter-spacing:0.2px;word-spacing:1.5px;}
#t28_1{left:114px;bottom:336px;letter-spacing:0.3px;word-spacing:0.3px;}
#t29_1{left:493px;bottom:336px;letter-spacing:0.4px;}
#t2a_1{left:648px;bottom:336px;letter-spacing:0.7px;word-spacing:0.6px;}
#t2b_1{left:29px;bottom:313px;letter-spacing:0.4px;word-spacing:0.4px;}
#t2c_1{left:493px;bottom:313px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t2d_1{left:29px;bottom:290px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2e_1{left:29px;bottom:268px;letter-spacing:0.3px;}
#t2f_1{left:493px;bottom:268px;letter-spacing:0.3px;word-spacing:0.8px;}
#t2g_1{left:193px;bottom:246px;letter-spacing:-0.2px;}
#t2h_1{left:314px;bottom:246px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t2i_1{left:493px;bottom:246px;letter-spacing:0.5px;word-spacing:0.3px;}
#t2j_1{left:493px;bottom:223px;}
#t2k_1{left:749px;bottom:223px;letter-spacing:0.4px;word-spacing:0.7px;}
#t2l_1{left:493px;bottom:178px;letter-spacing:0.3px;}
#t2m_1{left:591px;bottom:155px;letter-spacing:-0.2px;}
#t2n_1{left:688px;bottom:156px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t2o_1{left:411px;bottom:1225px;letter-spacing:-0.4px;}
#t2p_1{left:736px;bottom:1197px;letter-spacing:-0.2px;}
#t2q_1{left:193px;bottom:1143px;letter-spacing:-0.1px;word-spacing:0.1px;}
#t2r_1{left:197px;bottom:944px;letter-spacing:-0.2px;}
#t2s_1{left:197px;bottom:853px;letter-spacing:-0.2px;}
#t2t_1{left:501px;bottom:884px;letter-spacing:0.1px;}
#t2u_1{left:499px;bottom:869px;letter-spacing:0.1px;}
#t2v_1{left:512px;bottom:853px;letter-spacing:0.3px;}
#t2w_1{left:600px;bottom:840px;letter-spacing:0.2px;word-spacing:0.9px;}
#t2x_1{left:641px;bottom:825px;letter-spacing:0.2px;word-spacing:1.5px;}
#t2y_1{left:688px;bottom:809px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2z_1{left:649px;bottom:583px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t30_1{left:789px;bottom:583px;letter-spacing:0.3px;}
#t31_1{left:414px;bottom:1120px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t32_1{left:415px;bottom:1094px;}
#t33_1{left:194px;bottom:1071px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t34_1{left:333px;bottom:1071px;letter-spacing:0.3px;}
#t35_1{left:317px;bottom:1060px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t36_1{left:29px;bottom:1036px;letter-spacing:0.5px;word-spacing:0.8px;}
#t37_1{left:257px;bottom:1034px;letter-spacing:-0.5px;word-spacing:0.5px;}
#t38_1{left:194px;bottom:1118px;letter-spacing:-0.1px;word-spacing:0.1px;}

.s1{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s2{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s3{
	FONT-SIZE: 44px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s4{
	FONT-SIZE: 68.4px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s5{
	FONT-SIZE: 58.4px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

</style>
<!-- End inline CSS -->

<!-- Begin embedded font definitions -->
<style id="fonts1" type="text/css" >

@font-face {
	font-family: Calibri-Bold_d;
	src: url("/admin/css/put/fonts/Calibri-Bold_d.woff") format("woff");
}

@font-face {
	font-family: Calibri_h;
	src: url("/admin/css/put/fonts/Calibri_h.woff") format("woff");
}

</style>
<!-- End embedded font definitions -->

<!-- Begin page background -->
<div id="pg1Overlay" style="width:100%; height:100%; position:absolute; z-index:1; background-color:rgba(0,0,0,0); -webkit-user-select: none;"></div>
<div id="pg1" style="-webkit-user-select: none;"><object width="909" height="1286" data="/admin/css/put/1/1.svg" type="image/svg+xml" id="pdf1" style="width:909px; height:1286px; -moz-transform:scale(1); z-index: 0;"></object></div>
<!-- End page background -->


<!-- Begin text definitions (Positioned/styled in CSS) -->
<div id="t1_1" class="t s1">ПУТЕВОЙ ЛИСТ ЛЕГКОВОГО АВТОМОБИЛЯ</div>
<div id="t2_1" class="t s2">Д-1</div>
<div id="t3_1" class="t s2">№</div>
<div id="t4_1" class="t s2">85</div>
<div id="t5_1" class="t s2">серия</div>
<div id="t6_1" class="t s2">номер</div>
<div id="t7_1" class="t s2">дата</div>
<div id="t8_1" class="t s2">по ОКПО</div>
<div id="t9_1" class="t s2">Организация</div>
<div id="ta_1" class="t s2">Марка автомобиля</div>
<div id="tb_1" class="t s2">Разрешение </div>
<div id="tc_1" class="t s2">Государственный регистрационный знак</div>
<div id="td_1" class="t s3">Номер парковки</div>
<div id="te_1" class="t s2">Водитель</div>
<div id="tf_1" class="t s3">Табельный номер</div>
<div id="tg_1" class="t s4">Предрейсовый</div>
<div id="th_1" class="t s4">Медосмотр</div>
<div id="ti_1" class="t s4">Пройден</div>
<div id="tj_1" class="t s1">дата</div>
<div id="tk_1" class="t s2">г.</div>
<div id="tl_1" class="t s2">Время</div>
<div id="tm_1" class="t s2">'.$time1.'</div>
<div id="tn_1" class="t s1">должность: Врач</div>
<div id="to_1" class="t s2">подпись</div>
<div id="tp_1" class="t s2">'.$medic.'</div>
<div id="tq_1" class="t s2">Выезд с парковки</div>
<div id="tr_1" class="t s2">Дата</div>
<div id="ts_1" class="t s2">г. </div>
<div id="tt_1" class="t s2">Механик/ Диспечер ____'.$mech.'</div>
<div id="tu_1" class="t s2">Время</div>
<div id="tv_1" class="t s2">'.$time2.'</div>
<div id="tw_1" class="t s2">мин</div>
<div id="tx_1" class="t s2">Послерейсовый </div>
<div id="ty_1" class="t s2">медосмотр</div>
<div id="tz_1" class="t s2">дата, время</div>
<div id="t10_1" class="t s5">'.$today.'</div>
<div id="t11_1" class="t s2">г.</div>
<div id="t12_1" class="t s2">'.$time3.'</div>
<div id="t13_1" class="t s2">мин</div>
<div id="t14_1" class="t s2">пройден</div>
<div id="t15_1" class="t s2">дата</div>
<div id="t16_1" class="t s2">время</div>
<div id="t17_1" class="t s2">должность</div>
<div id="t18_1" class="t s2">подпись</div>
<div id="t19_1" class="t s5">Механик</div>
<div id="t1a_1" class="t s2">'.$mech.'</div>
<div id="t1b_1" class="t s3">подпись и расшифровка подписи</div>
<div id="t1c_1" class="t s2">Задание водителю</div>
<div id="t1d_1" class="t s1">Показание одометра, км</div>
<div id="t1e_1" class="t s2">В распоряжение</div>
<div id="t1f_1" class="t s2">Яндекс</div>
<div id="t1g_1" class="t s2">Механик</div>
<div id="t1h_1" class="t s2">'.$mech.'</div>
<div id="t1i_1" class="t s2">Адрес первой подачи</div>
<div id="t1j_1" class="t s2">Автомобиль в исправном состоянии принял</div>
<div id="t1k_1" class="t s2">Водитель</div>
<div id="t1l_1" class="t s2">По указанию Яндекс г. Москва и Московская область</div>
<div id="t1m_1" class="t s2">подпись</div>
<div id="t1n_1" class="t s3">расшифровка подписи</div>
<div id="t1o_1" class="t s2">Горючее</div>
<div id="t1p_1" class="t s2">марка</div>
<div id="t1q_1" class="t s2">код</div>
<div id="t1r_1" class="t s2">дата</div>
<div id="t1s_1" class="t s2">'.$year.' г.</div>
<div id="t1t_1" class="t s2">Движение горючего</div>
<div id="t1u_1" class="t s2">количество, л</div>
<div id="t1v_1" class="t s2">Время </div>
<div id="t1w_1" class="t s2">ч.</div>
<div id="t1x_1" class="t s2">мин</div>
<div id="t1y_1" class="t s2">при заезде на парковку</div>
<div id="t1z_1" class="t s2">Выдано: по заправочному</div>
<div id="t20_1" class="t s2">Диспетчер/механик</div>
<div id="t21_1" class="t s2">'.$mech.'</div>
<div id="t22_1" class="t s2">листу №</div>
<div id="t23_1" class="t s2">подпись</div>
<div id="t24_1" class="t s2">расшифровка подписи</div>
<div id="t25_1" class="t s2">остаток: </div>
<div id="t26_1" class="t s2">при выезде</div>
<div id="t27_1" class="t s2">Ожидание :</div>
<div id="t28_1" class="t s2">Опоздания, ожидания, простои в пути, заезды в гараж </div>
<div id="t29_1" class="t s2">расход</div>
<div id="t2a_1" class="t s2">по норме</div>
<div id="t2b_1" class="t s2">и прочие отметки</div>
<div id="t2c_1" class="t s2">Послерейсовый контроль технического состояния</div>
<div id="t2d_1" class="t s2">Автомобиль сдал</div>
<div id="t2e_1" class="t s2">водитель</div>
<div id="t2f_1" class="t s2">Показания одометра при</div>
<div id="t2g_1" class="t s2">подпись</div>
<div id="t2h_1" class="t s2">расшифровка подписи</div>
<div id="t2i_1" class="t s2">возвращении на парковку,</div>
<div id="t2j_1" class="t s2">км</div>
<div id="t2k_1" class="t s2">дата ; время</div>
<div id="t2l_1" class="t s2">Механик</div>
<div id="t2m_1" class="t s2">подпись</div>
<div id="t2n_1" class="t s3">расшифровка подписи</div>
<div id="t2o_1" class="t s5">'.$today.'</div>
<div id="t2p_1" class="t s2">'.$okpo.'</div>
<div id="t2q_1" class="t s2">'.$name_lic.'<br> ОГРН / ОГРНИП '.$ogrn.' ИНН '.$inn.'</div>
<div id="t2r_1" class="t s2">'.$today.'</div>
<div id="t2s_1" class="t s2">'.$today.'</div>
<div id="t2t_1" class="t s2">Предрейсовый </div>
<div id="t2u_1" class="t s2">(предсменный)</div>
<div id="t2v_1" class="t s2">(контроль)</div>
<div id="t2w_1" class="t s1">Предрейсовый контроль технического состояния</div>
<div id="t2x_1" class="t s1">транспортного средства пройден,</div>
<div id="t2y_1" class="t s1">выезд разрешен</div>
<div id="t2z_1" class="t s5">'.$fio_driver.'</div>
<div id="t30_1" class="t s5"></div>
<div id="t31_1" class="t s2">'.$razresh.'</div>
<div id="t32_1" class="t s5">'.$gosnomer.'</div>
<div id="t33_1" class="t s5">'.$fio_driver.' </div>
<div id="t34_1" class="t s5"></div>
<div id="t35_1" class="t s3">фамилия, имя, отчество</div>
<div id="t36_1" class="t s2">Удостоверение №</div>
<div id="t37_1" class="t s5">'.$udostov.'</div>
<div id="t38_1" class="t s5">'.$mark_model.'</div>

<!-- End text definitions -->


</div>




</body>
</html>

		
		
		
		
		';			
				
			
		
		}
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				

			
			}
			
			
		}
		
		
		
		
		
		}
		
		return $html;
	} 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
    public function actionIndex()
    {
		
		
		
		
		
		
		
		
		
		
		
        $searchModel = new PutevielistiSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		
		
		
		$model_contracts = Contracts::find()->joinWith(['automobiles', 'drivers'])->all();
		
		
		
		
		
		
		
		
		$arrTypes = [ 1 => 'С проставлением дат', 2 => 'Без дат)', ];
		
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrTypes' => $arrTypes,
			'model_contracts' => $model_contracts,
        ]);
    }

	
	
	
	
		 
	 
	 
    public function actionWeek($id = '')
    {	
		
		
		if (!empty($id)) {
			
			$model_contracts = Contracts::find()->where(['id_contract' => $id])->joinWith(['automobiles', 'drivers'])->all();
		} else {
			$model_contracts = Contracts::find()->joinWith(['automobiles', 'drivers'])->all();
		}
	
        $searchModel = new PutevielistiSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
	
		
	
		$arrTypes = [ 1 => 'С проставлением дат', 2 => 'Без дат)', ];
		
		
        return $this->render('week', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrTypes' => $arrTypes,
			'model_contracts' => $model_contracts,
        ]);
    }
	
	
	
		 
	 
	 
    public function actionMonth()
    {
		
		
        $searchModel = new PutevielistiSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		$model_contracts = Contracts::find()->joinWith(['automobiles', 'drivers'])->all();
				
		$arrTypes = [ 1 => 'С проставлением дат', 2 => 'Без дат)', ];
		
		
        return $this->render('month', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrTypes' => $arrTypes,
			'model_contracts' => $model_contracts,
        ]);
    }
	
	
	
	
	
	
	
	
	
	
    /**
     * Displays a single Putevielisti model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Putevielisti model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Putevielisti();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_putevoy]);
        }

		
		$model_autos = AutomobilesNewModel::find()->all();
		
		foreach ($model_autos as $value) {
			
			$arrAutos[$value->id_auto] = $value->mark. ' '.$value->model. ' '.$value->gosnomer;
			
		}
		
		
		$model_drivers = DriversModel::find()->all();
		
		
		foreach ($model_drivers as $value) {
			
			$arrDrivers[$value->id_driver] = $value->first_name. ' '.$value->middle_name. ' ' .$value->last_name; 
			
		}
		
		
        return $this->render('create', [
            'model' => $model,
			'arrAutos' => $arrAutos,
			'arrDrivers' => $arrDrivers,
        ]);
    }

    /**
     * Updates an existing Putevielisti model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_putevoy]);
        }
		
		
			
		$model_autos = AutomobilesNewModel::find()->all();
		
		foreach ($model_autos as $value) {
			
			$arrAutos[$value->id_auto] = $value->mark. ' '.$value->model. ' '.$value->gosnomer;
			
		}
		
		
		$model_drivers = DriversModel::find()->all();
		
		
		foreach ($model_drivers as $value) {
			
			$arrDrivers[$value->id_driver] = $value->first_name. ' '.$value->middle_name. ' ' .$value->last_name; 
			
		}
		

        return $this->render('update', [
            'model' => $model,
			'arrAutos' => $arrAutos,
			'arrDrivers' => $arrDrivers,
        ]);
    }

    /**
     * Deletes an existing Putevielisti model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

	
	
	
	
	
	
	
	
	
	
	
	
	
	public function actionPutevoy ($id = '') {
		
	
		
		//$today = date("d.m.Y");  
			
		$year = date("Y");
		
		
		$model = $this->findModel($id);
		
		//$today = date( $model->date_start, "d.m.Y");
		
		$date = new DateTime($model->date_start);
		$today = $date->format("d.m.Y");
		
		
		//Тип
		
		$put_type = $model->put_type;
		
		
		//Дата окончания
		
		$date = new DateTime($model->date_end);
		
		$date_end = $date->format("d.m.Y");
		
		
		$date_raznost = date_diff(new DateTime($model->date_start), new DateTime($model->date_end))->days;
		
		
		//Машина
		$auto = AutomobilesNewModel::find()->where(['id_auto' => $model->auto_id])->all();
		
		
		$mark_model = $auto[0]->mark.' '.$auto[0]->model;
		
		
		$gosnomer = $auto[0]->gosnomer;
		
		
		$lic_id  = $auto[0]['licen_id'];
		
		//$razresh_id = $auto[0]['razresh_id'];
		
		$lic_arr = Licen::findOne($lic_id);
		
	
		
		$name_lic = $lic_arr['name_lic'];
		$ogrn = $lic_arr['ogrn'];
		$inn = $lic_arr['inn'];
		$okpo = $lic_arr['okpo'];
		
		
		$mech = $lic_arr['mechanic'];
		$medic = $lic_arr['medic'];
		
		
		//Водитель
		$driver = DriversModel::find()->where(['id_driver' => $model->driver_id])->all();	
		
		$fio_driver = $driver[0]->last_name.' '.$driver[0]->first_name.' '.$driver[0]->middle_name;
		//print_r ($driver[0]);
		
		
		
		//$razresh_id = $auto[0]->razresh_id;
		
		
		
		//$razresh_arr = Razresheniya::findOne($razresh_id);
		// r_n
		
		//$razresh = $razresh_arr->r_n. ' Серия '. $razresh_arr->series. ' №' . $razresh_arr->number; 
		$razresh = $auto[0]->razresh_text; 
		
		$udostov = $driver[0]->lic_series.' '.$driver[0]->lic_number.' '.$driver[0]->lic_type;
		
			
		
		
		/*
		if ($put_type == 2) { 
			$date_raznost = 30;
		} 
		*/
		
		for ($i = 0; $i<= $date_raznost; $i++ ) {
		
		
		
		//$date = new DateTime('2006-12-12');
		//$date->modify('+1 day');
		
		if ($put_type == 1) {
		
			$date = new DateTime($model->date_start);
			$date->modify('+'.$i.' day');
			$today = $date->format("d.m.Y");
			
			$year = $date->format("Y");
			
			
			$time1 = '00 ч. 10 мин';
			$time2 = '00 ч. 20 мин';
			$time3 = '00 ч. 15 мин';
			$time4 = '08 ч. 10 мин';
			$time5 = '08 ч. 20 мин';
			$time6 = '08 ч. 15 мин';
			$time7 = '16 ч. 10 мин';
			$time8 = '16 ч. 20 мин';
			$time9 = '16 ч. 15 мин';
			
			
			$html = '
		
		<!DOCTYPE html >
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8" />
</head>

<body style="margin: 0;">

<div id="p1" style="overflow: hidden; position: relative; background-color: white; width: 909px; height: 1286px;">

<!-- Begin shared CSS values -->
<style class="shared-css" type="text/css" >
.t {
	-webkit-transform-origin: bottom left;
	-ms-transform-origin: bottom left;
	transform-origin: bottom left;
	-webkit-transform: scale(0.25);
	-ms-transform: scale(0.25);
	transform: scale(0.25);
	z-index: 2;
	position: absolute;
	white-space: pre;
	overflow: visible;
	line-height: 1.5;
}
</style>
<!-- End shared CSS values -->


<!-- Begin inline CSS -->
<style type="text/css" >

#t1_1{left:29px;bottom:1263px;letter-spacing:0.3px;word-spacing:1.7px;}
#t2_1{left:531px;bottom:1263px;}
#t3_1{left:591px;bottom:1263px;}
#t4_1{left:691px;bottom:1263px;}
#t5_1{left:493px;bottom:1251px;}
#t6_1{left:648px;bottom:1251px;letter-spacing:0.8px;}
#t7_1{left:361px;bottom:1211px;letter-spacing:0.4px;}
#t8_1{left:591px;bottom:1188px;letter-spacing:-0.3px;word-spacing:1.5px;}
#t9_1{left:29px;bottom:1165px;letter-spacing:0.3px;}
#ta_1{left:29px;bottom:1120px;letter-spacing:0.7px;word-spacing:0.8px;}
#tb_1{left:339px;bottom:1120px;letter-spacing:0.3px;}
#tc_1{left:29px;bottom:1097px;letter-spacing:0.4px;word-spacing:0.4px;}
#td_1{left:648px;bottom:1097px;letter-spacing:-0.2px;word-spacing:0.2px;}
#te_1{left:29px;bottom:1073px;letter-spacing:0.2px;}
#tf_1{left:648px;bottom:1074px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tg_1{left:29px;bottom:1010px;letter-spacing:0.1px;}
#th_1{left:29px;bottom:987px;letter-spacing:-0.2px;}
#ti_1{left:29px;bottom:965px;letter-spacing:0.3px;}
#tj_1{left:29px;bottom:944px;letter-spacing:0.5px;}
#tk_1{left:257px;bottom:944px;}
#tl_1{left:336px;bottom:944px;letter-spacing:0.7px;}
#tm_1{left:424px;bottom:944px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tn_1{left:29px;bottom:921px;letter-spacing:0.5px;word-spacing:-0.5px;}
#to_1{left:193px;bottom:921px;letter-spacing:-0.2px;}
#tp_1{left:336px;bottom:921px;letter-spacing:0.7px;word-spacing:-0.7px;}
#tq_1{left:29px;bottom:876px;letter-spacing:0.1px;word-spacing:-0.1px;}
#tr_1{left:29px;bottom:853px;letter-spacing:0.4px;}
#ts_1{left:257px;bottom:853px;}
#tt_1{left:270px;bottom:853px;letter-spacing:0.2px;word-spacing:1.5px;}
#tu_1{left:29px;bottom:829px;letter-spacing:0.7px;}
#tv_1{left:114px;bottom:829px;}
#tw_1{left:257px;bottom:829px;letter-spacing:0.8px;}
#tx_1{left:29px;bottom:806px;letter-spacing:0.1px;}
#ty_1{left:29px;bottom:783px;letter-spacing:0.4px;}
#tz_1{left:414px;bottom:783px;letter-spacing:0.5px;word-spacing:1.7px;}
#t10_1{left:508px;bottom:781px;letter-spacing:-0.4px;}
#t11_1{left:591px;bottom:783px;}
#t12_1{left:705px;bottom:783px;}
#t13_1{left:749px;bottom:783px;letter-spacing:0.8px;}
#t14_1{left:29px;bottom:759px;letter-spacing:0.2px;}
#t15_1{left:493px;bottom:759px;letter-spacing:0.4px;}
#t16_1{left:648px;bottom:759px;letter-spacing:0.9px;}
#t17_1{left:257px;bottom:744px;letter-spacing:0.2px;}
#t18_1{left:414px;bottom:744px;letter-spacing:-0.2px;}
#t19_1{left:494px;bottom:719px;letter-spacing:-0.2px;}
#t1a_1{left:749px;bottom:721px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1b_1{left:648px;bottom:699px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1c_1{left:29px;bottom:653px;letter-spacing:0.3px;word-spacing:1px;}
#t1d_1{left:493px;bottom:653px;letter-spacing:0.3px;word-spacing:0.4px;}
#t1e_1{left:29px;bottom:631px;letter-spacing:0.2px;word-spacing:1.3px;}
#t1f_1{left:294px;bottom:631px;}
#t1g_1{left:591px;bottom:631px;letter-spacing:0.3px;}
#t1h_1{left:749px;bottom:631px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1i_1{left:29px;bottom:609px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1j_1{left:591px;bottom:609px;letter-spacing:0.5px;word-spacing:-0.1px;}
#t1k_1{left:493px;bottom:586px;letter-spacing:0.2px;}
#t1l_1{left:29px;bottom:562px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t1m_1{left:591px;bottom:562px;letter-spacing:-0.2px;}
#t1n_1{left:688px;bottom:562px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1o_1{left:493px;bottom:539px;letter-spacing:0.3px;}
#t1p_1{left:680px;bottom:539px;letter-spacing:0.5px;}
#t1q_1{left:802px;bottom:539px;}
#t1r_1{left:29px;bottom:494px;letter-spacing:0.4px;}
#t1s_1{left:306px;bottom:494px;word-spacing:5.9px;}
#t1t_1{left:591px;bottom:494px;letter-spacing:0.3px;word-spacing:1.4px;}
#t1u_1{left:749px;bottom:471px;word-spacing:2.3px;}
#t1v_1{left:29px;bottom:449px;letter-spacing:0.6px;}
#t1w_1{left:74px;bottom:449px;}
#t1x_1{left:193px;bottom:449px;letter-spacing:0.8px;}
#t1y_1{left:29px;bottom:426px;letter-spacing:0.2px;word-spacing:0.8px;}
#t1z_1{left:493px;bottom:426px;letter-spacing:0.3px;word-spacing:0.5px;}
#t20_1{left:29px;bottom:404px;letter-spacing:0.2px;}
#t21_1{left:336px;bottom:404px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t22_1{left:493px;bottom:404px;}
#t23_1{left:273px;bottom:381px;letter-spacing:-0.2px;}
#t24_1{left:336px;bottom:381px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t25_1{left:493px;bottom:381px;letter-spacing:0.2px;}
#t26_1{left:575px;bottom:381px;letter-spacing:0.2px;word-spacing:1.4px;}
#t27_1{left:29px;bottom:358px;letter-spacing:0.2px;word-spacing:1.5px;}
#t28_1{left:114px;bottom:336px;letter-spacing:0.3px;word-spacing:0.3px;}
#t29_1{left:493px;bottom:336px;letter-spacing:0.4px;}
#t2a_1{left:648px;bottom:336px;letter-spacing:0.7px;word-spacing:0.6px;}
#t2b_1{left:29px;bottom:313px;letter-spacing:0.4px;word-spacing:0.4px;}
#t2c_1{left:493px;bottom:313px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t2d_1{left:29px;bottom:290px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2e_1{left:29px;bottom:268px;letter-spacing:0.3px;}
#t2f_1{left:493px;bottom:268px;letter-spacing:0.3px;word-spacing:0.8px;}
#t2g_1{left:193px;bottom:246px;letter-spacing:-0.2px;}
#t2h_1{left:314px;bottom:246px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t2i_1{left:493px;bottom:246px;letter-spacing:0.5px;word-spacing:0.3px;}
#t2j_1{left:493px;bottom:223px;}
#t2k_1{left:749px;bottom:223px;letter-spacing:0.4px;word-spacing:0.7px;}
#t2l_1{left:493px;bottom:178px;letter-spacing:0.3px;}
#t2m_1{left:591px;bottom:155px;letter-spacing:-0.2px;}
#t2n_1{left:688px;bottom:156px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t2o_1{left:411px;bottom:1225px;letter-spacing:-0.4px;}
#t2p_1{left:736px;bottom:1197px;letter-spacing:-0.2px;}
#t2q_1{left:193px;bottom:1143px;letter-spacing:-0.1px;word-spacing:0.1px;}
#t2r_1{left:197px;bottom:944px;letter-spacing:-0.2px;}
#t2s_1{left:197px;bottom:853px;letter-spacing:-0.2px;}
#t2t_1{left:501px;bottom:884px;letter-spacing:0.1px;}
#t2u_1{left:499px;bottom:869px;letter-spacing:0.1px;}
#t2v_1{left:512px;bottom:853px;letter-spacing:0.3px;}
#t2w_1{left:600px;bottom:840px;letter-spacing:0.2px;word-spacing:0.9px;}
#t2x_1{left:641px;bottom:825px;letter-spacing:0.2px;word-spacing:1.5px;}
#t2y_1{left:688px;bottom:809px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2z_1{left:649px;bottom:583px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t30_1{left:789px;bottom:583px;letter-spacing:0.3px;}
#t31_1{left:414px;bottom:1120px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t32_1{left:415px;bottom:1094px;}
#t33_1{left:194px;bottom:1071px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t34_1{left:333px;bottom:1071px;letter-spacing:0.3px;}
#t35_1{left:317px;bottom:1060px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t36_1{left:29px;bottom:1036px;letter-spacing:0.5px;word-spacing:0.8px;}
#t37_1{left:257px;bottom:1034px;letter-spacing:-0.5px;word-spacing:0.5px;}
#t38_1{left:194px;bottom:1118px;letter-spacing:-0.1px;word-spacing:0.1px;}

.s1{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s2{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s3{
	FONT-SIZE: 44px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s4{
	FONT-SIZE: 68.4px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s5{
	FONT-SIZE: 58.4px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

</style>
<!-- End inline CSS -->

<!-- Begin embedded font definitions -->
<style id="fonts1" type="text/css" >

@font-face {
	font-family: Calibri-Bold_d;
	src: url("/admin/css/put/fonts/Calibri-Bold_d.woff") format("woff");
}

@font-face {
	font-family: Calibri_h;
	src: url("/admin/css/put/fonts/Calibri_h.woff") format("woff");
}

</style>
<!-- End embedded font definitions -->

<!-- Begin page background -->
<div id="pg1Overlay" style="width:100%; height:100%; position:absolute; z-index:1; background-color:rgba(0,0,0,0); -webkit-user-select: none;"></div>
<div id="pg1" style="-webkit-user-select: none;"><object width="909" height="1286" data="/admin/css/put/1/1.svg" type="image/svg+xml" id="pdf1" style="width:909px; height:1286px; -moz-transform:scale(1); z-index: 0;"></object></div>
<!-- End page background -->


<!-- Begin text definitions (Positioned/styled in CSS) -->
<div id="t1_1" class="t s1">ПУТЕВОЙ ЛИСТ ЛЕГКОВОГО АВТОМОБИЛЯ</div>
<div id="t2_1" class="t s2">Д-1</div>
<div id="t3_1" class="t s2">№</div>
<div id="t4_1" class="t s2">85</div>
<div id="t5_1" class="t s2">серия</div>
<div id="t6_1" class="t s2">номер</div>
<div id="t7_1" class="t s2">дата</div>
<div id="t8_1" class="t s2">по ОКПО</div>
<div id="t9_1" class="t s2">Организация</div>
<div id="ta_1" class="t s2">Марка автомобиля</div>
<div id="tb_1" class="t s2">Разрешение </div>
<div id="tc_1" class="t s2">Государственный регистрационный знак</div>
<div id="td_1" class="t s3">Номер парковки</div>
<div id="te_1" class="t s2">Водитель</div>
<div id="tf_1" class="t s3">Табельный номер</div>
<div id="tg_1" class="t s4">Предрейсовый</div>
<div id="th_1" class="t s4">Медосмотр</div>
<div id="ti_1" class="t s4">Пройден</div>
<div id="tj_1" class="t s1">дата</div>
<div id="tk_1" class="t s2">г.</div>
<div id="tl_1" class="t s2">Время</div>
<div id="tm_1" class="t s2">'.$time1.'</div>
<div id="tn_1" class="t s1">должность: Врач</div>
<div id="to_1" class="t s2">подпись</div>
<div id="tp_1" class="t s2">'.$medic.'</div>
<div id="tq_1" class="t s2">Выезд с парковки</div>
<div id="tr_1" class="t s2">Дата</div>
<div id="ts_1" class="t s2">г. </div>
<div id="tt_1" class="t s2">Механик/ Диспечер ____'.$mech.'</div>
<div id="tu_1" class="t s2">Время</div>
<div id="tv_1" class="t s2">'.$time2.'</div>
<div id="tw_1" class="t s2">мин</div>
<div id="tx_1" class="t s2">Послерейсовый </div>
<div id="ty_1" class="t s2">медосмотр</div>
<div id="tz_1" class="t s2">дата, время</div>
<div id="t10_1" class="t s5">'.$today.'</div>
<div id="t11_1" class="t s2">г.</div>
<div id="t12_1" class="t s2">'.$time3.'</div>
<div id="t13_1" class="t s2">мин</div>
<div id="t14_1" class="t s2">пройден</div>
<div id="t15_1" class="t s2">дата</div>
<div id="t16_1" class="t s2">время</div>
<div id="t17_1" class="t s2">должность</div>
<div id="t18_1" class="t s2">подпись</div>
<div id="t19_1" class="t s5">Механик</div>
<div id="t1a_1" class="t s2">'.$mech.'</div>
<div id="t1b_1" class="t s3">подпись и расшифровка подписи</div>
<div id="t1c_1" class="t s2">Задание водителю</div>
<div id="t1d_1" class="t s1">Показание одометра, км</div>
<div id="t1e_1" class="t s2">В распоряжение</div>
<div id="t1f_1" class="t s2">Яндекс</div>
<div id="t1g_1" class="t s2">Механик</div>
<div id="t1h_1" class="t s2">'.$mech.'</div>
<div id="t1i_1" class="t s2">Адрес первой подачи</div>
<div id="t1j_1" class="t s2">Автомобиль в исправном состоянии принял</div>
<div id="t1k_1" class="t s2">Водитель</div>
<div id="t1l_1" class="t s2">По указанию Яндекс г. Москва и Московская область</div>
<div id="t1m_1" class="t s2">подпись</div>
<div id="t1n_1" class="t s3">расшифровка подписи</div>
<div id="t1o_1" class="t s2">Горючее</div>
<div id="t1p_1" class="t s2">марка</div>
<div id="t1q_1" class="t s2">код</div>
<div id="t1r_1" class="t s2">дата</div>
<div id="t1s_1" class="t s2">'.$year.' г.</div>
<div id="t1t_1" class="t s2">Движение горючего</div>
<div id="t1u_1" class="t s2">количество, л</div>
<div id="t1v_1" class="t s2">Время </div>
<div id="t1w_1" class="t s2">ч.</div>
<div id="t1x_1" class="t s2">мин</div>
<div id="t1y_1" class="t s2">при заезде на парковку</div>
<div id="t1z_1" class="t s2">Выдано: по заправочному</div>
<div id="t20_1" class="t s2">Диспетчер/механик</div>
<div id="t21_1" class="t s2">'.$mech.'</div>
<div id="t22_1" class="t s2">листу №</div>
<div id="t23_1" class="t s2">подпись</div>
<div id="t24_1" class="t s2">расшифровка подписи</div>
<div id="t25_1" class="t s2">остаток: </div>
<div id="t26_1" class="t s2">при выезде</div>
<div id="t27_1" class="t s2">Ожидание :</div>
<div id="t28_1" class="t s2">Опоздания, ожидания, простои в пути, заезды в гараж </div>
<div id="t29_1" class="t s2">расход</div>
<div id="t2a_1" class="t s2">по норме</div>
<div id="t2b_1" class="t s2">и прочие отметки</div>
<div id="t2c_1" class="t s2">Послерейсовый контроль технического состояния</div>
<div id="t2d_1" class="t s2">Автомобиль сдал</div>
<div id="t2e_1" class="t s2">водитель</div>
<div id="t2f_1" class="t s2">Показания одометра при</div>
<div id="t2g_1" class="t s2">подпись</div>
<div id="t2h_1" class="t s2">расшифровка подписи</div>
<div id="t2i_1" class="t s2">возвращении на парковку,</div>
<div id="t2j_1" class="t s2">км</div>
<div id="t2k_1" class="t s2">дата ; время</div>
<div id="t2l_1" class="t s2">Механик</div>
<div id="t2m_1" class="t s2">подпись</div>
<div id="t2n_1" class="t s3">расшифровка подписи</div>
<div id="t2o_1" class="t s5">'.$today.'</div>
<div id="t2p_1" class="t s2">'.$okpo.'</div>
<div id="t2q_1" class="t s2">'.$name_lic.'<br> ОГРН / ОГРНИП '.$ogrn.' ИНН '.$inn.'</div>
<div id="t2r_1" class="t s2">'.$today.'</div>
<div id="t2s_1" class="t s2">'.$today.'</div>
<div id="t2t_1" class="t s2">Предрейсовый </div>
<div id="t2u_1" class="t s2">(предсменный)</div>
<div id="t2v_1" class="t s2">(контроль)</div>
<div id="t2w_1" class="t s1">Предрейсовый контроль технического состояния</div>
<div id="t2x_1" class="t s1">транспортного средства пройден,</div>
<div id="t2y_1" class="t s1">выезд разрешен</div>
<div id="t2z_1" class="t s5">'.$fio_driver.'</div>
<div id="t30_1" class="t s5"></div>
<div id="t31_1" class="t s2">'.$razresh.'</div>
<div id="t32_1" class="t s5">'.$gosnomer.'</div>
<div id="t33_1" class="t s5">'.$fio_driver.' </div>
<div id="t34_1" class="t s5"></div>
<div id="t35_1" class="t s3">фамилия, имя, отчество</div>
<div id="t36_1" class="t s2">Удостоверение №</div>
<div id="t37_1" class="t s5">'.$udostov.'</div>
<div id="t38_1" class="t s5">'.$mark_model.'</div>

<!-- End text definitions -->


</div>







<div id="p1" style="overflow: hidden; position: relative; background-color: white; width: 909px; height: 1286px;">

<!-- Begin shared CSS values -->
<style class="shared-css" type="text/css" >
.t {
	-webkit-transform-origin: bottom left;
	-ms-transform-origin: bottom left;
	transform-origin: bottom left;
	-webkit-transform: scale(0.25);
	-ms-transform: scale(0.25);
	transform: scale(0.25);
	z-index: 2;
	position: absolute;
	white-space: pre;
	overflow: visible;
	line-height: 1.5;
}
</style>
<!-- End shared CSS values -->


<!-- Begin inline CSS -->
<style type="text/css" >

#t1_1{left:29px;bottom:1263px;letter-spacing:0.3px;word-spacing:1.7px;}
#t2_1{left:531px;bottom:1263px;}
#t3_1{left:591px;bottom:1263px;}
#t4_1{left:691px;bottom:1263px;}
#t5_1{left:493px;bottom:1251px;}
#t6_1{left:648px;bottom:1251px;letter-spacing:0.8px;}
#t7_1{left:361px;bottom:1211px;letter-spacing:0.4px;}
#t8_1{left:591px;bottom:1188px;letter-spacing:-0.3px;word-spacing:1.5px;}
#t9_1{left:29px;bottom:1165px;letter-spacing:0.3px;}
#ta_1{left:29px;bottom:1120px;letter-spacing:0.7px;word-spacing:0.8px;}
#tb_1{left:339px;bottom:1120px;letter-spacing:0.3px;}
#tc_1{left:29px;bottom:1097px;letter-spacing:0.4px;word-spacing:0.4px;}
#td_1{left:648px;bottom:1097px;letter-spacing:-0.2px;word-spacing:0.2px;}
#te_1{left:29px;bottom:1073px;letter-spacing:0.2px;}
#tf_1{left:648px;bottom:1074px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tg_1{left:29px;bottom:1010px;letter-spacing:0.1px;}
#th_1{left:29px;bottom:987px;letter-spacing:-0.2px;}
#ti_1{left:29px;bottom:965px;letter-spacing:0.3px;}
#tj_1{left:29px;bottom:944px;letter-spacing:0.5px;}
#tk_1{left:257px;bottom:944px;}
#tl_1{left:336px;bottom:944px;letter-spacing:0.7px;}
#tm_1{left:424px;bottom:944px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tn_1{left:29px;bottom:921px;letter-spacing:0.5px;word-spacing:-0.5px;}
#to_1{left:193px;bottom:921px;letter-spacing:-0.2px;}
#tp_1{left:336px;bottom:921px;letter-spacing:0.7px;word-spacing:-0.7px;}
#tq_1{left:29px;bottom:876px;letter-spacing:0.1px;word-spacing:-0.1px;}
#tr_1{left:29px;bottom:853px;letter-spacing:0.4px;}
#ts_1{left:257px;bottom:853px;}
#tt_1{left:270px;bottom:853px;letter-spacing:0.2px;word-spacing:1.5px;}
#tu_1{left:29px;bottom:829px;letter-spacing:0.7px;}
#tv_1{left:114px;bottom:829px;}
#tw_1{left:257px;bottom:829px;letter-spacing:0.8px;}
#tx_1{left:29px;bottom:806px;letter-spacing:0.1px;}
#ty_1{left:29px;bottom:783px;letter-spacing:0.4px;}
#tz_1{left:414px;bottom:783px;letter-spacing:0.5px;word-spacing:1.7px;}
#t10_1{left:508px;bottom:781px;letter-spacing:-0.4px;}
#t11_1{left:591px;bottom:783px;}
#t12_1{left:705px;bottom:783px;}
#t13_1{left:749px;bottom:783px;letter-spacing:0.8px;}
#t14_1{left:29px;bottom:759px;letter-spacing:0.2px;}
#t15_1{left:493px;bottom:759px;letter-spacing:0.4px;}
#t16_1{left:648px;bottom:759px;letter-spacing:0.9px;}
#t17_1{left:257px;bottom:744px;letter-spacing:0.2px;}
#t18_1{left:414px;bottom:744px;letter-spacing:-0.2px;}
#t19_1{left:494px;bottom:719px;letter-spacing:-0.2px;}
#t1a_1{left:749px;bottom:721px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1b_1{left:648px;bottom:699px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1c_1{left:29px;bottom:653px;letter-spacing:0.3px;word-spacing:1px;}
#t1d_1{left:493px;bottom:653px;letter-spacing:0.3px;word-spacing:0.4px;}
#t1e_1{left:29px;bottom:631px;letter-spacing:0.2px;word-spacing:1.3px;}
#t1f_1{left:294px;bottom:631px;}
#t1g_1{left:591px;bottom:631px;letter-spacing:0.3px;}
#t1h_1{left:749px;bottom:631px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1i_1{left:29px;bottom:609px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1j_1{left:591px;bottom:609px;letter-spacing:0.5px;word-spacing:-0.1px;}
#t1k_1{left:493px;bottom:586px;letter-spacing:0.2px;}
#t1l_1{left:29px;bottom:562px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t1m_1{left:591px;bottom:562px;letter-spacing:-0.2px;}
#t1n_1{left:688px;bottom:562px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1o_1{left:493px;bottom:539px;letter-spacing:0.3px;}
#t1p_1{left:680px;bottom:539px;letter-spacing:0.5px;}
#t1q_1{left:802px;bottom:539px;}
#t1r_1{left:29px;bottom:494px;letter-spacing:0.4px;}
#t1s_1{left:306px;bottom:494px;word-spacing:5.9px;}
#t1t_1{left:591px;bottom:494px;letter-spacing:0.3px;word-spacing:1.4px;}
#t1u_1{left:749px;bottom:471px;word-spacing:2.3px;}
#t1v_1{left:29px;bottom:449px;letter-spacing:0.6px;}
#t1w_1{left:74px;bottom:449px;}
#t1x_1{left:193px;bottom:449px;letter-spacing:0.8px;}
#t1y_1{left:29px;bottom:426px;letter-spacing:0.2px;word-spacing:0.8px;}
#t1z_1{left:493px;bottom:426px;letter-spacing:0.3px;word-spacing:0.5px;}
#t20_1{left:29px;bottom:404px;letter-spacing:0.2px;}
#t21_1{left:336px;bottom:404px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t22_1{left:493px;bottom:404px;}
#t23_1{left:273px;bottom:381px;letter-spacing:-0.2px;}
#t24_1{left:336px;bottom:381px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t25_1{left:493px;bottom:381px;letter-spacing:0.2px;}
#t26_1{left:575px;bottom:381px;letter-spacing:0.2px;word-spacing:1.4px;}
#t27_1{left:29px;bottom:358px;letter-spacing:0.2px;word-spacing:1.5px;}
#t28_1{left:114px;bottom:336px;letter-spacing:0.3px;word-spacing:0.3px;}
#t29_1{left:493px;bottom:336px;letter-spacing:0.4px;}
#t2a_1{left:648px;bottom:336px;letter-spacing:0.7px;word-spacing:0.6px;}
#t2b_1{left:29px;bottom:313px;letter-spacing:0.4px;word-spacing:0.4px;}
#t2c_1{left:493px;bottom:313px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t2d_1{left:29px;bottom:290px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2e_1{left:29px;bottom:268px;letter-spacing:0.3px;}
#t2f_1{left:493px;bottom:268px;letter-spacing:0.3px;word-spacing:0.8px;}
#t2g_1{left:193px;bottom:246px;letter-spacing:-0.2px;}
#t2h_1{left:314px;bottom:246px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t2i_1{left:493px;bottom:246px;letter-spacing:0.5px;word-spacing:0.3px;}
#t2j_1{left:493px;bottom:223px;}
#t2k_1{left:749px;bottom:223px;letter-spacing:0.4px;word-spacing:0.7px;}
#t2l_1{left:493px;bottom:178px;letter-spacing:0.3px;}
#t2m_1{left:591px;bottom:155px;letter-spacing:-0.2px;}
#t2n_1{left:688px;bottom:156px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t2o_1{left:411px;bottom:1225px;letter-spacing:-0.4px;}
#t2p_1{left:736px;bottom:1197px;letter-spacing:-0.2px;}
#t2q_1{left:193px;bottom:1143px;letter-spacing:-0.1px;word-spacing:0.1px;}
#t2r_1{left:197px;bottom:944px;letter-spacing:-0.2px;}
#t2s_1{left:197px;bottom:853px;letter-spacing:-0.2px;}
#t2t_1{left:501px;bottom:884px;letter-spacing:0.1px;}
#t2u_1{left:499px;bottom:869px;letter-spacing:0.1px;}
#t2v_1{left:512px;bottom:853px;letter-spacing:0.3px;}
#t2w_1{left:600px;bottom:840px;letter-spacing:0.2px;word-spacing:0.9px;}
#t2x_1{left:641px;bottom:825px;letter-spacing:0.2px;word-spacing:1.5px;}
#t2y_1{left:688px;bottom:809px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2z_1{left:649px;bottom:583px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t30_1{left:789px;bottom:583px;letter-spacing:0.3px;}
#t31_1{left:414px;bottom:1120px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t32_1{left:415px;bottom:1094px;}
#t33_1{left:194px;bottom:1071px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t34_1{left:333px;bottom:1071px;letter-spacing:0.3px;}
#t35_1{left:317px;bottom:1060px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t36_1{left:29px;bottom:1036px;letter-spacing:0.5px;word-spacing:0.8px;}
#t37_1{left:257px;bottom:1034px;letter-spacing:-0.5px;word-spacing:0.5px;}
#t38_1{left:194px;bottom:1118px;letter-spacing:-0.1px;word-spacing:0.1px;}

.s1{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s2{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s3{
	FONT-SIZE: 44px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s4{
	FONT-SIZE: 68.4px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s5{
	FONT-SIZE: 58.4px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

</style>
<!-- End inline CSS -->

<!-- Begin embedded font definitions -->
<style id="fonts1" type="text/css" >

@font-face {
	font-family: Calibri-Bold_d;
	src: url("/admin/css/put/fonts/Calibri-Bold_d.woff") format("woff");
}

@font-face {
	font-family: Calibri_h;
	src: url("/admin/css/put/fonts/Calibri_h.woff") format("woff");
}

</style>
<!-- End embedded font definitions -->

<!-- Begin page background -->
<div id="pg1Overlay" style="width:100%; height:100%; position:absolute; z-index:1; background-color:rgba(0,0,0,0); -webkit-user-select: none;"></div>
<div id="pg1" style="-webkit-user-select: none;"><object width="909" height="1286" data="/admin/css/put/1/1.svg" type="image/svg+xml" id="pdf1" style="width:909px; height:1286px; -moz-transform:scale(1); z-index: 0;"></object></div>
<!-- End page background -->


<!-- Begin text definitions (Positioned/styled in CSS) -->
<div id="t1_1" class="t s1">ПУТЕВОЙ ЛИСТ ЛЕГКОВОГО АВТОМОБИЛЯ</div>
<div id="t2_1" class="t s2">Д-1</div>
<div id="t3_1" class="t s2">№</div>
<div id="t4_1" class="t s2">85</div>
<div id="t5_1" class="t s2">серия</div>
<div id="t6_1" class="t s2">номер</div>
<div id="t7_1" class="t s2">дата</div>
<div id="t8_1" class="t s2">по ОКПО</div>
<div id="t9_1" class="t s2">Организация</div>
<div id="ta_1" class="t s2">Марка автомобиля</div>
<div id="tb_1" class="t s2">Разрешение </div>
<div id="tc_1" class="t s2">Государственный регистрационный знак</div>
<div id="td_1" class="t s3">Номер парковки</div>
<div id="te_1" class="t s2">Водитель</div>
<div id="tf_1" class="t s3">Табельный номер</div>
<div id="tg_1" class="t s4">Предрейсовый</div>
<div id="th_1" class="t s4">Медосмотр</div>
<div id="ti_1" class="t s4">Пройден</div>
<div id="tj_1" class="t s1">дата</div>
<div id="tk_1" class="t s2">г.</div>
<div id="tl_1" class="t s2">Время</div>
<div id="tm_1" class="t s2">'.$time4.'</div>
<div id="tn_1" class="t s1">должность: Врач</div>
<div id="to_1" class="t s2">подпись</div>
<div id="tp_1" class="t s2">'.$medic.'</div>
<div id="tq_1" class="t s2">Выезд с парковки</div>
<div id="tr_1" class="t s2">Дата</div>
<div id="ts_1" class="t s2">г. </div>
<div id="tt_1" class="t s2">Механик/ Диспечер ____'.$mech.'</div>
<div id="tu_1" class="t s2">Время</div>
<div id="tv_1" class="t s2">'.$time5.'</div>
<div id="tw_1" class="t s2">мин</div>
<div id="tx_1" class="t s2">Послерейсовый </div>
<div id="ty_1" class="t s2">медосмотр</div>
<div id="tz_1" class="t s2">дата, время</div>
<div id="t10_1" class="t s5">'.$today.'</div>
<div id="t11_1" class="t s2">г.</div>
<div id="t12_1" class="t s2">'.$time6.'</div>
<div id="t13_1" class="t s2">мин</div>
<div id="t14_1" class="t s2">пройден</div>
<div id="t15_1" class="t s2">дата</div>
<div id="t16_1" class="t s2">время</div>
<div id="t17_1" class="t s2">должность</div>
<div id="t18_1" class="t s2">подпись</div>
<div id="t19_1" class="t s5">Механик</div>
<div id="t1a_1" class="t s2">'.$mech.'</div>
<div id="t1b_1" class="t s3">подпись и расшифровка подписи</div>
<div id="t1c_1" class="t s2">Задание водителю</div>
<div id="t1d_1" class="t s1">Показание одометра, км</div>
<div id="t1e_1" class="t s2">В распоряжение</div>
<div id="t1f_1" class="t s2">Яндекс</div>
<div id="t1g_1" class="t s2">Механик</div>
<div id="t1h_1" class="t s2">'.$mech.'</div>
<div id="t1i_1" class="t s2">Адрес первой подачи</div>
<div id="t1j_1" class="t s2">Автомобиль в исправном состоянии принял</div>
<div id="t1k_1" class="t s2">Водитель</div>
<div id="t1l_1" class="t s2">По указанию Яндекс г. Москва и Московская область</div>
<div id="t1m_1" class="t s2">подпись</div>
<div id="t1n_1" class="t s3">расшифровка подписи</div>
<div id="t1o_1" class="t s2">Горючее</div>
<div id="t1p_1" class="t s2">марка</div>
<div id="t1q_1" class="t s2">код</div>
<div id="t1r_1" class="t s2">дата</div>
<div id="t1s_1" class="t s2">'.$year.' г.</div>
<div id="t1t_1" class="t s2">Движение горючего</div>
<div id="t1u_1" class="t s2">количество, л</div>
<div id="t1v_1" class="t s2">Время </div>
<div id="t1w_1" class="t s2">ч.</div>
<div id="t1x_1" class="t s2">мин</div>
<div id="t1y_1" class="t s2">при заезде на парковку</div>
<div id="t1z_1" class="t s2">Выдано: по заправочному</div>
<div id="t20_1" class="t s2">Диспетчер/механик</div>
<div id="t21_1" class="t s2">'.$mech.'</div>
<div id="t22_1" class="t s2">листу №</div>
<div id="t23_1" class="t s2">подпись</div>
<div id="t24_1" class="t s2">расшифровка подписи</div>
<div id="t25_1" class="t s2">остаток: </div>
<div id="t26_1" class="t s2">при выезде</div>
<div id="t27_1" class="t s2">Ожидание :</div>
<div id="t28_1" class="t s2">Опоздания, ожидания, простои в пути, заезды в гараж </div>
<div id="t29_1" class="t s2">расход</div>
<div id="t2a_1" class="t s2">по норме</div>
<div id="t2b_1" class="t s2">и прочие отметки</div>
<div id="t2c_1" class="t s2">Послерейсовый контроль технического состояния</div>
<div id="t2d_1" class="t s2">Автомобиль сдал</div>
<div id="t2e_1" class="t s2">водитель</div>
<div id="t2f_1" class="t s2">Показания одометра при</div>
<div id="t2g_1" class="t s2">подпись</div>
<div id="t2h_1" class="t s2">расшифровка подписи</div>
<div id="t2i_1" class="t s2">возвращении на парковку,</div>
<div id="t2j_1" class="t s2">км</div>
<div id="t2k_1" class="t s2">дата ; время</div>
<div id="t2l_1" class="t s2">Механик</div>
<div id="t2m_1" class="t s2">подпись</div>
<div id="t2n_1" class="t s3">расшифровка подписи</div>
<div id="t2o_1" class="t s5">'.$today.'</div>
<div id="t2p_1" class="t s2">'.$okpo.'</div>
<div id="t2q_1" class="t s2">'.$name_lic.'<br> ОГРН / ОГРНИП '.$ogrn.' ИНН '.$inn.'</div>
<div id="t2r_1" class="t s2">'.$today.'</div>
<div id="t2s_1" class="t s2">'.$today.'</div>
<div id="t2t_1" class="t s2">Предрейсовый </div>
<div id="t2u_1" class="t s2">(предсменный)</div>
<div id="t2v_1" class="t s2">(контроль)</div>
<div id="t2w_1" class="t s1">Предрейсовый контроль технического состояния</div>
<div id="t2x_1" class="t s1">транспортного средства пройден,</div>
<div id="t2y_1" class="t s1">выезд разрешен</div>
<div id="t2z_1" class="t s5">'.$fio_driver.' </div>
<div id="t30_1" class="t s5"></div>
<div id="t31_1" class="t s2">'.$razresh.'</div>
<div id="t32_1" class="t s5">'.$gosnomer.'</div>
<div id="t33_1" class="t s5">'.$fio_driver.' </div>
<div id="t34_1" class="t s5"></div>
<div id="t35_1" class="t s3">фамилия, имя, отчество</div>
<div id="t36_1" class="t s2">Удостоверение №</div>
<div id="t37_1" class="t s5">'.$udostov.'</div>
<div id="t38_1" class="t s5">'.$mark_model.'</div>

<!-- End text definitions -->


</div>





<div id="p1" style="overflow: hidden; position: relative; background-color: white; width: 909px; height: 1286px;">

<!-- Begin shared CSS values -->
<style class="shared-css" type="text/css" >
.t {
	-webkit-transform-origin: bottom left;
	-ms-transform-origin: bottom left;
	transform-origin: bottom left;
	-webkit-transform: scale(0.25);
	-ms-transform: scale(0.25);
	transform: scale(0.25);
	z-index: 2;
	position: absolute;
	white-space: pre;
	overflow: visible;
	line-height: 1.5;
}
</style>
<!-- End shared CSS values -->


<!-- Begin inline CSS -->
<style type="text/css" >

#t1_1{left:29px;bottom:1263px;letter-spacing:0.3px;word-spacing:1.7px;}
#t2_1{left:531px;bottom:1263px;}
#t3_1{left:591px;bottom:1263px;}
#t4_1{left:691px;bottom:1263px;}
#t5_1{left:493px;bottom:1251px;}
#t6_1{left:648px;bottom:1251px;letter-spacing:0.8px;}
#t7_1{left:361px;bottom:1211px;letter-spacing:0.4px;}
#t8_1{left:591px;bottom:1188px;letter-spacing:-0.3px;word-spacing:1.5px;}
#t9_1{left:29px;bottom:1165px;letter-spacing:0.3px;}
#ta_1{left:29px;bottom:1120px;letter-spacing:0.7px;word-spacing:0.8px;}
#tb_1{left:339px;bottom:1120px;letter-spacing:0.3px;}
#tc_1{left:29px;bottom:1097px;letter-spacing:0.4px;word-spacing:0.4px;}
#td_1{left:648px;bottom:1097px;letter-spacing:-0.2px;word-spacing:0.2px;}
#te_1{left:29px;bottom:1073px;letter-spacing:0.2px;}
#tf_1{left:648px;bottom:1074px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tg_1{left:29px;bottom:1010px;letter-spacing:0.1px;}
#th_1{left:29px;bottom:987px;letter-spacing:-0.2px;}
#ti_1{left:29px;bottom:965px;letter-spacing:0.3px;}
#tj_1{left:29px;bottom:944px;letter-spacing:0.5px;}
#tk_1{left:257px;bottom:944px;}
#tl_1{left:336px;bottom:944px;letter-spacing:0.7px;}
#tm_1{left:424px;bottom:944px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tn_1{left:29px;bottom:921px;letter-spacing:0.5px;word-spacing:-0.5px;}
#to_1{left:193px;bottom:921px;letter-spacing:-0.2px;}
#tp_1{left:336px;bottom:921px;letter-spacing:0.7px;word-spacing:-0.7px;}
#tq_1{left:29px;bottom:876px;letter-spacing:0.1px;word-spacing:-0.1px;}
#tr_1{left:29px;bottom:853px;letter-spacing:0.4px;}
#ts_1{left:257px;bottom:853px;}
#tt_1{left:270px;bottom:853px;letter-spacing:0.2px;word-spacing:1.5px;}
#tu_1{left:29px;bottom:829px;letter-spacing:0.7px;}
#tv_1{left:114px;bottom:829px;}
#tw_1{left:257px;bottom:829px;letter-spacing:0.8px;}
#tx_1{left:29px;bottom:806px;letter-spacing:0.1px;}
#ty_1{left:29px;bottom:783px;letter-spacing:0.4px;}
#tz_1{left:414px;bottom:783px;letter-spacing:0.5px;word-spacing:1.7px;}
#t10_1{left:508px;bottom:781px;letter-spacing:-0.4px;}
#t11_1{left:591px;bottom:783px;}
#t12_1{left:705px;bottom:783px;}
#t13_1{left:749px;bottom:783px;letter-spacing:0.8px;}
#t14_1{left:29px;bottom:759px;letter-spacing:0.2px;}
#t15_1{left:493px;bottom:759px;letter-spacing:0.4px;}
#t16_1{left:648px;bottom:759px;letter-spacing:0.9px;}
#t17_1{left:257px;bottom:744px;letter-spacing:0.2px;}
#t18_1{left:414px;bottom:744px;letter-spacing:-0.2px;}
#t19_1{left:494px;bottom:719px;letter-spacing:-0.2px;}
#t1a_1{left:749px;bottom:721px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1b_1{left:648px;bottom:699px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1c_1{left:29px;bottom:653px;letter-spacing:0.3px;word-spacing:1px;}
#t1d_1{left:493px;bottom:653px;letter-spacing:0.3px;word-spacing:0.4px;}
#t1e_1{left:29px;bottom:631px;letter-spacing:0.2px;word-spacing:1.3px;}
#t1f_1{left:294px;bottom:631px;}
#t1g_1{left:591px;bottom:631px;letter-spacing:0.3px;}
#t1h_1{left:749px;bottom:631px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1i_1{left:29px;bottom:609px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1j_1{left:591px;bottom:609px;letter-spacing:0.5px;word-spacing:-0.1px;}
#t1k_1{left:493px;bottom:586px;letter-spacing:0.2px;}
#t1l_1{left:29px;bottom:562px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t1m_1{left:591px;bottom:562px;letter-spacing:-0.2px;}
#t1n_1{left:688px;bottom:562px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1o_1{left:493px;bottom:539px;letter-spacing:0.3px;}
#t1p_1{left:680px;bottom:539px;letter-spacing:0.5px;}
#t1q_1{left:802px;bottom:539px;}
#t1r_1{left:29px;bottom:494px;letter-spacing:0.4px;}
#t1s_1{left:306px;bottom:494px;word-spacing:5.9px;}
#t1t_1{left:591px;bottom:494px;letter-spacing:0.3px;word-spacing:1.4px;}
#t1u_1{left:749px;bottom:471px;word-spacing:2.3px;}
#t1v_1{left:29px;bottom:449px;letter-spacing:0.6px;}
#t1w_1{left:74px;bottom:449px;}
#t1x_1{left:193px;bottom:449px;letter-spacing:0.8px;}
#t1y_1{left:29px;bottom:426px;letter-spacing:0.2px;word-spacing:0.8px;}
#t1z_1{left:493px;bottom:426px;letter-spacing:0.3px;word-spacing:0.5px;}
#t20_1{left:29px;bottom:404px;letter-spacing:0.2px;}
#t21_1{left:336px;bottom:404px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t22_1{left:493px;bottom:404px;}
#t23_1{left:273px;bottom:381px;letter-spacing:-0.2px;}
#t24_1{left:336px;bottom:381px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t25_1{left:493px;bottom:381px;letter-spacing:0.2px;}
#t26_1{left:575px;bottom:381px;letter-spacing:0.2px;word-spacing:1.4px;}
#t27_1{left:29px;bottom:358px;letter-spacing:0.2px;word-spacing:1.5px;}
#t28_1{left:114px;bottom:336px;letter-spacing:0.3px;word-spacing:0.3px;}
#t29_1{left:493px;bottom:336px;letter-spacing:0.4px;}
#t2a_1{left:648px;bottom:336px;letter-spacing:0.7px;word-spacing:0.6px;}
#t2b_1{left:29px;bottom:313px;letter-spacing:0.4px;word-spacing:0.4px;}
#t2c_1{left:493px;bottom:313px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t2d_1{left:29px;bottom:290px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2e_1{left:29px;bottom:268px;letter-spacing:0.3px;}
#t2f_1{left:493px;bottom:268px;letter-spacing:0.3px;word-spacing:0.8px;}
#t2g_1{left:193px;bottom:246px;letter-spacing:-0.2px;}
#t2h_1{left:314px;bottom:246px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t2i_1{left:493px;bottom:246px;letter-spacing:0.5px;word-spacing:0.3px;}
#t2j_1{left:493px;bottom:223px;}
#t2k_1{left:749px;bottom:223px;letter-spacing:0.4px;word-spacing:0.7px;}
#t2l_1{left:493px;bottom:178px;letter-spacing:0.3px;}
#t2m_1{left:591px;bottom:155px;letter-spacing:-0.2px;}
#t2n_1{left:688px;bottom:156px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t2o_1{left:411px;bottom:1225px;letter-spacing:-0.4px;}
#t2p_1{left:736px;bottom:1197px;letter-spacing:-0.2px;}
#t2q_1{left:193px;bottom:1143px;letter-spacing:-0.1px;word-spacing:0.1px;}
#t2r_1{left:197px;bottom:944px;letter-spacing:-0.2px;}
#t2s_1{left:197px;bottom:853px;letter-spacing:-0.2px;}
#t2t_1{left:501px;bottom:884px;letter-spacing:0.1px;}
#t2u_1{left:499px;bottom:869px;letter-spacing:0.1px;}
#t2v_1{left:512px;bottom:853px;letter-spacing:0.3px;}
#t2w_1{left:600px;bottom:840px;letter-spacing:0.2px;word-spacing:0.9px;}
#t2x_1{left:641px;bottom:825px;letter-spacing:0.2px;word-spacing:1.5px;}
#t2y_1{left:688px;bottom:809px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2z_1{left:649px;bottom:583px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t30_1{left:789px;bottom:583px;letter-spacing:0.3px;}
#t31_1{left:414px;bottom:1120px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t32_1{left:415px;bottom:1094px;}
#t33_1{left:194px;bottom:1071px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t34_1{left:333px;bottom:1071px;letter-spacing:0.3px;}
#t35_1{left:317px;bottom:1060px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t36_1{left:29px;bottom:1036px;letter-spacing:0.5px;word-spacing:0.8px;}
#t37_1{left:257px;bottom:1034px;letter-spacing:-0.5px;word-spacing:0.5px;}
#t38_1{left:194px;bottom:1118px;letter-spacing:-0.1px;word-spacing:0.1px;}

.s1{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s2{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s3{
	FONT-SIZE: 44px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s4{
	FONT-SIZE: 68.4px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s5{
	FONT-SIZE: 58.4px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

</style>
<!-- End inline CSS -->

<!-- Begin embedded font definitions -->
<style id="fonts1" type="text/css" >

@font-face {
	font-family: Calibri-Bold_d;
	src: url("/admin/css/put/fonts/Calibri-Bold_d.woff") format("woff");
}

@font-face {
	font-family: Calibri_h;
	src: url("/admin/css/put/fonts/Calibri_h.woff") format("woff");
}

</style>
<!-- End embedded font definitions -->

<!-- Begin page background -->
<div id="pg1Overlay" style="width:100%; height:100%; position:absolute; z-index:1; background-color:rgba(0,0,0,0); -webkit-user-select: none;"></div>
<div id="pg1" style="-webkit-user-select: none;"><object width="909" height="1286" data="/admin/css/put/1/1.svg" type="image/svg+xml" id="pdf1" style="width:909px; height:1286px; -moz-transform:scale(1); z-index: 0;"></object></div>
<!-- End page background -->


<!-- Begin text definitions (Positioned/styled in CSS) -->
<div id="t1_1" class="t s1">ПУТЕВОЙ ЛИСТ ЛЕГКОВОГО АВТОМОБИЛЯ</div>
<div id="t2_1" class="t s2">Д-1</div>
<div id="t3_1" class="t s2">№</div>
<div id="t4_1" class="t s2">85</div>
<div id="t5_1" class="t s2">серия</div>
<div id="t6_1" class="t s2">номер</div>
<div id="t7_1" class="t s2">дата</div>
<div id="t8_1" class="t s2">по ОКПО</div>
<div id="t9_1" class="t s2">Организация</div>
<div id="ta_1" class="t s2">Марка автомобиля</div>
<div id="tb_1" class="t s2">Разрешение </div>
<div id="tc_1" class="t s2">Государственный регистрационный знак</div>
<div id="td_1" class="t s3">Номер парковки</div>
<div id="te_1" class="t s2">Водитель</div>
<div id="tf_1" class="t s3">Табельный номер</div>
<div id="tg_1" class="t s4">Предрейсовый</div>
<div id="th_1" class="t s4">Медосмотр</div>
<div id="ti_1" class="t s4">Пройден</div>
<div id="tj_1" class="t s1">дата</div>
<div id="tk_1" class="t s2">г.</div>
<div id="tl_1" class="t s2">Время</div>
<div id="tm_1" class="t s2">'.$time7.'</div>
<div id="tn_1" class="t s1">должность: Врач</div>
<div id="to_1" class="t s2">подпись</div>
<div id="tp_1" class="t s2">'.$medic.'</div>
<div id="tq_1" class="t s2">Выезд с парковки</div>
<div id="tr_1" class="t s2">Дата</div>
<div id="ts_1" class="t s2">г. </div>
<div id="tt_1" class="t s2">Механик/ Диспечер ____'.$mech.'</div>
<div id="tu_1" class="t s2">Время</div>
<div id="tv_1" class="t s2">'.$time8.'</div>
<div id="tw_1" class="t s2">мин</div>
<div id="tx_1" class="t s2">Послерейсовый </div>
<div id="ty_1" class="t s2">медосмотр</div>
<div id="tz_1" class="t s2">дата, время</div>
<div id="t10_1" class="t s5">'.$today.'</div>
<div id="t11_1" class="t s2">г.</div>
<div id="t12_1" class="t s2">'.$time9.'</div>
<div id="t13_1" class="t s2">мин</div>
<div id="t14_1" class="t s2">пройден</div>
<div id="t15_1" class="t s2">дата</div>
<div id="t16_1" class="t s2">время</div>
<div id="t17_1" class="t s2">должность</div>
<div id="t18_1" class="t s2">подпись</div>
<div id="t19_1" class="t s5">Механик</div>
<div id="t1a_1" class="t s2">'.$mech.'</div>
<div id="t1b_1" class="t s3">подпись и расшифровка подписи</div>
<div id="t1c_1" class="t s2">Задание водителю</div>
<div id="t1d_1" class="t s1">Показание одометра, км</div>
<div id="t1e_1" class="t s2">В распоряжение</div>
<div id="t1f_1" class="t s2">Яндекс</div>
<div id="t1g_1" class="t s2">Механик</div>
<div id="t1h_1" class="t s2">'.$mech.'</div>
<div id="t1i_1" class="t s2">Адрес первой подачи</div>
<div id="t1j_1" class="t s2">Автомобиль в исправном состоянии принял</div>
<div id="t1k_1" class="t s2">Водитель</div>
<div id="t1l_1" class="t s2">По указанию Яндекс г. Москва и Московская область</div>
<div id="t1m_1" class="t s2">подпись</div>
<div id="t1n_1" class="t s3">расшифровка подписи</div>
<div id="t1o_1" class="t s2">Горючее</div>
<div id="t1p_1" class="t s2">марка</div>
<div id="t1q_1" class="t s2">код</div>
<div id="t1r_1" class="t s2">дата</div>
<div id="t1s_1" class="t s2">'.$year.' г.</div>
<div id="t1t_1" class="t s2">Движение горючего</div>
<div id="t1u_1" class="t s2">количество, л</div>
<div id="t1v_1" class="t s2">Время </div>
<div id="t1w_1" class="t s2">ч.</div>
<div id="t1x_1" class="t s2">мин</div>
<div id="t1y_1" class="t s2">при заезде на парковку</div>
<div id="t1z_1" class="t s2">Выдано: по заправочному</div>
<div id="t20_1" class="t s2">Диспетчер/механик</div>
<div id="t21_1" class="t s2">'.$mech.'</div>
<div id="t22_1" class="t s2">листу №</div>
<div id="t23_1" class="t s2">подпись</div>
<div id="t24_1" class="t s2">расшифровка подписи</div>
<div id="t25_1" class="t s2">остаток: </div>
<div id="t26_1" class="t s2">при выезде</div>
<div id="t27_1" class="t s2">Ожидание :</div>
<div id="t28_1" class="t s2">Опоздания, ожидания, простои в пути, заезды в гараж </div>
<div id="t29_1" class="t s2">расход</div>
<div id="t2a_1" class="t s2">по норме</div>
<div id="t2b_1" class="t s2">и прочие отметки</div>
<div id="t2c_1" class="t s2">Послерейсовый контроль технического состояния</div>
<div id="t2d_1" class="t s2">Автомобиль сдал</div>
<div id="t2e_1" class="t s2">водитель</div>
<div id="t2f_1" class="t s2">Показания одометра при</div>
<div id="t2g_1" class="t s2">подпись</div>
<div id="t2h_1" class="t s2">расшифровка подписи</div>
<div id="t2i_1" class="t s2">возвращении на парковку,</div>
<div id="t2j_1" class="t s2">км</div>
<div id="t2k_1" class="t s2">дата ; время</div>
<div id="t2l_1" class="t s2">Механик</div>
<div id="t2m_1" class="t s2">подпись</div>
<div id="t2n_1" class="t s3">расшифровка подписи</div>
<div id="t2o_1" class="t s5">'.$today.'</div>
<div id="t2p_1" class="t s2">'.$okpo.'</div>
<div id="t2q_1" class="t s2">'.$name_lic.'<br> ОГРН / ОГРНИП '.$ogrn.' ИНН '.$inn.'</div>
<div id="t2r_1" class="t s2">'.$today.'</div>
<div id="t2s_1" class="t s2">'.$today.'</div>
<div id="t2t_1" class="t s2">Предрейсовый </div>
<div id="t2u_1" class="t s2">(предсменный)</div>
<div id="t2v_1" class="t s2">(контроль)</div>
<div id="t2w_1" class="t s1">Предрейсовый контроль технического состояния</div>
<div id="t2x_1" class="t s1">транспортного средства пройден,</div>
<div id="t2y_1" class="t s1">выезд разрешен</div>
<div id="t2z_1" class="t s5">'.$fio_driver.' </div>
<div id="t30_1" class="t s5"></div>
<div id="t31_1" class="t s2">'.$razresh.'</div>
<div id="t32_1" class="t s5">'.$gosnomer.'</div>
<div id="t33_1" class="t s5">'.$fio_driver.'</div>
<div id="t34_1" class="t s5"></div>
<div id="t35_1" class="t s3">фамилия, имя, отчество</div>
<div id="t36_1" class="t s2">Удостоверение №</div>
<div id="t37_1" class="t s5">'.$udostov.'</div>
<div id="t38_1" class="t s5">'.$mark_model.'</div>

<!-- End text definitions -->


</div>








</body>
</html>

		
		
		
		
		
		';			
				
			
			
			
		} else {
			$today = '';
			$year = '';
		
		
		
			$time1 = '';
			$time2 = '';
			$time3 = '';
			$time4 = '';
			$time5 = '';
			$time6 = '';
			$time7 = '';
			$time8 = '';
			$time9 = '';
		
		
			$html = '
		
		<!DOCTYPE html >
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8" />
</head>

<body style="margin: 0;">

<div id="p1" style="overflow: hidden; position: relative; background-color: white; width: 909px; height: 1286px;">

<!-- Begin shared CSS values -->
<style class="shared-css" type="text/css" >
.t {
	-webkit-transform-origin: bottom left;
	-ms-transform-origin: bottom left;
	transform-origin: bottom left;
	-webkit-transform: scale(0.25);
	-ms-transform: scale(0.25);
	transform: scale(0.25);
	z-index: 2;
	position: absolute;
	white-space: pre;
	overflow: visible;
	line-height: 1.5;
}
</style>
<!-- End shared CSS values -->


<!-- Begin inline CSS -->
<style type="text/css" >

#t1_1{left:29px;bottom:1263px;letter-spacing:0.3px;word-spacing:1.7px;}
#t2_1{left:531px;bottom:1263px;}
#t3_1{left:591px;bottom:1263px;}
#t4_1{left:691px;bottom:1263px;}
#t5_1{left:493px;bottom:1251px;}
#t6_1{left:648px;bottom:1251px;letter-spacing:0.8px;}
#t7_1{left:361px;bottom:1211px;letter-spacing:0.4px;}
#t8_1{left:591px;bottom:1188px;letter-spacing:-0.3px;word-spacing:1.5px;}
#t9_1{left:29px;bottom:1165px;letter-spacing:0.3px;}
#ta_1{left:29px;bottom:1120px;letter-spacing:0.7px;word-spacing:0.8px;}
#tb_1{left:339px;bottom:1120px;letter-spacing:0.3px;}
#tc_1{left:29px;bottom:1097px;letter-spacing:0.4px;word-spacing:0.4px;}
#td_1{left:648px;bottom:1097px;letter-spacing:-0.2px;word-spacing:0.2px;}
#te_1{left:29px;bottom:1073px;letter-spacing:0.2px;}
#tf_1{left:648px;bottom:1074px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tg_1{left:29px;bottom:1010px;letter-spacing:0.1px;}
#th_1{left:29px;bottom:987px;letter-spacing:-0.2px;}
#ti_1{left:29px;bottom:965px;letter-spacing:0.3px;}
#tj_1{left:29px;bottom:944px;letter-spacing:0.5px;}
#tk_1{left:257px;bottom:944px;}
#tl_1{left:336px;bottom:944px;letter-spacing:0.7px;}
#tm_1{left:424px;bottom:944px;letter-spacing:0.3px;word-spacing:-0.3px;}
#tn_1{left:29px;bottom:921px;letter-spacing:0.5px;word-spacing:-0.5px;}
#to_1{left:193px;bottom:921px;letter-spacing:-0.2px;}
#tp_1{left:336px;bottom:921px;letter-spacing:0.7px;word-spacing:-0.7px;}
#tq_1{left:29px;bottom:876px;letter-spacing:0.1px;word-spacing:-0.1px;}
#tr_1{left:29px;bottom:853px;letter-spacing:0.4px;}
#ts_1{left:257px;bottom:853px;}
#tt_1{left:270px;bottom:853px;letter-spacing:0.2px;word-spacing:1.5px;}
#tu_1{left:29px;bottom:829px;letter-spacing:0.7px;}
#tv_1{left:114px;bottom:829px;}
#tw_1{left:257px;bottom:829px;letter-spacing:0.8px;}
#tx_1{left:29px;bottom:806px;letter-spacing:0.1px;}
#ty_1{left:29px;bottom:783px;letter-spacing:0.4px;}
#tz_1{left:414px;bottom:783px;letter-spacing:0.5px;word-spacing:1.7px;}
#t10_1{left:508px;bottom:781px;letter-spacing:-0.4px;}
#t11_1{left:591px;bottom:783px;}
#t12_1{left:705px;bottom:783px;}
#t13_1{left:749px;bottom:783px;letter-spacing:0.8px;}
#t14_1{left:29px;bottom:759px;letter-spacing:0.2px;}
#t15_1{left:493px;bottom:759px;letter-spacing:0.4px;}
#t16_1{left:648px;bottom:759px;letter-spacing:0.9px;}
#t17_1{left:257px;bottom:744px;letter-spacing:0.2px;}
#t18_1{left:414px;bottom:744px;letter-spacing:-0.2px;}
#t19_1{left:494px;bottom:719px;letter-spacing:-0.2px;}
#t1a_1{left:749px;bottom:721px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1b_1{left:648px;bottom:699px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1c_1{left:29px;bottom:653px;letter-spacing:0.3px;word-spacing:1px;}
#t1d_1{left:493px;bottom:653px;letter-spacing:0.3px;word-spacing:0.4px;}
#t1e_1{left:29px;bottom:631px;letter-spacing:0.2px;word-spacing:1.3px;}
#t1f_1{left:294px;bottom:631px;}
#t1g_1{left:591px;bottom:631px;letter-spacing:0.3px;}
#t1h_1{left:749px;bottom:631px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1i_1{left:29px;bottom:609px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t1j_1{left:591px;bottom:609px;letter-spacing:0.5px;word-spacing:-0.1px;}
#t1k_1{left:493px;bottom:586px;letter-spacing:0.2px;}
#t1l_1{left:29px;bottom:562px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t1m_1{left:591px;bottom:562px;letter-spacing:-0.2px;}
#t1n_1{left:688px;bottom:562px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t1o_1{left:493px;bottom:539px;letter-spacing:0.3px;}
#t1p_1{left:680px;bottom:539px;letter-spacing:0.5px;}
#t1q_1{left:802px;bottom:539px;}
#t1r_1{left:29px;bottom:494px;letter-spacing:0.4px;}
#t1s_1{left:306px;bottom:494px;word-spacing:5.9px;}
#t1t_1{left:591px;bottom:494px;letter-spacing:0.3px;word-spacing:1.4px;}
#t1u_1{left:749px;bottom:471px;word-spacing:2.3px;}
#t1v_1{left:29px;bottom:449px;letter-spacing:0.6px;}
#t1w_1{left:74px;bottom:449px;}
#t1x_1{left:193px;bottom:449px;letter-spacing:0.8px;}
#t1y_1{left:29px;bottom:426px;letter-spacing:0.2px;word-spacing:0.8px;}
#t1z_1{left:493px;bottom:426px;letter-spacing:0.3px;word-spacing:0.5px;}
#t20_1{left:29px;bottom:404px;letter-spacing:0.2px;}
#t21_1{left:336px;bottom:404px;letter-spacing:0.4px;word-spacing:-0.4px;}
#t22_1{left:493px;bottom:404px;}
#t23_1{left:273px;bottom:381px;letter-spacing:-0.2px;}
#t24_1{left:336px;bottom:381px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t25_1{left:493px;bottom:381px;letter-spacing:0.2px;}
#t26_1{left:575px;bottom:381px;letter-spacing:0.2px;word-spacing:1.4px;}
#t27_1{left:29px;bottom:358px;letter-spacing:0.2px;word-spacing:1.5px;}
#t28_1{left:114px;bottom:336px;letter-spacing:0.3px;word-spacing:0.3px;}
#t29_1{left:493px;bottom:336px;letter-spacing:0.4px;}
#t2a_1{left:648px;bottom:336px;letter-spacing:0.7px;word-spacing:0.6px;}
#t2b_1{left:29px;bottom:313px;letter-spacing:0.4px;word-spacing:0.4px;}
#t2c_1{left:493px;bottom:313px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t2d_1{left:29px;bottom:290px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2e_1{left:29px;bottom:268px;letter-spacing:0.3px;}
#t2f_1{left:493px;bottom:268px;letter-spacing:0.3px;word-spacing:0.8px;}
#t2g_1{left:193px;bottom:246px;letter-spacing:-0.2px;}
#t2h_1{left:314px;bottom:246px;letter-spacing:0.3px;word-spacing:-0.3px;}
#t2i_1{left:493px;bottom:246px;letter-spacing:0.5px;word-spacing:0.3px;}
#t2j_1{left:493px;bottom:223px;}
#t2k_1{left:749px;bottom:223px;letter-spacing:0.4px;word-spacing:0.7px;}
#t2l_1{left:493px;bottom:178px;letter-spacing:0.3px;}
#t2m_1{left:591px;bottom:155px;letter-spacing:-0.2px;}
#t2n_1{left:688px;bottom:156px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t2o_1{left:411px;bottom:1225px;letter-spacing:-0.4px;}
#t2p_1{left:736px;bottom:1197px;letter-spacing:-0.2px;}
#t2q_1{left:193px;bottom:1143px;letter-spacing:-0.1px;word-spacing:0.1px;}
#t2r_1{left:197px;bottom:944px;letter-spacing:-0.2px;}
#t2s_1{left:197px;bottom:853px;letter-spacing:-0.2px;}
#t2t_1{left:501px;bottom:884px;letter-spacing:0.1px;}
#t2u_1{left:499px;bottom:869px;letter-spacing:0.1px;}
#t2v_1{left:512px;bottom:853px;letter-spacing:0.3px;}
#t2w_1{left:600px;bottom:840px;letter-spacing:0.2px;word-spacing:0.9px;}
#t2x_1{left:641px;bottom:825px;letter-spacing:0.2px;word-spacing:1.5px;}
#t2y_1{left:688px;bottom:809px;letter-spacing:0.5px;word-spacing:-0.5px;}
#t2z_1{left:649px;bottom:583px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t30_1{left:789px;bottom:583px;letter-spacing:0.3px;}
#t31_1{left:414px;bottom:1120px;letter-spacing:0.2px;word-spacing:-0.2px;}
#t32_1{left:415px;bottom:1094px;}
#t33_1{left:194px;bottom:1071px;letter-spacing:-0.1px;word-spacing:-1.6px;}
#t34_1{left:333px;bottom:1071px;letter-spacing:0.3px;}
#t35_1{left:317px;bottom:1060px;letter-spacing:0.1px;word-spacing:-0.1px;}
#t36_1{left:29px;bottom:1036px;letter-spacing:0.5px;word-spacing:0.8px;}
#t37_1{left:257px;bottom:1034px;letter-spacing:-0.5px;word-spacing:0.5px;}
#t38_1{left:194px;bottom:1118px;letter-spacing:-0.1px;word-spacing:0.1px;}

.s1{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s2{
	FONT-SIZE: 48.9px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s3{
	FONT-SIZE: 44px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

.s4{
	FONT-SIZE: 68.4px;
	FONT-FAMILY: Calibri-Bold_d;
	color: rgb(0,0,0);
}

.s5{
	FONT-SIZE: 58.4px;
	FONT-FAMILY: Calibri_h;
	color: rgb(0,0,0);
}

</style>
<!-- End inline CSS -->

<!-- Begin embedded font definitions -->
<style id="fonts1" type="text/css" >

@font-face {
	font-family: Calibri-Bold_d;
	src: url("/admin/css/put/fonts/Calibri-Bold_d.woff") format("woff");
}

@font-face {
	font-family: Calibri_h;
	src: url("/admin/css/put/fonts/Calibri_h.woff") format("woff");
}

</style>
<!-- End embedded font definitions -->

<!-- Begin page background -->
<div id="pg1Overlay" style="width:100%; height:100%; position:absolute; z-index:1; background-color:rgba(0,0,0,0); -webkit-user-select: none;"></div>
<div id="pg1" style="-webkit-user-select: none;"><object width="909" height="1286" data="/admin/css/put/1/1.svg" type="image/svg+xml" id="pdf1" style="width:909px; height:1286px; -moz-transform:scale(1); z-index: 0;"></object></div>
<!-- End page background -->


<!-- Begin text definitions (Positioned/styled in CSS) -->
<div id="t1_1" class="t s1">ПУТЕВОЙ ЛИСТ ЛЕГКОВОГО АВТОМОБИЛЯ</div>
<div id="t2_1" class="t s2">Д-1</div>
<div id="t3_1" class="t s2">№</div>
<div id="t4_1" class="t s2">85</div>
<div id="t5_1" class="t s2">серия</div>
<div id="t6_1" class="t s2">номер</div>
<div id="t7_1" class="t s2">дата</div>
<div id="t8_1" class="t s2">по ОКПО</div>
<div id="t9_1" class="t s2">Организация</div>
<div id="ta_1" class="t s2">Марка автомобиля</div>
<div id="tb_1" class="t s2">Разрешение </div>
<div id="tc_1" class="t s2">Государственный регистрационный знак</div>
<div id="td_1" class="t s3">Номер парковки</div>
<div id="te_1" class="t s2">Водитель</div>
<div id="tf_1" class="t s3">Табельный номер</div>
<div id="tg_1" class="t s4">Предрейсовый</div>
<div id="th_1" class="t s4">Медосмотр</div>
<div id="ti_1" class="t s4">Пройден</div>
<div id="tj_1" class="t s1">дата</div>
<div id="tk_1" class="t s2">г.</div>
<div id="tl_1" class="t s2">Время</div>
<div id="tm_1" class="t s2">'.$time1.'</div>
<div id="tn_1" class="t s1">должность: Врач</div>
<div id="to_1" class="t s2">подпись</div>
<div id="tp_1" class="t s2">'.$medic.'</div>
<div id="tq_1" class="t s2">Выезд с парковки</div>
<div id="tr_1" class="t s2">Дата</div>
<div id="ts_1" class="t s2">г. </div>
<div id="tt_1" class="t s2">Механик/ Диспечер ____'.$mech.'</div>
<div id="tu_1" class="t s2">Время</div>
<div id="tv_1" class="t s2">'.$time2.'</div>
<div id="tw_1" class="t s2">мин</div>
<div id="tx_1" class="t s2">Послерейсовый </div>
<div id="ty_1" class="t s2">медосмотр</div>
<div id="tz_1" class="t s2">дата, время</div>
<div id="t10_1" class="t s5">'.$today.'</div>
<div id="t11_1" class="t s2">г.</div>
<div id="t12_1" class="t s2">'.$time3.'</div>
<div id="t13_1" class="t s2">мин</div>
<div id="t14_1" class="t s2">пройден</div>
<div id="t15_1" class="t s2">дата</div>
<div id="t16_1" class="t s2">время</div>
<div id="t17_1" class="t s2">должность</div>
<div id="t18_1" class="t s2">подпись</div>
<div id="t19_1" class="t s5">Механик</div>
<div id="t1a_1" class="t s2">'.$mech.'</div>
<div id="t1b_1" class="t s3">подпись и расшифровка подписи</div>
<div id="t1c_1" class="t s2">Задание водителю</div>
<div id="t1d_1" class="t s1">Показание одометра, км</div>
<div id="t1e_1" class="t s2">В распоряжение</div>
<div id="t1f_1" class="t s2">Яндекс</div>
<div id="t1g_1" class="t s2">Механик</div>
<div id="t1h_1" class="t s2">'.$mech.'</div>
<div id="t1i_1" class="t s2">Адрес первой подачи</div>
<div id="t1j_1" class="t s2">Автомобиль в исправном состоянии принял</div>
<div id="t1k_1" class="t s2">Водитель</div>
<div id="t1l_1" class="t s2">По указанию Яндекс г. Москва и Московская область</div>
<div id="t1m_1" class="t s2">подпись</div>
<div id="t1n_1" class="t s3">расшифровка подписи</div>
<div id="t1o_1" class="t s2">Горючее</div>
<div id="t1p_1" class="t s2">марка</div>
<div id="t1q_1" class="t s2">код</div>
<div id="t1r_1" class="t s2">дата</div>
<div id="t1s_1" class="t s2">'.$year.' г.</div>
<div id="t1t_1" class="t s2">Движение горючего</div>
<div id="t1u_1" class="t s2">количество, л</div>
<div id="t1v_1" class="t s2">Время </div>
<div id="t1w_1" class="t s2">ч.</div>
<div id="t1x_1" class="t s2">мин</div>
<div id="t1y_1" class="t s2">при заезде на парковку</div>
<div id="t1z_1" class="t s2">Выдано: по заправочному</div>
<div id="t20_1" class="t s2">Диспетчер/механик</div>
<div id="t21_1" class="t s2">'.$mech.'</div>
<div id="t22_1" class="t s2">листу №</div>
<div id="t23_1" class="t s2">подпись</div>
<div id="t24_1" class="t s2">расшифровка подписи</div>
<div id="t25_1" class="t s2">остаток: </div>
<div id="t26_1" class="t s2">при выезде</div>
<div id="t27_1" class="t s2">Ожидание :</div>
<div id="t28_1" class="t s2">Опоздания, ожидания, простои в пути, заезды в гараж </div>
<div id="t29_1" class="t s2">расход</div>
<div id="t2a_1" class="t s2">по норме</div>
<div id="t2b_1" class="t s2">и прочие отметки</div>
<div id="t2c_1" class="t s2">Послерейсовый контроль технического состояния</div>
<div id="t2d_1" class="t s2">Автомобиль сдал</div>
<div id="t2e_1" class="t s2">водитель</div>
<div id="t2f_1" class="t s2">Показания одометра при</div>
<div id="t2g_1" class="t s2">подпись</div>
<div id="t2h_1" class="t s2">расшифровка подписи</div>
<div id="t2i_1" class="t s2">возвращении на парковку,</div>
<div id="t2j_1" class="t s2">км</div>
<div id="t2k_1" class="t s2">дата ; время</div>
<div id="t2l_1" class="t s2">Механик</div>
<div id="t2m_1" class="t s2">подпись</div>
<div id="t2n_1" class="t s3">расшифровка подписи</div>
<div id="t2o_1" class="t s5">'.$today.'</div>
<div id="t2p_1" class="t s2">'.$okpo.'</div>
<div id="t2q_1" class="t s2">'.$name_lic.'<br> ОГРН / ОГРНИП '.$ogrn.' ИНН '.$inn.'</div>
<div id="t2r_1" class="t s2">'.$today.'</div>
<div id="t2s_1" class="t s2">'.$today.'</div>
<div id="t2t_1" class="t s2">Предрейсовый </div>
<div id="t2u_1" class="t s2">(предсменный)</div>
<div id="t2v_1" class="t s2">(контроль)</div>
<div id="t2w_1" class="t s1">Предрейсовый контроль технического состояния</div>
<div id="t2x_1" class="t s1">транспортного средства пройден,</div>
<div id="t2y_1" class="t s1">выезд разрешен</div>
<div id="t2z_1" class="t s5">'.$fio_driver.'</div>
<div id="t30_1" class="t s5"></div>
<div id="t31_1" class="t s2">'.$razresh.'</div>
<div id="t32_1" class="t s5">'.$gosnomer.'</div>
<div id="t33_1" class="t s5">'.$fio_driver.' </div>
<div id="t34_1" class="t s5"></div>
<div id="t35_1" class="t s3">фамилия, имя, отчество</div>
<div id="t36_1" class="t s2">Удостоверение №</div>
<div id="t37_1" class="t s5">'.$udostov.'</div>
<div id="t38_1" class="t s5">'.$mark_model.'</div>

<!-- End text definitions -->


</div>




</body>
</html>

		
		
		
		
		';			
				
			
		
		}
			


			
		
				
				
			$result.= $html;	
				
		}			
				
		
	
	//	$mpdf = new Mpdf();
    //    $mpdf->WriteHTML($html);
    //    $mpdf->Output();
    //    exit;
	return $result;
		
		
		
	}
	
	
	
	
	
	
	
	
    /**
     * Finds the Putevielisti model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Putevielisti the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Putevielisti::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
