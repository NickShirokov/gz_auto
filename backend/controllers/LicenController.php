<?php

namespace backend\controllers;

use Yii;
use app\models\Licen;
use app\models\Owners;
use backend\models\LicenSearchModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use backend\models\RazresheniyaSearchModel;

use app\models\Razresheniya;
use yii\filters\AccessControl;
/**
 * LicenController implements the CRUD actions for Licen model.
 */
class LicenController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
		
			[
				   'class' => AccessControl::className(),
				   'only' => ['index','create', 'view', 'update', 'delete'],
				   'rules' => [
						   [
								   'actions' => ['index','create', 'view', 'update', 'delete'],
								   'allow' => true,
								   'roles' => ['admin'],
						   ],
						   [
								   'actions' => [
												'index', 
												'view',
												'create',
												],
								   'allow' => true,
								   'roles' => ['manager', 'payer'],
						   ],
						   
						   [
								   'actions' => [
												'index', 
												'view',
												],
								   'allow' => true,
								   'roles' => ['user'],
						   ],
				   ],

		   ],
			
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'create', 'view', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
		
		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Licen models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LicenSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		$model_owners = Owners::find()->all();
		
		
		$arrOwners = array();
		
		foreach ($model_owners as $value) {
			
			
			$arrOwners[$value->id_owner] = $value->first_name. ' ' .$value->last_name;
			
		}
		
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'arrOwners' => $arrOwners,
        ]);
    }

    /**
     * Displays a single Licen model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
		
		
		$req = Yii::$app->request->queryParams;
		
		$req['RazresheniyaSearchModel']['licen_id'] = $id;
	
		
		$searchModel = new RazresheniyaSearchModel();
        $dataProvider = $searchModel->search($req);

		
		$model_licens = Licen::find()->all();
		
		
		$arrLicens = array ();
		
		foreach ($model_licens as $value) {
			
			$arrLicens[$value->id_lic] = $value->name_lic;
		}
		
		
		$modelRazresh = new Razresheniya();
		
		
		
		if ($modelRazresh->load(Yii::$app->request->post())) { 
		
			$modelRazresh->save(false);
			
			return $this->redirect(['licen/view', 'id' => $modelRazresh->licen_id]);
		
		
		}
		
		
        return $this->render('view', [
			'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $this->findModel($id),
			'arrLicens' => $arrLicens,
			'modelRazresh' => $modelRazresh,
        ]);
    }

    /**
     * Creates a new Licen model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Licen();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_lic]);
        }
		
		
		$model_owners = Owners::find()->all();
		
		
		$arrOwners = array();
		
		foreach ($model_owners as $value) {

			$arrOwners[$value->id_owner] = $value->first_name. ' ' .$value->last_name;
			
		}
		
        return $this->render('create', [
            'model' => $model,
			
			'arrOwners' => $arrOwners,
        ]);
    }

    /**
     * Updates an existing Licen model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_lic]);
        }
		
		$model_owners = Owners::find()->all();
		
		
		$arrOwners = array();
		
		foreach ($model_owners as $value) {
			
			
			$arrOwners[$value->id_owner] = $value->first_name. ' ' .$value->last_name;
			
		}
		
		
		
		

        return $this->render('update', [
            'model' => $model,
			'arrOwners' => $arrOwners,
        ]);
    }

    /**
     * Deletes an existing Licen model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Licen model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Licen the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
	 
	 
	 
	public function actionMultipledelete()
	{
		$pk = Yii::$app->request->post('row_id');
		
		

		foreach ($pk as $key => $value) {
			$sql = "DELETE FROM licen WHERE id_lic = $value";
			$query = Yii::$app->db->createCommand($sql)->execute();
		}

		return $this->redirect(['index']);

	} 
	
	 
	 
	 
	 
	 
	 
	 
	 
    protected function findModel($id)
    {
        if (($model = Licen::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
