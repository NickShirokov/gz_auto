<?php

namespace backend;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Driversfiles;

/**
 * modelsDriversfilesSearchModel represents the model behind the search form of `app\models\Driversfiles`.
 */
class modelsDriversfilesSearchModel extends Driversfiles
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_file', 'id_driver', 'file_link', 'comment'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Driversfiles::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_file' => $this->id_file,
            'id_driver' => $this->id_driver,
            'file_link' => $this->file_link,
            'comment' => $this->comment,
        ]);

        return $dataProvider;
    }
}
