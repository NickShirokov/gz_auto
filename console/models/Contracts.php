<?php

namespace console\models;

use Yii;
use common\models\DriversModel;
use common\models\AutomobilesNewModel;

use app\models\Ourpenalties;


/**
 * This is the model class for table "contracts".
 *
 * @property int $id_contract
 * @property int $id_owner
 * @property int $id_driver
 * @property int $id_auto
 * @property string $date_create
 */
class Contracts extends \yii\db\ActiveRecord
{
	
	
	
	
	
	
	//Поле для вычисления новой даты с учетом простоя
	public $date_pay_arenda_new;
	
	
	public $pdf_link;
	public $file_f;
	
	
	public $fio;
	
	
	public $newcreateauto;
	
	
	public $newcreatedriver;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contracts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
		
		if ($_SERVER['REQUEST_URI'] == '/admin/contracts/newcreate') {
			
			$arrRules = [
				
				[['owner_id', 'driver_id', 'auto_id'], 'safe'],
				[['last_name', 'gosnomer', 'VIN', 'p_vznos', 'arenda_type', 'arenda_stoim',
					'arenda_items','vykup','penalty_comission', 'discount_type',
					'date_pay_arenda', 'date_insurance_stop', 'vyezd_sotrudnikov',
					'comment', 'date_start', 'who_affilate', 'bonus_summ', 'status_itog',
					'date_pay_arenda_new', 'pdf_link', 'date_end', 'id_contract', 'newcreateauto', 'newcreatedriver'
				], 'safe'],
				
				
				[[ 'prosrochka',	
					'peredacha_drugomu', 
					'vyezd_bez_sogl_200', 
					'shtraf_pdd',		
					'snyat_atrib',		
					'no_put_list',	
					'no_osago',	
					'poterya_docs',	
					'poterya_keys',	
					'no_prodl_diag', 'text1', 'text2', 'text3', 'text4', 'text5', 'putevoy_type', 'prichina_zaversh', 'comment_zaversh'], 'safe'],
					
				[['file_f'],'file', 'extensions' => 'csv'],
				
			];

			
			
			
			
			
		} else {
		
		
			$arrRules = [
				[['owner_id', 'driver_id', 'auto_id'], 'required'],
				[['owner_id', 'driver_id', 'auto_id'], 'integer'],
				[['last_name', 'gosnomer', 'VIN', 'p_vznos', 'arenda_type', 'arenda_stoim',
					'arenda_items','vykup','penalty_comission', 'discount_type',
					'date_pay_arenda', 'date_insurance_stop', 'vyezd_sotrudnikov',
					'comment', 'date_start', 'who_affilate', 'bonus_summ', 'status_itog',
					'date_pay_arenda_new', 'pdf_link', 'date_end', 'id_contract', 'newcreateauto', 'newcreatedriver'
				], 'safe'],
				
				
				[[ 'prosrochka',	
					'peredacha_drugomu', 
					'vyezd_bez_sogl_200', 
					'shtraf_pdd',		
					'snyat_atrib',		
					'no_put_list',	
					'no_osago',	
					'poterya_docs',	
					'poterya_keys',	
					'no_prodl_diag', 'text1', 'text2', 'text3', 'text4', 'text5', 'putevoy_type', 'prichina_zaversh', 'comment_zaversh'], 'safe'],
					
				[['file_f'],'file', 'extensions' => 'csv'],
				
			];
		
		}
		
		return $arrRules;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_contract' => Yii::t('app', 'Id Договора'),
            'owner_id' => Yii::t('app', 'Владелец'),
            'driver_id' => Yii::t('app', 'Водитель'),
            'auto_id' => Yii::t('app', 'Автомобиль'),
			
			'date_start' => Yii::t('app', 'Дата заключения договора'),
			
			
			'last_name' => Yii::t('app', 'Водитель (Фамилия)'),
			'gosnomer' => Yii::t('app', 'Авто (Гос.номер)'),
			'VIN' => Yii::t('app', 'VIN'),
			
			
			'p_vznos' => Yii::t('app', 'Первоначальный взнос'),
			'arenda_type' => Yii::t('app', 'Тип выплат - раз в день, неделю, месяц...'),
			'arenda_stoim' => Yii::t('app', 'Платеж '),
			
			'fio' => Yii::t('app', 'ФИО '),
			
			'arenda_items' => Yii::t('app', 'Срок аренды'),
			'vykup' => Yii::t('app', 'Выкупной платеж'),
			
			//Здесь начнутся штрафы	
			
			'prosrochka' => Yii::t('app', 'Штраф за просрочку платежа'),
			'peredacha_drugomu' => Yii::t('app', 'Штраф за передачу руля другому лицу'),
			'vyezd_bez_sogl_200' => Yii::t('app', 'Штраф за выезд без согласия более 200 км от Москвы'),
			'shtraf_pdd' => Yii::t('app', 'Штраф за уплату ПДД в срок более 60 дней (коэффициент)'),
			'snyat_atrib' => Yii::t('app', 'Штраф за снятие с автомобился атрибутики такси'),
			'no_put_list' => Yii::t('app', 'Штраф за движение без путевого листа'),
			'no_osago' => Yii::t('app', 'Штраф за движение без ОСАГО'),
			'poterya_docs' => Yii::t('app', 'Штраф за утерю документов на автомобиль(полис ОСАГО, лицензия на такси,СТС)'),
			'poterya_keys' => Yii::t('app', 'Штраф за утерю ключа на автомобиль'),
			'no_prodl_diag' => Yii::t('app', 'Штраф за непродленную диагностическую карту'),
			
			
			'penalty_comission' => Yii::t('app', 'Штраф ПДД Комиссия'),
			'discount_type' => Yii::t('app', 'Скидка на штрафы (галочка)'),
			'date_pay_arenda' => Yii::t('app', 'Дата оплаты аренды за ТС'),
			'vyezd_sotrudnikov' => Yii::t('app', 'Оплата за выезд сотрудников'),
			
			
			'comment' => Yii::t('app', 'Комментарий'),
			'date_end' => Yii::t('app', 'Дата окончания договора'),
			
			
			'who_affilate' => Yii::t('app', 'От кого пришел'),
			'bonus_summ' => Yii::t('app', 'Бонусная сумма'),
			//'comment' => Yii::t('app', 'Итоговый статус'),
		
			
			'status_itog' => Yii::t('app', 'Итоговый статус'),
			'date_pay_arenda_new' => Yii::t('app', 'Дата аренды ТС с учетом простоя'),
			'pdf_link' => Yii::t('app', 'PDF'),
			'putevoy_type' => Yii::t('app', 'Тип путевого - с датами/Без'),
			'comment_zaversh' => Yii::t('app', 'Комментарий по завершении договора'),
			'prichina_zaversh' => Yii::t('app', 'Причина завершения'),
			'newcreateauto' => Yii::t('app', 'Создать новое авто'),
			'newcreatedriver' => Yii::t('app', 'Создать нового водителя'),
			
			
            
        ];
    }
	
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner0()
    {
	
        return $this->hasOne(Owners::className(), ['id_owner' => 'owner_id']);
				
    }
	
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getContractsstatus0()
    {
	
        return $this->hasOne(Contractsstatus::className(), ['c_status_id' => 'status_itog']);
				
    }
	
	
	
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getOwners()
	{
		return $this->hasMany(Owners::className(), ['id_owner' => 'owner_id']);
	}
	
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
/*	public function getDrivers()
	{
		return $this->hasMany(DriversModel::className(), ['id_driver' => 'driver_id']);
	}
	*/
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDriver()
	{
		return $this->hasOne(DriversModel::className(), ['id_driver' => 'driver_id']);
	}
	
	
	
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getautomobiles()
	{
		return $this->hasMany(AutomobilesNewModel::className(), ['id_auto' => 'auto_id']);
	}


	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getdrivers()
	{
		return $this->hasMany(DriversModel::className(), ['id_driver' => 'driver_id']);
	}

	
	//
	public function getPayments()
    {
        // same as above
        return $this->hasMany(Payments::className(), ['id_contract' => 'id_contract']);
    }
	
	
	public function getOur_penalties()
    {
        // same as above
        return $this->hasMany(Ourpenalties::className(), ['p_contract_id' => 'id_contract']);
    }
	
	
	//Добавляем штрафы ПДД
	public function getPdd_penalties()
    {
        // same as above
        return $this->hasMany(Pddpenalties::className(), ['contract_id' => 'id_contract']);
    }
	
	
	
}
