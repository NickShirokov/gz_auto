<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "pdd_pen_status_api".
 *
 * @property int $p_status_id
 * @property string $p_status_name
 */
class PddPenStatusApiModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pdd_pen_status_api';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['p_status_name'], 'required'],
            [['p_status_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'p_status_id' => Yii::t('app', 'P Status ID'),
            'p_status_name' => Yii::t('app', 'P Status Name'),
        ];
    }
}
