<?php

namespace console\models;

use Yii;

use common\models\AutomobilesNewModel;
use common\models\DriversModel;

/**
 * This is the model class for table "pdd_penalties".
 *
 * @property int $id_shtraf
 * @property int $contract_id
 * @property string $n_post
 * @property string $date_post
 * @property string $date_narush
 * @property int $type_opl
 * @property int $summa_opl
 */
class Pddpenalties extends \yii\db\ActiveRecord
{
	
	public $sts;
	
	public $gosnomer;
	
	public $auto_item;
	
	public $last_name;
	
	public $test_sum;
	
	public $file_f;
	
	
	public $file_upload;
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pdd_penalties';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['n_post', 'auto_id', 'param_id', 'driver_id', 'date_post', 'date_narush', 'type_opl', 'summa_opl'], 'required'],
            [['type_opl', 'type_opl_api', 'summa_opl'], 'integer'],
            [['date_post', 'date_narush', 'comment', 'gosnomer','last_name', 'skidka_status', 'test_sum', 'contract_id', 'narushitel'], 'safe'],
            [['n_post'], 'string', 'max' => 100],
			[['image_doc'], 'string', 'max' => 255],
			[['file_f'],'file', 'extensions' => 'jpg, png, jpeg, pdf'],
			[['file_upload'],'file', 'extensions' => 'csv'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_shtraf' => Yii::t('app', 'Id Штрафа'),
            
            'auto_id' => Yii::t('app', 'Машина'),
            'param_id' => Yii::t('app', 'СТС-Госномер'),
            'contract_id' => Yii::t('app', 'Договор'),
            'driver_id' => Yii::t('app', 'Водитель'),
			
            'n_post' => Yii::t('app', 'Номер постановления'),
            'date_post' => Yii::t('app', 'Дата постановления'),
            'date_narush' => Yii::t('app', 'Дата нарушения'),
            
            'summa_opl' => Yii::t('app', 'Сумма к оплате'),
			'comment' => Yii::t('app', 'Комментарий'),
			'gosnomer' => Yii::t('app', 'госномер'),
			'auto_item' => Yii::t('app', 'Машина (VIN)'),
			
			'last_name' => Yii::t('app', 'Водитель (фамилия)'),
			'skidka_status' => Yii::t('app', 'Работает ли скидка'),
			'test_sum' => Yii::t('app', 'Сумма к оплате'),
			'image_doc' => Yii::t('app', 'File'),
			
			'type_opl' => Yii::t('app', 'Нам оплатили'),
			'type_opl_api' => Yii::t('app', 'Мы оплатили'),
			
			'narushitel' => Yii::t('app', 'Нарушитель'),
			
			
        ];
    }
	
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getPddpenaltiesstatus0()
    {
        return $this->hasOne(Pddpenstatus::className(), ['p_status_id' => 'type_opl']);
    }
	
	
	
		/**
     * @return \yii\db\ActiveQuery
     */
    public function getpdd_pen_status()
    {
        return $this->hasMany(Pddpenstatus::className(), ['p_status_id' => 'type_opl']);
    }
	
	
	
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getPddpenaltiesstatusapi0()
    {
        return $this->hasOne(Pddpenstatusapi::className(), ['p_status_id' => 'type_opl_api']);
    }
	
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getpdd_pen_status_api()
    {
        return $this->hasMany(Pddpenstatusapi::className(), ['p_status_id' => 'type_opl_api']);
    }
	
	
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getdrivers()
    {
        return $this->hasMany(DriversModel::className(), ['id_driver' => 'driver_id']);
    }
	
	
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getautomobiles()
    {
        return $this->hasMany(AutomobilesNewModel::className(), ['id_auto' => 'auto_id']);
    }
	
	
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getautoparams()
    {
        return $this->hasMany(Autoparams::className(), ['id_param' => 'param_id']);
    }
	
	
	
	
		//
	public function getContracts()
    {
        // same as above
        return $this->hasMany(Contracts::className(), ['id_contract' => 'contract_id'])
			->joinWith('drivers')->joinWith('automobiles');
    }
	
	
	
}
